<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xd"
    version="1.0">
 
    <xsl:output method="html" media-type="text/html" encoding="UTF-8" indent="yes"/>
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jul 23, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> aberge</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="repInfo">
        <html>
            <head>
                <title>PDF validation report</title>
            </head>
            <body>
                <h2>PDF Validation report</h2>
                <br/>
                <!-- general information -->
                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header">General Information</div>
                    <div class="rich-panel-body">
                        <table border="0">
                            <tr>
                                <td>
                                    <b>JHOVE module</b>
                                </td>
                                <td><xsl:value-of select="reportingModule"/> (<xsl:value-of select="reportingModule/@release"/>)</td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Validation Status</b>
                                </td>
                                <td><xsl:value-of select="status"/></td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Mime-type</b>
                                </td>
                                <td><xsl:value-of select="mimeType"/></td>
                            </tr>
                            <xsl:if test="count(profiles) &gt; 0">
                                <tr>
                                    <td><b>PDF Modules</b></td>
                                    <td>
                                        <ul>
                                            <xsl:for-each select="profiles/profile">
                                                <li><xsl:value-of select="text()"/></li>
                                            </xsl:for-each>
                                        </ul>
                                    </td>
                                </tr>
                            </xsl:if>
                            <xsl:if test="count(note) &gt; 0">
                                <tr>
                                    <td>
                                        <b>Note</b>
                                        <td><xsl:value-of select="note"/></td>
                                    </td>
                                </tr>
                            </xsl:if>
                        </table>
                    </div>
                </div>
                <br/>
                <!-- validation details -->
                <xsl:if test="count(messages) &gt; 0">
                    <div class="rich-panel styleResultBackground" id="schematron">
                        <div class="rich-panel-header">Detailed results</div>
                        <div class="rich-panel-body">
                            <!-- the list of messages is first processed to extract all the errors -->
                            <xsl:variable name="nbOfErrors" select="count(messages/message[@severity='error'])"/>
                            <xsl:variable name="nbOfInfos" select="count(messages/message[@severity='info'])"/>
                            <xsl:variable name="nbOfWarnings" select="count(messages/message[@severity='warning'])"/>
                  		<p><b>Summary</b></p> 
                            <p><xsl:choose>
                                <xsl:when test="$nbOfErrors &gt; 0">
                                    <a href="#errors"><xsl:value-of select="$nbOfErrors"/> errors</a> 
                                </xsl:when>
                                <xsl:otherwise>No error</xsl:otherwise>
                            </xsl:choose>
<br/>                            
<xsl:choose>
                                <xsl:when test="$nbOfWarnings &gt; 0">
                                    <a href="#warnings"><xsl:value-of select="$nbOfWarnings"/> warnings</a> 
                                </xsl:when>
                                <xsl:otherwise>No warning</xsl:otherwise>
                           </xsl:choose>
<br/>
                            <xsl:choose>
                                <xsl:when test="$nbOfInfos &gt; 0">
                                    <a href="#infos"><xsl:value-of select="$nbOfInfos"/> infos</a> 
                                </xsl:when>
                                <xsl:otherwise>No info</xsl:otherwise>
                            </xsl:choose></p>
                            <!--errors-->
<xsl:if test="$nbOfErrors &gt; 0">	                            
<p id="errors">
                                <b>Errors</b>
                            </p> 
                            <xsl:for-each select="messages/message">
                                <xsl:if test="contains(@severity, 'error')">
                                    <xsl:apply-templates select="current()"/>
                                </xsl:if>
                            </xsl:for-each>
</xsl:if>
<xsl:if test="$nbOfWarnings &gt; 0">
                            <!-- warnings -->
                            <p id="warnings">
                                <b>Warnings</b>
                            </p> 
                            <xsl:for-each select="messages/message">
                                <xsl:if test="contains(@severity, 'warning')">
                                    <xsl:apply-templates select="current()"/>
                                </xsl:if>
                            </xsl:for-each>
</xsl:if>
<xsl:if test="$nbOfInfos &gt; 0">
                            <!-- infos -->
                            <p id="infos">
                                <b>Infos</b>
                            </p>
                            <xsl:for-each select="messages/message">
                                <xsl:if test="contains(@severity, 'info')">
                                    <xsl:apply-templates select="current()"/>
                                </xsl:if>
                            </xsl:for-each>
</xsl:if>
                        </div>
                    </div>
                </xsl:if>
            </body>
        </html>
        
    </xsl:template>
    <xsl:template match="message">
           
        <!-- param = severity -->
        <xsl:element name="table">
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="contains(@severity, 'error')">
                        <xsl:text>Error</xsl:text>
                    </xsl:when>
                    <xsl:when test="contains(@severity, 'warning')">
                        <xsl:text>Warning</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>Note</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                
            </xsl:attribute>
            <xsl:attribute name="width">98%</xsl:attribute>
            <tr>
                <td valign="top" width="100">
                    <b>Location</b>
                </td>
                <td>
                    <xsl:if test="count(@offset) &gt; 0">
                        <xsl:text>Offset: </xsl:text><xsl:value-of select="@offset"/>
                    </xsl:if>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <b>Description</b>
                </td>
                <td>
                    <xsl:if test="count(@subMessage) &gt; 0">
                        <xsl:value-of select="@subMessage"/>
                        <br/>
                    </xsl:if>
                    <xsl:value-of select="text()"/>
                </td>
            </tr>
       </xsl:element>
<br/>
    </xsl:template>
</xsl:stylesheet>
