<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:dsml="urn:oasis:names:tc:DSML:2:0:core"
    version="2.0">
    <xsl:output method="html" encoding="UTF-8"/>
    <xsl:template match="dsml:batchRequest">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">BatchRequest</div>
            <div class="rich-panel-body"> 
                <table border="0">
                    <xsl:if test="@requestID">
                        <tr>
                            <td style="font-weight: bold;">requestID</td>
                            <td><xsl:value-of select="@requestID"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@processing">
                        <tr>
                            <td style="font-weight: bold;">processing</td>
                            <td><xsl:value-of select="@processing"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@responseOrder">
                        <tr>
                            <td style="font-weight: bold;">responseOrder</td>
                            <td><xsl:value-of select="@responseOrder"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@onError">
                        <tr>
                            <td style="font-weight: bold;">onError</td>
                            <td><xsl:value-of select="@onError"/></td>
                        </tr>
                    </xsl:if>
                </table>
                <xsl:apply-templates select="child::node()"/>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="dsml:addRequest">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">Add request</div>
            <div class="rich-panel-body"> 
                <table border="0">
                    <xsl:if test="@requestID">
                        <tr>
                            <td style="font-weight: bold;">requestID</td>
                            <td><xsl:value-of select="@requestID"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@dn">
                        <tr>
                            <td style="font-weight: bold;">DN</td>
                            <td><xsl:value-of select="@dn"/></td>
                        </tr>
                    </xsl:if>
                </table>
                <h3>Attributes</h3>
                <table border="1">
                    <tr style="font-weight:bold;">
                        <td>name</td>
                        <td>value(s)</td>
                    </tr>
                    <xsl:apply-templates select="dsml:attr"/>
                </table>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="dsml:modifyRequest">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">Modify request</div>
            <div class="rich-panel-body"> 
                <table border="0">
                    <xsl:if test="@requestID">
                        <tr>
                            <td style="font-weight: bold;">requestID</td>
                            <td colspan="2"><xsl:value-of select="@requestID"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="count(dsml:control) &gt; 0">
                        <tr>
                            <xsl:element name="td">
                                <xsl:attribute name="rowspan">
                                    <xsl:value-of select="count(dsml:control)"/>
                                </xsl:attribute>
                                <xsl:text>Control(s)</xsl:text>
                            </xsl:element>
                            <td><xsl:text>type (criticality)</xsl:text></td>
                            <td><xsl:text>control value</xsl:text></td>
                        </tr>
                        <xsl:for-each select="dsml:control">
                            <tr>
                                <td><xsl:value-of select="@type"/><xsl:text> (</xsl:text><xsl:value-of select="@criticality"/><xsl:text>)</xsl:text></td>
                                <td><xsl:value-of select="dsml:controlValue"/></td>
                            </tr>
                        </xsl:for-each>
                        
                    </xsl:if>
                    <xsl:if test="@dn">
                        <tr>
                            <td style="font-weight: bold;">DN</td>
                            <td colspan="2"><xsl:value-of select="@dn"/></td>
                        </tr>
                    </xsl:if>
                </table>
                <h3>Modifications</h3>
                <table border="1">
                    <tr style="font-weight:bold;">
                        <td>Modified attribute</td>
                        <td>operation</td>
                        <td>value(s)</td>
                    </tr>
                    <xsl:for-each select="dsml:modification">
                        <tr>
                            <td><xsl:value-of select="@name"/></td>
                            <td><xsl:value-of select="@operation"/></td>
                            <td>
                                <ul>
                                    <xsl:for-each select="dsml:value">
                                        <li><xsl:value-of select="text()"/></li>
                                    </xsl:for-each>
                                </ul>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="dsml:modDNRequest">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">Modify DN request</div>
            <div class="rich-panel-body"> 
                <table border="0">
                    <xsl:if test="@requestID">
                        <tr>
                            <td style="font-weight: bold;">requestID</td>
                            <td colspan="2"><xsl:value-of select="@requestID"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@dn">
                        <tr>
                            <td style="font-weight: bold;">DN</td>
                            <td colspan="2"><xsl:value-of select="dn"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@newrdn">
                        <tr>
                            <td style="font-weight: bold;">new RDN</td>
                            <td colspan="2"><xsl:value-of select="@newrdn"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@deleteorldrdn">
                        <tr>
                            <td style="font-weight: bold;">delete old RDN</td>
                            <td colspan="2"><xsl:value-of select="@deleteoldrdn"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@newSuperior">
                        <tr>
                            <td style="font-weight: bold;">new superior</td>
                            <td colspan="2"><xsl:value-of select="@newSuperior"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="count(dsml:control) &gt; 0">
                        <tr>
                            <xsl:element name="td">
                                <xsl:attribute name="rowspan">
                                    <xsl:value-of select="count(dsml:control)"/>
                                </xsl:attribute>
                                <xsl:text>Control(s)</xsl:text>
                            </xsl:element>
                            <td><xsl:text>type (criticality)</xsl:text></td>
                            <td><xsl:text>control value</xsl:text></td>
                        </tr>
                        <xsl:for-each select="dsml:control">
                            <tr>
                                <td><xsl:value-of select="@type"/><xsl:text> (</xsl:text><xsl:value-of select="@criticality"/><xsl:text>)</xsl:text></td>
                                <td><xsl:value-of select="dsml:controlValue"/></td>
                            </tr>
                        </xsl:for-each>
                        
                    </xsl:if>
                </table>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="dsml:delRequest">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">Delete request</div>
            <div class="rich-panel-body"> 
                <table border="0">
                    <xsl:if test="@requestID">
                        <tr>
                            <td style="font-weight: bold;">requestID</td>
                            <td colspan="2"><xsl:value-of select="@requestID"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="count(dsml:control) &gt; 0">
                        <tr>
                            <xsl:element name="td">
                                <xsl:attribute name="rowspan">
                                    <xsl:value-of select="count(dsml:control)"/>
                                </xsl:attribute>
                                <xsl:text>Control(s)</xsl:text>
                            </xsl:element>
                            <td><xsl:text>type (criticality)</xsl:text></td>
                            <td><xsl:text>control value</xsl:text></td>
                        </tr>
                        <xsl:for-each select="dsml:control">
                            <tr>
                                <td><xsl:value-of select="@type"/><xsl:text> (</xsl:text><xsl:value-of select="@criticality"/><xsl:text>)</xsl:text></td>
                                <td><xsl:value-of select="dsml:controlValue"/></td>
                            </tr>
                        </xsl:for-each>
                        
                    </xsl:if>
                    <xsl:if test="@dn">
                        <tr>
                            <td style="font-weight: bold;">DN</td>
                            <td colspan="2"><xsl:value-of select="@dn"/></td>
                        </tr>
                    </xsl:if>
                </table>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="dsml:searchRequest">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">Search request</div>
            <div class="rich-panel-body"> 
                <table border="0">
                    <xsl:if test="@requestID">
                        <tr>
                            <td style="font-weight: bold;">requestID</td>
                            <td colspan="2"><xsl:value-of select="@requestID"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@dn">
                        <tr>
                            <td style="font-weight: bold;">DN</td>
                            <td colspan="2"><xsl:value-of select="@dn"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@scope">
                        <tr>
                            <td style="font-weight: bold;">Scope</td>
                            <td colspan="2"><xsl:value-of select="@scope"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@derefAliases">
                        <tr>
                            <td style="font-weight: bold;">deref aliases</td>
                            <td colspan="2"><xsl:value-of select="@derefAliases"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@sizeLimit">
                        <tr>
                            <td style="font-weight: bold;">size limit</td>
                            <td colspan="2"><xsl:value-of select="@sizeLimit"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@timeLimit">
                        <tr>
                            <td style="font-weight: bold;">time limit</td>
                            <td colspan="2"><xsl:value-of select="@timeLimit"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="@typesOnly">
                        <tr>
                            <td style="font-weight: bold;">types only</td>
                            <td colspan="2"><xsl:value-of select="@typesOnly"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="count(dsml:control) &gt; 0">
                        <tr>
                            <xsl:element name="td">
                                <xsl:attribute name="rowspan">
                                    <xsl:value-of select="count(dsml:control)"/>
                                </xsl:attribute>
                                <xsl:text>Control(s)</xsl:text>
                            </xsl:element>
                            <td><xsl:text>type (criticality)</xsl:text></td>
                            <td><xsl:text>control value</xsl:text></td>
                        </tr>
                        <xsl:for-each select="dsml:control">
                            <tr>
                                <td><xsl:value-of select="@type"/><xsl:text> (</xsl:text><xsl:value-of select="@criticality"/><xsl:text>)</xsl:text></td>
                                <td><xsl:value-of select="dsml:controlValue"/></td>
                            </tr>
                        </xsl:for-each>
                    </xsl:if>
                </table>
                <h3>Filter</h3>
                <p><xsl:apply-templates select="dsml:filter/node()"/></p>
                <xsl:if test="count(dsml:attributes) &gt; 0">
                    <h3>Requested attributes</h3>
                    <ul>
                        <xsl:for-each select="dsml:attributes/dsml:attribute">
                            <li><xsl:value-of select="@name"/></li>
                        </xsl:for-each>
                    </ul>
                </xsl:if>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="dsml:batchResponse">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">Batch Response</div>
            <div class="rich-panel-body"> 
                <table border="0">
                    <xsl:if test="@requestID">
                        <tr>
                            <td style="font-weight: bold;">requestID</td>
                            <td><xsl:value-of select="@requestID"/></td>
                        </tr>
                    </xsl:if>
                </table>
                <xsl:apply-templates select="child::node()"/>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="dsml:searchResponse">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">search Request</div>
            <div class="rich-panel-body"> 
                <table border="0">
                    <xsl:if test="@requestID">
                        <tr>
                            <td style="font-weight: bold;">requestID</td>
                            <td><xsl:value-of select="@requestID"/></td>
                        </tr>
                    </xsl:if>
                </table>
                <xsl:apply-templates select="child::node()"/>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="dsml:searchResultEntry">
        <h2>Search result entry</h2>
        <xsl:call-template name="header"/>
        <h3>Attributes</h3>
        <table border="1">
            <tr style="font-weight:bold;">
                <td>name</td>
                <td>value(s)</td>
            </tr>
            <xsl:apply-templates select="dsml:attr"/>
        </table>
    </xsl:template>
    <xsl:template match="dsml:searchResultReference">
        <h2>Search Result Reference</h2>
            <xsl:call-template name="header"/>
        <h3>References</h3>
        <ul>
            <xsl:for-each select="dsml:ref">
                <li><xsl:value-of select="text()"/></li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    <xsl:template match="dsml:searchResultDone">
        <h2>Search Result Done</h2>
        <xsl:call-template name="header"/>
        <xsl:call-template name="result"/>
    </xsl:template>
    <xsl:template match="dsml:modifyResponse">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">modify Response</div>
            <xsl:call-template name="header"/>
            <xsl:call-template name="result"/>
        </div>
    </xsl:template>
    <xsl:template match="dsml:addResponse">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">add Response</div>
            <xsl:call-template name="header"/>
            <xsl:call-template name="result"/>
        </div>
    </xsl:template>
    <xsl:template match="dsml:delResponse">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">delete Response</div>
            <xsl:call-template name="header"/>
            <xsl:call-template name="result"/>
        </div>
    </xsl:template>
    <xsl:template match="dsml:modDNResponse">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">modify DN Response</div>
            <xsl:call-template name="header"/>
            <xsl:call-template name="result"/>
        </div>
    </xsl:template>
    <xsl:template match="dsml:errorResponse">
        <div class="rich-panel styleResultBackground">
            <div class="rich-panel-header">error Response</div>
            <table border="0">
                <xsl:if test="@requestID">
                    <tr>
                        <td style="font-weight: bold;">requestID</td>
                        <td><xsl:value-of select="@requestID"/></td>
                    </tr>
                </xsl:if>
                <xsl:if test="@type">
                    <tr>
                        <td style="font-weight: bold;">type</td>
                        <td><xsl:value-of select="@type"/></td>
                    </tr>
                </xsl:if>
            </table>
            <xsl:if test="count(dsml:message) &gt; 0">
            <h2>Message</h2>
                <xsl:value-of select="dsml:message/text()"/>
            </xsl:if>
        </div>
    </xsl:template>
    <xsl:template match="dsml:attr">
        <tr>
            <td style="font-weight:bold"><xsl:value-of select="@name"/></td>
            <td>
                <ul>
                    <xsl:for-each select="dsml:value">
                        <li><xsl:value-of select="text()"/></li>
                    </xsl:for-each>
                </ul>
            </td>
        </tr>
    </xsl:template>
    <xsl:template match="dsml:or">
        <xsl:for-each select="child::node()">
            <xsl:if test="@name or local-name() = 'and' or local-name() = 'or'">
                <xsl:element name="br"/>
                <xsl:apply-templates select="."/>
                <xsl:element name="br"/>
                <xsl:if test="count(following-sibling::node()[@name or local-name() = 'and' or local-name() = 'or']) &gt; 0">
                    <b><xsl:text> or </xsl:text></b>
                    <xsl:element name="br"/>
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="dsml:and">
        <xsl:for-each select="child::node()">
            <xsl:if test="@name or local-name() = 'and' or local-name() = 'or'">
                <xsl:apply-templates select="."/>
                <xsl:element name="br"/>
                <xsl:if test="count(following-sibling::node()[@name or local-name() = 'and' or local-name() = 'or']) &gt; 0">
                    <b><xsl:text> and </xsl:text></b>
                </xsl:if>
                <xsl:element name="br"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="dsml:not">
        <b><xsl:text>not (</xsl:text></b>
        <xsl:apply-templates select="child::node()"/>
        <b><xsl:text>) </xsl:text></b>
    </xsl:template>
    <xsl:template match="dsml:equalityMatch">
        <xsl:value-of select="@name"/>
        <b><xsl:text> = </xsl:text></b>
        <i><xsl:value-of select="dsml:value"/></i>
    </xsl:template>
    <xsl:template match="dsml:substrings">
        <xsl:value-of select="@name"/>
        <xsl:apply-templates select="child::node()"/>
    </xsl:template>
    <xsl:template match="dsml:initial">
        <b><xsl:text> starts with </xsl:text></b>
        <i><xsl:value-of select="text()"/></i>
        <xsl:text> </xsl:text>
    </xsl:template>
    <xsl:template match="dsml:any">
        <b><xsl:text> contains </xsl:text></b>
        <i><xsl:value-of select="text()"/></i>
        <xsl:text> </xsl:text>
    </xsl:template>
    <xsl:template match="dsml:final">
        <b><xsl:text> ends with </xsl:text></b>
        <i><xsl:value-of select="text()"/></i>
    </xsl:template>
    <xsl:template match="dsml:greaterOrEqual">
        <xsl:value-of select="@name"/>
        <b><xsl:text> &gt;= </xsl:text></b>
        <i><xsl:value-of select="dsml:value"/></i>
    </xsl:template>
    <xsl:template match="dsml:lessOrEqual">
        <xsl:value-of select="@name"/>
        <b><xsl:text> &lt;= </xsl:text></b>
        <i><xsl:value-of select="dsml:value"/></i>
    </xsl:template>
    <xsl:template match="dsml:present">
        <xsl:value-of select="@name"/>
        <b><xsl:text> is present</xsl:text></b>
    </xsl:template>
    <xsl:template match="dsml:approxMatch">
        <xsl:value-of select="@name"/>
        <b><xsl:text> like </xsl:text></b>
        <i><xsl:value-of select="dsml:value"/></i>
    </xsl:template>
    <xsl:template match="dsml:extensibleMatch">
        <xsl:value-of select="@name"/>
        <xsl:text> </xsl:text>
        <b><xsl:value-of select="@matchingRule"/></b>
        <xsl:text> </xsl:text>
        <i><xsl:value-of select="dsml:value"/></i>
    </xsl:template>
    <xsl:template name="header">
        <table border="0">
            <xsl:if test="@requestID">
                <tr>
                    <td style="font-weight: bold;">requestID</td>
                    <td colspan="2"><xsl:value-of select="@requestID"/></td>
                </tr>
            </xsl:if>
            <xsl:if test="count(dsml:control) &gt; 0">
                <tr>
                    <xsl:element name="td">
                        <xsl:attribute name="rowspan">
                            <xsl:value-of select="count(dsml:control)"/>
                        </xsl:attribute>
                        <xsl:text>Control(s)</xsl:text>
                    </xsl:element>
                    <td><xsl:text>type (criticality)</xsl:text></td>
                    <td><xsl:text>control value</xsl:text></td>
                </tr>
                <xsl:for-each select="dsml:control">
                    <tr>
                        <td><xsl:value-of select="@type"/><xsl:text> (</xsl:text><xsl:value-of select="@criticality"/><xsl:text>)</xsl:text></td>
                        <td><xsl:value-of select="dsml:controlValue"/></td>
                    </tr>
                </xsl:for-each>
                
            </xsl:if>
            <xsl:if test="@dn">
                <tr>
                    <td style="font-weight: bold;">DN</td>
                    <td colspan="2"><xsl:value-of select="@dn"/></td>
                </tr>
            </xsl:if>
            <xsl:if test="@matchedDN">
                <tr>
                    <td style="font-weight: bold;">Matched DN</td>
                    <td colspan="2"><xsl:value-of select="@matchedDN"/></td>
                </tr>
            </xsl:if>
        </table>
    </xsl:template>
    <xsl:template name="result">
        <h3>Result code</h3>
        <table border="0">
            <tr>
                <td style="font-weight:bold;"><xsl:text>code</xsl:text></td>
                <td><xsl:value-of select="dsml:resultCode/@code"></xsl:value-of></td>
            </tr>
            <xsl:if test="resultCode/@descr">
                <tr>
                    <td style="font-weight:bold;"><xsl:text>description</xsl:text></td>
                    <td><xsl:value-of select="dsml:resultCode/@descr"></xsl:value-of></td>
                </tr>
            </xsl:if>
        </table>
        <xsl:if test="count(dsml:errorMessage) &gt; 0">
            <h3>Error Message</h3>
            <xsl:value-of select="dsml:errorMessage/text()"/>
        </xsl:if>
        <xsl:if test="count(dsml:referral) &gt; 0">
            <h3>Referral(s)</h3>
            <ul>
                <xsl:for-each select="dsml:referral">
                    <li><xsl:value-of select="text()"/></li>
                </xsl:for-each>
            </ul>
        </xsl:if>
    </xsl:template>
    <xsl:template match="text()">
        <!-- do nothing -->
    </xsl:template>
</xsl:stylesheet>