<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"  
  xmlns:b64="https://github.com/ilyakharlamov/xslt_base64">
  <xsl:output method="xml" version="1.0" omit-xml-declaration="no"
    standalone="no"
    encoding="UTF-8" indent="yes"/>
  <xsl:include href="base64.xsl"/>
  
  <xsl:template match="/">
    <html>
      <head>
      </head>
      <body>
        <h3>Audit Message Content</h3>
        <xsl:apply-templates select="*"/>       
      </body>
    </html>
  </xsl:template>
  <xsl:template match="AuditMessage">
    <xsl:apply-templates select="*"/>
  </xsl:template>
  <!-- =========================================== -->
  <!-- EventIdentification                         -->
  <!-- =========================================== -->
  <xsl:template match="EventIdentification">
    <h4 style="margin-top:10px;">
      Event ID:  <xsl:choose>
        <xsl:when test="@EventActionCode='C'">Create</xsl:when>  
        <xsl:when test="@EventActionCode='U'">Update</xsl:when>
        <xsl:when test="@EventActionCode='D'">Delete</xsl:when>
        <xsl:when test="@EventActionCode='R'">Read</xsl:when>
        <xsl:when test="@EventActionCode='E'">Execute</xsl:when>
        <xsl:otherwise>Unknown Event Action Code</xsl:otherwise>
      </xsl:choose>
      <xsl:text>  </xsl:text>
      <xsl:value-of select="EventID/@originalText"/> (<xsl:value-of select="EventID/@code"/><xsl:value-of
        select="EventID/@csd-code"/>)
      
    </h4>
    <table  class="table table-hover table-responsive table-bordered table-condensed"  width="100%">
      <tr><td style="width:200px;text-align:right;padding-right:4px" ><strong>Timestamp:</strong></td><td style="padding-left:4px"><xsl:value-of select="@EventDateTime"/></td></tr>
      
      
      <xsl:apply-templates select="EventTypeCode"/>
    </table>
  </xsl:template>
  
  <!-- =========================================== -->
  <!-- EventTypeCode                               -->
  <!-- =========================================== --> 
  <xsl:template match="EventTypeCode">
    <tr><td style="width:200px;text-align:right;padding-right:4px" ><strong>Trigger:</strong></td><td style="padding-left:4px"><xsl:value-of select="@codeSystemName"/><xsl:text> : </xsl:text>
      <xsl:value-of select="@code"/><xsl:value-of
        select="@csd-code"/><xsl:text> - </xsl:text><xsl:value-of select="@originalText"/></td></tr>
    
  </xsl:template>
  
  <!-- =========================================== -->
  <!-- EventDateTime                               -->
  <!-- =========================================== --> 
  <xsl:template match="EventDateTime">
    <p>Timestamp for the event  <xsl:value-of select="@codeSystemName"/><xsl:text> : </xsl:text>
      <xsl:value-of select="@code"/><xsl:value-of
        select="@csd-code"/><xsl:text> - </xsl:text><xsl:value-of select="@originalText"/>
    </p>
  </xsl:template>
  
  <!-- =========================================== -->
  <!-- Active Participant                          -->
  <!-- =========================================== -->
  <xsl:template match="ActiveParticipant">
    <h4 style="margin-top:10px;">Active Participant : <xsl:apply-templates select="RoleIDCode"/></h4>
    <table  class="table table-hover table-responsive table-bordered table-condensed"  width="100%">
      
      <tr><td style="width:200px;text-align:right;padding-right:4px" ><strong>
        <xsl:text>ID:</xsl:text>
      </strong> 
      </td><td style="padding-left:4px">
        <xsl:value-of select="@UserID"/>
      </td></tr>
   
      <xsl:if test="@AlternativeUserID">
        <tr><td style="width:200px;text-align:right;padding-right:4px" ><strong>
          <xsl:text>Alternative ID:</xsl:text>
        </strong>
        </td><td style="padding-left:4px">    
          <xsl:value-of select="@AlternativeUserID"/>
        </td></tr>
      </xsl:if>
      
      
      <xsl:if test="@UserName">
        <tr><td style="width:200px;text-align:right;padding-right:4px" ><strong>
          <xsl:text>Name:</xsl:text>
        </strong>
        </td><td style="padding-left:4px">
          <xsl:value-of select="@UserName"/>
        </td></tr>
      </xsl:if>
      
      <xsl:if test="@NetworkAccessPointID">
        
        <tr><td style="width:200px;text-align:right;padding-right:4px" ><strong>
          <xsl:choose>
            <xsl:when test="@NetworkAccessPointTypeCode=1">Host:</xsl:when>
            <xsl:when test="@NetworkAccessPointTypeCode=2">IP Address:</xsl:when>
            <xsl:when test="@NetworkAccessPointTypeCode=3">Tel.No.:</xsl:when>
            <xsl:otherwise>,&#32;</xsl:otherwise>
          </xsl:choose></strong>
        </td><td style="padding-left:4px">
          <xsl:value-of select="@NetworkAccessPointID"/>
        </td>
        </tr>
        
      </xsl:if>
    </table>
  </xsl:template>
  <xsl:template match="RoleIDCode">
    
    <xsl:choose>
      <xsl:when test="position()=1"></xsl:when>
      <xsl:otherwise>,&#32;</xsl:otherwise>
    </xsl:choose>
    <xsl:value-of select="@originalText"/>
    <xsl:if test="position()=last()"></xsl:if>
  </xsl:template>
  <!-- =========================================== -->
  <!-- AuditSourceIdentification                   -->
  <!-- =========================================== -->
  <xsl:template match="AuditSourceIdentification">
    
    <h4 style="margin-top:10px;">Audit&#160;Source:&#160;</h4>
    <table  class="table table-hover table-responsive table-bordered table-condensed"  width="100%"><tr><td style="width:200px;text-align:right;padding-right:4px" ><strong>
      <xsl:text>ID:</xsl:text></strong>
    </td>
      <td style="padding-left:4px">
        <xsl:value-of select="@AuditSourceID"/>
      </td>
    </tr>
      <tr><td style="width:200px;text-align:right;padding-right:4px" ><strong> 
        <xsl:text>Site:</xsl:text></strong>
      </td><td style="padding-left:4px">
        <xsl:if test="@AuditEnterpriseSiteID">  
          <xsl:value-of select="@AuditEnterpriseSiteID"/>
        </xsl:if>
      </td></tr>
      <tr><td style="width:200px;text-align:right;padding-right:4px" ><strong> 
        <xsl:text>Code:</xsl:text></strong>
      </td><td style="padding-left:4px">
        <xsl:choose>
          <xsl:when test="@code=1">End-user interface</xsl:when>
          <xsl:when test="@code=2">Data acquisition device or instrument</xsl:when>
          <xsl:when test="@code=3">Web server process tier in a multi-tier system</xsl:when>
          <xsl:when test="@code=4">Application server process tier in a multi-tier system</xsl:when>
          <xsl:when test="@code=5">Database server process tier in a multi-tier system</xsl:when>
          <xsl:when test="@code=6">Security server, e.g., a domain controller</xsl:when>
          <xsl:when test="@code=7">ISO level 1-3 network component</xsl:when>
          <xsl:when test="@code=8">ISO level 4-6 operating software</xsl:when>
          <xsl:when test="@code=9">External source, other or unknown type</xsl:when>
        </xsl:choose>
      </td></tr>
    </table>
    
  </xsl:template>
  <!-- =========================================== -->
  <!-- ParticipantObjectIdentification             -->
  <!-- =========================================== -->
  <xsl:template match="ParticipantObjectIdentification">
    <h4 style="margin-top:10px;">Participant:   
      <xsl:choose>
        
        <xsl:when test="@ParticipantObjectTypeCode=1">Person (1)</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCode=2">System Object (2)</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCode=3">Organization (3)</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCode=4">Other (4)</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCode">
          Unknown code (<xsl:value-of select="@ParticipantObjectTypeCode"/>):
        </xsl:when>
      </xsl:choose>
      <xsl:text> </xsl:text>
      <xsl:choose>
        <xsl:when test="@ParticipantObjectTypeCodeRole=1">Patient</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=2">Location</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=3">Report</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=4">Resource</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=5">Master&#160;file</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=6">User</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=7">List</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=8">Doctor</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=9">Subscriber</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=10">Guarantor</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=11"
          >Security&#160;User&#160;Entity</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=12"
          >Security&#160;User&#160;Group</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=13"
          >Security&#160;Resource</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=14"
          >Security&#160;Granualarity&#160;Definition</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=15">Provider</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=16"
          >Report&#160;Destination</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=17"
          >Report&#160;Library</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=18">Schedule</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=19">Customer</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=20">Job</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=21">Job&#160;Stream</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=22">Table</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=23"
          >Routing&#160;Criteria</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole=24">Query</xsl:when>
        <xsl:when test="@ParticipantObjectTypeCodeRole">
          Unknown role</xsl:when>
      </xsl:choose> (<xsl:value-of select="@ParticipantObjectTypeCodeRole"/>):
      
    </h4>
    
    <table  class="table table-hover table-responsive table-bordered table-condensed"  width="100%">
      <tr><td style="width:200px;text-align:right;padding-right:4px" ><strong>
        ID:</strong>
      </td>
        <td style="padding-left:4px">
          <xsl:value-of select="@ParticipantObjectID"/> 
        </td></tr>
      
      <tr><td style="width:200px;text-align:right;padding-right:4px" ><strong>
        <xsl:apply-templates select="ParticipantObjectIDTypeCode"/>:    </strong>
      </td>
        <td style="padding-left:4px">
          <xsl:value-of select="ParticipantObjectIDTypeCode/@codeSystemName"/> <xsl:text> : </xsl:text>
          <xsl:value-of select="ParticipantObjectIDTypeCode/@code"/> <xsl:text> - </xsl:text>
          <xsl:value-of select="ParticipantObjectIDTypeCode/@displayName"/> 
        </td></tr>
      <xsl:if test="ParticipantObjectName">
        <tr>
          <td style="width:200px;text-align:right;padding-right:4px" ><strong>Name</strong></td>
          <td style="padding-left:4px"><xsl:value-of select="ParticipantObjectName"/></td>
        </tr>
      </xsl:if>
      
      <xsl:if test="ParticipantObjectQuery">
        <tr>
          <td style="width:200px;text-align:right;padding-right:4px" ><strong>Query:</strong></td>
          <td style="padding-left:4px">
            
            <xsl:call-template name="b64:decode">
              <xsl:with-param name="base64String" select="ParticipantObjectQuery"></xsl:with-param>
            </xsl:call-template>
            
          </td>
        </tr>
      </xsl:if>
      
      <xsl:if test="ParticipantObjectDetail">
        <tr>
          <td style="width:200px;text-align:right;padding-right:4px" ><strong>Detail (<xsl:value-of
            select="ParticipantObjectDetail/@type"/>):</strong> </td>
          <td style="padding-left:4px">
            <xsl:call-template name="b64:decode">
              <xsl:with-param name="base64String" select="ParticipantObjectDetail/@value"></xsl:with-param>
            </xsl:call-template>
            
          </td>
        </tr>
      </xsl:if>    
    </table>
  </xsl:template>
  <xsl:template match="ParticipantObjectIDTypeCode">
    <xsl:choose>
      <xsl:when test="@displayName">
        <xsl:value-of select="@displayName"/>
      </xsl:when>
      <xsl:when test="@code=1">Medical&#160;Record&#160;Number</xsl:when>
      <xsl:when test="@code=2">Patient&#160;Number</xsl:when>
      <xsl:when test="@code=3">Encounter&#160;Number</xsl:when>
      <xsl:when test="@code=4">Enrollee&#160;Number</xsl:when>
      <xsl:when test="@code=5">Social&#160;Security&#160;Number</xsl:when>
      <xsl:when test="@code=6">Account&#160;Number</xsl:when>
      <xsl:when test="@code=7">Guarantor&#160;Number</xsl:when>
      <xsl:when test="@code=8">Report&#160;Name</xsl:when>
      <xsl:when test="@code=9">Report&#160;Number</xsl:when>
      <xsl:when test="@code=10">Search&#160;Criteria</xsl:when>
      <xsl:when test="@code=11">User&#160;Identifier</xsl:when>
      <xsl:when test="@code=12">URI</xsl:when>
      <xsl:when test="@code='110180'">Study&#160;Instance&#160;UID</xsl:when>
      <xsl:when test="@code='110181'">SOP&#160;Class&#160;UID</xsl:when>
      <xsl:when test="@code='110182'">Node&#160;ID</xsl:when>
      <xsl:when test="@code='urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd'">submission&#160;set&#160;classificationNode</xsl:when>
      <xsl:when test="@csd-code=1">Medical&#160;Record&#160;Number</xsl:when>
      <xsl:when test="@csd-code=2">Patient&#160;Number</xsl:when>
      <xsl:when test="@csd-code=3">Encounter&#160;Number</xsl:when>
      <xsl:when test="@csd-code=4">Enrollee&#160;Number</xsl:when>
      <xsl:when test="@csd-code=5">Social&#160;Security&#160;Number</xsl:when>
      <xsl:when test="@csd-code=6">Account&#160;Number</xsl:when>
      <xsl:when test="@csd-code=7">Guarantor&#160;Number</xsl:when>
      <xsl:when test="@csd-code=8">Report&#160;Name</xsl:when>
      <xsl:when test="@csd-code=9">Report&#160;Number</xsl:when>
      <xsl:when test="@csd-code=10">Search&#160;Criteria</xsl:when>
      <xsl:when test="@csd-code=11">User&#160;Identifier</xsl:when>
      <xsl:when test="@csd-code=12">URI</xsl:when>
      <xsl:when test="@csd-code='110180'">Study&#160;Instance&#160;UID</xsl:when>
      <xsl:when test="@csd-code='110181'">SOP&#160;Class&#160;UID</xsl:when>
      <xsl:when test="@csd-code='110182'">Node&#160;ID</xsl:when>
      <xsl:when test="@csd-code='urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd'">submission&#160;set&#160;classificationNode</xsl:when>
      <xsl:otherwise>
        Unknown code
        (<xsl:value-of select="@code"/><xsl:value-of select="@csd-code"/>)
     </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  
  
</xsl:stylesheet>
