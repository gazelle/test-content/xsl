<?xml version="1.0" encoding="UTF-8"?>
<!--
 
 This stylesheet convert an HL7 XML V2.x message into ER7 (or FIELD_SEP and hat) message 
 
 Programme : xml2pipe.xsl
 Author : Eric Poiseau
 Date :    May 30th 2005
 
Copyright (c) 2005 Laboratoire IDM, Faculté de Médecine, ave du Prof Leon Bernard, Rennes, France
 
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; 

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


 History

 Version : 0.1 : Initial Release
 
 Assumptions : 
 
 The stylesheet make the assumption that the field are sorted in the ascending order. Will not produce any relevant messages if 
 the field are not sorted out. 
 
 There is not assumption about the separator in the message.
 
 Todo : 
 
 Need error checking concerning the order of the fields in the XML message. 
 Need to work further on the validation of the tool. Qualité assurance is not yet completed. 
 
 Validatation: 
     - Sample messages both in XML and ER7. The conversion should match.
     
 Licence
 
 -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:variable name="DEBUG">
        <xsl:value-of select="0"/>
    </xsl:variable>
    <xsl:variable name="ANNOTATE">
        <xsl:value-of select="1"/>
    </xsl:variable>
    <xsl:variable name="WARN">
        <xsl:value-of select="1"/>
    </xsl:variable>
    <xsl:variable name="newline">
        <xsl:text>
</xsl:text>
    </xsl:variable>
    <xsl:output method="text"/>
    <!-- 
    Default HL7 delimiters may not be used, so we'd better be safe and use the one specified in the XML message.
    -->
    <xsl:variable name="FIELD_SEPARATOR">
        <xsl:value-of select="//MSH.1"/>
    </xsl:variable>
    <xsl:variable name="COMPONENT_SEPARATOR">
        <xsl:value-of select="substring(//MSH.2,1,1)"/>
    </xsl:variable>
    <xsl:variable name="REPETITION_SEPARATOR">
        <xsl:value-of select="substring(//MSH.2,2,1)"/>
    </xsl:variable>
    <xsl:variable name="ESCAPE">
        <xsl:value-of select="substring(//MSH.2,3,1)"/>
    </xsl:variable>
    <xsl:variable name="SUBCOMPONENT_SEPARATOR">
        <xsl:value-of select="substring(//MSH.2,4,1)"/>
    </xsl:variable>
    <!--
    
    Search for all entries. If an entry name has a length of 3 then this is a segment.
    Note that there might be some subfields in a segment that may also be coded on 3 letters.
    This will not be seen, has we only need to recurse when we are in a group. The recursion stops at the 
    segment level
   
    -->
    <xsl:template match="/">
        <xsl:message>Converting message using the following separators <xsl:value-of
                select="$FIELD_SEPARATOR"/>
            <xsl:value-of select="$COMPONENT_SEPARATOR"/>
            <xsl:value-of select="$SUBCOMPONENT_SEPARATOR"/>
            <xsl:value-of select="$REPETITION_SEPARATOR"/>
            <xsl:value-of select="$ESCAPE"/>
        </xsl:message>
        <xsl:for-each select="*">
            <xsl:if test="$DEBUG=1">
                <xsl:message>
                    <xsl:text>Processing node  </xsl:text>
                    <xsl:value-of select="name(.)"/>
                </xsl:message>
            </xsl:if>
            <xsl:apply-templates select="*[string-length(name())=3]"/>
            <xsl:apply-templates select="*[string-length(name())&gt;3]"/>
        </xsl:for-each>
    </xsl:template>
    <!-- 
    
    Template to process groups
    
    -->
    <xsl:template name="process_group" match="*[string-length(name())&gt;3]">
        <xsl:choose>
            <xsl:when test="string-length(name()) = 3">
                <xsl:if test="$ANNOTATE=1">
                    <xsl:message>
                        <xsl:text>Found segment</xsl:text>
                        <xsl:value-of select="name(.)"/>
                    </xsl:message>
                </xsl:if>
                <xsl:call-template name="process_segment"/>
                <xsl:text>
</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="child::*"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- 
    
    Template to process segments
        
    -->
    <xsl:template name="process_segment" match="*[string-length(name())=3]">
        <xsl:if test="$ANNOTATE=1">
            <xsl:message>
                <xsl:text>Processing segment </xsl:text>
                <xsl:value-of select="name()"/>
            </xsl:message>
        </xsl:if>
        <xsl:value-of select="normalize-space(name(.))"/>
        <!-- Do not write the FIELD_SEPARATOR following the MSH string in the ER7 messages as it 
        will be created by the content of the MSH.1 content-->
        <xsl:if test="name(.)!='MSH'">
            <xsl:value-of select="$FIELD_SEPARATOR"/>
        </xsl:if>
        <!-- For each fields -->
        <xsl:for-each select="*">
            <xsl:if test="$ANNOTATE=1">
                <xsl:message>Processing field <xsl:value-of select="normalize-space(name(.))"/>
                </xsl:message>
            </xsl:if>
            <!-- Handles the case of the empty fields -->
            <xsl:choose>
                <xsl:when test="position() &lt; number(substring-after(name(.),'.'))">
                    <xsl:call-template name="empty_fields">
                        <xsl:with-param name="pos">
                            <xsl:value-of select="position()"/>
                        </xsl:with-param>
                        <xsl:with-param name="curr">
                            <xsl:value-of select="number(substring-after(name(.),'.'))"/>
                        </xsl:with-param>
                        <xsl:with-param name="char">
                            <xsl:value-of select="$FIELD_SEPARATOR"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
            <xsl:choose>
                <!-- Handles the case where we have fields -->
                <xsl:when test="count(*)!=0">
                    <xsl:for-each select="*">
                        <xsl:if test="$ANNOTATE=1">
                            <xsl:message>Processing subfield <xsl:value-of
                                    select="normalize-space(name(.))"/>
                            </xsl:message>
                        </xsl:if>
                        <xsl:if test="$DEBUG=1">
                            <xsl:message>
                                <xsl:text>curr = </xsl:text>
                                <xsl:value-of select=" number(substring-after(name(.),'.'))"/>
                                <xsl:text> and pos =</xsl:text>
                                <xsl:value-of select="position()"/>
                            </xsl:message>
                        </xsl:if>
                        <!-- Handles the case of the empty subfields  -->
                        <xsl:choose>
                            <!-- For the last one write a '|' and not a '^' -->
                            <xsl:when test="position() &lt;
                                number(substring-after(name(.),'.'))">
                                <xsl:call-template name="empty_fields">
                                    <xsl:with-param name="pos">
                                        <xsl:value-of select="position()"/>
                                    </xsl:with-param>
                                    <xsl:with-param name="curr">
                                        <xsl:value-of select="number(substring-after(name(.),'.'))"
                                        />
                                    </xsl:with-param>
                                    <xsl:with-param name="char">
                                        <xsl:value-of select="$COMPONENT_SEPARATOR"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                                <xsl:value-of select="normalize-space(.)"/>
                            </xsl:when>
                            <xsl:when test="position() = number(substring-after(name(.),'.')) and
                                count(*)=0">
                                <xsl:value-of select="normalize-space(.)"/>
        <xsl:choose>
                                    <xsl:when test="name(.)=name(following-sibling::*)">
                                        <xsl:value-of select="$REPETITION_SEPARATOR"/>
                                    </xsl:when>
                                    <xsl:when test="position()!=last()">
                                        <xsl:value-of select="$COMPONENT_SEPARATOR"/>
                                    </xsl:when>
        </xsl:choose>
                                </xsl:when>
                        </xsl:choose>
                        <xsl:choose>
                            <!-- Handles the case where we have subcomponents -->
                            <xsl:when test="count(*) &gt; 0">
                                <xsl:for-each select="*">
                                    <xsl:choose>
                                        <xsl:when test="position() &lt;
                                            number(substring-after(name(.),'.'))">
                                            <xsl:call-template name="empty_fields">
                                                <xsl:with-param name="pos">
                                                  <xsl:value-of select="position()"/>
                                                </xsl:with-param>
                                                <xsl:with-param name="curr">
                                                  <xsl:value-of
                                                  select="number(substring-after(name(.),'.'))"
                                                  />
                                                </xsl:with-param>
                                                <xsl:with-param name="char">
                                                  <xsl:value-of select="$SUBCOMPONENT_SEPARATOR"/>
                                                </xsl:with-param>
                                            </xsl:call-template>
                                            <xsl:value-of select="normalize-space(.)"/>
                                        </xsl:when>
                                        <xsl:when test="position() =
                                            number(substring-after(name(.),'.')) and count(*)=0">
                                            <xsl:value-of select="normalize-space(.)"/>
                                            <xsl:if test="position()!=last()">
                                                <xsl:value-of select="$SUBCOMPONENT_SEPARATOR"/>
                                            </xsl:if>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>
                                <xsl:choose>
                                    <xsl:when test="name(.)=name(following-sibling::*)">
                                        <xsl:value-of select="$REPETITION_SEPARATOR"/>
                                    </xsl:when>
                                    <xsl:when test="position()!=last()">
                                        <xsl:value-of select="$COMPONENT_SEPARATOR"/>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
            <!-- If we are not in case of the MSH.1 (MSH.1 is the field separator, so should not be repeated ! -->
            <xsl:if test="normalize-space(name(.))!='MSH.1'">
                <xsl:choose>
                    <xsl:when test="name(.)=name(following-sibling::*)">
                        <xsl:value-of select="$REPETITION_SEPARATOR"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$FIELD_SEPARATOR"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </xsl:for-each>
        <xsl:value-of select="$newline"/>
    </xsl:template>
    <!-- 
    
    
    This is the template used to write the repeating FIELD_SEPs and carrot
    
    
    -->
    <xsl:template name="empty_fields">
        <xsl:param name="pos"/>
        <xsl:param name="curr"/>
        <xsl:param name="char"/>
        <xsl:if test="$DEBUG=1">
            <xsl:message>
                <xsl:text>curr = </xsl:text>
                <xsl:value-of select="$curr"/>
                <xsl:text> and pos =</xsl:text>
                <xsl:value-of select="$pos"/>
            </xsl:message>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="$pos &lt; $curr">
                <xsl:value-of select="$char"/>
                <xsl:call-template name="empty_fields">
                    <xsl:with-param name="pos" select="$pos + 1"/>
                    <xsl:with-param name="curr" select="$curr"/>
                    <xsl:with-param name="char" select="$char"/>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
