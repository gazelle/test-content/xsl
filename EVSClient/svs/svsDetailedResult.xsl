<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 02, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Abderrazek Boufahja, IHE Development, Kereval</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="viewdown">true</xsl:param>
    <xsl:param name="constraintPath">#</xsl:param>
    <xsl:param name="assertionManagerPath">/AssertionManagerGui/</xsl:param>
    <xsl:param name="ShowGeneralInformation">false</xsl:param>
    <xsl:param name="ShowResultOverview">false</xsl:param>
    <xsl:param name="displayMax">100</xsl:param>
    <xsl:param name="selectedLanguage">fr</xsl:param>
    <xsl:param name="standalone">true</xsl:param>

    <xsl:param name="doTranslate">false</xsl:param>
    <xsl:variable name="ShowGeneralInformationLocal">
        <xsl:choose>
            <xsl:when test="normalize-space($ShowGeneralInformation)">
                <xsl:value-of select="$ShowGeneralInformation"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="true()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:param name="index"/>

    <xsl:template match="/">
        <html>
            <xsl:call-template name="insertHeader">
                <xsl:with-param name="type">Schematron</xsl:with-param>
            </xsl:call-template>

            <body>


                <!-- general information -->
                <xsl:if test="$ShowGeneralInformation = 'true'">
                    <xsl:call-template name="viewGeneralInformation"/>
                </xsl:if>

                <!-- XML well formed report -->

                <xsl:call-template name="displayXMLWellFormReport"/>

                <!-- XML XSD validation report -->

                <xsl:call-template name="displayXSDValidationReport"/>

                <xsl:call-template name="displayMBValidatorResults">
                    <xsl:with-param name="showProgressBar">true</xsl:with-param>
                    <xsl:with-param name="showNavBar">true</xsl:with-param>
                </xsl:call-template>

                <script type="text/javascript" defer="defer" language="javascript">
                    //<![CDATA[
         jq162(document).ready( computeStats());
        //]]>
                </script>
            </body>
        </html>
    </xsl:template>


    <xsl:include href="/xsl/EVSClient/common/common.xsl"/>

</xsl:stylesheet>
