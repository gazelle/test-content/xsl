<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0" 
	xmlns:java="http://xml.apache.org/xslt/java"
    exclude-result-prefixes="java"
	xmlns:apm="net.ihe.gazelle.common.application.action.ApplicationPreferenceManager" >
	
	<xsl:output encoding="UTF-8" indent="yes" method="html"
		media-type="text/html" />
	
    <xsl:param name="appName">EVSClient</xsl:param>
	<xsl:template match="/">
		<html>
			<head>
				<title>Test Dicom Results</title>
				<link>
					<xsl:attribute name="type">text/css</xsl:attribute>
					<xsl:attribute name="rel">stylesheet</xsl:attribute>
					<xsl:attribute name="href">/<xsl:value-of select="$appName"></xsl:value-of>/stylesheet/theme-EVSClient.css</xsl:attribute>
				</link>
			</head> 
			<body>
				<h2>Result summary :</h2>
				<br />
				<b>
					<xsl:text>Validation Date : </xsl:text>
				</b>
				<xsl:value-of select="systemSOP/validationDate" />
				<br />
				<b>
					<xsl:text>System name : </xsl:text>
				</b>
				<xsl:value-of select="systemSOP/systemName" />
				<br />
				<b>
					<xsl:text>IP : </xsl:text>
				</b>
				<xsl:value-of select="systemSOP/ip" />
				<br />
				<b>
					<xsl:text>Port : </xsl:text>
				</b>
				<xsl:value-of select="systemSOP/port" />
				<br />
				<b>
					<xsl:text>AETitle : </xsl:text>
				</b>
				<xsl:value-of select="systemSOP/aeTitle" />
				<br />

				<br />
				<table border="1"
					class="rich-table sort01 table-autofilter table-autosort table-stripeclass:alternate table-page-number:t1page table-page-count:t1pages table-filtered-rowcount:t1filtercount table-rowcount:t1allcount table-autopage:15"
					id="TABLE_6">
					<thead>
						<tr class="rich-table-subheader ">
							<th class="rich-table-subheadercell table-sortable:default table-sortable" title="Click to sort"
								width="300">
								SOP Class OID
							</th>
							<th class="rich-table-subheadercell table-sortable:default table-sortable" title="Click to sort"
								width="600">
								SOP Class Name
							</th>
							<th class="rich-table-subheadercell table-sortable:default table-sortable" title="Click to sort"
								width="150">Supported
								<br /><select onchange="Table.filter(this,this)" onclick="Table.cancelBubble(event)" class="table-autofilter">
									<option value="">Filter: All</option>
									<option value="YES">YES</option>
									<option value="NO">NO</option>
								</select>
							</th>
							<th class="rich-table-subheadercell" width="100">Details</th>
						</tr>
					</thead>
					<tbody>
						<xsl:for-each select="systemSOP/sopClassList/sopClass">
							<xsl:sort select="Supported" order="descending" />
							<xsl:sort select="SOPClassName" order="ascending" />
							<tr>
								<td class="rich-table-cell">
									<xsl:value-of select="SOPClassUID" />
								</td>
								<td class="rich-table-cell">
									<xsl:value-of select="SOPClassName" />
								</td>
								<td class="rich-table-cell">
									<xsl:value-of select="Supported" />
								</td>
								<td class="rich-table-cell">
									<xsl:element name="a">
										<xsl:attribute name="href">
										<xsl:text>#</xsl:text>
										<xsl:value-of select="SOPClassName" />
									</xsl:attribute>
										<xsl:text>Link</xsl:text>
									</xsl:element>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
					<tfoot>
						<tr class="rich-table-subheader">
							<td class="rich-table-subheadercell table-page:previous" style="cursor:pointer;">&lt; &lt; Previous</td>
							<td class="rich-table-subheadercell" style="text-align:center;">
								Page
								<span id="t1page"></span>
								&#160;of
								<span id="t1pages"></span>
							</td>
							<td colspan="2" class="rich-table-subheadercell table-page:next" style="cursor:pointer;text-align:right;">Next &gt; &gt;</td>
						</tr>
						<tr class="rich-table-subheader">
							<td class="rich-table-subheadercell" style="text-align:center;" colspan="4">
								<span id="t1filtercount"></span>
								&#160;of
								<span id="t1allcount"></span>
								&#160;rows
								match filter(s)
							</td>
						</tr>
					</tfoot>
				</table>
				<br />
				<br />

				<a name="Detailed results" />
				<h2>Detailed results :</h2>
				<br />
				<xsl:element name="select">
					<xsl:attribute name="name">listbox</xsl:attribute>
					<xsl:attribute name="onChange">location = this.options[this.selectedIndex].value;</xsl:attribute>
					<xsl:for-each select="systemSOP/sopClassList/sopClass">
						<xsl:sort select="SOPClassName" />
						<xsl:element name="option">
							<xsl:attribute name="value">
								#<xsl:value-of select="SOPClassName" />
								</xsl:attribute>
							<xsl:value-of select="SOPClassName" />
							<xsl:text> - </xsl:text>
							<xsl:value-of select="SOPClassUID" />
						</xsl:element>
					</xsl:for-each>
				</xsl:element>
				<br />
				<br />
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>

	<xsl:template match="validationDate">
		<p>
			<b>Validation Date :</b>
			<xsl:value-of select="." />
		</p>
	</xsl:template>
	<xsl:template match="systemName">
		<p>
			<b>System Name : </b>
			<xsl:value-of select="." />
		</p>
	</xsl:template>
	<xsl:template match="ip">
		<p>
			<b>ip : </b>
			<xsl:value-of select="." />
		</p>
	</xsl:template>
	<xsl:template match="port">
		<p>
			<b>port : </b>
			<xsl:value-of select="." />
		</p>
	</xsl:template>
	<xsl:template match="aeTitle">
		<p>
			<b>aeTitle : </b>
			<xsl:value-of select="." />
		</p>
	</xsl:template>



	<xsl:template match="sopClass">

		<xsl:element name="a">
			<xsl:attribute name="name">
				<xsl:value-of select="SOPClassName" />
			</xsl:attribute>
		</xsl:element>
		<ul>
			<p>
				<b>SOP Class UID :</b>
			</p>
			<ul>
				<xsl:apply-templates select="SOPClassUID" />
			</ul>
			<p>
				<b>SOP Class Name :</b>
			</p>
			<ul>
				<xsl:apply-templates select="SOPClassName" />
			</ul>
			<p>
				<b>SOP Class Supported :</b>
			</p>
			<ul>
				<xsl:apply-templates select="Supported" />
			</ul>
			<xsl:if test="count(SupportedTF) &gt; 0">
				<p>
					<b>Supported Transfer Syntax(es) :</b>
				</p>
				<ul>
					<xsl:apply-templates select="SupportedTF" />
				</ul>
			</xsl:if>
			<xsl:if test="count(NoSupportedTF) &gt; 0">
				<p>
					<b>Not Supported Transfer Syntax(es) :</b>
				</p>
				<ul>
					<xsl:apply-templates select="NoSupportedTF" />
				</ul>
			</xsl:if>
			<p>
				<b>SOP Class Required Level :</b>
			</p>
			<ul>
				<xsl:apply-templates select="Required" />
			</ul>
		</ul>

		<p>
			<xsl:element name="a">
				<xsl:attribute name="href">
				<xsl:text>#Top</xsl:text>
			</xsl:attribute>
				<xsl:text>Back to Top</xsl:text>
			</xsl:element>
		</p>

		<hr style="width:250px;" align="left" />
	</xsl:template>

	<xsl:template match="SOPClassUID">
		<li>
			<xsl:value-of select="." />
		</li>
	</xsl:template>
	<xsl:template match="SOPClassName">
		<li>
			<xsl:value-of select="." />
		</li>
	</xsl:template>
	<xsl:template match="SupportedTF">
		<li>
			<xsl:value-of select="." />
		</li>
	</xsl:template>
	<xsl:template match="NoSupportedTF">
		<li>
			<xsl:value-of select="." />
		</li>
	</xsl:template>
	<xsl:template match="Supported">
		<li>
			<xsl:value-of select="." />
		</li>
	</xsl:template>
	<xsl:template match="Required">
		<li>
			<xsl:value-of select="." />
		</li>
	</xsl:template>

</xsl:stylesheet>
