<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>

    <xd:doc>
        <xd:desc>
            <xd:p>Vocabulary file containing language dependant strings such as labels</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="vocFile" select="'/xsl/EVSClient/common/common_l10n.xml'"/>
    <xd:doc>
        <xd:desc>
            <xd:p>Cache language dependant strings</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="vocMessages" select="document($vocFile)"/>

    <xd:doc>
        <xd:desc>
            <xd:p>Default language for retrieval of language dependant strings such as labels, e.g. 'en-US'. This is the
                fallback language in case the string is not available in the actual language. See also <xd:ref
                        name="textLang" type="parameter">textLang</xd:ref>.
            </xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="textlangDefault" select="'$selectedLanguage'"/>


    <xsl:template name="insertHeader">
        <xsl:param name="type"/>

        <head>
            <title>$type Validation Report</title>
        </head>

        <script language="javascript" defer="true">
            <xsl:comment>
                <![CDATA[
         $(document).ready( computeStats());
        ]]>
            </xsl:comment>
        </script>

    </xsl:template>


    <xsl:template match="Error">
        <xsl:call-template name="Notification">
            <xsl:with-param name="classnames">gzl-notification-red</xsl:with-param>
            <xsl:with-param name="kind">error</xsl:with-param>
            <xsl:with-param name="prefix">E</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="Warning">
        <xsl:call-template name="Notification">
            <xsl:with-param name="classnames">gzl-notification-orange</xsl:with-param>
            <xsl:with-param name="kind">warning</xsl:with-param>
            <xsl:with-param name="prefix">W</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="Info">
        <xsl:call-template name="Notification">
            <xsl:with-param name="classnames">gzl-notification-blue</xsl:with-param>
            <xsl:with-param name="kind">info</xsl:with-param>
            <xsl:with-param name="prefix">I</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="Note">
        <xsl:call-template name="Notification">
            <xsl:with-param name="classnames">gzl-notification-green</xsl:with-param>
            <xsl:with-param name="kind">report</xsl:with-param>
            <xsl:with-param name="prefix">R</xsl:with-param>
        </xsl:call-template>
    </xsl:template>


    <xsl:template name="Notification">
        <xsl:param name="kind"/>
        <xsl:param name="classnames"/>
        <xsl:param name="prefix"/>
        <xsl:choose>
            <xsl:when test="($kind != 'error') and ($displayMax+1 = number(position()))">
                <xsl:element name="div">
                    <xsl:attribute name="class">
                        <xsl:text>gzl-alert gzl-alert-blue</xsl:text>
                    </xsl:attribute>
                    <xsl:text>Only the first </xsl:text>
                    <xsl:value-of select="$displayMax"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$kind"/>
                    <xsl:text>s are displayed, download the report to access the complete list</xsl:text>
                </xsl:element>
            </xsl:when>
            <xsl:when test="($kind = 'error') or ($displayMax &gt;= position())">
                <xsl:element name="dl">
                    <xsl:attribute name="data-value">
                        <xsl:value-of select="concat($kind, 'E')"/>
                    </xsl:attribute>
                    <xsl:attribute name="style">
                        <xsl:text>display:block;</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="class">
                        <xsl:text>dl-horizontal gzl-notification </xsl:text>
                        <xsl:value-of select="$classnames"/>
                    </xsl:attribute>
                    <xsl:if test="Test">
                        <dt>
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'Test'"/>
                            </xsl:call-template>
                        </dt>
                        <xsl:element name="dd">
                            <xsl:attribute name="data-value">
                                <xsl:value-of select="Test"/>
                            </xsl:attribute>
                            <xsl:choose>
                                <xsl:when test="$doTranslate = 'true'">
                                    <xsl:call-template name="getLocalizedString">
                                        <xsl:with-param name="key" select="Test"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="Test"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <span class="report-number">
                                <xsl:element name="a">
                                    <xsl:attribute name="id">
                                        <xsl:value-of select="translate($prefix,'REIW','reiw')"/>
                                        <xsl:value-of select="position()"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="$prefix"/> -
                                    <xsl:value-of select="position()"/>
                                </xsl:element>
                            </span>
                        </xsl:element>
                    </xsl:if>
                    <xsl:if test="Location">
                        <dt>
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'Location'"/>
                            </xsl:call-template>
                        </dt>
                        <dd>
                            <xsl:attribute name="data-value">
                                <xsl:value-of select="Location"/>
                            </xsl:attribute>
                            <xsl:value-of select="Location"/>
                            <xsl:if test="$viewdown = 'true'">
                                <xsl:element name="a">
                                    <xsl:attribute name="title">View in document</xsl:attribute>

                                    <xsl:attribute name="onclick">gotoNode(this, false)</xsl:attribute>
                                    <xsl:attribute name="id">
                                        <xsl:value-of select="generate-id()"/>
                                    </xsl:attribute>
                                    <span class="gzl-icon-eye"/>
                                </xsl:element>
                            </xsl:if>
                        </dd>
                    </xsl:if>
                    <xsl:if test="count(Value) &gt; 0">
                        <dt>
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'Value'"/>
                            </xsl:call-template>
                        </dt>
                        <dd>
                            <xsl:value-of select="Value"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="Description">

                        <dt>Description</dt>
                        <dd>
                            <xsl:choose>
                                <xsl:when test="Identifiant = 'print_description'">
                                    <xsl:value-of select="Description"/>
                                </xsl:when>
                                <xsl:when test="($doTranslate = 'true') and Identifiant">
                                    <xsl:call-template name="getLocalizedString">
                                        <xsl:with-param name="key" select="Identifiant"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="Description"/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:if test="Identifiant and ($constraintPath != '#')">[
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="$constraintPath"
                                        /><xsl:value-of select="Identifiant"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="target">_blank</xsl:attribute>
                                    Constraint...
                                </a>
                                ]
                            </xsl:if>
                            <xsl:if test="assertion and ($assertionManagerPath != '#')">[
                                <a>
                                    <xsl:attribute name="href"><xsl:value-of select="$assertionManagerPath"
                                    />rest/testAssertion/assertion?idScheme=<xsl:value-of
                                            select="assertion/@idScheme"/>&amp;assertionId=<xsl:value-of
                                            select="assertion/@assertionId"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="target">_blank</xsl:attribute>
                                    Assertion...
                                </a>
                                ]
                            </xsl:if>
                        </dd>
                    </xsl:if>
                    <xsl:if test="Type">
                        <dt>Type</dt>
                        <xsl:element name="dd">
                            <xsl:attribute name="data-value">
                                <xsl:value-of select="concat(Type, 'E')"/>
                            </xsl:attribute>
                            <xsl:value-of select="Type"/>
                        </xsl:element>
                    </xsl:if>
                    <xsl:if test="umlPackage">
                        <dt>Package</dt>
                        <xsl:element name="dd">
                            <xsl:attribute name="data-value">
                                <xsl:value-of select="concat(umlPackage, 'E')"/>
                            </xsl:attribute>
                            <xsl:value-of select="umlPackage"/>
                        </xsl:element>
                    </xsl:if>
                    <xsl:if test="HL7Table">
                        <dt>HL7Table</dt>
                        <dd>
                            <xsl:element name="a">
                                <xsl:attribute name="href">
                                    <xsl:text>/GazelleHL7Validator/viewResource.seam?oid=</xsl:text>
                                    <xsl:value-of
                                            select="HL7Table"/>
                                </xsl:attribute>
                                <xsl:attribute name="target">blank</xsl:attribute>
                                <xsl:value-of select="HL7Table"/>
                            </xsl:element>
                        </dd>
                    </xsl:if>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>


    <xsl:template name="viewnotification">
        <xsl:param name="kind"/>
        <xsl:param name="type"/>
        <xsl:param name="position"/>
        <xsl:param name="node"/>
        <xsl:param name="isSchematronValidation"/>
        <xsl:param name="displayMax">50</xsl:param>
        <xsl:param name="textLangLowerCase"/>

        <xsl:choose>
            <xsl:when test="$displayMax+1 = number(position())">
                <xsl:text>Only the first </xsl:text>
                <xsl:value-of select="$displayMax"/><xsl:text> reports are displayed</xsl:text>
            </xsl:when>
            <xsl:when test="$displayMax &gt;= position()">
                <xsl:element name="dl">
                    <xsl:attribute name="data-value">
                        <xsl:value-of select="concat($kind, 'E')"/>
                    </xsl:attribute>
                    <xsl:attribute name="style">
                        <xsl:text>display:block;</xsl:text>
                    </xsl:attribute>

                    <xsl:attribute name="class">
                        <xsl:value-of select="$kind"/>
                    </xsl:attribute>
                    <xsl:if test="count(Test) &gt; 0">
                        <dt>
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'Test'"/>
                            </xsl:call-template>
                        </dt>

                        <xsl:element name="dd">
                            <xsl:attribute name="data-value">
                                <xsl:value-of select="Test"/>
                            </xsl:attribute>
                            <xsl:value-of select="Test"/>
                            <span class="report-number">
                                <xsl:element name="a">
                                    <xsl:attribute name="id">
                                        <xsl:value-of select="translate($type,'REIW','reiw')"/>
                                        <xsl:value-of select="$position"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="$type"/> -
                                    <xsl:value-of select="$position"/>
                                </xsl:element>
                            </span>
                        </xsl:element>
                    </xsl:if>
                    <xsl:if test="count(Location) &gt; 0">
                        <dt>
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'Location'"/>
                            </xsl:call-template>
                        </dt>
                        <xsl:element name="dd">
                            <xsl:attribute name="data-value">
                                <xsl:value-of select="Location"/>
                            </xsl:attribute>
                            <xsl:value-of select="Location"/>
                            <xsl:if test="$viewdown = 'true'">
                                <xsl:element name="a">
                                    <xsl:attribute name="title">
                                        <xsl:call-template name="getLocalizedString">
                                            <xsl:with-param name="key" select="'View in document'"/>
                                        </xsl:call-template>
                                    </xsl:attribute>
                                    <xsl:attribute name="onclick">gotoNode(this,
                                        <xsl:value-of select="$isSchematronValidation"/>)
                                    </xsl:attribute>
                                    <xsl:attribute name="id">
                                        <xsl:value-of select="generate-id()"/>
                                    </xsl:attribute>
                                    <span class="gzl-icon-eye"></span>
                                </xsl:element>
                            </xsl:if>
                        </xsl:element>
                    </xsl:if>
                    <xsl:if test="count(Value) &gt; 0">
                        <dt>Value</dt>
                        <dd>
                            <xsl:value-of select="Value"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="count(Type) &gt; 0">
                        <dt>
                            Type
                        </dt>
                        <xsl:element name="dd">
                            <xsl:attribute name="data-value">
                                <xsl:value-of select="concat(Type, 'E')"/>
                            </xsl:attribute>
                            <xsl:value-of select="Type"/>
                        </xsl:element>
                    </xsl:if>
                    <xsl:if test="count(HL7Table) &gt; 0">
                        <dt>HL7 Table</dt>
                        <dd>
                            <xsl:element name="a">
                                <xsl:attribute name="href">
                                    <xsl:text>/GazelleHL7Validator/viewResource.seam?oid=</xsl:text>
                                    <xsl:value-of select="HL7Table"/>
                                </xsl:attribute>
                                <xsl:attribute name="target">blank</xsl:attribute>
                                <xsl:value-of select="HL7Table"/>
                            </xsl:element>
                        </dd>

                    </xsl:if>
                    <xsl:if test="umlPackage">
                        <dt>Package</dt>
                        <xsl:element name="dd">
                            <xsl:attribute name="data-value">
                                <xsl:value-of select="concat(umlPackage, 'E')"/>
                            </xsl:attribute>
                            <xsl:value-of select="umlPackage"/>
                        </xsl:element>
                    </xsl:if>
                    <dt>Description</dt>
                    <dd>
                        <xsl:value-of select="Description" disable-output-escaping="yes"/>
                        <xsl:if test="Identifiant and ($constraintPath != '#')">
                            [
                            <a>
                                <xsl:attribute name="href">
                                    <xsl:value-of select="$constraintPath"/><xsl:value-of select="Identifiant"/>.html
                                </xsl:attribute>
                                <xsl:attribute name="target">_blank</xsl:attribute>
                                Constraint...
                            </a>
                            ]
                        </xsl:if>
                        <xsl:if test="assertion and ($assertionManagerPath != '#')">
                            [
                            <a>
                                <xsl:attribute name="href"><xsl:value-of
                                        select="$assertionManagerPath"/>rest/testAssertion/assertion?idScheme=<xsl:value-of
                                        select="assertion/@idScheme"/>&amp;assertionId=<xsl:value-of
                                        select="assertion/@assertionId"/>
                                </xsl:attribute>
                                <xsl:attribute name="target">_blank</xsl:attribute>
                                Assertion...
                            </a>
                            ]
                        </xsl:if>
                    </dd>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise/>

        </xsl:choose>

    </xsl:template>

    <xsl:template name="viewXSDFailMessages">
        <xsl:param name="path2"/>
        <dl class="dl-horizontal">
            <dt>Summary of reports</dt>
            <dd>
                <xsl:call-template name="ewirProgressBar">
                    <xsl:with-param name="error" select="count($path2/XSDMessage[(Severity = 'error') or (Severity = 'fatal')])"/>
                    <xsl:with-param name="info" select="count($path2/XSDMessage[Severity = 'information'])"/>
                    <xsl:with-param name="success" select="0"/>
                    <xsl:with-param name="warning" select="count($path2/XSDMessage[Severity = 'warning'])"/>
                    <xsl:with-param name="unknown" select="0"/>
                </xsl:call-template>
            </dd>
        </dl>
        <xsl:for-each select="$path2/XSDMessage">
            <xsl:call-template name="XSDMessageDisplay">
                <xsl:with-param name="color">red</xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="XSDMessageDisplay">
        <xsl:param name="color"/>
        <div>
            <xsl:attribute name="class">
                <xsl:text>gzl-notification gzl-notification-</xsl:text>
                <xsl:value-of select="$color"/>
            </xsl:attribute>
            <dl class="dl-horizontal">
                <xsl:if test="lineNumber">
                    <dt>Location</dt>
                    <dd>
                        <xsl:element name="a">
                            <xsl:attribute name="href">
                                <xsl:text>#line_</xsl:text>
                                <xsl:value-of select="lineNumber"/>
                            </xsl:attribute>
                            <xsl:attribute name="onclick">
                                <xsl:text>updateColorationOfLines(</xsl:text>
                                <xsl:value-of select="lineNumber"/>
                                <xsl:text>,</xsl:text>
                                <xsl:value-of select="Severity"/>
                                <xsl:text>)</xsl:text>
                            </xsl:attribute>
                            <xsl:text>line </xsl:text>
                            <xsl:value-of select="lineNumber"/>
                            <xsl:text>, column </xsl:text>
                            <xsl:value-of select="columnNumber"/>
                        </xsl:element>
                    </dd>
                </xsl:if>

                <!-- This part could be removed when the ticket SCHVAL-167 is solved -->
                <xsl:if test="LineNumber">
                    <dt>Location</dt>
                    <dd>
                        <xsl:element name="a">
                            <xsl:attribute name="href">
                                <xsl:text>#line_</xsl:text>
                                <xsl:value-of select="LineNumber"/>
                            </xsl:attribute>
                            <xsl:attribute name="onclick">
                                <xsl:text>updateColorationOfLines(</xsl:text>
                                <xsl:value-of select="LineNumber"/>
                                <xsl:text>,</xsl:text>
                                <xsl:value-of select="Severity"/>
                                <xsl:text>)</xsl:text>
                            </xsl:attribute>
                            <xsl:text>line </xsl:text>
                            <xsl:value-of select="LineNumber"/>
                            <xsl:text>, column </xsl:text>
                            <xsl:value-of select="ColumnNumber"/>
                        </xsl:element>
                    </dd>
                </xsl:if>
                <!-- This part could be removed when the ticket SCHVAL-167 is solved -->

                <dt>Description</dt>
                <dd>
                    <xsl:value-of select="Message"/>
                </dd>
            </dl>
        </div>
    </xsl:template>

    <!-- general information -->
    <xsl:template name="viewGeneralInformation">
        <h2>Validation Report</h2>
        <div class="panel panel-default">
            <div class="panel-heading">General Information</div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <div>
                        <dt>Validation Date</dt>
                        <dd>
                            <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationDate"/>
                            -
                            <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTime"
                            />
                        </dd>
                    </div>
                    <div>
                        <dt>Validation Service</dt>
                        <dd>
                            <xsl:value-of
                                    select="detailedResult/ValidationResultsOverview/ValidationServiceName"/>
                            <xsl:if
                                    test="count(detailedResult/ValidationResultsOverview/ValidationServiceVersion) = 1">
                                (Version :<xsl:value-of
                                    select="detailedResult/ValidationResultsOverview/ValidationServiceVersion"/>)
                            </xsl:if>
                        </dd>
                    </div>

                    <xsl:if test="count(detailedResult/ValidationResultsOverview/Schematron) = 1">
                        <div>
                            <dt>
                                Schematron
                            </dt>
                            <dd>
                                <xsl:value-of select="detailedResult/ValidationResultsOverview/Schematron"/>
                            </dd>
                        </div>
                    </xsl:if>
                    <div>
                        <dt>
                            <b>Validation Test Status</b>
                        </dt>
                        <dd>
                            <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                <div class="PASSED">
                                    <xsl:value-of
                                            select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                </div>
                            </xsl:if>
                            <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                <div class="FAILED">
                                    <xsl:value-of
                                            select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                </div>
                            </xsl:if>
                            <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                                <div class="ABORTED">
                                    <xsl:value-of
                                            select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                </div>
                            </xsl:if>
                        </dd>
                    </div>
                </dl>
            </div>
        </div>
    </xsl:template>

    <!-- result overview -->

    <xsl:template name="displayResultOverview">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Result Overview</h3>
            </div>
            <div class="panel-body">
                <span>
                    <div>
                        <table border="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <b>
                                            <a href="#xml">Well-formedness</a>
                                        </b>
                                    </td>
                                    <td>
                                        <div>
                                            <xsl:call-template name="statusLabel">
                                                <xsl:with-param name="status"
                                                                select="detailedResult/DocumentWellFormed/Result"/>
                                            </xsl:call-template>

                                        </div>
                                    </td>
                                </tr>
                                <xsl:if test="count(detailedResult/DocumentValidXSD/Result) = 1">
                                    <tr>
                                        <td>
                                            <b>
                                                <a href="#schema-validation">Schema validation</a>
                                            </b>
                                        </td>
                                        <td>
                                            <div>
                                                <xsl:call-template name="statusLabel">
                                                    <xsl:with-param name="status"
                                                                    select="detailedResult/DocumentValidXSD/Result"/>
                                                </xsl:call-template>
                                            </div>
                                        </td>
                                    </tr>
                                </xsl:if>
                                <xsl:if test="count(detailedResult/DocumentValidCDA) = 1">
                                    <tr>
                                        <td>
                                            <b>
                                                <a href="#xsd">CDA</a>
                                            </b>
                                            (<a href="https://gazelle.ihe.net/xsd/CDA.xsd">CDA.xsd</a>)
                                        </td>
                                        <td>
                                            <xsl:call-template name="statusLabel">
                                                <xsl:with-param name="status"
                                                                select="detailedResult/DocumentValidXSD/Result"/>
                                            </xsl:call-template>
                                            <xsl:choose>
                                                <xsl:when
                                                        test="contains(detailedResult/DocumentValidCDA/Result, 'PASSED')">
                                                    <div class="PASSED">
                                                        PASSED
                                                    </div>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <div class="FAILED">
                                                        FAILED
                                                    </div>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </td>
                                    </tr>
                                </xsl:if>
                                <xsl:if test="count(detailedResult/DocumentValidEpsos) = 1">
                                    <tr>
                                        <td>
                                            <b>
                                                <a href="#xsd">epSOS</a>
                                            </b>
                                            (<a href="https://gazelle.ihe.net/xsd/CDA_extended.xsd">CDA_extended.xsd</a>)
                                        </td>
                                        <td>
                                            <xsl:choose>
                                                <xsl:when
                                                        test="contains(detailedResult/DocumentValidEpsos/Result, 'PASSED')">
                                                    <div class="PASSED">
                                                        PASSED
                                                    </div>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <div class="FAILED">
                                                        FAILED
                                                    </div>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </td>
                                    </tr>
                                </xsl:if>
                                <xsl:if test="count(detailedResult/DocumentValid/Result) = 1">
                                    <tr>
                                        <td>
                                            <b>
                                                <a href="#schema-validation">XSD</a>
                                            </b>
                                        </td>
                                        <td>
                                            <div>
                                                <xsl:call-template name="statusLabel">
                                                    <xsl:with-param name="status"
                                                                    select="detailedResult/DocumentValid/Result"/>
                                                </xsl:call-template>

                                            </div>
                                        </td>
                                    </tr>
                                </xsl:if>
                                <tr>
                                    <td>
                                        <b>
                                            <a href="#mbv">Model-based Validation</a>
                                        </b>
                                    </td>
                                    <td>
                                        <div>
                                            <xsl:call-template name="statusLabel">
                                                <xsl:with-param name="status"
                                                                select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                            </xsl:call-template>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </span>
            </div>
        </div>
    </xsl:template>

    <!-- XML well formed report -->
    <xsl:template name="displayXMLWellFormReport">
        <xsl:param name="isUrl"/>

        <div class="panel panel-default">

            <div class="panel-heading ">
                <h3 class="panel-title">
                    <xsl:choose>
                        <xsl:when test="$isUrl=true()">
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'URL Validation Report'"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'XML Validation Report'"/>
                            </xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:call-template name="statusLabel">
                        <xsl:with-param name="status" select="detailedResult/DocumentWellFormed/Result"/>
                    </xsl:call-template>
                </h3>
            </div>
            <div class="panel-body ">
                <xsl:choose>
                    <xsl:when test="$isUrl=true()">
                        <i>List of issues with URL provided.
                        </i>
                    </xsl:when>
                    <xsl:otherwise>
                        <d>The document you have validated is supposed to be a JSON document. The validator has checked if it is well-formed, results of this validation are gathered in this part.</d>
                    </xsl:otherwise>
                </xsl:choose>


                <span>
                    <div style="padding-top: 3; padding-bottom: 3;">
                        <xsl:choose>
                            <xsl:when test="detailedResult/DocumentWellFormed/Result = 'PASSED'">
                                <div class="gzl-alert gzl-alert-green">
                                    <xsl:choose>
                                        <xsl:when test="$isUrl=true()">
                                            No issue with the parsing of the URL
                                        </xsl:when>
                                        <xsl:otherwise>
                                            The JSON document is well-formed
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </div>

                            </xsl:when>
                            <xsl:otherwise>
                                <div class="gzl-alert gzl-alert-red">
                                    <xsl:choose>
                                        <xsl:when test="$isUrl=true()">
                                        </xsl:when>
                                        <xsl:otherwise>
                                            The JSON document is not well-formed, for the following reasons:
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </div>
                                <xsl:if test="count(/detailedResult/DocumentWellFormed/XSDMessage) &gt; 0">
                                    <xsl:call-template name="viewXSDFailMessages">
                                        <xsl:with-param name="path2" select="/detailedResult/DocumentWellFormed"/>
                                    </xsl:call-template>
                                </xsl:if>
                            </xsl:otherwise>
                        </xsl:choose>
                    </div>
                </span>
            </div>
        </div>
    </xsl:template>

    <!-- XML XSD validation report -->

    <xsl:template name="displayXSDValidationReport">
        <xsl:if test="count(detailedResult/DocumentValidXSD) = 1">
            <xsl:call-template name="displayXSDValidationReportGeneric">
                <xsl:with-param name="path" select="detailedResult/DocumentValidXSD"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="displayXSDValidationReportGeneric">
        <xsl:param name="path"/>

        <xsl:if test="count($path) = 1">

            <div class="panel panel-default">
                <div class="panel-heading ">

                    <h3 class="panel-title">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key" select="'Structure parsing result'"/>
                        </xsl:call-template>

                        <xsl:call-template name="statusLabel">
                            <xsl:with-param name="status" select="$path/Result"/>
                        </xsl:call-template>
                    </h3>
                </div>

                <div class="panel-body">
                    <i>Your JSON document has been parsed against a structuring object that represent JSON Web Token, here is the detail of the validation outcome.</i>
                    <xsl:if test="count($path) = 1">
                        <span>
                            <div style="padding-top: 3; padding-bottom: 3;">
                                <xsl:choose>
                                    <xsl:when test="($path/Result='PASSED') or ($path/nbOfErrors=0 and $path/nbOfWarnings=0 and count($path/Result)=0)">
                                        <div class="gzl-alert gzl-alert-green">
                                            <xsl:call-template name="getLocalizedString">
                                                <xsl:with-param name="key" select="'The JSON document is valid regarding the structure'"/>
                                            </xsl:call-template>
                                        </div>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <div class="gzl-alert gzl-alert-red">
                                            <xsl:call-template name="getLocalizedString">
                                                <xsl:with-param name="key" select="'The JSON document is not valid regarding the structure because of the following reasons :'"/>
                                            </xsl:call-template>
                                        </div>
                                        <xsl:if test="count($path/*[contains(local-name(),'message')]) &gt; 0">
                                            <ul>
                                                <xsl:for-each select="$path/*[contains(local-name(),'message')]">
                                                    <li>
                                                        <xsl:value-of select="."/>
                                                    </li>
                                                </xsl:for-each>
                                            </ul>
                                        </xsl:if>
                                        <xsl:if test="count($path/XSDMessage) &gt; 0">
                                            <xsl:call-template name="viewXSDFailMessages">
                                                <xsl:with-param name="path2" select="$path"/>
                                            </xsl:call-template>
                                        </xsl:if>

                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                        </span>
                    </xsl:if>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="displayMBValidatorResults">
        <xsl:param name="showProgressBar">true</xsl:param>
        <xsl:param name="showNavBar">true</xsl:param>
        <xsl:if test="count(//Note) + count(//Info) + count(//Error) + count(//Report) &gt; 0">


            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <a name="mbv">
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key">Model-Based validator results</xsl:with-param>
                            </xsl:call-template>
                        </a>
                        <xsl:call-template name="statusLabel">
                            <xsl:with-param name="status" select="detailedResult/MDAValidation/Result"/>
                        </xsl:call-template>
                    </h3>
                </div>
                <div class="panel-body">
                    <xsl:if test="$showProgressBar='true'">
                        <dl class="dl-horizontal">
                            <dt>
                                <xsl:call-template name="getLocalizedString">
                                    <xsl:with-param name="key">Summary of checks</xsl:with-param>
                                </xsl:call-template>
                            </dt>
                            <dd>
                                <xsl:call-template name="ewirProgressBar">
                                    <xsl:with-param name="success" select="count(//Note)"/>
                                    <xsl:with-param name="info" select="count(//Info)"/>
                                    <xsl:with-param name="error" select="count(//Error)"/>
                                    <xsl:with-param name="warning" select="count(//Warning)"/>
                                    <xsl:with-param name="unknown" select="count(//Unknown)"/>
                                </xsl:call-template>
                            </dd>
                        </dl>
                    </xsl:if>
                    <div class="gzl-container">
                        <div class="row">
                            <xsl:if test="$showNavBar='true'">
                                <div class="col-md-2 col-sd-2" role="complementary">
                                    <xsl:call-template name="navbar"/>
                                </div>
                            </xsl:if>

                            <div class="col-md-10 col-sd-10" role="main">
                                <div>
                                    <xsl:apply-templates select="//Error"/>
                                    <xsl:apply-templates select="//Warning"/>
                                    <xsl:apply-templates select="//Info"/>
                                    <xsl:apply-templates select="//Note"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="getLocalizedString">
        <xsl:param name="pre" select="''"/>
        <xsl:param name="key"/>
        <xsl:param name="post" select="''"/>
        <xsl:param name="post2" select="''"/>
        <xsl:choose>
            <xsl:when test="$vocMessages/*/*/key[@value=$key]/value[@lang=$selectedLanguage]">
                <xsl:value-of
                        select="concat($pre,$vocMessages//key[@value=$key]/value[@lang=$selectedLanguage]/text(),$post)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($pre,$key,$post2)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="statusLabel">
        <xsl:param name="status"/>
        <xsl:element name="span">
            <xsl:attribute name="class">
                <xsl:text>gzl-label gzl-label-</xsl:text>
                <xsl:choose>
                    <xsl:when test="$status = 'PASSED'">
                        <xsl:text>green</xsl:text>
                    </xsl:when>
                    <xsl:when test="$status = 'FAILED'">
                        <xsl:text>red</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>blue</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="style">
                <xsl:text>margin-left:10px</xsl:text>
            </xsl:attribute>
            <xsl:call-template name="getLocalizedString">
                <xsl:with-param name="key">
                    <xsl:value-of select="$status"/>
                </xsl:with-param>
            </xsl:call-template>

        </xsl:element>
    </xsl:template>

    <xsl:template name="navbar">
        <nav class="gzl-sidebar">
            <ul class="nav">
                <li>
                    <xsl:call-template name="severityFilter"/>
                </li>
                <xsl:if test="count(//Type) &gt; 0">
                    <li>
                        <xsl:call-template name="typeFilter"/>
                    </li>
                </xsl:if>
                <xsl:if test="count(//umlPackage) &gt; 0">
                    <li>
                        <xsl:call-template name="packageFilter"/>
                    </li>
                </xsl:if>

                <!-- <xsl:if test="count(//Test) &gt; 0">
                    <li>
                        <xsl:call-template name="testFilter"/>
                    </li>
                </xsl:if>
        -->
                <li>
                    <input type="hidden" data-value="none" id="selectedLocation"/>
                    <a onclick="resetFilters()">
                        <b>
                            <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key">Reset filters</xsl:with-param>
                            </xsl:call-template>
                        </b>
                    </a>
                </li>

            </ul>
        </nav>
    </xsl:template>

    <xsl:template name="severityFilter">
        <a onclick="changeCriteriaStatus('severity')" data-value="severity" class="mainCriteria">
            <span id="severity" class="fa fa-check-square-o"></span>
            <xsl:text>Severity</xsl:text>
        </a>
        <ul id="severityEntries" class="nav" style="display:block;">
            <li>
                <a data-value="error" class="nostate" onclick="showHide(this)">
                    <span class="statlabel">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key">Errors</xsl:with-param>
                        </xsl:call-template>
                    </span>
                    <span class="statvalue" data-value="error"/>
                </a>
            </li>
            <li>
                <a data-value="warning" class="nostate" onclick="showHide(this)">
                    <span class="statlabel">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key">Warnings</xsl:with-param>
                        </xsl:call-template>
                    </span>
                    <span class="statvalue" data-value="warning"/>
                </a>
            </li>
            <li>
                <a data-value="info" class="nostate" onclick="showHide(this)">
                    <span class="statlabel">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key">Infos</xsl:with-param>
                        </xsl:call-template>
                    </span>
                    <span class="statvalue" data-value="info"/>
                </a>
            </li>
            <li>
                <a data-value="unknown" class="nostate" onclick="showHide(this)">
                    <span class="statlabel">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key">Unknown</xsl:with-param>
                        </xsl:call-template>
                    </span>
                    <span class="statvalue" data-value="unknown"/>
                </a>
            </li>
            <li>
                <a data-value="report" class="nostate" onclick="showHide(this)">
                    <span class="statlabel">
                        <xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key">Reports</xsl:with-param>
                        </xsl:call-template>
                    </span>
                    <span class="statvalue" data-value="report"/>
                </a>
            </li>
        </ul>
    </xsl:template>

    <xsl:template name="typeFilter">
        <a onclick="changeCriteriaStatus('type')" data-value="type" class="mainCriteria">
            <span id="type" class="fa fa-check-square-o"/>
            <xsl:text>Type</xsl:text>
        </a>
        <xsl:if test="count(//Type[not(. = following::Type)]) &gt; 0">
            <ul id="typeEntries" style="display:block;" class="nav">
                <xsl:for-each select="//Type[not(. = following::Type)]">
                    <li>
                        <xsl:call-template name="criteriaEntry">
                            <xsl:with-param name="entry">
                                <xsl:value-of select="current()"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </li>
                </xsl:for-each>
            </ul>
        </xsl:if>
    </xsl:template>

    <xsl:template name="packageFilter">
        <a onclick="changeCriteriaStatus('package')" data-value="package" class="mainCriteria">
            <span id="package" class="fa fa-check-square-o"/>
            <xsl:text>Package</xsl:text>
        </a>
        <xsl:if test="count(//umlPackage[not(. = following::umlPackage)]) &gt; 0">
            <ul id="packageEntries" style="display:block;" class="nav">
                <xsl:for-each select="//umlPackage[not(. = following::umlPackage)]">
                    <li>
                        <xsl:call-template name="criteriaEntry">
                            <xsl:with-param name="entry">
                                <xsl:value-of select="current()"/>
                            </xsl:with-param>
                        </xsl:call-template>
                    </li>
                </xsl:for-each>
            </ul>
        </xsl:if>
    </xsl:template>

    <xsl:template name="criteriaEntry">
        <xsl:param name="entry"/>
        <xsl:element name="a">
            <xsl:attribute name="data-value">
                <xsl:value-of select="text()"/>
            </xsl:attribute>
            <xsl:attribute name="onclick">showHide(this)</xsl:attribute>
            <xsl:attribute name="class">nostate</xsl:attribute>
            <span class="statlabel">
                <xsl:value-of select="text()"/>
            </span>
            <xsl:element name="span">
                <xsl:attribute name="class">statvalue</xsl:attribute>
                <xsl:attribute name="data-value">
                    <xsl:value-of select="text()"/>
                </xsl:attribute>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xsl:template name="testFilter">
        <a onclick="changeCriteriaStatus('test')" data-value="test" class="mainCriteria">
            <span id="test" class="fa fa-check-square-o"/>
            <xsl:text>Test name</xsl:text>
        </a>
        <select name="testFilter" onchange="updatePage()" class="form-control" id="testEntries"
                style="display:block;">
            <option id="0">All tests</option>
            <xsl:for-each select="//Test[not(. = preceding::Test)]">
                <option>
                    <xsl:attribute name="id">
                        <xsl:value-of select="text()"/>
                    </xsl:attribute>
                    <xsl:value-of select="text()"/>
                </option>
            </xsl:for-each>
        </select>
    </xsl:template>


    <xsl:template name="ewirProgressBar">
        <xsl:param name="success"/>
        <xsl:param name="info"/>
        <xsl:param name="warning"/>
        <xsl:param name="error"/>
        <xsl:param name="unknown"/>
        <xsl:variable name="total" select="$success + $info + $warning + $error +$unknown"/>
        <xsl:if test="$total &gt; 0">
            <xsl:variable name="percentageInfo">
                <xsl:call-template name="choosePercentage">
                    <xsl:with-param name="checks" select="$info"/>
                    <xsl:with-param name="total" select="$total"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="percentageWarning">
                <xsl:call-template name="choosePercentage">
                    <xsl:with-param name="checks" select="$warning"/>
                    <xsl:with-param name="total" select="$total"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="percentageError">
                <xsl:call-template name="choosePercentage">
                    <xsl:with-param name="checks" select="$error"/>
                    <xsl:with-param name="total" select="$total"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="percentageUnknown">
                <xsl:call-template name="choosePercentage">
                    <xsl:with-param name="checks" select="$unknown"/>
                    <xsl:with-param name="total" select="$total"/>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="percentageSuccess"
                          select="100 - ($percentageError + $percentageInfo + $percentageWarning + $percentageUnknown)"/>

            <div class="progress" style="width:90%">
                <xsl:attribute name="title">
                    <xsl:text>Error:</xsl:text>
                    <xsl:value-of select="$error"/>
                    <xsl:text>, Warning:</xsl:text>
                    <xsl:value-of select="$warning"/>
                    <xsl:text>, Info:</xsl:text>
                    <xsl:value-of select="$info"/>
                    <xsl:text>, Successful:</xsl:text>
                    <xsl:value-of select="$success"/>
                    <xsl:text>, Unknown:</xsl:text>
                    <xsl:value-of select="$unknown"/>
                    <xsl:text>, Total:</xsl:text>
                    <xsl:value-of select="$total"/>
                </xsl:attribute>
                <xsl:if test="$error &gt; 0">
                    <div class="progress-bar progress-bar-danger">
                        <xsl:attribute name="style">
                            <xsl:text>width:</xsl:text>
                            <xsl:value-of select="$percentageError"/>
                            <xsl:text>%</xsl:text>
                        </xsl:attribute>
                        <span>
                            <xsl:value-of select="$error"/>
                        </span>
                    </div>
                </xsl:if>
                <xsl:if test="$warning &gt; 0">
                    <div class="progress-bar progress-bar-warning">
                        <xsl:attribute name="style">
                            <xsl:text>width:</xsl:text>
                            <xsl:value-of select="$percentageWarning"/>
                            <xsl:text>%</xsl:text>
                        </xsl:attribute>
                        <span>
                            <xsl:value-of select="$warning"/>
                        </span>
                    </div>
                </xsl:if>
                <xsl:if test="$info &gt; 0">
                    <div class="progress-bar progress-bar-info">
                        <xsl:attribute name="style">
                            <xsl:text>width:</xsl:text>
                            <xsl:value-of select="$percentageInfo"/>
                            <xsl:text>%</xsl:text>
                        </xsl:attribute>
                        <span>
                            <xsl:value-of select="$info"/>
                        </span>
                    </div>
                </xsl:if>
                <xsl:if test="$success &gt; 0">
                    <div class="progress-bar progress-bar-success">
                        <xsl:attribute name="style">
                            <xsl:text>width:</xsl:text>
                            <xsl:value-of select="$percentageSuccess"/>
                            <xsl:text>%</xsl:text>
                        </xsl:attribute>
                        <span>
                            <xsl:value-of select="$success"/>
                        </span>
                    </div>
                </xsl:if>
                <xsl:if test="$unknown &gt; 0">
                    <div class="progress-bar progress-bar-success">
                        <xsl:attribute name="style">
                            <xsl:text>width:</xsl:text>
                            <xsl:value-of select="$percentageUnknown"/>
                            <xsl:text>%</xsl:text>
                        </xsl:attribute>
                        <span>
                            <xsl:value-of select="$unknown"/>
                        </span>
                    </div>
                </xsl:if>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="choosePercentage">
        <xsl:param name="checks"/>
        <xsl:param name="total"/>
        <xsl:choose>
            <xsl:when test="($checks * 100 div $total) &gt; 3">
                <xsl:value-of select="$checks * 100 div $total"/>
            </xsl:when>
            <xsl:when test="$checks = 0">
                <xsl:value-of select="'0'"/>
            </xsl:when>
            <xsl:when test="$checks &lt; 10">
                <xsl:value-of select="'1'"/>
            </xsl:when>
            <xsl:when test="$checks &lt; 100">
                <xsl:value-of select="'2'"/>
            </xsl:when>
            <xsl:when test="$checks &lt; 1000">
                <xsl:value-of select="'3'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$checks * 100 div $total"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


</xsl:stylesheet>
