<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                exclude-result-prefixes="xd"
                version="1.0">

    <xsl:output method="html" media-type="text/html" encoding="UTF-8" indent="yes"/>
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b>
                Jun 15, 2021
            </xd:p>
            <xd:p>
                <xd:b>Author:</xd:b>
                ycadoret
            </xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>

    <xsl:param name="ShowGeneralInformation">true</xsl:param>
    <xsl:param name="ShowResultOverview">true</xsl:param>


    <xsl:template match="validationReport">
        <html>
            <head>
                <title>DGCG QR Code validation report</title>
            </head>
            <body>

                <br/>


                <!-- validation details -->
                <xsl:variable name="nbOfErrors" select="count(reports/constraints[testResult='FAILED'])"/>
                <xsl:variable name="nbOfInfos" select="count(reports/constraints[testResult='PASSED'])"/>


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Validation counters</h3>
                    </div>
                    <div class="panel-body">
                        <ul>
                            <li>
                                <xsl:choose>
                                    <xsl:when test="$nbOfErrors &gt; 0">
                                        <a href="#Errors_p">
                                            <xsl:value-of select="$nbOfErrors"/>
                                            error(s)
                                        </a>
                                    </xsl:when>
                                    <xsl:otherwise>No error</xsl:otherwise>
                                </xsl:choose>
                            </li>
                            <li>
                                <xsl:choose>
                                    <xsl:when test="$nbOfInfos &gt; 0">
                                        <a href="#Reports_p">
                                            <xsl:value-of select="$nbOfInfos"/>
                                            report(s)
                                        </a>
                                    </xsl:when>
                                    <xsl:otherwise>No report</xsl:otherwise>
                                </xsl:choose>
                            </li>
                        </ul>
                    </div>
                </div>

                <br/>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Validation details</h3>
                    </div>
                    <div class="panel-body">

                        <xsl:for-each select="reports">
                            <xsl:apply-templates select="."></xsl:apply-templates>
                        </xsl:for-each>
                    </div>
                </div>
            </body>
        </html>

    </xsl:template>
    <xsl:template match="reports">
        <span style="font-weight:bold;"><xsl:value-of select="name"/></span>


        <div class="panel panel-default">
            <div class="panel-body ">
                <xsl:for-each select="constraints">
                    <xsl:apply-templates select="."></xsl:apply-templates>
                </xsl:for-each>
            </div>
        </div>

    </xsl:template>



    <xsl:template match="constraints">
        <dl>
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="contains(testResult, 'FAILED')">
                        <xsl:value-of
                                select="'dl-horizontal gzl-notification gzl-notification-red'"/>
                    </xsl:when>
                    <xsl:when test="contains(testResult, 'PASSED')">
                        <xsl:value-of
                                select="'dl-horizontal gzl-notification gzl-notification-green'"/>
                    </xsl:when>
                </xsl:choose>

            </xsl:attribute>
            <dt>Constraint</dt>
            <dd>
                <p>
                    <xsl:value-of select="description"/>
                </p>
            </dd>
            <dt>Priority</dt>
            <dd>
                    <p>
                        <xsl:value-of select="priority"/>
                    </p>
            </dd>
            <dt>Result</dt>
            <dd>
                    <p>
                        <xsl:value-of select="testResult"/>
                    </p>
            </dd>
            <dt>Severity</dt>
            <dd>
                    <p>
                        <xsl:value-of select="severity"/>
                    </p>
            </dd>
        </dl>

    </xsl:template>

</xsl:stylesheet>
