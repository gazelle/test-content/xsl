<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output indent="yes"/>
    <xsl:param name="jsonText"/>
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:template name="init">
        <xsl:apply-templates select="json-to-xml($jsonText)"/>
    </xsl:template>


    <xsl:template match="map" xpath-default-namespace="http://www.w3.org/2005/xpath-functions">
        <dl>
            <xsl:if test="*[@key='ver']">
                <dt>Schema version</dt>
                <dd>
                    <xsl:value-of select="*[@key='ver']"/>
                </dd>
            </xsl:if>
            <dt>Names</dt>
            <ul>
                <dl>
                    <dt>Surname</dt>
                    <dd>
                        <xsl:value-of select="map/*[@key='fn']"/>
                    </dd>
                    <dt>Standardised surname</dt>
                    <dd>
                        <xsl:value-of select="map/*[@key='fnt']"/>
                    </dd>
                    <dt>Forename</dt>
                    <dd>
                        <xsl:value-of select="map/*[@key='gn']"/>
                    </dd>
                    <dt>Standardised forename</dt>
                    <dd>
                        <xsl:value-of select="map/*[@key='gnt']"/>
                    </dd>
                </dl>
            </ul>
            <xsl:if test="*[@key='dob']">
            <dt>Date of birth</dt>
            <dd>
                <xsl:value-of select="*[@key='dob']"/>
            </dd>
            </xsl:if>
            <dt>Document type</dt>
            <xsl:choose>
                <xsl:when test="array[@key='v']">
                    <dd>Vaccination Group</dd>
                </xsl:when>
                <xsl:when test="array[@key='t']">
                    <dd>Test Group</dd>
                </xsl:when>
                <xsl:when test="array[@key='r']">
                    <dd>Recovery Group</dd>
                </xsl:when>
            </xsl:choose>
            <xsl:if test="//*[@key='tg']">
            <dt>Disease or agent targeted</dt>
            <dd>
                <xsl:value-of select="//*[@key='tg']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='vp']">
            <dt>Vaccine or prophylaxis</dt>
            <dd>
                <xsl:value-of select="//*[@key='vp']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='mp']">
            <dt>Vaccine medicinal product</dt>
            <dd>
                <xsl:value-of select="//*[@key='mp']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='ma']">
            <dt>Marketing Authorization Holder - if no MAH present, then manufacturer</dt>
            <dd>
                <xsl:value-of select="//*[@key='ma']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='dn']">
            <dt>Dose Number</dt>
            <dd>
                <xsl:value-of select="//*[@key='dn']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='sd']">
            <dt>Total Series of Doses</dt>
            <dd>
                <xsl:value-of select="//*[@key='sd']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='dt']">
            <dt>Date of Vaccination</dt>
            <dd>
                <xsl:value-of select="//*[@key='dt']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='co']">
            <dt>Country of Vaccination/Test</dt>
            <dd>
                <xsl:value-of select="//*[@key='co']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='is']">
            <dt>Certificate Issuer</dt>
            <dd>
                <xsl:value-of select="//*[@key='is']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='ci']">
            <dt>Unique Certificate Identifier</dt>
            <dd>
                <xsl:value-of select="//*[@key='ci']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='tt']">
            <dt>Type of Test</dt>
            <dd>
                <xsl:value-of select="//*[@key='tt']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='nm']">
            <dt>NAA Test Name</dt>
            <dd>
                <xsl:value-of select="//*[@key='nm']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='sc']">
            <dt>Date/Time of Sample Collection</dt>
            <dd>
                <xsl:value-of select="//*[@key='sc']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='tr']">
            <dt>Test Result</dt>
            <dd>
                <xsl:value-of select="//*[@key='tr']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='tc']">
            <dt>Test Centre</dt>
            <dd>
                <xsl:value-of select="//*[@key='tc']"/>
            </dd>
            </xsl:if>
            <xsl:if test="//*[@key='fr']">
                <dt>D date of first positive NAA test result</dt>
                <dd>
                    <xsl:value-of select="//*[@key='fr']"/>
                </dd>
            </xsl:if>
            <xsl:if test="//*[@key='df']">
                <dt>Certificate Valid From</dt>
                <dd>
                    <xsl:value-of select="//*[@key='df']"/>
                </dd>
            </xsl:if>
            <xsl:if test="//*[@key='du']">
                <dt>Certificate Valid Until</dt>
                <dd>
                    <xsl:value-of select="//*[@key='du']"/>
                </dd>
            </xsl:if>
        </dl>
    </xsl:template>


</xsl:stylesheet>
