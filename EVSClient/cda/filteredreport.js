function computeStats() {
    var statElements = document.getElementsByClassName('statvalue');
    var index;
    for (index = 0; index < statElements.length; index++) {
        var datavalue = statElements[index].getAttribute('data-value') + 'E';
        var displayed = getNumberOfElements(datavalue, true);
        var total = getNumberOfElements(datavalue, false);
        /*statElements[index].textContent = displayed + "/" + total;*/
        statElements[index].textContent = displayed;
    }

   /* var items = document.getElementById('testEntries').options;
    var index;
    for (index = 1; index < items.length; index++) {
        displayed = getNumberOfElements(items[index].id, true);
        total = getNumberOfElements(items[index].id, false);
        var label = items[index].id + ' (' + displayed + '/' + total + ')';
        var label = items[index].id + ' (' + displayed + ')';
        items[index].textContent = label;
    }*/
}

function filterOnSelectedTemplate(location, selectedNode) {
    // select all tree leaves to reset their font-weight to inherit
    var ids = jq162("div[class='rf-trn']");
    if (ids != null){
        var index;
        for (index = 0; index < ids.length; index++){
            ids[index].style.fontWeight = 'inherit';
        }
    }
    selectedNode.style.fontWeight = 'bold';
    var item = document.getElementById('selectedLocation');
    item.setAttribute('data-value', location);
    updatePage();
}

function createFilterOnTemplate() {
    var item = document.getElementById('selectedLocation');
    var location = item.getAttribute('data-value');
    if (location != 'none') {
        return "dl:has(dd[data-value*='" + location + "'])";
    }
    else {
        return null;
    }
}

function updatePage() {
    var filterSeverity = createFilter('severityEntries', 'dl', '');
    var filterType = createFilter('typeEntries', 'dl:has(dd', ')');
    var filterOrigine = createFilter('packageEntries', 'dl:has(dd', ')');
    var filterLocation = createFilterOnTemplate();
    // createFilter on test
    /*var testElement = document.getElementById('testEntries');
    var selectedTest = testElement.options[testElement.selectedIndex].id;
    var filterTest = "";
    if (selectedTest != '0') {
        filterTest = "dl:has(dd[data-value='" + selectedTest + "'])";
    }*/
    var selected;
    var isFilterSet = false;
    if (filterSeverity != "") {
        isFilterSet = true;
        selected = jq162(filterSeverity);
    }
    if (filterType != "") {
        isFilterSet = true;
        if (selected != null) {
            selected = selected.filter(filterType)
        } else {
            selected = jq162(filterType);
        }
    }
    if (filterOrigine != "") {
        isFilterSet = true;
        if (selected != null) {
            selected = selected.filter(filterOrigine);
        } else {
            selected = jq162(filterOrigine);
        }
    }
    /*if (filterTest != "") {
        isFilterSet = true;
        if (selected != null) {
            selected = selected.filter(filterTest);
        } else {
            selected = jq162(filterTest);
        }
    }*/
    if (filterLocation != null){
        isFilterSet = true;
        if (selected != null){
            selected = selected.filter(filterLocation);
        } else {
            selected = jq162(filterLocation);
        }
    }
    if (isFilterSet) {
        hideAllNotifications();
        changeElementState(selected, 'block');
    } else {
        showAllNotifications();
    }
    computeStats();
}
