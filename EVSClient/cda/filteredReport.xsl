<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html" omit-xml-declaration="yes"/>
    <xsl:param name="viewdown">true</xsl:param>
    <xsl:param name="constraintPath">/CDAGenerator/docum/constraints/constraintDescriptor.seam?identifier=</xsl:param>
    <xsl:param name="assertionManagerPath">/AssertionManagerGui/</xsl:param>
    <xsl:param name="ShowGeneralInformation">true</xsl:param>
    <xsl:param name="ShowResultOverview">false</xsl:param>
    <xsl:param name="displayMax">200</xsl:param>
    <xsl:param name="maxError">999</xsl:param>
    <xsl:param name="maxReport">200</xsl:param>
    <xsl:param name="maxWarning">999</xsl:param>
    <xsl:param name="maxInfo">20</xsl:param>
    <xsl:param name="selectedLanguage">fr</xsl:param>
    <xsl:param name="standalone">true</xsl:param>
    <xsl:param name="doTranslate">false</xsl:param>
    <xsl:variable name="report_index" select="0" />

    <xsl:template match="/">

        <link href="/xsl/EVSClient/common/filteredreport.css" rel="stylesheet"/>

        <!-- general information -->
        <xsl:if test="$ShowGeneralInformation = 'false'">
            <xsl:call-template name="viewGeneralInformation"/>
        </xsl:if>

        <!-- overview

<xsl:call-template name="displayResultOverview"/>
        -->
        <!-- XML well formed report -->

        <xsl:call-template name="displayXMLWellFormReport"/>

        <!-- XML XSD validation report -->

        <xsl:call-template name="displayXSDValidationReport"/>


        <xsl:call-template name="displayMBValidatorResults">
            <xsl:with-param name="showProgressBar">true</xsl:with-param>
            <xsl:with-param name="showNavBar">true</xsl:with-param>

        </xsl:call-template>
        <script type="text/javascript" defer="defer" language="javascript">
          //<![CDATA[
         jq162(document).ready( computeStats());
         jq162(document).ready( updatePage());
        //]]>
      </script>
    </xsl:template>



    <xsl:include href="/xsl/EVSClient/common/common.xsl"/>


</xsl:stylesheet>
