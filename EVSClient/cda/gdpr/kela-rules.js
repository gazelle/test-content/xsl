/**
 * Check Kela PIDs
 * @author Youn Cadoret
 *
 */


function ruleControl(newXml) {

    /*<------------ var declaration ------------>*/
    var message, popUpMessage, title, tagResult, docValidation = "true", root, extension, found ='',i = 0, pathRoot, code;
    var listValidExtension = [];
    var listInvalidExtension = [];
    var localValidate = document.getElementById('scriptResult');

    //<== Regex rules declaration ==>
    var extensionRegex = new RegExp('[0-3][0-9][0,1][0-9][0-9]{2}[-A][0-8][0-9]{2}[A-Z0-9]');
    // add another Regex ...
    //</== Regex rules declaration ==>

    //<== xPath declaration ==>
    var pathAllAtt = "//*/@*";
    var pathAll = "//text()";
    // add another xPath ...
    //</== xPath declaration ==>
    /*<------------ /var declaration ------------>*/



    /*<------------ check rules ------------>*/
    //<== Check PIDs in attribute for each line ==>
    if (newXml.evaluate) {
        var nodesPids = newXml.evaluate(pathAllAtt, newXml, null, XPathResult.ANY_TYPE, null);
        var resultPids = nodesPids.iterateNext();
        while (resultPids) {
            var code = resultPids.value;
            if (code.match(extensionRegex)) {
                docValidation = "false";
                listInvalidExtension.push(code);
                found += "\u2022 This PIDs '" + code + "' is invalid \n";
                i = i + 1;
            }
            resultPids = nodesPids.iterateNext();
        }
    }
    //</==Check PIDs in attribute for each line ==>

    //<== Check PIDs node for each line ==>
    if (newXml.evaluate) {
        var nodes = newXml.evaluate(pathAll, newXml, null, XPathResult.ANY_TYPE, null);
        var resultPids = nodes.iterateNext();
        while (resultPids) {
            var code = resultPids.textContent;
            if (code != null) {
                if (code.match(extensionRegex)) {
                    docValidation = "false";
                    listInvalidExtension.push(code);
                    found += "\u2022 This PIDs '" + code + "' is invalid \n";
                    i = i + 1;
                }
            }
            resultPids = nodes.iterateNext();
        }
    }
    //</==Check PIDs node for each line ==>


    //<== Message Result ==>
    if (i == 0 && docValidation == "true") {
        $(".rf-fu-btn-upl").click();
        message = "Document Successfully Verified";
        localValidate.style.color = "green";
        localValidate.style.border = "5px solid rgba(214, 233, 198)";
        localValidate.style.background = "rgba(214, 233, 198)";
        localValidate.innerText = "Document successfully verified for potential patient data exposure";
        //popUpMessage = message+"\n\n"+ found;

    } else {
        $(".rf-fu-btn-upl").hide();
        tagResult = "DANGER : ";
        title = tagResult + "Potential Patient Data Exposure";
        message = "It seems that the uploaded document contains real patient data, therefore the document";
        message += " will not be further processed and will not be send to the validation tool. Please review ";
        message += "the document and make sure that it does not contain real patient data.";
        localValidate.style.color = "red";
        localValidate.style.border = "5px solid rgba(235, 204, 209)";
        localValidate.style.background = "rgba(235, 204, 209)";
        localValidate.innerText = 'Document seems to expose patient data cannot be processed';
        popUpMessage = title + "\n\n" + message + "\n\n" + found;
        window.alert(popUpMessage);
    }
    //</== Message Result ==>

}
