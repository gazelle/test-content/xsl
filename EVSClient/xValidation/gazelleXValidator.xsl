<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	 version="1.0">
	<xsl:template match="/">
		<html>
			<head>
				<title>Gazelle X Validator</title>
				<xsl:element name="link">
					<xsl:attribute name="rel">
						<xsl:text>stylesheet</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="type">
						<xsl:text>text/css</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="media">
						<xsl:text>screen</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="href">
						<xsl:text>http://gazelle.ihe.net/GazelleXValidatorRuleEditor/resources/stylesheet/gazelle-theme.css</xsl:text>
					</xsl:attribute>
				</xsl:element>
			</head>
			<body>
				<xsl:apply-templates select="GazelleCrossValidator"/>
				<h2>Validator inputs</h2>
				<xsl:apply-templates select="GazelleCrossValidator/ValidatedObject"/>
				<h2>Configuration</h2>
				<xsl:apply-templates select="GazelleCrossValidator/Configuration"/>
				<h2>Rules</h2>
				<xsl:apply-templates select="GazelleCrossValidator/Rule"/>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="GazelleCrossValidator">
		<h1>
			<xsl:value-of select="@affinityDomain"/>
			<xsl:text> - </xsl:text>
			<xsl:value-of select="@name"/>
			<xsl:text> (</xsl:text>
			<xsl:value-of select="@version"/>
			<xsl:text>)</xsl:text>
		</h1>
		<p>
			<xsl:value-of select="Description"/>
		</p>
		<div class="gzl-notification gzl-notification-blue">
			<h4>
				<xsl:text>Details</xsl:text>
			</h4>
			<div class="row">
				<dl class="dl-horizontal col-md-4">
					<dt>Name</dt>
					<dd>
						<xsl:value-of select="@name"/>
					</dd>
					<dt>Affinity domain</dt>
					<dd>
						<xsl:value-of select="@affinityDomain"/>
					</dd>
					<dt>Version</dt>
					<dd>
						<xsl:value-of select="@version"/>
					</dd>
					<dt>Status</dt>
					<dd>
						<xsl:value-of select="@status"/>
					</dd>
					<dt>Last modifier</dt>
					<dd>
						<xsl:value-of select="@lastModifier"/>
					</dd>
					<dt>Last modified on</dt>
					<dd>
						<xsl:value-of select="@lastModified"/>
					</dd>
					<xsl:if test="count(ValidatorParent) &gt; 0">
						<dt>Inherit rules from</dt>
						<dd>
							<ul>
								<xsl:apply-templates select="ValidatorParent"/>
							</ul>
						</dd>
					</xsl:if>
				</dl>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="ValidatedObject">
		<div class="gzl-notification gzl-notification-blue">
			<h4>
				<xsl:value-of select="ReferencedObject/@keyword"/>
			</h4>
			<p>
				<xsl:value-of select="ReferencedObject/@description"/>
			</p>
			<dl class="dl-horizontal">
				<dt>Type</dt>
				<dd>
					<xsl:value-of select="ReferencedObject/@objectType"/>
				</dd>
				<dt>Cardinality</dt>
				<dd>
					<xsl:choose>
						<xsl:when test="@minQuantity">
							<xsl:value-of select="@minQuantity"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>1</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>..</xsl:text>
					<xsl:choose>
						<xsl:when test="@maxQuantity and @maxQuantity = '-1'">
							<xsl:text>*</xsl:text>
						</xsl:when>
						<xsl:when test="@maxQuantity">
							<xsl:value-of select="@maxQuantity"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>1</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</dd>
				<xsl:if test="xsdLocation">
					<dt>XSD</dt>
					<dd>
						<xsl:value-of select="ReferencedObject/xsdLocation"/>
					</dd>
				</xsl:if>
			</dl>
		</div>
	</xsl:template>
	<xsl:template match="Configuration">
		<div class="gzl-notification gzl-notification-blue">
			<xsl:if test="count(NamespaceDeclaration) &gt; 0">
				<h4>Namespaces</h4>
				<ul>
					<xsl:apply-templates select="NamespaceDeclaration"/>
				</ul>
			</xsl:if>
			<xsl:if test="ValueSetProviderUrl">
				<h4>Value Sets</h4>
				<dl class="dl-horizontal">
					<dt>Value Set Repository</dt>
					<dd>
						<xsl:value-of select="ValueSetProviderUrl/text()"/>
					</dd>
				</dl>
			</xsl:if>
			<xsl:if test="DICOMLibrary">
				<h4>DICOM</h4>
				<dl class="dl-horizontal">
					<dt>DICOM to XML library</dt>
					<dd>
						<xsl:value-of select="DICOMLibrary/text()"/>
					</dd>
				</dl>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template match="NamespaceDeclaration">
		<li>
			<xsl:text>xmlns:</xsl:text>
			<xsl:value-of select="@prefix"/>
			<xsl:text>="</xsl:text>
			<xsl:value-of select="@uri"/>
			<xsl:text>"</xsl:text>
		</li>
	</xsl:template>
	<xsl:template match="Rule">
		<div>
			<xsl:attribute name="class">
				<xsl:if test="not(@status = 'ACTIVE')">
					<xsl:text>highlight </xsl:text>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="@level = 'ERROR'">
						<xsl:text>gzl-notification gzl-notification-red</xsl:text>
					</xsl:when>
					<xsl:when test="@level = 'WARNING'">
						<xsl:text>gzl-notification gzl-notification-orange</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>gzl-notification gzl-notification-blue</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<h4>
				<xsl:value-of select="@keyword"/>
				<xsl:text> (</xsl:text>
				<xsl:value-of select="@version"/>
				<xsl:text>)</xsl:text>
			</h4>
			<p>
				<xsl:value-of select="@description"/>
			</p>
			<div class="row">
				<dl class="dl-horizontal col-md-4">
					<dt>Severity</dt>
					<dd>
						<xsl:value-of select="@level"/>
					</dd>
					<dt>Status</dt>
					<dd>
						<xsl:value-of select="@status"/>
					</dd>
					<xsl:if test="count(Assertion) &gt; 0">
						<dt>Assertions</dt>
						<dd>
							<xsl:apply-templates select="Assertion">
								<xsl:with-param name="nbOfEntries">
									<xsl:value-of select="count(Assertion)"/>
								</xsl:with-param>
							</xsl:apply-templates>
						</dd>
					</xsl:if>
				</dl>
				<dl class="dl-horizontal col-md-4">
					<xsl:if test="count(AppliesTo) &gt; 0">
						<dt>Applies to</dt>
						<dd>
							<xsl:apply-templates select="AppliesTo">
								<xsl:with-param name="nbOfEntries">
									<xsl:value-of select="count(AppliesTo)"/>
								</xsl:with-param>
							</xsl:apply-templates>
						</dd>
					</xsl:if>
					<dt>Modified by</dt>
					<dd>
						<xsl:value-of select="@lastModifier"/>
					</dd>
					<dt>Modified on</dt>
					<dd>
						<xsl:value-of select="@lastModified"/>
					</dd>
				</dl>
			</div>
			<dl class="dl-horizontal">
				<dt>Expression</dt>
				<dd>
					<code>
						<xsl:apply-templates select="Expression/child::node()"/>
					</code>
				</dd>
			</dl>
		</div>
	</xsl:template>
	<xsl:template match="Assertion">
		<xsl:param name="nbOfEntries"/>
		<xsl:value-of select="@idScheme"/>
		<xsl:text>:</xsl:text>
		<xsl:value-of select="@assertionId"/>
		<xsl:if test="position() &lt; $nbOfEntries">
			<xsl:text>, </xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="AppliesTo">
		<xsl:param name="nbOfEntries"/>
		<xsl:value-of select="text()"/>
		<xsl:if test="position() &lt; $nbOfEntries">
			<xsl:text>, </xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Not">
		<b>
			<xsl:text>not (</xsl:text>
		</b>
		<xsl:apply-templates select="child::node()"/>
		<b>
			<xsl:text>) </xsl:text>
		</b>
	</xsl:template>
	<xsl:template match="Present">
		<xsl:for-each select="Locator">
			<xsl:call-template name="displayLocator"/>
		</xsl:for-each>
		<b>
			<xsl:text> is present</xsl:text>
		</b>
	</xsl:template>
	<xsl:template match="InValueSet">
		<xsl:for-each select="Locator">
			<xsl:call-template name="displayLocator"/>
		</xsl:for-each>
		<b>
			<xsl:text> exists in value set </xsl:text>
		</b>
		<xsl:value-of select="@valueSetId"/>
		<xsl:if test="@lang">
			<xsl:text>&amp;lang=</xsl:text>
			<xsl:value-of select="@lang"/>
		</xsl:if>
	</xsl:template>
	<xsl:template match="True">
		<b>
			<xsl:text>(</xsl:text>
		</b>
		<xsl:for-each select="XPath">
			<xsl:call-template name="displayLocator"/>
		</xsl:for-each>
		<b>
			<xsl:text>) == True</xsl:text>
		</b>
	</xsl:template>
	<xsl:template match="BasicOperation">
		<xsl:if
			test="@basicOperator = 'sizeEqual' or @basicOperator = 'sizeLessOrEqual' or @basicOperator = 'sizeGreaterOrEqual'">
			<b>
				<xsl:text>count(</xsl:text>
			</b>
		</xsl:if>
		<xsl:for-each select="Locator1">
			<xsl:call-template name="displayLocator"/>
		</xsl:for-each>
		<xsl:if
			test="@basicOperator = 'sizeEqual' or @basicOperator = 'sizeLessOrEqual' or @basicOperator = 'sizeGreaterOrEqual'">
			<b>
				<xsl:text>)</xsl:text>
			</b>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="@basicOperator = 'sizeEqual'">
				<b>
					<xsl:text> = count(</xsl:text>
				</b>
			</xsl:when>
			<xsl:when test="@basicOperator = 'sizeLessOrEqual'">
				<b>
					<xsl:text> &lt;= count(</xsl:text>
				</b>
			</xsl:when>
			<xsl:when test="@basicOperator = 'sizeGreaterOrEqual'">
				<b>
					<xsl:text> &gt;= count(</xsl:text>
				</b>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
				<b>
					<xsl:value-of select="@basicOperator"/>
				</b>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:for-each select="Locator2">
			<xsl:call-template name="displayLocator"/>
		</xsl:for-each>
		<xsl:if
			test="@basicOperator = 'sizeEqual' or @basicOperator = 'sizeLessOrEqual' or @basicOperator = 'sizeGreaterOrEqual'">
			<b>
				<xsl:text>)</xsl:text>
			</b>
		</xsl:if>
	</xsl:template>
	<xsl:template match="DateComparison">
        <xsl:text>OK</xsl:text>
		<xsl:for-each select="Locator1">
			<xsl:call-template name="displayLocator"/>
		</xsl:for-each>
		<xsl:for-each select="DateFormats1">
            <xsl:call-template name="dateFormats"/>
		</xsl:for-each>
		<xsl:choose>
			<xsl:when test="@dateOperator = 'equal'">
				<b>
					<xsl:text> = </xsl:text>
				</b>
			</xsl:when>
			<xsl:when test="@dateOperator = 'before'">
				<b>
					<xsl:text> &lt; (</xsl:text>
				</b>
			</xsl:when>
			<xsl:when test="@dateOperator = 'after'">
				<b>
					<xsl:text> &gt; </xsl:text>
				</b>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text>
				<b>
					<xsl:value-of select="@basicOperator"/>
				</b>
				<xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:for-each select="Locator2">
			<xsl:call-template name="displayLocator"/>
		</xsl:for-each>
        <xsl:for-each select="DateFormats2">
            <xsl:call-template name="dateFormats"/>
        </xsl:for-each>
	</xsl:template>
	<xsl:template match="LogicalOperation">
		<xsl:variable name="operator">
			<xsl:value-of select="@logicalOperator"/>
		</xsl:variable>
		<xsl:variable name="nbOfDirectChildren">
			<xsl:value-of select="count(./*)"/>
		</xsl:variable>
		<xsl:for-each select="./*">
			<b>
				<xsl:text> (</xsl:text>
			</b>
			<xsl:apply-templates select="."/>
			<b>
				<xsl:text>) </xsl:text>
			</b>
			<xsl:if test="position() &lt; $nbOfDirectChildren">
				<xsl:element name="br"/>
				<b>
					<xsl:value-of select="$operator"/>
				</b>
				<xsl:element name="br"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="ForEach">
		<b>
			<xsl:text>For each </xsl:text>
		</b>
		<xsl:value-of select="@var"/>
		<b>
			<xsl:text> in </xsl:text>
		</b>
		<xsl:for-each select="./*">
			<xsl:choose>
				<xsl:when test="local-name() = 'Locator'">
					<xsl:call-template name="displayLocator"/>
				</xsl:when>
				<xsl:otherwise>
					<b>
						<xsl:text> { </xsl:text>
					</b>
					<xsl:element name="br"/>
					<xsl:apply-templates select="current()"/>
					<xsl:element name="br"/>
					<b>
						<xsl:text> }</xsl:text>
					</b>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="displayLocator">
		<xsl:value-of select="@appliesOn"/>
		<xsl:text>::</xsl:text>
		<xsl:value-of select="@path"/>
	</xsl:template>
	<xsl:template match="ValidatorParent">
		<li>
			<xsl:value-of select="@name"/>
			<xsl:text> - </xsl:text>
			<xsl:value-of select="@affinityDomain"/>
		</li>
	</xsl:template>
	<xsl:template name="dateFormats">
		<b>
			<xsl:text>Allowed Date formats :</xsl:text>
		</b>
		<xsl:for-each select="/DateFormatType">
			<xsl:value-of select="/@value"/>
			<xsl:element name="br"/>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
