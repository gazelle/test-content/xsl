<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xsl:param name="viewdown">false</xsl:param>
    <xsl:param name="constraintPath">#</xsl:param>
    <xsl:param name="assertionManagerPath">/AssertionManagerGui/</xsl:param>
    <xsl:param name="ShowGeneralInformation">true</xsl:param>
    <xsl:param name="ShowResultOverview">false</xsl:param>
    <xsl:param name="displayMax">100</xsl:param>
    <xsl:param name="maxError">999</xsl:param>
    <xsl:param name="maxReport">200</xsl:param>
    <xsl:param name="maxWarning">999</xsl:param>
    <xsl:param name="maxInfo">20</xsl:param>
    <xsl:param name="selectedLanguage">fr</xsl:param>
    <xsl:param name="standalone">true</xsl:param>
    <xsl:param name="doTranslate">false</xsl:param>
    <xsl:include href="/xsl/EVSClient/common/common.xsl"/>

    <xsl:template match="/">
        <html>
            <xsl:call-template name="insertHeader">
                <xsl:with-param name="type">XD*</xsl:with-param>
            </xsl:call-template>
            <head>
                <title>XD* Validation Report</title>
                <link href="https://gazelle.ihe.net/xsl/EVSClient/resultStyle.css" rel="stylesheet" type="text/css" media="screen"/>
            </head>
            <body>


                <!-- general information -->
                <xsl:if test="$ShowGeneralInformation = 'true'">
                    <xsl:call-template name="viewGeneralInformation"/>
                </xsl:if>
                <!-- overview -->

                <!-- overview

                <xsl:call-template name="displayResultOverview"/>
                  -->
                <!-- XML well formed report -->

                <xsl:call-template name="displayXMLWellFormReport"/>

                <!-- XML XSD validation report -->

                <xsl:call-template name="displayXSDValidationReport"/>

                <xsl:call-template name="displayMBValidatorResults">
                    <xsl:with-param name="showNavBar">true</xsl:with-param>
                    <xsl:with-param name="showProgressBar">true</xsl:with-param>
                </xsl:call-template>

                <xsl:call-template name="displayNISTValidationReport"/>


                <script type="text/javascript" defer="defer" language="javascript">
                    //<![CDATA[
         jq162(document).ready( computeStats());
        //]]>
                </script>
            </body>
        </html>
    </xsl:template>





</xsl:stylesheet>
