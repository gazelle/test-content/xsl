<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html" media-type="text/html"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jul 19, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Anne-Gaëlle BERGE, IHE Europe</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="ShowGeneralInformation">true</xsl:param>
    <xsl:param name="ShowResultOverview">false</xsl:param>

    <xsl:variable name="ShowGeneralInformationLocal">
        <xsl:choose>
            <xsl:when test="normalize-space($ShowGeneralInformation)">
                <xsl:value-of select="$ShowGeneralInformation"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="true()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:template match="/">
        <html>
            <head>
                <link href="https://gazelle.ihe.net/xsl/EVSClient/resultStyle.css" rel="stylesheet" type="text/css" media="screen"/>
            </head>
            <h2>External Validation Report</h2>

            <!-- general information -->
            <xsl:if test="ShowGeneralInformationLocal = 'true'">
                <div class="panel panel-default">
                    <div class="panel-heading">General Informations</div>
                    <div class="panel-body">
                        <dl class="dl-horizontal">
                            <div>
                                <dt>Validation Date</dt>
                                <dd>
                                    <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationDate"/> -
                                    <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTime"
                                    /></dd>
                            </div>
                            <div>
                                <dt>Validation Service</dt>
                                <dd>
                                    <xsl:value-of
                                            select="detailedResult/ValidationResultsOverview/ValidationServiceName"/> (
                                    <xsl:value-of
                                            select="detailedResult/ValidationResultsOverview/ValidationServiceVersion"/> )
                                    <xsl:value-of
                                            select="detailedResult/ValidationResultsOverview/ValidationMethod" />
                                </dd>
                            </div>

                            <div>
                                <dt>Certificate Revocation</dt>
                                <dd>
                                    <xsl:choose>
                                        <xsl:when test="contains(detailedResult/ValidationResultsOverview/CheckRevocation, 'true')">
                                            <xsl:text>Checked</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:text>Not checked</xsl:text>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </dd>
                            </div>

                            <xsl:if test="count(detailedResult/ValidationResultsOverview/Oid) = 1">
                                <div>
                                    <dt>Message OID</dt>
                                    <dd>
                                        <xsl:value-of select="SummaryResults/ValidationResultsOverview/Oid"/>
                                    </dd>
                                </div>

                            </xsl:if>
                            <xsl:if test="count(detailedResult/ValidationResultsOverview/MessageOID) = 1">
                                <div>
                                    <dt>Message OID</dt>
                                    <dd>
                                        <xsl:value-of select="SummaryResults/ValidationResultsOverview/MessageOID"/>
                                    </dd>
                                </div>

                            </xsl:if>

                            <xsl:if test="count(detailedResult/ValidationResultsOverview/ProfileOID) = 1">
                                <div>
                                    <dt>Profile OID</dt>
                                    <dd>
                                        <xsl:value-of select="SummaryResults/ValidationResultsOverview/ProfileOID"/>
                                        <xsl:if test="SummaryResults/ValidationResultsOverview/ProfileRevision">
                                            <xsl:text> (Revision: </xsl:text>
                                            <xsl:value-of
                                                    select="SummaryResults/ValidationResultsOverview/ProfileRevision"/>
                                            <xsl:text>)</xsl:text>
                                        </xsl:if>
                                    </dd>
                                </div>

                            </xsl:if>



                            <div>
                                <dt>Validation Test Status</dt>
                                <dd>
                                    <xsl:if
                                            test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                        <div class="PASSED">
                                            <xsl:value-of
                                                    select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                        </div>
                                    </xsl:if>
                                    <xsl:if
                                            test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                        <div class="FAILED">
                                            <xsl:value-of
                                                    select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                        </div>
                                    </xsl:if>
                                    <xsl:if
                                            test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                                        <div class="ABORTED">
                                            <xsl:value-of
                                                    select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                        </div>
                                    </xsl:if>
                                    <xsl:if
                                            test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'INVALID REQUEST')">
                                        <div class="ABORTED">
                                            <xsl:value-of
                                                    select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                        </div>
                                    </xsl:if>
                                </dd>
                            </div>

                        </dl>

                    </div>
                </div>

            </xsl:if>
            <br/>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Validation counters</h3></div>
                <div class="panel-body">
                    <ul>
                        <li>
                            <xsl:choose>
                                <xsl:when test="count(detailedResult/CertificatesValidation/Error) &gt; 0">
                                    <a href="#Errors_p">
                                        <xsl:value-of select="count(detailedResult/CertificatesValidation/Error)"/>
                                        error(s) </a>
                                </xsl:when>
                                <xsl:otherwise> No error </xsl:otherwise>
                            </xsl:choose>
                        </li>
                        <li>
                            <xsl:choose>
                                <xsl:when test="count(detailedResult/CertificatesValidation/Warning) &gt; 0">

                                    <a href="#Warnings_p">
                                        <xsl:value-of select="count(detailedResult/CertificatesValidation/Warning)"/>
                                        warning(s) </a>
                                </xsl:when>
                                <xsl:otherwise> No warning </xsl:otherwise>
                            </xsl:choose>
                        </li>
                        <li>
                            <xsl:choose>
                                <xsl:when test="count(detailedResult/CertificatesValidation/Info) &gt; 0">
                                    <a href="#Reports_p">
                                        <xsl:value-of select="count(detailedResult/CertificatesValidation/Info)"/>
                                        report(s) </a>
                                </xsl:when>
                                <xsl:otherwise> No report </xsl:otherwise>
                            </xsl:choose>
                        </li>
                    </ul>
                </div>
            </div>
            <br/>
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Validation details</h3></div>
                <div class="panel-body">


                        <xsl:if test="count(detailedResult/CertificatesValidation/Error) &gt; 0">

                            <span style="font-weight:bold;">Errors </span>

                            <div id="Errors_p">

                                <div class="panel panel-default">
                                    <div class="panel-body ">
                                        <xsl:for-each
                                                select="detailedResult/CertificatesValidation/Error">
                                            <xsl:call-template name="viewnotification">
                                                <xsl:with-param name="kind">dl-horizontal gzl-notification
                                                    gzl-notification-red</xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:for-each>
                                    </div>
                                </div>
                            </div>

                        </xsl:if>
                    <xsl:if test="count(detailedResult/CertificatesValidation/Warning) &gt; 0">
                        <span style="font-weight:bold;">Warnings </span>


                        <div id="Warnings_p">
                            <div class="panel panel-default">
                                <div class="panel-body ">
                                    <xsl:for-each
                                            select="detailedResult/CertificatesValidation/Warning">
                                        <xsl:call-template name="viewnotification">
                                            <xsl:with-param name="kind">dl-horizontal gzl-notification
                                                gzl-notification-orange</xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:for-each>
                                </div>
                            </div>
                        </div>
                    </xsl:if>

                    <xsl:if test="count(detailedResult/CertificatesValidation/Report) &gt; 0">
                        <span style="font-weight:bold;">Reports </span>


                        <div id="Reports_p">
                            <div class="panel panel-default">
                                <div class="panel-body ">
                                    <xsl:for-each
                                            select="detailedResult/CertificatesValidation/Report">
                                        <xsl:call-template name="viewnotification">
                                            <xsl:with-param name="kind">dl-horizontal gzl-notification
                                                gzl-notification-green</xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:for-each>
                                </div>
                            </div>
                        </div>
                    </xsl:if>


                </div>
            </div>
            <br/>


        </html>
    </xsl:template>


    <xsl:template name="viewnotification">
        <xsl:param name="kind"/>

        <dl>
            <xsl:attribute name="class">
                <xsl:value-of select="$kind"/>
            </xsl:attribute>
            <xsl:if test="count(Test) &gt; 0">
                <dt>Test</dt>
                <dd>
                    <xsl:value-of select="Test"/>
                </dd>

            </xsl:if>
            <xsl:if test="count(Location) &gt; 0">
                <dt>Location</dt>
                <dd>
                    <xsl:value-of select="Location"/>
                </dd>

            </xsl:if>
            <xsl:if test="count(Value) &gt; 0">
                <dt>Value</dt>
                <dd>
                    <xsl:value-of select="Value"/>
                </dd>

            </xsl:if>
            <dt>Description</dt>
            <dd>
                <xsl:value-of select="Description"/>

            </dd>

        </dl>

    </xsl:template>

</xsl:stylesheet>
