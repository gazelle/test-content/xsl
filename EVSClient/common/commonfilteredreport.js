function createFilter(mainCriteriaId, filterPrefix, filterSuffix){
    var filter = "";
    var criteria = document.getElementById(mainCriteriaId);
    if (criteria != null && criteria.style.display == 'block') {
        var children = criteria.childNodes;
        var entIndex;
        for (entIndex = 0; entIndex < children.length; entIndex++) {
            if (children[entIndex].tagName == 'LI') {
                var liChildren = children[entIndex].childNodes;
                var liIndex;
                for (liIndex = 0; liIndex < liChildren.length; liIndex++) {
                    if (liChildren[liIndex].tagName == 'A') {
                        var aElem = liChildren[liIndex];
                        if (aElem.className == 'critSelected') {
                            if (filter.length > 0) {
                                filter = filter + ",";
                            }
                            filter = filter + filterPrefix + "[data-value='" + aElem.getAttribute('data-value') + "E" + "']" + filterSuffix;
                        }
                    }
                }
            }
        }

    }
    return filter;
}
function getNumberOfElements(datavalue, countOnlyIfDisplayed){
    var items = selectElementByDataValue(datavalue);
    if (items == null){
        return 0;
    } else if (countOnlyIfDisplayed){
        var quantity = 0;
        var index;
        for (index = 0; index < items.length; index++){
            if (items[index].tagName == 'DL' && items[index].style.display == 'block'){
                quantity++;
            }
            else if (items[index].parentElement.style.display == 'block'){
                quantity++;
            }
        }
        return quantity;
    } else {
        return items.length;
    }
}

function changeCriteriaStatus(id){
    var icon = document.getElementById(id);
    var iconclass = icon.className;
// uncheck/check the box
    if (iconclass == 'fa fa-check-square-o'){
        icon.className = 'fa fa-square-o';
    } else {
        icon.className = 'fa fa-check-square-o';
    }
    var listid = id + 'Entries';
    var entries = document.getElementById(listid);
    if (entries.style.display == 'block'){
        entries.style.display = 'none';
    } else {
        entries.style.display = 'block';
    }
    updatePage();
}

function showHide(item){
    var status = item.className;
    if (status == 'nostate'){
        // other criteria are critUnselected, update their class name
        var items = document.getElementsByClassName('nostate');
        var index;
        for (index = 0; index < items.length; index++){
            items[index].className = 'critUnselected';
        }
        item.className = 'critSelected';
    } else if (status == 'critSelected') {
        item.className = 'critUnselected';
    } else if (status == 'critUnselected'){
        item.className = 'critSelected';
    }
    updatePage();
}

function hideAllElementsMatching(descriminator){
    var items = selectElementByDataValue(descriminator);
    changeElementState(items, 'none');
}

function showAllElementsMatching(descriminator){
    var items = selectElementByDataValue(descriminator);
    changeElementState(items, 'block');
}

function selectElementByDataValue(descriminator){
    var selector = "[data-value = '" + descriminator +"']";
    return document.querySelectorAll(selector);
}

function changeElementState(items, displayMode){
    if (items != null && items.length > 0){
        var index;
        for (index = 0; index < items.length ; index++){
            var current = items[index];
            if (current.tagName == 'DL'){
                current.style.display = displayMode;
            } else {
                current.parentElement.style.display = displayMode;
            }
        }
    }
}

function changeElementStateWithLimit(items, displayMode, n){
    if (items != null && items.length > 0){
        document.getElementById('displayWarningMax').style.display = 'none';
        var index;
        for (index = 0; index < items.length && index < n ; index++){
            var current = items[index];
            if (current.tagName == 'DL'){
                current.style.display = displayMode;
            } else {
                current.parentElement.style.display = displayMode;
            }
        }
        if (items.length > n){
            document.getElementById('displayWarningMax').style.display = 'block';
        }
    }
}
function hideAllNotifications(){
    var items = document.getElementsByClassName('gzl-notification');
    changeElementState(items, 'none');
}

function showAllNotifications(){
    var items = document.getElementsByClassName('gzl-notification');
    changeElementStateWithLimit(selected, 'block', 5);}

function resetFilters(){
    location.reload();
}

function computeStatsOld(){
    var statElements =  document.getElementsByClassName('statvalue');
    var index;
    for (index = 0; index < statElements.length; index++){
        var datavalue = statElements[index].getAttribute('data-value') + 'E';
        var displayed = getNumberOfElements(datavalue, true);
        var total = getNumberOfElements(datavalue, false);
        statElements[index].textContent = displayed + "/" + total;
    }
}
function computeStats() {
    var statElements = document.getElementsByClassName('statvalue');
    var index;
    for (index = 0; index < statElements.length; index++) {
        var datavalue = statElements[index].getAttribute('data-value') + 'E';
        var displayed = getNumberOfElements(datavalue, true);
        var total = getNumberOfElements(datavalue, false);
        statElements[index].textContent = displayed + "/" + total;
      //  statElements[index].textContent = displayed;
    }

    var testEntries = document.getElementById('testEntries');
    if (testEntries != null) {
        var items = testEntries.options;
        var index;
        for (index = 1; index < items.length; index++) {
            displayed = getNumberOfElements(items[index].id, true);
            total = getNumberOfElements(items[index].id, false);
            /*var label = items[index].id + ' (' + displayed + '/' + total + ')';*/
            var label = items[index].id + ' (' + displayed + ')';
            items[index].textContent = label;
        }
    }
}

function createFilterOnTemplate() {
    var item = document.getElementById('selectedLocation');
    if (item != null)
        var location = item.getAttribute('data-value');
    if (location != 'none') {
        return "dl:has(dd[data-value*='" + location + "'])";
    }
    else {
        return null;
    }
}

function updatePage() {
    var filterSeverity = "";
    var filterType = "";
    var filterOrigine = "";
    var filterLocation = "" ;
    var filterTest = "" ;
    var testElement = "";
    if (document.getElementById('severity')) {
        filterSeverity = createFilter('severityEntries', 'dl', '');
    }
    if (document.getElementById('type')) {
        filterType = createFilter('typeEntries', 'dl:has(dd', ')');
    }
    if (document.getElementById('origine')) {
        filterOrigine = createFilter('packageEntries', 'dl:has(dd', ')');
    }
    if (document.getElementById('template')) {
        filterLocation = createFilterOnTemplate();
    }
    if (document.getElementById('test')) {
        testElement = document.getElementById('testEntries');

        if(testElement != null) {
            var selectedTest = testElement.options[testElement.selectedIndex].id;
        }

        if (selectedTest != '0') {
            filterTest = "dl:has(dd[data-value='" + selectedTest + "'])";
        }
    }

    var selected;
    var isFilterSet = false;
    if (filterSeverity != "") {
        isFilterSet = true;
        selected = jq162(filterSeverity);
    }
    if (filterType != "") {
        isFilterSet = true;
        if (selected != null) {
            selected = selected.filter(filterType)
        } else {
            selected = jq162(filterType);
        }
    }
    if (filterOrigine != "") {
        isFilterSet = true;
        if (selected != null) {
            selected = selected.filter(filterOrigine);
        } else {
            selected = jq162(filterOrigine);
        }
    }
    if (filterTest != "") {
        isFilterSet = true;
        if (selected != null) {
            selected = selected.filter(filterTest);
        } else {
            selected = jq162(filterTest);
        }
    }
    if (filterLocation != ""){
        isFilterSet = true;
        if (selected != null){
            selected = selected.filter(filterLocation);
        } else {
            selected = jq162(filterLocation);
        }
    }
    if (isFilterSet) {
        hideAllNotifications();
        changeElementStateWithLimit(selected, 'block', 5);
    } else {
        showAllNotifications();
    }
    computeStats();
}
