function computeStatsHlv2(){
  var statElements =  document.getElementsByClassName('statvalue');
  var index;
  for (index = 0; index < statElements.length; index++){
    var datavalue = statElements[index].getAttribute('data-value') + 'E';
    var displayed = getNumberOfElements(datavalue, true);
    var total = getNumberOfElements(datavalue, false);
    statElements[index].textContent = displayed + "/" + total;
  }
}


function updatePageHl7v2(){
  var filterSeverity = createFilter('severityEntries', 'dl', '');
  var filterType = createFilter('typeEntries', 'dl:has(dd', ')');
  // createFilter on test
  var selected;
  var isFilterSet = false;
  if (filterSeverity != ""){
    isFilterSet = true;
    selected = jq162(filterSeverity);
  }
  if (filterType != ""){
    isFilterSet = true;
    if (selected != null){
      selected = selected.filter(filterType)
    } else {
      selected = jq162(filterType);
    }
  }
  if (isFilterSet){
    hideAllNotifications();
    changeElementState(selected, 'block');
  } else {
    showAllNotifications();
  }
  computeStats();
}
