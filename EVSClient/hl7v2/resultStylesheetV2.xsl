<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
	<xsl:param name="viewdown">false</xsl:param>
	<xsl:param name="constraintPath">#</xsl:param>
	<xsl:param name="assertionManagerPath">/AssertionManagerGui/</xsl:param>
	<xsl:param name="ShowGeneralInformation">true</xsl:param>
	<xsl:param name="displayMax">100</xsl:param>
	<xsl:param name="selectedLanguage">fr</xsl:param>
	<xsl:param name="standalone">true</xsl:param>
	<xsl:param name="doTranslate">false</xsl:param>

	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
		<xd:desc>
			<xd:p>
				<xd:b>Created on:</xd:b> Jul 5, 2010 </xd:p>
			<xd:p>
				<xd:b>Modified on:</xd:b> Feb 11, 2011 </xd:p>
			<xd:p>
				<xd:b>Modified on:</xd:b> Jun 29, 2012 </xd:p>
			<xd:p>
				<xd:b>Modified on:</xd:b> Oct 8, 2013 </xd:p>
			<xd:p>
				<xd:b>Modified on:</xd:b> Sept 23, 201 </xd:p>
			<xd:p>
				<xd:b>Author:</xd:b> Anne-Gaëlle BERGE, IHE Development, INRIA Rennes </xd:p>
			<xd:p/>
		</xd:desc>
	</xd:doc>
	<xsl:variable name="ShowGeneralInformationLocal">
		<xsl:choose>
			<xsl:when test="normalize-space($ShowGeneralInformation)">
				<xsl:value-of select="$ShowGeneralInformation"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="true()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:variable name="limitDisplayTo">50</xsl:variable>
		<html>
			<head>
			</head>
			<body>
				<script type="text/javascript">
					function hideOrViewValidationDetails() {
					var detaileddiv = document.getElementById('resultdetailedann');
					if (detaileddiv != null){
					var onn = document.getElementById('resultdetailedann_switch_on');
					if (onn != null){
					if (onn.style.display == 'block') onn.style.display = 'none';
					else if (onn.style.display == 'none') onn.style.display = 'block';
					}
					var off = document.getElementById('resultdetailedann_switch_off');
					if (off != null){
					if (off.style.display == 'block') off.style.display = 'none';
					else if (off.style.display == 'none') off.style.display = 'block';
					}
					var body = document.getElementById('resultdetailedann_body');
					if (body != null){
					if (body.style.display == 'block') body.style.display = 'none';
					else if (body.style.display == 'none') body.style.display = 'block';
					}
					}
					}
					function hideOrUnhide(elem){
					var elemToHide = document.getElementById(elem.name + '_p');
					if (elemToHide != null){
					if (elem.checked){
					elemToHide.style.display = 'none';
					}
					else{
					elemToHide.style.display = 'block';
					}
					}
					}

				</script>
				<h2>External Validation Report</h2>
				<br/>
				<xsl:if test="count(SummaryResults/ValidationResultsOverview/Disclaimer) = 1">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">Disclaimer</h3></div>
						<div class="panel-body">
							<xsl:value-of select="SummaryResults/ValidationResultsOverview/Disclaimer"/>
						</div>
					</div>
					<br/>
				</xsl:if>
				<xsl:if test="ShowGeneralInformationLocal = 'true'">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">General Informations</h3></div>
						<div class="panel-body">
							<dl class="dl-horizontal">
								<div>
									<dt>Validation Date</dt>
									<dd>
										<xsl:value-of select="SummaryResults/ValidationResultsOverview/ValidationDate"/> -
										<xsl:value-of select="/SummaryResults/ValidationResultsOverview/ValidationTime"
										/></dd>
								</div>
								<div>
									<dt>Validation Service</dt>
									<dd>
										<xsl:value-of
												select="SummaryResults/ValidationResultsOverview/ValidationServiceName"/> (
										<xsl:value-of
												select="SummaryResults/ValidationResultsOverview/ValidationServiceVersion"/> )
									</dd>
								</div>

								<xsl:if test="count(SummaryResults/ValidationResultsOverview/Oid) = 1">
									<div>
										<dt>Message OID</dt>
										<dd>
											<xsl:value-of select="SummaryResults/ValidationResultsOverview/Oid"/>
										</dd>
									</div>

								</xsl:if>
								<xsl:if test="count(SummaryResults/ValidationResultsOverview/MessageOID) = 1">
									<div>
										<dt>Message OID</dt>
										<dd>
											<xsl:value-of select="SummaryResults/ValidationResultsOverview/MessageOID"/>
										</dd>
									</div>

								</xsl:if>

								<xsl:if test="count(SummaryResults/ValidationResultsOverview/ProfileOID) = 1">
									<div>
										<dt>Profile OID</dt>
										<dd>
											<xsl:value-of select="SummaryResults/ValidationResultsOverview/ProfileOID"/>
											<xsl:if test="SummaryResults/ValidationResultsOverview/ProfileRevision">
												<xsl:text> (Revision: </xsl:text>
												<xsl:value-of
														select="SummaryResults/ValidationResultsOverview/ProfileRevision"/>
												<xsl:text>)</xsl:text>
											</xsl:if>
										</dd>
									</div>

								</xsl:if>



								<div>
									<dt>Validation Test Status</dt>
									<dd>
										<xsl:if
												test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
											<div class="PASSED">
												<xsl:value-of
														select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
											</div>
										</xsl:if>
										<xsl:if
												test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
											<div class="FAILED">
												<xsl:value-of
														select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
											</div>
										</xsl:if>
										<xsl:if
												test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
											<div class="ABORTED">
												<xsl:value-of
														select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
											</div>
										</xsl:if>
										<xsl:if
												test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'INVALID REQUEST')">
											<div class="ABORTED">
												<xsl:value-of
														select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
											</div>
										</xsl:if>
									</dd>
								</div>

							</dl>

						</div>
					</div>
				</xsl:if>

				<xsl:if test="SummaryResults/Resources">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">Resources</h3></div>

						<div class="panel-body">


							<p>Find below the list of HL7 tables which have been used to validate the coded values of the
								message</p>
							<ul>
								<xsl:for-each select="SummaryResults/Resources/Resource">
									<li>
										<xsl:value-of select="oid"/>
										<xsl:text> (Revision: </xsl:text>
										<xsl:value-of select="revision"/>
										<xsl:text>)</xsl:text>
									</li>
								</xsl:for-each>
							</ul>
						</div>
					</div>
				</xsl:if>
				<xsl:if test="count(SummaryResults/ValidationResultsOverview/ReferencedStandard) = 1">

					<div class="panel panel-default">

						<div class="panel-heading"><h3 class="panel-title">Referenced Standard</h3></div>
						<div class="panel-body">

							<table border="0">
								<tr>
									<td>
										<b>Name</b>
									</td>
									<td>
										<xsl:value-of
												select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardName"
										/>
									</td>
								</tr>
								<tr>
									<td>
										<b>Version</b>
									</td>
									<td>
										<xsl:value-of
												select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardVersion"
										/>
									</td>
								</tr>
								<tr>
									<td>
										<b>Extension</b>
									</td>
									<td>
										<xsl:value-of
												select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardExtension"
										/>
									</td>
								</tr>
							</table>
						</div>
					</div>

				</xsl:if>

				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Validation counters</h3></div>
					<div class="panel-body">
						<ul>
							<li>

								<xsl:choose>
									<xsl:when test="SummaryResults/ValidationCounters/NrOfValidationErrors &gt; 0">
										<a href="#Errors_p">
											<xsl:value-of select="SummaryResults/ValidationCounters/NrOfValidationErrors"/>
											error(s) </a>
									</xsl:when>
									<xsl:otherwise> No error </xsl:otherwise>
								</xsl:choose>
							</li>
							<li>
								<xsl:choose>
									<xsl:when test="SummaryResults/ValidationCounters/NrOfValidationWarnings &gt; 0">
										<a href="#Warnings_p">
											<xsl:value-of select="SummaryResults/ValidationCounters/NrOfValidationWarnings"
											/> warning(s) </a>
									</xsl:when>
									<xsl:otherwise> No warning </xsl:otherwise>
								</xsl:choose>
							</li>

							<xsl:choose>
								<xsl:when test="SummaryResults/ValidationCounters/NrOfValidationUnknown &gt; 0">
									<li>
										<a href="#Unknowns_p">
											<xsl:value-of select="SummaryResults/ValidationCounters/NrOfValidationUnknown"
											/> unknown condition(s) </a>
									</li>
								</xsl:when>
							</xsl:choose>

							<xsl:choose>
								<xsl:when test="SummaryResults/ValidationCounters/NrOfValidationConditions &gt; 0">
									<li><a href="#Conditions_p">
										<xsl:value-of select="SummaryResults/ValidationCounters/NrOfValidationConditions"
										/> Condition(s) </a></li>
								</xsl:when>
							</xsl:choose>

							<li>
								<xsl:choose>
									<xsl:when test="SummaryResults/ValidationCounters/NrOfValidatedAssertions &gt; 0">
										<a href="#Reports_p">
											<xsl:value-of select="SummaryResults/ValidationCounters/NrOfValidatedAssertions"
											/> report(s) </a>
									</xsl:when>
									<xsl:otherwise> No report </xsl:otherwise>
								</xsl:choose>
							</li>
						</ul>

						<b>HIDE : </b>
						<input type="checkbox" onclick="hideOrUnhide(this)" name="Errors">Errors</input>
						<input type="checkbox" onclick="hideOrUnhide(this)" name="Warnings">Warnings</input>
						<xsl:choose>
							<xsl:when test="SummaryResults/ValidationCounters/NrOfValidationUnknown &gt; 0">
								<input type="checkbox" onclick="hideOrUnhide(this)" name="Unknowns">Unknowns</input>
							</xsl:when>
						</xsl:choose>
						<xsl:choose>
							<xsl:when test="SummaryResults/ValidationCounters/NrOfValidationConditions &gt; 0">
								<input type="checkbox" onclick="hideOrUnhide(this)" name="Condition">Condition</input>
							</xsl:when>
						</xsl:choose>
						<input type="checkbox" onclick="hideOrUnhide(this)" name="Reports">Reports</input>

					</div>
				</div>
				<br/>
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Validation details</h3></div>
					<div class="panel-body">


						<xsl:if test="SummaryResults/ValidationCounters/NrOfValidationErrors &gt; 0">


							<div id="Errors_p">
								<xsl:choose>
									<xsl:when test="count(SummaryResults/ValidationResults/Error) &gt;= $limitDisplayTo">
										<span style="font-weight:bold;">Errors (only first <xsl:value-of
												select="$limitDisplayTo"/> th displayed) </span>

									</xsl:when>
									<xsl:otherwise>
										<span style="font-weight:bold;">Errors </span>
									</xsl:otherwise>
								</xsl:choose>
								<div class="panel panel-default">
									<div class="panel-body ">
										<xsl:for-each
												select="SummaryResults/ValidationResults/Error[position() &lt;= $limitDisplayTo]">
											<xsl:call-template name="viewnotification">
												<xsl:with-param name="kind">dl-horizontal gzl-notification
													gzl-notification-red</xsl:with-param>
												<xsl:with-param name="type">E</xsl:with-param>
												<xsl:with-param name="position" select="position()"/>
												<xsl:with-param name="node" select="this"/>
												<xsl:with-param name="isSchematronValidation" select="false()"/>
											</xsl:call-template>
										</xsl:for-each>
									</div>
								</div>
							</div>

						</xsl:if>
						<xsl:if test="SummaryResults/ValidationCounters/NrOfValidationWarnings &gt; 0">


							<div id="Warnings_p">
								<xsl:choose>
									<xsl:when
											test="count(SummaryResults/ValidationResults/ResultXML/Warning) &gt;= $limitDisplayTo">
										<p id="warnings">
											<b> Warnings <span class="FAILED"> (only first <xsl:value-of
													select="$limitDisplayTo"/> th displayed) </span>
											</b>
										</p>
									</xsl:when>
									<xsl:otherwise>
										<p id="warnings">
											<b>Warnings</b>
										</p>
									</xsl:otherwise>
								</xsl:choose>
								<div class="panel panel-default">
									<div class="panel-body ">
										<xsl:for-each
												select="SummaryResults/ValidationResults/Warning[position() &lt;= $limitDisplayTo]">
											<xsl:call-template name="viewnotification">
												<xsl:with-param name="kind">dl-horizontal gzl-notification
													gzl-notification-orange</xsl:with-param>
												<xsl:with-param name="type">W</xsl:with-param>
												<xsl:with-param name="position" select="position()"/>
												<xsl:with-param name="node" select="this"/>
												<xsl:with-param name="isSchematronValidation" select="false()"/>
											</xsl:call-template>
										</xsl:for-each>
									</div>
								</div>
							</div>
						</xsl:if>
						<xsl:if test="SummaryResults/ValidationCounters/NrOfValidationConditions &gt; 0">
							<div id="Conditions_p">
								<xsl:choose>
									<xsl:when
											test="count(SummaryResults/ValidationResults/ResultXML/Condition) &gt;= $limitDisplayTo">
										<p id="conditions">
											<b>Conditions <span class="FAILED"> (only first <xsl:value-of
													select="$limitDisplayTo"/> th displayed) </span>
											</b>
										</p>
									</xsl:when>
									<xsl:otherwise>
										<p id="conditions">
											<b>Conditions</b>
										</p>
									</xsl:otherwise>
								</xsl:choose>
								<div class="panel panel-default">
									<div class="panel-body ">
										<xsl:for-each
												select="SummaryResults/ValidationResults/Condition[position() &lt;= $limitDisplayTo]">
											<xsl:call-template name="viewnotification">
												<xsl:with-param name="kind">dl-horizontal gzl-notification
												</xsl:with-param>
												<xsl:with-param name="type">W</xsl:with-param>
												<xsl:with-param name="position" select="position()"/>
												<xsl:with-param name="node" select="this"/>
												<xsl:with-param name="isSchematronValidation" select="false()"/>
											</xsl:call-template>
										</xsl:for-each>
									</div>
								</div>
							</div>
						</xsl:if>
						<xsl:if test="SummaryResults/ValidationCounters/NrOfValidationUnknown &gt; 0">
							<p id="unknown">
								<b>Unknown exceptions</b>
							</p>
							<xsl:for-each select="SummaryResults/ValidationResults/ResultXML/Unknown">
								<table class="unknown">
									<xsl:if test="count(Test) &gt; 0">
										<tr>
											<td width="100" valign="top">
												<b>Test</b>
											</td>
											<td>
												<xsl:value-of select="Test"/>
											</td>
										</tr>
									</xsl:if>
									<tr>
										<td width="100" valign="top">
											<b>Location</b>
										</td>
										<td>
											<xsl:value-of select="Location"/>
										</td>
									</tr>
									<tr>
										<td width="100" valign="top">
											<b>Description</b>
										</td>
										<td>
											<xsl:value-of select="Description"/>
										</td>
									</tr>
								</table>
								<br/>
							</xsl:for-each>
						</xsl:if>
						<xsl:if test="SummaryResults/ValidationCounters/NrOfValidatedAssertions &gt; 0">


							<div id="Reports_p">
								<xsl:choose>
									<xsl:when test="count(SummaryResults/ValidationResults/Report) &gt;= $limitDisplayTo">
										<p id="reports">
											<b> Report <span class="FAILED"> (only first <xsl:value-of select="$limitDisplayTo"
											/> reports are displayed) </span>
											</b>
										</p>
									</xsl:when>
									<xsl:otherwise>
										<p id="reports">
											<b>Reports</b>
										</p>
									</xsl:otherwise>
								</xsl:choose>
								<div class="panel panel-default">
									<div class="panel-body ">
										<xsl:for-each
												select="SummaryResults/ValidationResults/Report[position() &lt;= $limitDisplayTo]">
											<xsl:call-template name="viewnotification">
												<xsl:with-param name="kind">dl-horizontal gzl-notification
													gzl-notification-green</xsl:with-param>
												<xsl:with-param name="type">R</xsl:with-param>
												<xsl:with-param name="position" select="position()"/>
												<xsl:with-param name="node" select="this"/>
												<xsl:with-param name="isSchematronValidation" select="false()"/>
											</xsl:call-template>
										</xsl:for-each>
									</div>
								</div>
							</div>
						</xsl:if>
					</div>
				</div>
				<br/>
				<xsl:if test="count(SummaryResults/ValidationResults/ResultXML/ProfileExceptions) &gt; 0">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">Profile exceptions</h3></div>
						<div class="panel-body">
							<p>Those profile exceptions have been raised because the HL7 message profile, on some points,
								differs from the Java class representing the HL7 message. Those are not errors in the given
								message but may cause (partially) wrong validation results</p>
							<ul>
								<xsl:for-each
										select="SummaryResults/ValidationResults/ResultXML/ProfileExceptions/Exception">
									<li>
										<xsl:value-of select="text()"/>
									</li>
								</xsl:for-each>
							</ul>

						</div>
					</div>
				</xsl:if>
				<xsl:if test="count(SummaryResults/ValidationResults/ProfileException) &gt; 0">
					<div class="panel panel-default">
						<div class="panel-heading"><h3 class="panel-title">Profile exceptions</h3></div>
						<div class="panel-body">
							<p>Those profile exceptions have been raised because the HL7 message profile, on some points,
								differs from the Java class representing the HL7 message. Those are not errors in the given
								message but may cause (partially) wrong validation results</p>
							<ul>
								<xsl:for-each select="SummaryResults/ValidationResults/ProfileException">
									<li>
										<xsl:value-of select="Description"/>
									</li>
								</xsl:for-each>
							</ul>

						</div>
					</div>
				</xsl:if>
			</body>
		</html>
	</xsl:template>

	<xsl:include href="/xsl/EVSClient/common/common.xsl"/>
</xsl:stylesheet>