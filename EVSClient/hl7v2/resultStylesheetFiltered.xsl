<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html" omit-xml-declaration="yes"/>
    <xsl:param name="viewdown">false</xsl:param>
    <xsl:param name="constraintPath">#</xsl:param>
    <xsl:param name="assertionManagerPath">/AssertionManagerGui/</xsl:param>
    <xsl:param name="ShowGeneralInformation">true</xsl:param>
    <xsl:param name="ShowResultOverview">false</xsl:param>
    <xsl:param name="displayMax">100</xsl:param>
    <xsl:param name="selectedLanguage">fr</xsl:param>
    <xsl:param name="standalone">true</xsl:param>
    <xsl:param name="doTranslate">false</xsl:param>

    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>


            </head>
            <body >
                <h2>External Validation Report</h2>
                <br/>

                <xsl:if test="ShowGeneralInformationLocal = 'true'">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">General Informations</h3>
                        </div>
                        <div class="panel-body">
                            <dl class="dl-horizontal">
                                <div>
                                    <dt>Validation Date</dt>
                                    <dd>
                                        <xsl:value-of select="SummaryResults/ValidationResultsOverview/ValidationDate"/>
                                        -
                                        <xsl:value-of select="/SummaryResults/ValidationResultsOverview/ValidationTime"
                                        />
                                    </dd>
                                </div>
                                <div>
                                    <dt>Validation Service</dt>
                                    <dd>
                                        <xsl:value-of
                                                select="SummaryResults/ValidationResultsOverview/ValidationServiceName"/>
                                        (
                                        <xsl:value-of
                                                select="SummaryResults/ValidationResultsOverview/ValidationServiceVersion"/>
                                        )
                                    </dd>
                                </div>

                                <xsl:if test="count(SummaryResults/ValidationResultsOverview/Oid) = 1">
                                    <div>
                                        <dt>Message OID</dt>
                                        <dd>
                                            <xsl:value-of select="SummaryResults/ValidationResultsOverview/Oid"/>
                                        </dd>
                                    </div>

                                </xsl:if>
                                <xsl:if test="count(SummaryResults/ValidationResultsOverview/MessageOID) = 1">
                                    <div>
                                        <dt>Message OID</dt>
                                        <dd>
                                            <xsl:value-of select="SummaryResults/ValidationResultsOverview/MessageOID"/>
                                        </dd>
                                    </div>

                                </xsl:if>

                                <xsl:if test="count(SummaryResults/ValidationResultsOverview/ProfileOID) = 1">
                                    <div>
                                        <dt>Profile OID</dt>
                                        <dd>
                                            <xsl:value-of select="SummaryResults/ValidationResultsOverview/ProfileOID"/>
                                            <xsl:if test="SummaryResults/ValidationResultsOverview/ProfileRevision">
                                                <xsl:text> (Revision: </xsl:text>
                                                <xsl:value-of
                                                        select="SummaryResults/ValidationResultsOverview/ProfileRevision"/>
                                                <xsl:text>)</xsl:text>
                                            </xsl:if>
                                        </dd>
                                    </div>

                                </xsl:if>


                                <div>
                                    <dt>Validation Test Status</dt>
                                    <dd>
                                        <xsl:if
                                                test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                            <div class="PASSED">
                                                <xsl:value-of
                                                        select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
                                            </div>
                                        </xsl:if>
                                        <xsl:if
                                                test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                            <div class="FAILED">
                                                <xsl:value-of
                                                        select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
                                            </div>
                                        </xsl:if>
                                        <xsl:if
                                                test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                                            <div class="ABORTED">
                                                <xsl:value-of
                                                        select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
                                            </div>
                                        </xsl:if>
                                        <xsl:if
                                                test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'INVALID REQUEST')">
                                            <div class="ABORTED">
                                                <xsl:value-of
                                                        select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
                                            </div>
                                        </xsl:if>
                                    </dd>
                                </div>

                            </dl>

                        </div>
                    </div>
                </xsl:if>

                <xsl:if test="SummaryResults/Resources">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Resources</h3>
                        </div>

                        <div class="panel-body">


                            <p>Find below the list of HL7 tables which have been used to validate the coded values of
                                the
                                message
                            </p>
                            <ul>
                                <xsl:for-each select="SummaryResults/Resources/Resource">
                                    <li>
                                        <xsl:element name="a">
                                            <xsl:attribute name="href"><xsl:text>/GazelleHL7Validator/viewResource.seam?oid=</xsl:text><xsl:value-of select="oid"/></xsl:attribute>
                                        <xsl:value-of select="oid"/>
                                        </xsl:element>

                                        <xsl:text> (Revision: </xsl:text>
                                        <xsl:value-of select="revision"/>
                                        <xsl:text>)</xsl:text>
                                    </li>
                                </xsl:for-each>
                            </ul>
                        </div>
                    </div>
                </xsl:if>
                <xsl:if test="count(SummaryResults/ValidationResultsOverview/ReferencedStandard) = 1">

                    <div class="panel panel-default">

                        <div class="panel-heading">
                            <h3 class="panel-title">Referenced Standard</h3>
                        </div>
                        <div class="panel-body">

                            <table border="0">
                                <tr>
                                    <td>
                                        <b>Name</b>
                                    </td>
                                    <td>
                                        <xsl:value-of
                                                select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardName"
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Version</b>
                                    </td>
                                    <td>
                                        <xsl:value-of
                                                select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardVersion"
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Extension</b>
                                    </td>
                                    <td>
                                        <xsl:value-of
                                                select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardExtension"
                                        />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </xsl:if>

                <br/>
                <xsl:choose>
                    <xsl:when test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                        <div class="panel panel-default">
                            <div class="panel-heading" >
                                <h3 class="panel-title">Aborted Reason</h3>
                            </div>
                            <div class ="panel-body">
                                <xsl:value-of
                                        select="SummaryResults/ValidationResultsOverview/ValidationAbortedReason"/>
                            </div>
                            <div class="panel panel-default">
				<div class="panel-heading" role="tab" id="collapse_title">
				        <h4 class="panel-title">
				            <a role="button" data-toggle="collapse" href="#collapse_result"
				               aria-expanded="true" aria-controls="collapse_result">
				                Validation details (click to collapse)
				            </a>
				        </h4>
				    </div>
				    <div id="collapse_result" class="panel-collapse" role="tabpanel"
				         aria-labelledby="collapse_title">
				        <div class="panel-body">
				            <div class="gzl-container">
				                <div class="row">
				                    <div class="col-md-2 col-sd-2" role="complementary">
				                        <xsl:call-template name="navbar"/>
				                    </div>
				                    <div class="col-md-10 col-sd-10" role="main">
				                        <div>
				                            <xsl:apply-templates select="//ProfileException"/>
				                            </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				<script type="text/javascript" defer="defer" language="javascript">
				    //<![CDATA[
					 jq162(document).ready( computeStats());
				    //]]>
				</script>
                        </div>
                    </xsl:when>
                    <xsl:otherwise>


                <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="collapse_title">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse_result"
                               aria-expanded="true" aria-controls="collapse_result">
                                Validation details (click to collapse)
                            </a>
                        </h4>
                    </div>
                    <div id="collapse_result" class="panel-collapse" role="tabpanel"
                         aria-labelledby="collapse_title">
                        <div class="panel-body">
                            <div class="gzl-container">
                                <div class="row">
                                    <div class="col-md-2 col-sd-2" role="complementary">
                                        <xsl:call-template name="navbar"/>
                                    </div>
                                    <div class="col-md-10 col-sd-10" role="main">
                                        <div>
                                            <xsl:apply-templates select="//ProfileException"/>
                                            <xsl:apply-templates select="//Error"/>
                                            <xsl:apply-templates select="//Warning"/>
                                            <xsl:apply-templates select="//Condition"/>
                                            <xsl:apply-templates select="//Unknown"/>
                                            <xsl:apply-templates select="//Report"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script type="text/javascript" defer="defer" language="javascript">
                    //<![CDATA[
         jq162(document).ready( computeStats());
        //]]>
                </script>
                    </xsl:otherwise>
                </xsl:choose>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="ProfileException">
        <xsl:call-template name="viewnotification">
            <xsl:with-param name="classnames">gzl-notification-red</xsl:with-param>
            <xsl:with-param name="kind">exception</xsl:with-param>
            <xsl:with-param name="prefix">EX</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="Error">
        <xsl:call-template name="viewnotification">
            <xsl:with-param name="classnames">gzl-notification-red</xsl:with-param>
            <xsl:with-param name="kind">error</xsl:with-param>
            <xsl:with-param name="prefix">E</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="Warning">
        <xsl:call-template name="Notification">
            <xsl:with-param name="classnames">gzl-notification-orange</xsl:with-param>
            <xsl:with-param name="kind">warning</xsl:with-param>
            <xsl:with-param name="prefix">W</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="Condition">
        <xsl:call-template name="Notification">
            <xsl:with-param name="classnames">gzl-notification-orange</xsl:with-param>
            <xsl:with-param name="kind">warning</xsl:with-param>
            <xsl:with-param name="prefix">W</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="Unknown">
        <xsl:call-template name="Notification">
            <xsl:with-param name="classnames">gzl-notification-blue</xsl:with-param>
            <xsl:with-param name="kind">unknown</xsl:with-param>
            <xsl:with-param name="prefix">U</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="Report">
        <xsl:call-template name="Notification">
            <xsl:with-param name="classnames">gzl-notification-green</xsl:with-param>
            <xsl:with-param name="kind">report</xsl:with-param>
            <xsl:with-param name="prefix">R</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template name="NotificationOld">
        <xsl:param name="kind"/>
        <xsl:param name="classnames"/>
        <xsl:param name="prefix"/>
        <xsl:element name="dl">
            <xsl:attribute name="data-value">
                <xsl:value-of select="concat($kind, 'E')"/>
            </xsl:attribute>
            <xsl:attribute name="style">
                <xsl:text>display:block;</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="class">
                <xsl:text>dl-horizontal gzl-notification </xsl:text>
                <xsl:value-of select="$classnames"/>
            </xsl:attribute>
            <dt>Location</dt>
            <dd>
                <xsl:value-of select="Location"/>
                <span class="report-number">
                    <xsl:element name="a">
                        <xsl:attribute name="id">
                            <xsl:value-of select="translate($prefix,'REIW','reiw')"/>
                            <xsl:value-of select="position()"/>
                        </xsl:attribute>
                        <xsl:value-of select="$prefix"/> -
                        <xsl:value-of select="position()"/>
                    </xsl:element>
                </span>
            </dd>
            <xsl:if test="count(Value) &gt; 0">
                <dt>Value</dt>
                <dd>
                    <xsl:value-of select="Value"/>
                </dd>
            </xsl:if>
            <dt>Description</dt>
            <dd>
                <xsl:value-of select="Description"/>
            </dd>
            <dt>Type</dt>
            <xsl:element name="dd">
                <xsl:attribute name="data-value">
                    <xsl:value-of select="concat(Type, 'E')"/>
                </xsl:attribute>
                <xsl:value-of select="Type"/>
            </xsl:element>
            <xsl:if test="count(HL7Table) &gt; 0">
                <dt>HL7Table</dt>
                <dd>
                    <xsl:element name="a">
                        <xsl:attribute name="href">
                            <xsl:text>/GazelleHL7Validator/viewResource.seam?oid=</xsl:text>
                            <xsl:value-of
                                    select="HL7Table"/>
                        </xsl:attribute>
                        <xsl:attribute name="target">blank</xsl:attribute>
                        <xsl:value-of select="HL7Table"/>
                    </xsl:element>
                </dd>
            </xsl:if>
        </xsl:element>

        <script type="text/javascript" defer="defer" language="javascript">
            //<![CDATA[
         jq162(document).ready( computeStats());
        //]]>
        </script>
    </xsl:template>

    <xsl:template name="navbarHl7">
        <nav class="gzl-sidebar">
            <ul class="nav">
                <li>
                    <xsl:call-template name="severityFilter"/>
                </li>
                <li>
                    <xsl:call-template name="typeFilter"/>
                </li>
                <li>
                    <a onclick="resetFilters()">
                        <b>Reset filters</b>
                    </a>
                </li>
            </ul>
        </nav>
    </xsl:template>

    <xsl:template name="severityFilterOld">
        <xsl:element name="a">
            <xsl:attribute name="onclick">
                <xsl:text>changeCriteriaStatus('severity')</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="data-value">
                <xsl:text>severity</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="class">
                <xsl:text>mainCriteria</xsl:text>
            </xsl:attribute>
            <xsl:element name="span">
                <xsl:attribute name="id">
                    <xsl:text>severity</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="class">
                    <xsl:text>fa fa-check-square-o</xsl:text>
                </xsl:attribute>
            </xsl:element>
            <xsl:text>Severity</xsl:text>
        </xsl:element>
        <xsl:element name="ul">
            <xsl:attribute name="id">
                <xsl:text>severityEntries</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="class">
                <xsl:text>nav</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="style">
                <xsl:text>display:block;</xsl:text>
            </xsl:attribute>

	    <!--exception-->
            <xsl:element name="li">
                <xsl:element name="a">
                    <xsl:attribute name="data-value">
                        <xsl:text>exception</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="onclick">
                        <xsl:text>showHide(this)</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="class">
                        <xsl:text>nostate</xsl:text>
                    </xsl:attribute>
                    <xsl:element name="span">
                        <xsl:attribute name="class">
                            <xsl:text>statlabel</xsl:text>
                        </xsl:attribute>
                        <xsl:text>Exception</xsl:text>
                    </xsl:element>
                    <xsl:element name="span">
                        <xsl:attribute name="class">
                            <xsl:text>statvalue</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="data-value">
                            <xsl:text>Profile Exception</xsl:text>
                        </xsl:attribute>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
            <!--error-->
            <xsl:element name="li">
                <xsl:element name="a">
                    <xsl:attribute name="data-value">
                        <xsl:text>error</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="onclick">
                        <xsl:text>showHide(this)</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="class">
                        <xsl:text>nostate</xsl:text>
                    </xsl:attribute>
                    <xsl:element name="span">
                        <xsl:attribute name="class">
                            <xsl:text>statlabel</xsl:text>
                        </xsl:attribute>
                        <xsl:text>Error</xsl:text>
                    </xsl:element>
                    <xsl:element name="span">
                        <xsl:attribute name="class">
                            <xsl:text>statvalue</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="data-value">
                            <xsl:text>error</xsl:text>
                        </xsl:attribute>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
           

            <!--warning-->
            <xsl:element name="li">
                <xsl:element name="a">
                    <xsl:attribute name="data-value">
                        <xsl:text>warning</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="onclick">
                        <xsl:text>showHide(this)</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="class">
                        <xsl:text>nostate</xsl:text>
                    </xsl:attribute>
                    <xsl:element name="span">
                        <xsl:attribute name="class">
                            <xsl:text>statlabel</xsl:text>
                        </xsl:attribute>
                        <xsl:text>Warning</xsl:text>
                    </xsl:element>
                    <xsl:element name="span">
                        <xsl:attribute name="class">
                            <xsl:text>statvalue</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="data-value">
                            <xsl:text>warning</xsl:text>
                        </xsl:attribute>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
            <!--unknown-->
            <xsl:element name="li">
                <xsl:element name="a">
                    <xsl:attribute name="data-value">
                        <xsl:text>unknown</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="onclick">
                        <xsl:text>showHide(this)</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="class">
                        <xsl:text>nostate</xsl:text>
                    </xsl:attribute>
                    <xsl:element name="span">
                        <xsl:attribute name="class">
                            <xsl:text>statlabel</xsl:text>
                        </xsl:attribute>
                        <xsl:text>Unknown</xsl:text>
                    </xsl:element>
                    <xsl:element name="span">
                        <xsl:attribute name="class">
                            <xsl:text>statvalue</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="data-value">
                            <xsl:text>Unknown</xsl:text>
                        </xsl:attribute>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
            <!--report-->
            <xsl:element name="li">
                <xsl:element name="a">
                    <xsl:attribute name="data-value">
                        <xsl:text>report</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="onclick">
                        <xsl:text>showHide(this)</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="class">
                        <xsl:text>nostate</xsl:text>
                    </xsl:attribute>
                    <xsl:element name="span">
                        <xsl:attribute name="class">
                            <xsl:text>statlabel</xsl:text>
                        </xsl:attribute>
                        <xsl:text>Successful check</xsl:text>
                    </xsl:element>
                    <xsl:element name="span">
                        <xsl:attribute name="class">
                            <xsl:text>statvalue</xsl:text>
                        </xsl:attribute>
                        <xsl:attribute name="data-value">
                            <xsl:text>report</xsl:text>
                        </xsl:attribute>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xsl:template name="typeFilterOld">
        <a onclick="changeCriteriaStatus('type')" data-value="type" class="mainCriteria">
            <i id="type" class="fa fa-check-square-o"/>
            <b>Type</b>
        </a>
        <ul id="typeEntries" style="display:block;" class="nav">
            <xsl:for-each select="//Type[not(. = following::Type)]">
                <li>
                    <xsl:call-template name="criteriaEntry">
                        <xsl:with-param name="entry">
                            <xsl:value-of select="current()"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
    <xsl:template name="criteriaEntryOld">
        <xsl:param name="entry"/>
        <xsl:element name="a">
            <xsl:attribute name="data-value">
                <xsl:value-of select="text()"/>
            </xsl:attribute>
            <xsl:attribute name="onclick">showHide(this)</xsl:attribute>
            <xsl:attribute name="class">nostate</xsl:attribute>
            <span class="statlabel">
                <xsl:value-of select="text()"/>
            </span>
            <xsl:element name="span">
                <xsl:attribute name="class">statvalue</xsl:attribute>
                <xsl:attribute name="data-value">
                    <xsl:value-of select="text()"/>
                </xsl:attribute>
            </xsl:element>
        </xsl:element>
    </xsl:template>


    <xsl:include href="/xsl/EVSClient/common/common-hl7v2.xsl"/>



</xsl:stylesheet>
