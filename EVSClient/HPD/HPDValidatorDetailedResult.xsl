<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xsl:param name="viewdown">true</xsl:param>
    <xsl:param name="constraintPath">#</xsl:param>
    <xsl:param name="assertionManagerPath">/AssertionManagerGui/</xsl:param>
    <xsl:param name="ShowGeneralInformation">true</xsl:param>
    <xsl:param name="ShowResultOverview">false</xsl:param>
    <xsl:param name="displayMax">100</xsl:param>
    <xsl:param name="selectedLanguage">fr</xsl:param>
    <xsl:param name="standalone">true</xsl:param>
    <xsl:param name="doTranslate">false</xsl:param>

    <xsl:template match="/">
        <html>
            <xsl:call-template name="insertHeader">
                <xsl:with-param name="type">HPD</xsl:with-param>
            </xsl:call-template>
            <body>

                <!-- general information -->
                <xsl:if test="$ShowGeneralInformation = 'true'">
                    <xsl:call-template name="viewGeneralInformation"/>
                </xsl:if>

                <!-- overview -->
                <xsl:if test="$ShowResultOverview = 'true'">
                    <xsl:call-template name="displayResultOverview"/>
                </xsl:if>
                <!-- XML well formed report -->

                <xsl:call-template name="displayXMLWellFormReport"/>

                <!-- XML XSD validation report -->

                <xsl:call-template name="displayXSDValidationReport"/>
                <!-- general information -->

                <xsl:call-template name="displayMBValidatorResults">
                    <xsl:with-param name="showProgressBar">true</xsl:with-param>
                    <xsl:with-param name="showNavBar">true</xsl:with-param>
                </xsl:call-template>

                <script type="text/javascript" defer="defer" language="javascript">
                    //<![CDATA[
         jq162(document).ready( computeStats());
        //]]>
                </script>
            </body>
        </html>
    </xsl:template>


    <xsl:include href="/xsl/EVSClient/common/common.xsl"/>



</xsl:stylesheet>
