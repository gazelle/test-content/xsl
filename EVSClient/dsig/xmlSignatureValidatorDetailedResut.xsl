<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xsl:param name="viewdown">true</xsl:param>
    <xsl:param name="constraintPath">#</xsl:param>
    <xsl:param name="assertionManagerPath">/AssertionManagerGui/</xsl:param>
    <xsl:param name="ShowGeneralInformation">true</xsl:param>
    <xsl:param name="displayMax">100</xsl:param>
    <xsl:param name="selectedLanguage">fr</xsl:param>
    <xsl:param name="standalone">true</xsl:param>
    <xsl:param name="doTranslate">true</xsl:param>

    <xd:doc>
        <xd:desc>
            <xd:p>Vocabulary file containing language dependant strings such as labels</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="vocFile" select="'/xsl/EVSClient/dsig/dsig_l10n.xml'"/>

    <xd:doc>
        <xd:desc>
            <xd:p>Cache language dependant strings</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:variable name="vocMessages" select="document($vocFile)"/>

    <xd:doc>
        <xd:desc>
            <xd:p>Default language for retrieval of language dependant strings such as labels, e.g. 'en-US'. This is the
                fallback language in case the string is not available in the actual language. See also <xd:ref
                        name="textLang" type="parameter">textLang</xd:ref>.
            </xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="textlangDefault" select="'$selectedLanguage'"/>

    <xsl:template match="/">
        <html>
            <head>
                <link href="/xsl/EVSClient/resultStyle.css" rel="stylesheet" type="text/css" media="screen"/>
            </head>
            <xsl:call-template name="insertHeader">
                <xsl:with-param name="type">HL7v3</xsl:with-param>
            </xsl:call-template>

            <body>


                <!-- general information -->
                <xsl:if test="$ShowGeneralInformation = 'true'">
                    <xsl:call-template name="viewGeneralInformation"/>
                </xsl:if>

                <!-- overview

                <xsl:call-template name="displayResultOverview"/>
-->
                <!-- XML well formed report -->
                <xsl:if test="count(detailedResult/DocumentWellFormed) &gt; 0">
                <xsl:call-template name="displayXMLWellFormReport">
                    <xsl:with-param name="isUrl" select="false()"/>
                </xsl:call-template>
                </xsl:if>
                <!-- XML XSD validation report -->

                <xsl:call-template name="displayXSDValidationReport"/>

                <xsl:call-template name="displayMBValidatorResults">
                    <xsl:with-param name="showProgressBar">true</xsl:with-param>
                    <xsl:with-param name="showNavBar">true</xsl:with-param>
                </xsl:call-template>

                <script type="text/javascript" defer="defer" language="javascript">
                    //<![CDATA[
         jq162(document).ready( computeStats());
        //]]>
                </script>

            </body>
        </html>
    </xsl:template>

    <xsl:include href="/xsl/EVSClient/common/common.xsl"/>

</xsl:stylesheet>
