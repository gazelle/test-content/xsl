<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html" omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jul 5, 2010</xd:p>
            <xd:p><xd:b>Modified on:</xd:b> Aug 24, 2010</xd:p>
            <xd:p><xd:b>Modified on:</xd:b> June 30, 2011</xd:p>
            <xd:p><xd:b>Author:</xd:b> Anne-Gaëlle BERGE, IHE Development, INRIA Rennes</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>

    <xsl:param name="viewdown">false</xsl:param>
    <xsl:param name="constraintPath">#</xsl:param>
    <xsl:param name="assertionManagerPath">/AssertionManagerGui/</xsl:param>
    <xsl:param name="ShowGeneralInformation">true</xsl:param>
    <xsl:param name="ShowResultOverview">false</xsl:param>
    <xsl:param name="displayMax">100</xsl:param>
    <xsl:param name="selectedLanguage">fr</xsl:param>
    <xsl:param name="standalone">true</xsl:param>
    <xsl:param name="maxError">100</xsl:param>
    <xsl:param name="maxWarning">100</xsl:param>
    <xsl:param name="maxInfo">100</xsl:param>
    <xsl:param name="maxReport">100</xsl:param>
    <xsl:param name="doTranslate">false</xsl:param>

    <xsl:template match="/">
        <html>

            <xsl:call-template name="insertHeader">
                <xsl:with-param name="type">Schematron</xsl:with-param>
            </xsl:call-template>

            <body>


                <!-- general information -->
                <xsl:if test="$ShowGeneralInformation = 'true'">
                    <xsl:call-template name="viewGeneralInformation"/>
                </xsl:if>

                <!-- overview

                <xsl:call-template name="displayResultOverview"/>
                -->
                <!-- XML well formed report -->
                <xsl:if test="count(detailedResult/DocumentWellFormed) = 1">
                <xsl:call-template name="displayXMLWellFormReport"/>
                </xsl:if>

                <!-- XML XSD validation report -->

                <xsl:call-template name="displayXSDValidationReport"/>
                <!-- XML Daffodil Transformation   report -->

                <xsl:call-template name="displayDaffodilTransformationReport"/>


                <!-- MIF Validation -->

                <xsl:if test="count(detailedResult/MIFValidation) = 1">
                    <div class="rich-panel styleResultBackround" id="mif">
                        <div class="panel-heading">MIF Validation</div>
                        <div class="panel-body">
                            <table>
                                <tr>
                                    <td>
                                        <b>Result</b>
                                    </td>
                                    <td>
                                        <xsl:choose>
                                            <xsl:when test="contains(detailedResult/MIFValidation/Result, 'PASSED')">
                                                <div class="PASSED">
                                                    PASSED
                                                </div>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <div class="FAILED">
                                                    FAILED
                                                </div>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <b>Summary</b>
                                    </td>
                                    <td>
                                        <xsl:choose>
                                            <xsl:when test="detailedResult/MIFValidation/ValidationCounters/NrOfValidationErrors &gt; 0">
                                                <a href="#miferrors"><xsl:value-of select="detailedResult/MIFValidation/ValidationCounters/NrOfValidationErrors"/> error(s)</a>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                No error
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        <br/>
                                        <xsl:choose>
                                            <xsl:when test="detailedResult/MIFValidation/ValidationCounters/NrOfValidationWarnings &gt; 0">
                                                <a href="#mifwarnings"><xsl:value-of select="detailedResult/MIFValidation/ValidationCounters/NrOfValidationWarnings"/> warning(s)</a>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                No warning
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        <br/>
                                        <xsl:choose>
                                            <xsl:when test="detailedResult/MIFValidation/ValidationCounters/NrOfValidationNotes &gt; 0">
                                                <a href="#mifinfo"><xsl:value-of select="detailedResult/MIFValidation/ValidationCounters/NrOfValidationInfos"/> info(s)</a>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                No info
                                            </xsl:otherwise>
                                        </xsl:choose>

                                    </td>
                                </tr>
                            </table>
                            <xsl:if test="count(detailedResult/MIFValidation/Error) &gt; 0">
                                <p id="miferrors">Errors</p>
                                <xsl:for-each select="detailedResult/MIFValidation/Error">

                                    <table class="Error" width="98%">
                                        <tr>
                                            <td valign="top" width="100"><b>Location</b></td>
                                            <td align="left"><xsl:value-of select="@location"/> ( Line: <xsl:value-of select="@startLine"/>, Column: <xsl:value-of select="@startColumn"/>)</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><b>Description</b></td>
                                            <td align="left"><xsl:value-of select="@message"/></td>
                                        </tr>
                                    </table>
                                    <br/>
                                </xsl:for-each>
                            </xsl:if>
                            <xsl:if test="count(detailedResult/MIFValidation/Warning) &gt; 0">
                                <p id="mifwarnings">Warnings</p>
                                <xsl:for-each select="detailedResult/MIFValidation/Warning">
                                    <table class="Warning" width="98%">
                                        <tr>
                                            <td valign="top" width="100"><b>Location</b></td>
                                            <td><xsl:value-of select="@location"/> ( Line: <xsl:value-of select="@startLine"/>, Column: <xsl:value-of select="@startColumn"/>)</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><b>Description</b></td>
                                            <td><xsl:value-of select="@message"/></td>
                                        </tr>
                                    </table>
                                    <br/>
                                </xsl:for-each>
                            </xsl:if>
                            <xsl:if test="count(detailedResult/MIFValidation/Info) &gt; 0">
                                <p id="mifinfos">Infos</p>
                                <xsl:for-each select="detailedResult/MIFValidation/Info">

                                    <table class="Note" width="98%">
                                        <tr>
                                            <td valign="top" width="100"><b>Location</b></td>
                                            <td><xsl:value-of select="@location"/> ( Line: <xsl:value-of select="@startLine"/>, Column: <xsl:value-of select="@startColumn"/>)</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><b>Description</b></td>
                                            <td><xsl:value-of select="@message"/></td>
                                        </tr>
                                    </table>
                                    <br/>
                                </xsl:for-each>
                            </xsl:if>
                            <xsl:if test="count(detailedResult/MIFValidation/Problem) &gt; 0">
                                <p>Other problems</p>
                                <xsl:for-each select="detailedResult/MIFValidation/Problem">

                                    <table class="Unkown" width="98%">
                                        <tr>
                                            <td valign="top" width="100"><b>Location</b></td>
                                            <td><xsl:value-of select="@location"/> ( Line: <xsl:value-of select="@startLine"/> - Column: <xsl:value-of select="@startColumn"/>)</td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><b>Description</b></td>
                                            <td><xsl:value-of select="@message"/></td>
                                        </tr>

                                    </table>
                                    <br/>
                                </xsl:for-each>
                            </xsl:if>

                        </div>
                    </div>
                </xsl:if>
                <!-- Schematron Validation -->

                <xsl:if test="count(detailedResult/SchematronValidation) = 1">
                    <xsl:call-template name="displaySchematronReport">
                        <xsl:with-param name="showProgressBar">true</xsl:with-param>
                    </xsl:call-template>
                </xsl:if>

            </body>
        </html>
    </xsl:template>

    <xsl:template name="displaySchematronReport">
        <xsl:param name="showProgressBar">true</xsl:param>

        <xsl:if test="count(//Note) + count(//Warning) + count(//Error)+ count(//Unknown) + count(//Report) &gt; 0">

        <div class="panel panel-default" id="schematron">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a name="sch">Schematron Validation details</a>
                    <xsl:call-template name="statusLabel">
                        <xsl:with-param name="status" select="detailedResult/SchematronValidation/Result"/>
                    </xsl:call-template>
                </h3>
            </div>
            <div class="panel-body">
                <xsl:if test="$showProgressBar='true'">
                    <dl class="dl-horizontal">
                        <dt><xsl:call-template name="getLocalizedString">
                            <xsl:with-param name="key">Summary of checks</xsl:with-param>
                        </xsl:call-template>
                        </dt>
                        <dd>
                            <xsl:call-template name="ewirProgressBar">
                                <xsl:with-param name="success" select="count(//Report)"/>
                                <xsl:with-param name="info" select="count(//Note)"/>
                                <xsl:with-param name="error" select="count(//Error)"/>
                                <xsl:with-param name="warning" select="count(//Warning)"/>
                                <xsl:with-param name="unknown" select="count(//Unknown)"/>

                            </xsl:call-template>
                        </dd>
                    </dl>
                </xsl:if>

                <table>
                    <tr>
                        <td>
                            <b> <xsl:call-template name="getLocalizedString">
                                <xsl:with-param name="key" select="'Result'"/>
                            </xsl:call-template></b>
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when test="contains(detailedResult/SchematronValidation/Result, 'PASSED')">
                                    <div class="PASSED">
                                        <xsl:call-template name="getLocalizedString">
                                            <xsl:with-param name="key" select="'PASSED'"/>
                                        </xsl:call-template>
                                    </div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:choose>
                                        <xsl:when test="contains(detailedResult/SchematronValidation/Result, 'NOT PERFORMED')">
                                            <div class="ABORTED">
                                                NOT PERFORMED
                                            </div>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <div class="FAILED">
                                                FAILED
                                            </div>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <b>Summary</b>
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when test="detailedResult/SchematronValidation/ValidationCounters/NrOfChecks &gt; 0">
                                    <xsl:value-of select="detailedResult/SchematronValidation/ValidationCounters/NrOfChecks"/> check(s) performed
                                </xsl:when>
                                <xsl:otherwise>
                                    No check was performed
                                </xsl:otherwise>
                            </xsl:choose>
                            <br/>
                            <xsl:choose>
                                <xsl:when test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationErrors &gt; 0">
                                    <a href="#Errors_sch"><xsl:value-of select="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationErrors"/> error(s)</a>
                                </xsl:when>
                                <xsl:otherwise>
                                    No error
                                </xsl:otherwise>
                            </xsl:choose>
                            <br/>
                            <xsl:choose>
                                <xsl:when test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationWarnings &gt; 0">
                                    <a href="#Warnings_sch"><xsl:value-of select="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationWarnings"/> warning(s)</a>
                                </xsl:when>
                                <xsl:otherwise>
                                    No warning
                                </xsl:otherwise>
                            </xsl:choose>
                            <br/>
                            <xsl:choose>
                                <xsl:when test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationNotes &gt; 0">
                                    <a href="#Notes_sch"><xsl:value-of select="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationNotes"/> note(s)</a>
                                </xsl:when>
                                <xsl:otherwise>
                                    No note
                                </xsl:otherwise>
                            </xsl:choose>
                            <br/>
                            <xsl:choose>
                                <xsl:when test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationReports &gt; 0">
                                    <a href="#Reports_sch"><xsl:value-of select="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationReports"/> successful check(s)</a>
                                </xsl:when>
                                <xsl:otherwise>
                                    No successful check
                                </xsl:otherwise>
                            </xsl:choose>
                            <br/>
                            <xsl:choose>
                                <xsl:when test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationUnknown &gt; 0">
                                    <a href="#unknown"><xsl:value-of select="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationUnknown"/> unknown exception(s)</a>
                                </xsl:when>
                                <xsl:otherwise>
                                    No unknown exception
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                </table>
                <b>HIDE : </b>
                <input type="checkbox" onclick="hideOrUnhideSch(this)" name="Errors">Errors</input>
                <input type="checkbox" onclick="hideOrUnhideSch(this)" name="Warnings">Warnings</input>
                <input type="checkbox" onclick="hideOrUnhideSch(this)" name="Notes">Notes</input>
                <input type="checkbox" onclick="hideOrUnhideSch(this)" name="Reports">Reports</input>

                <br/>

                <xsl:if test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationErrors &gt; 0">
                    <div id="Errors_sch">
                        <span style="font-weight:bold;">Errors</span>
                        <div class="panel panel-default">
                            <div class="panel-body ">

                                <xsl:for-each select="detailedResult/SchematronValidation/Error">
                                    <xsl:call-template name="viewnotification">
                                        <xsl:with-param name="kind">dl-horizontal gzl-notification
                                            gzl-notification-red</xsl:with-param>
                                        <xsl:with-param name="type">E</xsl:with-param>
                                        <xsl:with-param name="position" select="position()"/>
                                        <xsl:with-param name="node" select="this"/>
                                        <xsl:with-param name="isSchematronValidation" select="true()"/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </div>
                        </div>
                    </div>
                </xsl:if>
                <xsl:if test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationWarnings &gt; 0">
                    <div id="Warnings_sch">
                        <span style="font-weight:bold;">Warnings</span>
                        <div class="panel panel-default">
                            <div class="panel-body ">

                                <xsl:for-each select="detailedResult/SchematronValidation/Warning">
                                    <xsl:call-template name="viewnotification">
                                        <xsl:with-param name="kind">dl-horizontal gzl-notification
                                            gzl-notification-orange</xsl:with-param>
                                        <xsl:with-param name="type">W</xsl:with-param>
                                        <xsl:with-param name="position" select="position()"/>
                                        <xsl:with-param name="node" select="this"/>
                                        <xsl:with-param name="isSchematronValidation" select="true()"/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </div>
                        </div>
                    </div>
                </xsl:if>
                <xsl:if test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationNotes &gt; 0">
                    <div id="Notes_sch">
                        <span style="font-weight:bold;">Notes</span>
                        <div class="panel panel-default">
                            <div class="panel-body ">

                                <xsl:for-each select="detailedResult/SchematronValidation/Note">
                                    <xsl:call-template name="viewnotification">
                                        <xsl:with-param name="kind">dl-horizontal gzl-notification
                                            gzl-notification-blue</xsl:with-param>
                                        <xsl:with-param name="type">R</xsl:with-param>
                                        <xsl:with-param name="position" select="position()"/>
                                        <xsl:with-param name="node" select="this"/>
                                        <xsl:with-param name="isSchematronValidation" select="true()"/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </div>
                        </div>
                    </div>

                </xsl:if>
                <xsl:if test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationUnknown &gt; 0">
                    <div id="unknown">
                        <span style="font-weight:bold;">Unkown Exceptions</span>
                        <div class="panel panel-default">
                            <div class="panel-body ">
                                <xsl:for-each select="detailedResult/SchematronValidation/Unknown">
                                    <xsl:call-template name="viewnotification">
                                        <xsl:with-param name="kind">dl-horizontal gzl-notification
                                            gzl-notification-orange</xsl:with-param>
                                        <xsl:with-param name="type">U</xsl:with-param>
                                        <xsl:with-param name="position" select="position()"/>
                                        <xsl:with-param name="node" select="this"/>
                                        <xsl:with-param name="isSchematronValidation" select="true()"/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </div>
                        </div>
                    </div>
                </xsl:if>
                <xsl:if test="detailedResult/SchematronValidation/ValidationCounters/NrOfValidationReports &gt; 0">
                    <div id="Reports_sch">
                        <span style="font-weight:bold;">Reports</span>
                        <div class="panel panel-default">
                            <div class="panel-body ">
                                <xsl:for-each select="detailedResult/SchematronValidation/Report">
                                    <xsl:call-template name="viewnotification">
                                        <xsl:with-param name="kind">dl-horizontal gzl-notification
                                            gzl-notification-green</xsl:with-param>
                                        <xsl:with-param name="type">R</xsl:with-param>
                                        <xsl:with-param name="position" select="position()"/>
                                        <xsl:with-param name="node" select="this"/>
                                        <xsl:with-param name="isSchematronValidation" select="true()"/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </div>
                        </div>
                    </div>
                </xsl:if>
            </div>
        </div>
        </xsl:if>
    </xsl:template>

    <xsl:include href="/xsl/EVSClient/common/common.xsl"/>

</xsl:stylesheet>