<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xd" version="1.0" >
    <xsl:output indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Apr 10, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Anne-Gaëlle Bergé (IHE Europe)</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>
    <xsl:template match="Summary">
        <html>
            <head>
                <title>DCCHECK Report</title>
            </head>
            <body>
                <h2>DCCHECK Report</h2>
                <br/>
                <!-- general information -->
                <div class="rich-panel">
                    <div class="rich-panel-header">General Information</div>
                    <div class="rich-panel-body">
                        <p><b>Tool name and version: </b><xsl:value-of select="Tool"/></p>
                        <p><b>Copyright: </b><xsl:value-of select="Copyright"/></p>
                        <p><b><a href="#disclaimer">Disclaimer</a></b></p>
                        <p><b>Validation Date: </b><xsl:value-of select="TestStartDate"/></p>
                        <xsl:if test="UnsupportDispCharSet/@flag > 0">
                            <p style="font-color:red">Unsupported Character Set has been encountered, this may lead to limited checking</p>    
                        </xsl:if>
                        <xsl:if test="InconsistDispCharSet/@flag > 0">
                            <p style="font-color:red">Inconsistent Character Set has been encountered, this may lead to limited checking</p>    
                        </xsl:if>
                    </div>
                </div>
                <div class="rich-panel">
                    <div class="rich-panel-header">Validation report</div>
                    <div class="rich-panel-body">
                        <!-- Client developed at IHE-Europe only validates one file at a time-->
                        <xsl:for-each select="FileReportList/FileReport">
                            <p><b>File name: </b><xsl:value-of select="FileName"/></p>
                            <xsl:if test="UnsupportDispCharSet/@flag = 1"><p style="font-color:red;">Unsupported Character Set has been encountered, this may lead to limited checking</p></xsl:if>
                            <xsl:if test="CheckingLimitation/@flag = 1"><p style="font-color:red;">SOP Class unknown</p></xsl:if>
                            <xsl:if test="ToolErrorOccured/@flag = 1"><p style="font-color:red;">A crash occurred during validation</p></xsl:if>
                            <p><b>Number of errors: </b><xsl:value-of select="NbOfError"/></p>
                            <p><b>Number of warnings: </b><xsl:value-of select="NbOfWarning"/></p>     
                            <p><b>Number of checks to be performed manually: </b><xsl:value-of select="NbOfManCheck"/></p>
                        </xsl:for-each>
                    </div>
                </div>
                <div class="rich-panel">
                    <div class="rich-panel-header">Reported errors</div>
                    <div class="rich-panel-body">
                        <xsl:apply-templates select="FileReportList/FileReport/ErrorList/Error"/>
                    </div>
                </div>
		<br/>
                <div class="rich-panel">
                    <div class="rich-panel-header">Reported warnings</div>
                    <div class="rich-panel-body">
                        <xsl:apply-templates select="FileReportList/FileReport/WarningList/Warning"/>
                    </div>
                </div>
                <div class="rich-panel">
                    <div class="rich-panel-header">Disclaimer</div>
                    <div class="rich-panel-body">
                        <a name="disclaimer"></a>
                        <xsl:value-of select="Disclaimer"/>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="StandardRef">
        <li>
            <xsl:choose>
                <xsl:when test="@std = 'DICOM' and @vers = '2011'">
                    <xsl:element name="a">
                        <xsl:attribute name="target">_blank</xsl:attribute>
                        <xsl:attribute name="href">ftp://medical.nema.org/medical/dicom/2011/11_<xsl:value-of select="substring-after(@vol, ' ')"/>pu.pdf</xsl:attribute>
                        <xsl:value-of select="@std"/><xsl:text> </xsl:text><xsl:value-of select="@vers"/><xsl:text> </xsl:text><xsl:value-of select="@vol"/><xsl:text> </xsl:text><xsl:value-of select="@refTyp"/><xsl:text> </xsl:text><xsl:value-of select="@refID"/>
                    </xsl:element>    
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@std"/><xsl:text> </xsl:text><xsl:value-of select="@vers"/><xsl:text> </xsl:text><xsl:value-of select="@vol"/><xsl:text> </xsl:text><xsl:value-of select="@refTyp"/><xsl:text> </xsl:text><xsl:value-of select="@refID"/>
                </xsl:otherwise>
            </xsl:choose>
        </li>                     
    </xsl:template>
    <xsl:template match="Error">
        <table  width="100%" style="font-size:10;" class="Error">
            <tr>
                <td  width="15%" style="font-weight:bold;" valign="top">Error type</td>
                <td valign="top"><xsl:value-of select="@type"/></td>
            </tr>       
            <!-- (FileID|DicomMessage|Tag|ModuleID)? -->            
            <xsl:choose>
                <xsl:when test="count(FileID) > 0">
                    <tr>
                        <td width="15%" style="font-weight:bold;" valign="top">File ID</td>
                        <td><xsl:value-of select="FileID"/></td>
                    </tr>
                </xsl:when>
                <xsl:when test="count(DicomMessage) > 0">
                    <tr>
                        <td width="15%" style="font-weight:bold;" valign="top">DICOM message</td>
                        <td><xsl:value-of select="DicomMessage"/> (<xsl:value-of select="DicomMessage/@sopClassUID"/>)</td>
                    </tr>
                </xsl:when>
                <xsl:when test="count(ModuleID) > 0">
                    <tr>
                        <td width="15%" style="font-weight:bold;" valign="top">Module ID</td>
                        <td><xsl:value-of select="ModuleID/@id"/></td>
                    </tr>
                </xsl:when>
                <xsl:when test="count(Tag) > 0">
                    <tr>
                        <td width="15%" style="font-weight:bold;" valign="top">Tag</td>
                        <td><xsl:value-of select="Tag"/></td>
                    </tr>
                </xsl:when>
            </xsl:choose>
            <xsl:if test="count(Name) > 0">
                <tr>
                    <td width="15%" style="font-weight:bold;" valign="top">Name</td>
                    <td><xsl:value-of select="Name"/></td>
                </tr>
            </xsl:if>
            <xsl:if test="count(Value) > 0">
                <tr>
                    <td width="15%" style="font-weight:bold;" valign="top">Value</td>
                    <td><xsl:value-of select="Value"/> <xsl:if test="Value/@truncated = 1">(extract)</xsl:if>
                        <ul>
                            <li><i>VR: <xsl:value-of select="Value/@vr"/></i></li>
                            <li><i>length: <xsl:value-of select="Value/@length"/></i></li>
                    </ul>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td width="15%" style="font-weight:bold;" valign="top">Finding</td>
                <td><xsl:value-of select="Finding"/></td>
            </tr>
            <xsl:if test="count(StandardRef) > 0">
                <tr>
                    <td width="15%" style="font-weight:bold;" valign="top">References to standard</td>
                    <td>
                        <ul>
                            <xsl:apply-templates select="StandardRef"/>
                        </ul>
                    </td>
                </tr>
            </xsl:if>
        </table>
        <br/>
    </xsl:template>
    <xsl:template match="Warning">
        <table class="warning" width="100%" style="font-size:10;">
            <tr>
                <td width="15%" style="font-weight:bold;" valign="top">Warning type</td>
                <td><xsl:value-of select="@type"/></td>
            </tr>       
            <!-- (FileID|DicomMessage|Tag|ModuleID)? -->            
            <xsl:choose>
                <xsl:when test="count(FileID) > 0">
                    <tr>
                        <td width="15%" style="font-weight:bold;" valign="top">File ID</td>
                        <td><xsl:value-of select="FileID"/></td>
                    </tr>
                </xsl:when>
                <xsl:when test="count(DicomMessage) > 0">
                    <tr>
                        <td width="15%" style="font-weight:bold;" valign="top">DICOM message</td>
                        <td><xsl:value-of select="DicomMessage"/> (<xsl:value-of select="DicomMessage/@sopClassUID"/>)</td>
                    </tr>
                </xsl:when>
                <xsl:when test="count(ModuleID) > 0">
                    <tr>
                        <td width="15%" style="font-weight:bold;" valign="top">Module ID</td>
                        <td><xsl:value-of select="ModuleID/@id"/></td>
                    </tr>
                </xsl:when>
                <xsl:when test="count(Tag) > 0">
                    <tr>
                        <td width="15%" style="font-weight:bold;" valign="top">Tag</td>
                        <td><xsl:value-of select="Tag"/></td>
                    </tr>
                </xsl:when>
            </xsl:choose>
            <xsl:if test="count(Name) > 0">
                <tr>
                    <td width="15%" style="font-weight:bold;" valign="top">Name</td>
                    <td><xsl:value-of select="Name"/></td>
                </tr>
            </xsl:if>
            <xsl:if test="count(Value) > 0">
                <tr>
                    <td width="15%" style="font-weight:bold;" valign="top">Value</td>
                    <td><xsl:value-of select="Value"/> <xsl:if test="Value/@truncated = 1">(extract)</xsl:if>
                        <ul>
                            <li><i>VR: <xsl:value-of select="Value/@vr"/></i></li>
                            <li><i>length: <xsl:value-of select="Value/@length"/></i></li>
                        </ul>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td width="15%" style="font-weight:bold;" valign="top">Finding</td>
                <td><xsl:value-of select="Finding"/></td>
            </tr>
            <xsl:if test="count(StandardRef) > 0">
                <tr>
                    <td width="15%" style="font-weight:bold;" valign="top">References to standard</td>
                    <td>
                        <ul>
                            <xsl:apply-templates select="StandardRef"/>
                        </ul>
                    </td>
                </tr>
            </xsl:if>
        </table>
        <br/>
    </xsl:template>
    
</xsl:stylesheet>
