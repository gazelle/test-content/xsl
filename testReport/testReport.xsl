<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html" media-type="text/xml" omit-xml-declaration="yes"/>
    <xsl:template match="/">
        <html lang="en">
            <head>
                <link href="http://gazelle.ihe.net/css/twitter_bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen"/>
                <link href="http://gazelle.ihe.net/css/assertion_manager.css" rel="stylesheet" type="text/css" media="screen"/>
            </head>
            
            <body class="container-fluid">
                <div class="fixed">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <a class="brand" href="http://gazelle.ihe.net">
                                <img src="http://gazelle.ihe.net/sites/default/files/logo.gif" height="20" width="60"/>
                            </a>
                            <small class="brand"><xsl:value-of select="TestReport/Tool/Name"/><xsl:text> | Test report #</xsl:text><xsl:value-of select="TestReport/TestId"/></small>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <xsl:apply-templates select="TestReport"/>
                </div>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="TestReport">
        <section class="span5 offset2">
        <h3>Tool description</h3>
        <xsl:apply-templates select="Tool"/>
        <h3>Test summary</h3>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Name</th>
                <td><xsl:value-of select="Test"/></td>
            </tr>
            <tr>
                <th>Id</th>
                <td><xsl:value-of select="TestId"/></td>
            </tr>
            <tr>
                <th>Date</th>
                <td><xsl:value-of select="Date"/></td>
            </tr>
            <tr>
                <th>Initiator</th>
                <td><xsl:value-of select="Source"/></td>
            </tr>
            <tr>
                <th>Responder</th>
                <td><xsl:value-of select="Target"/></td>
            </tr>
            <tr>
                <th>Acknowledgement Code</th>
                <td><xsl:value-of select="AcknowledgementCode"/></td>
            </tr>
            <tr>
                <th>Details</th>
                <td>
                    <xsl:element name="a">
                        <xsl:attribute name="href">
                            <xsl:value-of select="URL"/>
                        </xsl:attribute>
                        <xsl:attribute name="class">
                            <xsl:text>btn</xsl:text>
                        </xsl:attribute>
                        <xsl:text>View</xsl:text>
                    </xsl:element>
                </td>
            </tr>
        </table>
        </section>
        <section class="span5 offset2">
        <h3>Messages</h3>
        <xsl:apply-templates select="Message"/>
        </section>
    </xsl:template>
    
    <xsl:template match="Message[Type='request']">
        <h4>Request</h4>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Actor</th>
                <td><xsl:value-of select="Actor"/></td>
            </tr>
            <tr>
                <th>Message type</th>
                <td><xsl:value-of select="MessageType"/></td>
            </tr>
            <tr>
                <th>Validation result</th>
                <td> <xsl:choose>
                    <xsl:when test="ValidationResult = 'PASSED'">
                        <span class="label label-success">PASSED</span>  
                    </xsl:when>
                    <xsl:when test="ValidationResult = 'FAILED'">
                        <span class="label label-important">FAILED</span>  
                    </xsl:when>
                    <xsl:otherwise>
                        <span class="label">UNKNOWN</span>
                    </xsl:otherwise>
                </xsl:choose></td>
            </tr>
        </table>
    </xsl:template>
    
    <xsl:template match="Message[Type='response']">
        <h4>Response</h4>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Actor</th>
                <td><xsl:value-of select="Actor"/></td>
            </tr>
            <tr>
                <th>Message type</th>
                <td><xsl:value-of select="MessageType"/></td>
            </tr>
            <tr>
                <th>Validation result</th>
                <td>
                    <xsl:choose>
                        <xsl:when test="ValidationResult = 'PASSED'">
                            <span class="label label-success">PASSED</span>  
                        </xsl:when>
                        <xsl:when test="ValidationResult = 'FAILED'">
                            <span class="label label-important">FAILED</span>  
                        </xsl:when>
                        <xsl:otherwise>
                            <span class="label">UNKNOWN</span>
                        </xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <xsl:template match="Tool">
        <table class="table table-striped table-bordered">
            <tr>
                <th>Name</th>
                <td><xsl:value-of select="Name"/></td>
            </tr>
            <tr>
                <th>URL</th>
                <td><xsl:element name="a">
                    <xsl:attribute name="href">
                        <xsl:value-of select="URL"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">
                        <xsl:text>blank</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="URL"/>
                </xsl:element></td>
            </tr>
        </table>
    </xsl:template>
</xsl:stylesheet>