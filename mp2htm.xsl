<?xml version='1.0'?>
<!DOCTYPE xsl:stylesheet [
<!ENTITY CDA-Stylesheet
    '-//HL7//XSL HL7 V1.1 CDA Stylesheet: 2000-08-03//EN'>
]>
<xsl:stylesheet version='1.0'
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"

  xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output method='html' indent='yes' version='4.01'
    doctype-public='-//W3C//DTD HTML 4.01//EN'/>
  <!-- If 'true', XHTML document uses JavaScript for added
       functionality, such as pop-up windows and information-
       hiding.
       Otherwise, XHTML document does not use JavaScript. -->
  <xsl:param name="useJavaScript">true</xsl:param>


<xsl:key name="SVN-Ids" match="//comment()[contains(normalize-space(.),'$Id: ')]"
    use="count( preceding-sibling::comment() )"/>

  <xd:doc>
    <xd:desc>On matching the root, get the Subversion Id string and hand it to <xd:ref
        name="get_SVN-Id_itself" type="template"/> to be parsed, using the output thereof as the
      output of the template (which, in turn, is the output of the entire stylesheet).</xd:desc>
  </xd:doc>

  <xd:doc>
    <xd:desc>
      <xd:p>look through the input file, and find a suitable substituted Subversion Id keyword
        string</xd:p>
      <xd:p>First, try in the <tt>&lt;editionStmt></tt> with <tt>&lt;teiCorpus></tt> as root; then
        in the <tt>&lt;editionStmt></tt> with <tt>&lt;TEI></tt> as root. Last, look in
        comments.</xd:p>
    </xd:desc>
    <xd:return>The entire text node containing the substituted Subversion Id keyword string, or (if
      none was found) the keyword <xd:i>IDUNNO</xd:i>, as an xs:string</xd:return>
  </xd:doc>
  <xsl:template name="get_SVN-Id_string">
    <xsl:choose>
      <xsl:when test="//comment()[contains(normalize-space(.),'$Id: ')]">
        <xsl:value-of select="//comment()[contains(normalize-space(.),'$Id: ')][1]"/>
      </xsl:when>
      <xsl:when test="key('SVN-Ids','0')">
        <xsl:value-of select="key('SVN-Ids','0')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>IDUNNO</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xd:doc>
    <xd:desc>Take the string that contains the Subversion substituted Id keyword, and parse the
      Subversion substituted Id keyword out of it. This is done by trimming off everything before
        <code>$Id: </code> and after <code> $</code>, and then tacking those strings back on the
      front and end of the remaining string.</xd:desc>
    <xd:param><xd:i>node-string</xd:i> is the entire textual content of either the comment or
      element that was found by <xd:ref name="get_SVN-Id_string" type="template"/> as an
      xs:string</xd:param>
    <xd:return>The Subversion substituted Id keyword extracted from <xd:i>node-string</xd:i> (which
      may include other things besides the Subversion substituted Id keyword)</xd:return>
  </xd:doc>
  <xsl:template name="get_SVN-Id_itself">
    <xsl:param name="node-string"/>
    <xsl:choose>
      <xsl:when test="$node-string='IDUNNO'"/>
      <xsl:otherwise>
        <xsl:variable name="node-stringTrimStart" select="substring-after($node-string,'$Id: ')"/>
        <xsl:variable name="node-stringTrimEnd"
          select="substring-before($node-stringTrimStart,' $')"/>
        <xsl:value-of select="concat('$Id: ',$node-stringTrimEnd,' $
')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <xsl:template match='HL7v2xConformanceProfile'>
    <html>
      <head>
        <meta name='Generator' content='HL7-Message-Profile-Stylesheet;'/>
        <link rel="stylesheet" type="text/css" title="Default" href="/common/gazelle-theme.css" />
        <xsl:comment>
          do NOT edit this HTML directly, it was generated
          via an XSLT transformation from the original Level 1
          document.
        </xsl:comment>

        <script type="text/javascript">
          <xsl:text>
            var ol_shadow = 1;var ol_shadowcolor = '#222277';
            var ol_shadowopacity = 60;
          </xsl:text>
        </script>
<style>
.X {color:red; text-decoration: line-through;}
</style>
        <script type="text/javascript" src="/common/js/overlib.js" />
        <title>
          Message Profile of type
          <xsl:value-of select='@ProfileType'/>
          version
          <xsl:value-of select='@HL7Version'/>

        </title>


        <!-- Add Javascript code to make the collapseable boxes work -->
        <xsl:if test="normalize-space(translate($useJavaScript,'TRUE','true'))='true'">
          <xsl:call-template name="PrintJSCode">
            <xsl:with-param name="code">
              <xsl:call-template name="DocumentJSCode"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:if>
      </head>
      <body onload='show_hide_init();' >
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:10000;"></div>
        <div id="banner">

        </div>
        <div id="container">
          <div class="row">
            <div id="sidebar" class="col-sm-1 col-md-1 scrollspy">
              <nav id="nav-sidebar" class="gzl-sidebar">
                <ul id="nav" class="nav">
                  <li>
                    <img src="https://gazelle.ihe.net/sites/default/files/logo.gif"/>
                  </li>
                  <li>
                  <form>
                    <input type="button" value="Back" onClick="history.go(-1);return true;" class="gzl-btn" style="margin-top:20px"/>
                  </form>
                    </li>
                </ul>
              </nav>
            </div>
            <div class="col-md-11 documentation">
          <xsl:apply-templates select='HL7v2xStaticDef'>
           <xsl:with-param name="version" select="@HL7Version"/>
            <xsl:with-param name="type" select="@ProfileType"/>
          </xsl:apply-templates>
        </div>
        </div>
        <footer class="footer">
          <div class="text-center">
            <div class="row">
              <div class="col-md-offset-1 col-md-3 col-xs-12">
                <xsl:call-template name="get_SVN-Id_itself">
                  <xsl:with-param name="node-string">
                    <xsl:call-template name="get_SVN-Id_string"/>
                  </xsl:with-param>
                </xsl:call-template>
              </div>
              <div class="col-md-2 col-xs-12">
                <p class="text-muted"><a href="mailto:eric.poiseau@inria.fr">Webmaster</a></p>
              </div>
              <div class="col-md-3 col-xs-12">
                <p class="text-muted"><a href="https://gazelle.ihe.net/jira/browse/HLVAL">Report an issue</a></p>
              </div>
            </div>
          </div>
        </footer>
       </div>
      </body>
    </html>
  </xsl:template>

<xsl:template match="/">
     <xsl:apply-templates select='HL7v2xConformanceProfile'/>
  </xsl:template>

      <xsl:template  match='HL7v2xStaticDef'>
        <xsl:param name="version"/>
        <xsl:param name="type"/>
        <div class='segment'>
          <h1>
          <xsl:value-of select='@MsgType'/>
          <xsl:text>&#94;</xsl:text>
          <xsl:value-of select='@EventType'/>
            <xsl:text>&#94;</xsl:text>
            <xsl:value-of select="@MsgStructID"/>
            <xsl:text> (</xsl:text>
            <xsl:value-of select="$version"/>
            <xsl:text>)</xsl:text>
          </h1>
          <h3><xsl:value-of select="$type"/><xsl:text> HL7 Message Profile</xsl:text></h3>
          <span class="help-block"><xsl:text>Event description: </xsl:text><xsl:value-of select="@EventDesc"/></span>
          <table width="100%">
            <xsl:apply-templates select='Segment|SegGroup'/>
          </table>
        </div>
      </xsl:template>

      <xsl:template match='SegGroup'>
        <xsl:variable name="min" select="@Min"/>
        <xsl:variable name="max" select="@Max"/>
        <tr>
          <td colspan="4" >
            <div class='panel panel-info'>
              <table width="100%">
                <tr>
                  <td >
                    <xsl:if test='$min=0'> [ </xsl:if>
                    <xsl:if test='$max="*"'> { </xsl:if>
                    <xsl:if test='substring(@LongName, 4, 1) != "." or substring(@LongName, 8, 1) != "."'>
                    <strong>
                      <xsl:value-of select='@LongName'/>
                    </strong>
                      <xsl:if test='ImpNote'><i><xsl:text> (Implementation note: </xsl:text><xsl:value-of select="ImpNote"/><xsl:text>)</xsl:text></i><br/></xsl:if>
                  </xsl:if>
                </td>

              </tr>
              <xsl:apply-templates select='Segment|SegGroup'/>
              <tr>
                <td>
                  <xsl:if test='$max="*"'> } </xsl:if>
                  <xsl:if test='$min=0'> ] </xsl:if>
                </td>
              </tr>
            </table>
          </div>
        </td>
      </tr>
    </xsl:template>



      <xsl:template match='Segment'>
        <xsl:variable name="min" select="@Min"/>
        <xsl:variable name="max" select="@Max"/>
        <xsl:variable name="usage" select="@Usage"/>
        <xsl:variable name="uid" select="concat(@Name, generate-id ())"/>
        <xsl:variable name="buttonID" select="concat($uid, '_button')"/>
        <xsl:variable name='segmentnumber' select="1"/>
        <tr>
          <td class="{$usage}">
            <xsl:if test='$min=0'> [ </xsl:if>
            <xsl:if test='$max="*"'> { </xsl:if>
            <i class="control fa fa-plus-square-o" id="{$buttonID}" onclick="switchState('{$uid}'); return false;" style="display: none"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select='@Name'/>
            <xsl:if test='$max="*"'> } </xsl:if>
            <xsl:if test='$min=0'> ] </xsl:if>

            <xsl:if test='ImpNote'><i><xsl:text> (Implementation note: </xsl:text><xsl:value-of select="ImpNote"/><xsl:text>)</xsl:text></i><br/></xsl:if>
          </td>
        </tr>
        <tr>
          <td colspan="4">
            <div id="{$uid}" class="panel panel-info">
              <div class="panel-heading"><xsl:value-of select='@Name'/><xsl:text>: </xsl:text><xsl:value-of select='@LongName'/>
<xsl:if test='$usage="X"'> -- FORBIDDEN -- </xsl:if></div>
              <div class="panel-body">
              <table class="table table-striped table-hover" width="100%">
                <tr>
                  <th colspan="2">Field</th>
                  <th>Name</th>
                  <th>Datatype</th>
                  <th>Length</th>
                  <th>Usage</th>
                  <th>Card.</th>
                </tr>
                <xsl:apply-templates select='Field'/>
              </table>
              </div>
              </div>
          </td>
        </tr>

      </xsl:template>

      <xsl:template match='Component' >
        <xsl:variable name="table" select="@Table"/>
       <tr>
          <td>
            <xsl:number value="position()" format="1" />
          </td>
          <td>
            <xsl:value-of select='@Name'/>
          </td>
          <td>
            <xsl:value-of select='@Datatype'/>
          </td>
          <td>
            <xsl:value-of select='@Length'/>
          </td>
          <td>
              <xsl:value-of select='@Usage'/>
          </td>
         <td>
           <xsl:value-of select='@ConstantValue'/>
         </td>
          <td>
             <xsl:value-of select='$table'/>
        </td>
        </tr>
      </xsl:template>

      <xsl:template name='Field' match='Field'>
        <xsl:variable name="uid" select="concat(@Datatype, generate-id ())"/>
        <xsl:variable name="buttonID" select="concat($uid, '_button')"/>

        <tr>
          <xsl:if test="Component">
            <td>
              <i class="control fa fa-plus-square-o" id="{$buttonID}" onclick="switchState('{$uid}'); return false;" style="display: none"/>
            </td>
          </xsl:if>
          <xsl:if test="not(Component)">
            <td>
          </td>
        </xsl:if>
        <td>
          <xsl:number value="position()" format="1" />
          </td>
          <td>
            <xsl:value-of select='@Name'/>
          </td>
          <td>
            <xsl:value-of select='@Datatype'/>
          </td>
          <td>
            <xsl:value-of select='@Length'/>
          </td>
          <td>
            <xsl:value-of select='@Usage'/>
          </td>
          <td>
            <xsl:text>[</xsl:text>
            <xsl:value-of select='@Min'/>
            <xsl:text>..</xsl:text>
            <xsl:value-of select='@Max'/>
            <xsl:text>]</xsl:text>
          </td>
        </tr>

        <xsl:if test="Component">
          <tr>
            <td colspan="6">
              <div id="{$uid}" class="box2 panel panel-default">
                <div class="panel-heading"><xsl:text>Component details: </xsl:text><xsl:value-of select="@Datatype"/></div>
                <div class="panel-body">
                <table class="table table-striped table-hover" width="100%" >
                  <tr>
                    <th>Field</th>
                    <th>Name</th>
                    <th>Datatype</th>
                    <th>Length</th>
                    <th>Usage</th>
                    <th>ConstantValue</th>
                    <th>Table</th>
                  </tr>
                  <xsl:apply-templates select='Component'/>
                </table>
                </div>
                </div>
            </td>
          </tr>
        </xsl:if>

      </xsl:template>




      <!--
           Java Script code required by the entire HTML document.
           -->

      <xsl:template name="getSegmentName" >
        <xsl:for-each select= 'Segment'>
          <xsl:text>, </xsl:text>
          <xsl:text>'</xsl:text>
          <xsl:value-of select='@Name'/>
          <xsl:text>'</xsl:text>
        </xsl:for-each>
      </xsl:template>

      <xsl:template name="getGroupSegments" >
        <xsl:for-each select= 'Segment'>
          <xsl:text>, </xsl:text>
          <xsl:text>'</xsl:text>
          <xsl:value-of select='@Name'/>
          <xsl:text>'</xsl:text>
        </xsl:for-each>
      </xsl:template>

      <xsl:template name="scBoxes"  >
        <!-- Get all IDs of XML Instance Representation boxes
             and place them in an array. -->
        <xsl:text>
          /* IDs of XML Instance Representation boxes */
        </xsl:text>
        <xsl:text>var scBoxes = new Array('void' </xsl:text>
        <xsl:for-each select= '/descendant::*'>
          <xsl:for-each select= 'Segment|SegGroup'>
            <xsl:text>, </xsl:text>
            <xsl:text>'</xsl:text>
            <xsl:variable name="uid" select="concat(@Name, generate-id ())"/>
            <xsl:value-of select='$uid'/>
            <xsl:text>'</xsl:text>
          </xsl:for-each>
           <xsl:for-each select= 'Field'>
            <xsl:text>, </xsl:text>
            <xsl:text>'</xsl:text>
            <xsl:variable name="uid" select="concat(@Datatype, generate-id ())"/>
            <xsl:value-of select='$uid'/>
            <xsl:text>'</xsl:text>
          </xsl:for-each>
       </xsl:for-each>
        <xsl:text>);</xsl:text>
      </xsl:template>

      <xsl:template name="DocumentJSCode"  >
        <xsl:call-template name="scBoxes"/>

        <!-- Functions -->
        <xsl:text>

/**
 * Can get the ID of the button controlling
 * a collapseable box by concatenating
 * this string onto the ID of the box itself.
 */
var B_SFIX = "_button";

/**
 * Counter of documentation windows
 * Used to give each window a unique name
 */
var windowCount = 0;

  /**
  * Initialises the state of the HTML page.
  */
  function show_hide_init() {
  var obj = getElementObject("globalControls");
  if (obj != null) {
  obj.style.display="block";
  }
  obj = getElementObject("printerControls");
  if (obj != null) {
    obj.style.display="inline";
  }
  //  expandAll(xiBoxes);
  collapseAll(scBoxes);
  //  viewControlButtons(xiBoxes);
  viewControlButtons(scBoxes);
  }


/**
 * Returns an element in the current HTML document.
 *
 * @param elementID Identifier of HTML element
 * @return               HTML element object
 */
function getElementObject(elementID) {
    var elemObj = null;
    if (document.getElementById) {
        elemObj = document.getElementById(elementID);
    }
    return elemObj;
}

/**
 * Closes a collapseable box.
 *
 * @param boxObj       Collapseable box
 * @param buttonObj Button controlling box
 */
function closeBox(boxObj, buttonObj) {
  if (boxObj == null || buttonObj == null) {
     // Box or button not found
  } else {
     // Change 'display' CSS property of box
     boxObj.style.display="none";

     // Change text of button
     if (boxObj.style.display=="none") {
          buttonObj.className="control fa fa-plus-square-o";
     }
  }
}

/**
 * Opens a collapseable box.
 *
 * @param boxObj       Collapseable box
 * @param buttonObj Button controlling box
 */
function openBox(boxObj, buttonObj) {
  if (boxObj == null || buttonObj == null) {
     // Box or button not found
  } else {
     // Change 'display' CSS property of box
     boxObj.style.display="block";

     // Change text of button
     if (boxObj.style.display=="block") {
          buttonObj.className="control fa fa-minus-square-o";
     }
  }
}

/**
 * Sets the state of a collapseable box.
 *
 * @param boxID Identifier of box
 * @param open If true, box is "opened",
 *             Otherwise, box is "closed".
 */
function setState(boxID, open) {
  var boxObj = getElementObject(boxID);
  var buttonObj = getElementObject(boxID+B_SFIX);
  if (boxObj == null || buttonObj == null) {
     // Box or button not found
  } else if (open) {
     openBox(boxObj, buttonObj);
     // Make button visible
     buttonObj.style.display="inline";
  } else {
     closeBox(boxObj, buttonObj);
     // Make button visible
     buttonObj.style.display="inline";
  }
}

/**
 * Switches the state of a collapseable box, e.g.
 * if it's opened, it'll be closed, and vice versa.
 *
 * @param boxID Identifier of box
 */
function switchState(boxID) {
  var boxObj = getElementObject(boxID);
  var buttonObj = getElementObject(boxID+B_SFIX);
  if (boxObj == null || buttonObj == null) {
     // Box or button not found
  } else if (boxObj.style.display=="none") {
     // Box is closed, so open it
     openBox(boxObj, buttonObj);
  } else if (boxObj.style.display=="block") {
     // Box is opened, so close it
     closeBox(boxObj, buttonObj);
  }
}

/**
 * Closes all boxes in a given list.
 *
 * @param boxList Array of box IDs
 */
function collapseAll(boxList) {
  var idx;
  for (idx = 0; idx &lt; boxList.length; idx++) {
     var boxObj = getElementObject(boxList[idx]);
     var buttonObj = getElementObject(boxList[idx]+B_SFIX);
     closeBox(boxObj, buttonObj);
  }
}

/**
 * Open all boxes in a given list.
 *
 * @param boxList Array of box IDs
 */
function expandAll(boxList) {
  var idx;
  for (idx = 0; idx &lt; boxList.length; idx++) {
     var boxObj = getElementObject(boxList[idx]);
     var buttonObj = getElementObject(boxList[idx]+B_SFIX);
     openBox(boxObj, buttonObj);
  }
}

/**
 * Makes all the control buttons of boxes appear.
 *
 * @param boxList Array of box IDs
 */
function viewControlButtons(boxList) {
    var idx;
    for (idx = 0; idx &lt; boxList.length; idx++) {
        buttonObj = getElementObject(boxList[idx]+B_SFIX);
        if (buttonObj != null) {
            buttonObj.style.display = "inline";
        }
    }
}

/**
 * Makes all the control buttons of boxes disappear.
 *
 * @param boxList Array of box IDs
 */
function hideControlButtons(boxList) {
    var idx;
    for (idx = 0; idx &lt; boxList.length; idx++) {
        buttonObj = getElementObject(boxList[idx]+B_SFIX);
        if (buttonObj != null) {
            buttonObj.style.display = "none";
        }
    }
}

/**
 * Sets the page for either printing mode
 * or viewing mode. In printing mode, the page
 * is made to be more readable when printing it out.
 * In viewing mode, the page is more browsable.
 *
 * @param isPrinterVersion If true, display in
 *                                 printing mode; otherwise,
 *                                 in viewing mode
 */
function displayMode(isPrinterVersion) {
    var obj;
    if (isPrinterVersion) {
        // Hide global control buttons
        obj = getElementObject("globalControls");
        if (obj != null) {
            obj.style.visibility = "hidden";
        }
        // Hide Legend
        obj = getElementObject("legend");
        if (obj != null) {
            obj.style.display = "none";
        }
        obj = getElementObject("legendTOC");
        if (obj != null) {
            obj.style.display = "none";
        }
        // Hide Glossary
        obj = getElementObject("glossary");
        if (obj != null) {
            obj.style.display = "none";
        }
        obj = getElementObject("glossaryTOC");
        if (obj != null) {
            obj.style.display = "none";
        }


        // Expand all Schema Component Representation tables
        expandAll(scBoxes);

        // Hide Control buttons
        hideControlButtons(scBoxes);
    } else {
        // View global control buttons
        obj = getElementObject("globalControls");
        if (obj != null) {
            obj.style.visibility = "visible";
        }
        // View Legend
        obj = getElementObject("legend");
        if (obj != null) {
            obj.style.display = "block";
        }
        obj = getElementObject("legendTOC");
        if (obj != null) {
            obj.style.display = "block";
        }
        // View Glossary
        obj = getElementObject("glossary");
        if (obj != null) {
            obj.style.display = "block";
        }
        obj = getElementObject("glossaryTOC");
        if (obj != null) {
            obj.style.display = "block";
        }


        // Collapse all Schema Component Representation tables
        collapseAll(scBoxes);

        // View Control buttons
        viewControlButtons(scBoxes);
    }
}

/**
 * Opens up a window displaying the documentation
 * of a schema component in the XML Instance
 * Representation table.
 *
 * @param compDesc      Description of schema component
 * @param compName      Name of schema component
 * @param docTextArray Array containing the paragraphs
 *                           of the new document
 */
function viewDocumentation(compDesc, compName, docTextArray) {
  var width = 400;
  var height = 200;
  var locX = 100;
  var locY = 200;

  /* Generate content */
  var actualText = "&lt;html>";
  actualText += "&lt;head>&lt;title>";
  actualText += compDesc;
  if (compName != '') {
     actualText += ": " + compName;
  }
  actualText += "&lt;/title>&lt;/head>";
  actualText += "&lt;body bgcolor=\"#FFFFEE\">";
  // Title
  actualText += "&lt;p style=\"font-family: Arial, sans-serif; font-size: 12pt; font-weight: bold; letter-spacing:1px;\">";
  actualText += compDesc;
  if (compName != '') {
     actualText += ": &lt;span style=\"color:#006699\">" + compName + "&lt;/span>";
  }
  actualText += "&lt;/p>";
  // Documentation
  var idx;
  for (idx = 0; idx &lt; docTextArray.length; idx++) {
     actualText += "&lt;p style=\"font-family: Arial, sans-serif; font-size: 10pt;\">" + docTextArray[idx] + "&lt;/p>";
  }
  // Link to close window
  actualText += "&lt;a href=\"javascript:void(0)\" onclick=\"window.close();\" style=\"font-family: Arial, sans-serif; font-size: 8pt;\">Close&lt;/a>";
  actualText += "&lt;/body>&lt;/html>";

  /* Display window */
  windowCount++;
  var docWindow = window.open("", "documentation"+windowCount, "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable,alwaysRaised,dependent,titlebar=no,width="+width+",height="+height+",screenX="+locX+",left="+locX+",screenY="+locY+",top="+locY);
  docWindow.document.write(actualText);
}
</xsl:text>
</xsl:template>

   <!--
     Prints out JavaScript code.
     NOTE: Javascript code is placed within comments to make it
     work with current browsers. In strict XHTML, JavaScript code
     should be placed within CDATA sections. However, most
     browsers generate a syntax error if the page contains
     CDATA sections. Placing Javascript code within comments
     means that the code cannot contain two dashes.
     Param(s):
            code (Result Tree Fragment) required
                Javascript code
  -->
   <xsl:template name="PrintJSCode">
      <xsl:param name="code"/>

      <script type="text/javascript">
         <!-- If browsers start supporting CDATA sections,
              uncomment the following piece of code. -->
         <!-- <xsl:text disable-output-escaping="yes">
&lt;![CDATA[
</xsl:text> -->
         <!-- If browsers start supporting CDATA sections,
              remove the following piece of code. -->
         <xsl:text disable-output-escaping="yes">
&lt;!--
</xsl:text>

         <xsl:value-of select="$code" disable-output-escaping="yes"/>
         <!-- If browsers start supporting CDATA sections,
              remove the following piece of code. -->
         <xsl:text disable-output-escaping="yes">
// --&gt;
</xsl:text>
         <!-- If browsers start supporting CDATA sections,
              uncomment the following piece of code. -->
         <!-- <xsl:text disable-output-escaping="yes">
]]&gt;
</xsl:text> -->
      </script>
   </xsl:template>

</xsl:stylesheet>
