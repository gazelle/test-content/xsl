<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                exclude-result-prefixes="xd"
                version="1.0">

    <xsl:output method="html" media-type="text/html" encoding="UTF-8" indent="yes"/>
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jul 23, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> aberge</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>

    <xsl:param name="ShowGeneralInformation">true</xsl:param>

    <xsl:template match="repInfo">
        <html>
            <head>
                <title>PDF validation report</title>
            </head>
            <body>
                <h2>PDF Validation report</h2>
                <br/>
                <!-- general information -->
                <xsl:if test="$ShowGeneralInformation = 'true'">

                    <div class="panel panel-default">
                        <div class="panel-heading"><h3 class="panel-title">General Informations</h3></div>
                        <div class="panel-body">
                            <dl class="dl-horizontal">

                                <dt>JHOVE module</dt>
                                <dd>
                                    <xsl:value-of select="reportingModule"/> (<xsl:value-of select="reportingModule/@release"/>)

                                </dd>

                                <dt>Validation Test Status</dt>
                                <dd>
                                    <xsl:value-of select="status"/>
                                </dd>

                                <dt>Mime-type</dt>
                                <dd>
                                    <xsl:value-of select="mimeType"/>
                                </dd>
                                <xsl:if test="count(profiles) &gt; 0">
                                    <dt>PDF Modules</dt>
                                    <dd>
                                        <xsl:for-each select="profiles/profile">
                                            <li><xsl:value-of select="text()"/></li>
                                        </xsl:for-each>
                                    </dd>
                                </xsl:if>
                                <xsl:if test="count(note) &gt; 0">
                                    <dt>Note</dt>
                                    <dd><xsl:value-of select="note"/></dd>
                                </xsl:if>
                            </dl>
                        </div>

                    </div>
                </xsl:if>

                <br/>


                <!-- validation details -->
                <xsl:variable name="nbOfErrors" select="count(messages/message[@severity='error'])"/>
                <xsl:variable name="nbOfInfos" select="count(messages/message[@severity='info'])"/>
                <xsl:variable name="nbOfWarnings" select="count(messages/message[@severity='warning'])"/>

                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Validation counters</h3></div>
                    <div class="panel-body">
                                                <ul>
                            <li>
                                <xsl:choose>
                                    <xsl:when test="$nbOfErrors &gt; 0">
                                        <a href="#Errors_p">
                                            <xsl:value-of select="$nbOfErrors"/>
                                            error(s) </a>
                                    </xsl:when>
                                    <xsl:otherwise> No error </xsl:otherwise>
                                </xsl:choose>
                            </li>
                            <li>
                                <xsl:choose>
                                    <xsl:when test="$nbOfWarnings &gt; 0">

                                        <a href="#Warnings_p">
                                            <xsl:value-of select="$nbOfWarnings"/>
                                            warning(s) </a>
                                    </xsl:when>
                                    <xsl:otherwise> No warning </xsl:otherwise>
                                </xsl:choose>
                            </li>
                            <li>
                                <xsl:choose>
                                    <xsl:when test="$nbOfInfos &gt; 0">
                                        <a href="#Reports_p">
                                            <xsl:value-of select="$nbOfInfos"/>
                                            report(s) </a>
                                    </xsl:when>
                                    <xsl:otherwise> No report </xsl:otherwise>
                                </xsl:choose>
                            </li>
                        </ul>
                    </div>
                </div>

                <br/>
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Validation details</h3></div>
                    <div class="panel-body">


                        <xsl:if test="$nbOfErrors &gt; 0">

                            <span style="font-weight:bold;">Errors </span>

                            <div id="Errors_p">

                                <div class="panel panel-default">
                                    <div class="panel-body ">
                                        <xsl:for-each select="messages/message[@severity='error']">
                                                <xsl:call-template name="viewnotification">
                                                    <xsl:with-param name="kind">dl-horizontal gzl-notification
                                                        gzl-notification-red</xsl:with-param>
                                                </xsl:call-template>
                                        </xsl:for-each>
                                    </div>
                                </div>
                            </div>

                        </xsl:if>
                        <xsl:if test="$nbOfWarnings &gt; 0">
                            <span style="font-weight:bold;">Warnings </span>


                            <div id="Warnings_p">
                                <div class="panel panel-default">
                                    <div class="panel-body ">
                                        <xsl:for-each select="messages/message[@severity='warning']">
                                                <xsl:call-template name="viewnotification">
                                                    <xsl:with-param name="kind">dl-horizontal gzl-notification
                                                        gzl-notification-orange</xsl:with-param>
                                                </xsl:call-template>

                                        </xsl:for-each>
                                    </div>
                                </div>
                            </div>
                        </xsl:if>
                        <xsl:if test="$nbOfInfos &gt; 0">
                            <span style="font-weight:bold;">Report </span>


                            <div id="Warnings_p">
                                <div class="panel panel-default">
                                    <div class="panel-body ">
                                        <xsl:for-each select="messages/message[@severity='info']">
                                            <xsl:call-template name="viewnotification">
                                                <xsl:with-param name="kind">dl-horizontal gzl-notification
                                                    gzl-notification-green</xsl:with-param>
                                            </xsl:call-template>

                                        </xsl:for-each>
                                    </div>
                                </div>
                            </div>
                        </xsl:if>
                    </div>
                </div>



            </body>
        </html>

    </xsl:template>


    <xsl:template name="viewnotification">
        <xsl:param name="kind"/>

        <dl>
            <xsl:attribute name="class">
                <xsl:value-of select="$kind"/>
            </xsl:attribute>

            <dt>Location</dt>
            <dd>
                <xsl:value-of select="message"/>
                <xsl:if test="count(@offset) &gt; 0">
                    <xsl:text>Offset: </xsl:text><xsl:value-of select="@offset"/>
                </xsl:if>
            </dd>


            <dt>Description</dt>
            <dd>
                <xsl:if test="count(@subMessage) &gt; 0">
                    <xsl:value-of select="@subMessage"/>
                    <br/>
                </xsl:if>
                <xsl:value-of select="text()"/>
            </dd>

        </dl>

    </xsl:template>
</xsl:stylesheet>
