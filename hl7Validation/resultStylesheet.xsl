<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="html"
				media-type="text/html" />
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
		<xd:desc>
			<xd:p>
				<xd:b>Created on:</xd:b>
				Jul 5, 2010
			</xd:p>
			<xd:p>
				<xd:b>Modified on:</xd:b>
				Feb 11, 2011
			</xd:p>
			<xd:p>
				<xd:b>Modified on:</xd:b>
				Jun 29, 2012
			</xd:p>
			<xd:p>
				<xd:b>Modified on:</xd:b>
				Oct 8, 2013
			</xd:p>
			<xd:p>
				<xd:b>Author:</xd:b>
				Anne-Gaëlle BERGE, IHE Development, INRIA Rennes
			</xd:p>
			<xd:p></xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="/">
		<xsl:variable name="limitDisplayTo">
			50
		</xsl:variable>
		<html>
			<head>
				<link rel="stylesheet" type="text/css"
					  href="https://gazelle.ihe.net/xsl/hl7Validation/resultStyle.css" />
			</head>
			<h2>External Validation Report</h2>
			<br />
			<xsl:if
					test="count(SummaryResults/ValidationResultsOverview/Disclaimer) = 1">
				<div class="rich-panel">
					<div class="rich-panel-header">Disclaimer</div>
					<div class="rich-panel-body">
						<xsl:value-of
								select="SummaryResults/ValidationResultsOverview/Disclaimer" />
					</div>
				</div>
				<br />
			</xsl:if>
			<div class="rich-panel">
				<div class="rich-panel-header">General Information</div>
				<div class="rich-panel-body">
					<table border="0">
						<tr>
							<td>
								<b>Validation Date</b>
							</td>
							<td>
								<xsl:value-of
										select="SummaryResults/ValidationResultsOverview/ValidationDate" />
								-
								<xsl:value-of
										select="/SummaryResults/ValidationResultsOverview/ValidationTime" />
							</td>
						</tr>
						<tr>
							<td>
								<b>Validation Service</b>
							</td>
							<td>
								<xsl:value-of
										select="SummaryResults/ValidationResultsOverview/ValidationServiceName" />
								(
								<xsl:value-of
										select="SummaryResults/ValidationResultsOverview/ValidationServiceVersion" />
								)
							</td>
						</tr>
						<xsl:if test="count(SummaryResults/ValidationResultsOverview/Oid) = 1">
							<tr>
								<td>
									<b>Message OID</b>
								</td>
								<td>
									<xsl:value-of select="SummaryResults/ValidationResultsOverview/Oid" />
								</td>
							</tr>
						</xsl:if>
						<xsl:if
								test="count(SummaryResults/ValidationResultsOverview/MessageOID) = 1">
							<tr>
								<td>
									<b>Message OID</b>
								</td>
								<td>
									<xsl:value-of
											select="SummaryResults/ValidationResultsOverview/MessageOID" />
								</td>
							</tr>
						</xsl:if>
						<xsl:if
								test="count(SummaryResults/ValidationResultsOverview/ProfileOID) = 1">
							<tr>
								<td>
									<b>Profile OID</b>
								</td>
								<td>
									<xsl:value-of
											select="SummaryResults/ValidationResultsOverview/ProfileOID" />
									<xsl:if
											test="SummaryResults/ValidationResultsOverview/ProfileRevision">
										<xsl:text> (Revision: </xsl:text>
										<xsl:value-of
												select="SummaryResults/ValidationResultsOverview/ProfileRevision" />
										<xsl:text>)</xsl:text>
									</xsl:if>
								</td>
							</tr>
						</xsl:if>
						<tr>
							<td>
								<b>Validation Test Status</b>
							</td>
							<td>
								<xsl:if
										test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
									<div class="PASSED">
										<xsl:value-of
												select="SummaryResults/ValidationResultsOverview/ValidationTestResult" />
									</div>
								</xsl:if>
								<xsl:if
										test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
									<div class="FAILED">
										<xsl:value-of
												select="SummaryResults/ValidationResultsOverview/ValidationTestResult" />
									</div>
								</xsl:if>
								<xsl:if
										test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
									<div class="ABORTED">
										<xsl:value-of
												select="SummaryResults/ValidationResultsOverview/ValidationTestResult" />
									</div>
								</xsl:if>
								<xsl:if
										test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'INVALID REQUEST')">
									<div class="ABORTED">
										<xsl:value-of
												select="SummaryResults/ValidationResultsOverview/ValidationTestResult" />
									</div>
								</xsl:if>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<br />
			<xsl:if test="SummaryResults/Resources">
				<div class="rich-panel">
					<div class="rich-panel-header">Resources</div>
					<div class="rich-panel-body">
						<p>Find below the list of HL7 tables which have been used to
							validate the coded values of the message</p>
						<ul>
							<xsl:for-each select="SummaryResults/Resources/Resource">
								<li><xsl:value-of select="oid"/><xsl:text> (Revision: </xsl:text><xsl:value-of select="revision"/><xsl:text>)</xsl:text></li>
							</xsl:for-each>
						</ul>
					</div>
				</div>
			</xsl:if>
			<xsl:if
					test="count(SummaryResults/ValidationResultsOverview/ReferencedStandard) = 1">
				<div class="rich-panel">
					<div class="rich-panel-header">Referenced Standard</div>
					<div class="rich-panel-body">
						<table border="0">
							<tr>
								<td>
									<b>Name</b>
								</td>
								<td>
									<xsl:value-of
											select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardName" />
								</td>
							</tr>
							<tr>
								<td>
									<b>Version</b>
								</td>
								<td>
									<xsl:value-of
											select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardVersion" />
								</td>
							</tr>
							<tr>
								<td>
									<b>Extension</b>
								</td>
								<td>
									<xsl:value-of
											select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardExtension" />
								</td>
							</tr>
						</table>
					</div>
				</div>
				<br />
			</xsl:if>

			<br />
			<div class="rich-panel">
				<div class="rich-panel-header">Validation counters</div>
				<div class="rich-panel-body">
					<xsl:choose>
						<xsl:when
								test="SummaryResults/ValidationCounters/NrOfValidationErrors &gt; 0">
							<a href="#errors">
								<xsl:value-of
										select="SummaryResults/ValidationCounters/NrOfValidationErrors" />
								error(s)
							</a>
						</xsl:when>
						<xsl:otherwise>
							No error
						</xsl:otherwise>
					</xsl:choose>
					<br />
					<xsl:choose>
						<xsl:when
								test="SummaryResults/ValidationCounters/NrOfValidationWarnings &gt; 0">
							<a href="#warnings">
								<xsl:value-of
										select="SummaryResults/ValidationCounters/NrOfValidationWarnings" />
								warning(s)
							</a>
						</xsl:when>
						<xsl:otherwise>
							No warning
						</xsl:otherwise>
					</xsl:choose>
					<br />
					<xsl:choose>
						<xsl:when
								test="SummaryResults/ValidationCounters/NrOfValidatedAssertions &gt; 0">
							<a href="#reports">
								<xsl:value-of
										select="SummaryResults/ValidationCounters/NrOfValidatedAssertions" />
								report(s)
							</a>
						</xsl:when>
						<xsl:otherwise>
							No report
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</div>
			<br />
			<div class="rich-panel">
				<div class="rich-panel-header">Validation details</div>
				<div class="rich-panel-body">
					<xsl:if
							test="SummaryResults/ValidationCounters/NrOfValidationErrors &gt; 0">
						<xsl:choose>
							<xsl:when
									test="count(SummaryResults/ValidationResults/ResultXML/Error) &gt;= $limitDisplayTo">
								<p id="errors">
									<b>
										Errors
										<span class="FAILED">
											(only first
											<xsl:value-of select="$limitDisplayTo" />
											th displayed)
										</span>
									</b>
								</p>
							</xsl:when>
							<xsl:otherwise>
								<p id="errors">
									<b>Errors</b>
								</p>
							</xsl:otherwise>
						</xsl:choose>

						<xsl:for-each
								select="SummaryResults/ValidationResults/ResultXML/Error[position() &lt;= $limitDisplayTo]">
							<table class="errortwo">
								<xsl:if test="count(Test) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Test</b>
										</td>
										<td>
											<xsl:value-of select="Test" />
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="count(Type) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Type</b>
										</td>
										<td>
											<xsl:value-of select="Type" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Location</b>
									</td>
									<td>
										<xsl:value-of select="Location" />
									</td>
								</tr>
								<xsl:if test="count(Value) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Value</b>
										</td>
										<td>
											<xsl:value-of select="Value" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Description</b>
									</td>
									<td>
										<xsl:value-of select="Description" />
									</td>
								</tr>
								<xsl:if test="count(HL7Table) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>HL7 Table</b>
										</td>
										<td>
											<xsl:element name="a">
												<xsl:attribute name="href">
													<xsl:text>http://gazelle.ihe.net/GazelleHL7v2Validator/viewResource.seam?oid=</xsl:text>
													<xsl:value-of
															select="HL7Table" />
												</xsl:attribute>
												<xsl:attribute name="target">blank</xsl:attribute>
												<xsl:value-of select="HL7Table" />
											</xsl:element>
										</td>
									</tr>
								</xsl:if>
							</table>
							<br />
						</xsl:for-each>
						<xsl:for-each
								select="SummaryResults/ValidationResults/Error[position() &lt;= $limitDisplayTo]">
							<table class="errortwo">
								<xsl:if test="count(Test) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Test</b>
										</td>
										<td>
											<xsl:value-of select="Test" />
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="count(Type) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Type</b>
										</td>
										<td>
											<xsl:value-of select="Type" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Location</b>
									</td>
									<td>
										<xsl:value-of select="Location" />
									</td>
								</tr>
								<xsl:if test="count(Value) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Value</b>
										</td>
										<td>
											<xsl:value-of select="Value" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Description</b>
									</td>
									<td>
										<xsl:value-of select="Description" />
									</td>
								</tr>
								<xsl:if test="count(HL7Table) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>HL7 Table</b>
										</td>
										<td>
											<xsl:element name="a">
												<xsl:attribute name="href">
													<xsl:text>http://gazelle.ihe.net/GazelleHL7v2Validator/viewResource.seam?oid=</xsl:text>
													<xsl:value-of
															select="HL7Table" />
												</xsl:attribute>
												<xsl:attribute name="target">blank</xsl:attribute>
												<xsl:value-of select="HL7Table" />
											</xsl:element>
										</td>
									</tr>
								</xsl:if>
							</table>
							<br />
						</xsl:for-each>
					</xsl:if>
					<xsl:if
							test="SummaryResults/ValidationCounters/NrOfValidationWarnings &gt; 0">
						<xsl:choose>
							<xsl:when
									test="count(SummaryResults/ValidationResults/ResultXML/Warning) &gt;= $limitDisplayTo">
								<p id="warnings">
									<b>
										Warnings
										<span class="FAILED">
											(only first
											<xsl:value-of select="$limitDisplayTo" />
											th displayed)
										</span>
									</b>
								</p>
							</xsl:when>
							<xsl:otherwise>
								<p id="warnings">
									<b>Warnings</b>
								</p>
							</xsl:otherwise>
						</xsl:choose>

						<xsl:for-each
								select="SummaryResults/ValidationResults/ResultXML/Warning[position() &lt;= $limitDisplayTo]">
							<table class="warning">
								<xsl:if test="count(Test) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Test</b>
										</td>
										<td>
											<xsl:value-of select="Test" />
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="count(Type) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Type</b>
										</td>
										<td>
											<xsl:value-of select="Type" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Location</b>
									</td>
									<td>
										<xsl:value-of select="Location" />
									</td>
								</tr>
								<xsl:if test="count(Value) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Value</b>
										</td>
										<td>
											<xsl:value-of select="Value" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Description</b>
									</td>
									<td>
										<xsl:value-of select="Description" />
									</td>
								</tr>
								<xsl:if test="count(HL7Table) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>HL7 Table</b>
										</td>
										<td>
											<xsl:element name="a">
												<xsl:attribute name="href">
													<xsl:text>http://gazelle.ihe.net/GazelleHL7v2Validator/viewResource.seam?oid=</xsl:text>
													<xsl:value-of
															select="HL7Table" />
												</xsl:attribute>
												<xsl:attribute name="target">blank</xsl:attribute>
												<xsl:value-of select="HL7Table" />
											</xsl:element>
										</td>
									</tr>
								</xsl:if>
							</table>
							<br />
						</xsl:for-each>
						<xsl:for-each
								select="SummaryResults/ValidationResults/Warning[position() &lt;= $limitDisplayTo]">
							<table class="warning">
								<xsl:if test="count(Test) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Test</b>
										</td>
										<td>
											<xsl:value-of select="Test" />
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="count(Type) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Type</b>
										</td>
										<td>
											<xsl:value-of select="Type" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Location</b>
									</td>
									<td>
										<xsl:value-of select="Location" />
									</td>
								</tr>
								<xsl:if test="count(Value) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Value</b>
										</td>
										<td>
											<xsl:value-of select="Value" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Description</b>
									</td>
									<td>
										<xsl:value-of select="Description" />
									</td>
								</tr>
								<xsl:if test="count(HL7Table) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>HL7 Table</b>
										</td>
										<td>
											<xsl:element name="a">
												<xsl:attribute name="href">
													<xsl:text>http://gazelle.ihe.net/GazelleHL7v2Validator/viewResource.seam?oid=</xsl:text>
													<xsl:value-of
															select="HL7Table" />
												</xsl:attribute>
												<xsl:attribute name="target">blank</xsl:attribute>
												<xsl:value-of select="HL7Table" />
											</xsl:element>
										</td>
									</tr>
								</xsl:if>
							</table>
							<br />
						</xsl:for-each>
					</xsl:if>
					<xsl:if
							test="SummaryResults/ValidationCounters/NrOfValidationConditions &gt; 0">
						<p id="conditions">
							<b>Conditions</b>
						</p>
						<xsl:for-each
								select="SummaryResults/ValidationResults/ResultXML/Condition">
							<table class="note">
								<xsl:if test="count(Test) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Test</b>
										</td>
										<td>
											<xsl:value-of select="Test" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Location</b>
									</td>
									<td>
										<xsl:value-of select="Location" />
									</td>
								</tr>
								<tr>
									<td width="100" valign="top">
										<b>Description</b>
									</td>
									<td>
										<xsl:value-of select="Description" />
									</td>
								</tr>
							</table>
							<br />
						</xsl:for-each>
					</xsl:if>
					<xsl:if
							test="SummaryResults/ValidationCounters/NrOfValidationUnknown &gt; 0">
						<p id="unknown">
							<b>Unknown exceptions</b>
						</p>
						<xsl:for-each select="SummaryResults/ValidationResults/ResultXML/Unknown">
							<table class="unknown">
								<xsl:if test="count(Test) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Test</b>
										</td>
										<td>
											<xsl:value-of select="Test" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Location</b>
									</td>
									<td>
										<xsl:value-of select="Location" />
									</td>
								</tr>
								<tr>
									<td width="100" valign="top">
										<b>Description</b>
									</td>
									<td>
										<xsl:value-of select="Description" />
									</td>
								</tr>
							</table>
							<br />
						</xsl:for-each>
					</xsl:if>
					<xsl:if
							test="SummaryResults/ValidationCounters/NrOfValidatedAssertions &gt; 0">
						<xsl:choose>
							<xsl:when
									test="count(SummaryResults/ValidationResults/Report) &gt;= $limitDisplayTo">
								<p id="reports">
									<b>
										Report
										<span class="FAILED">
											(only first
											<xsl:value-of select="$limitDisplayTo" />
											th displayed)
										</span>
									</b>
								</p>
							</xsl:when>
							<xsl:otherwise>
								<p id="reports">
									<b>Reports</b>
								</p>
							</xsl:otherwise>
						</xsl:choose>

						<xsl:for-each
								select="SummaryResults/ValidationResults/Report[position() &lt;= $limitDisplayTo]">
							<table class="report">
								<xsl:if test="count(Test) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Test</b>
										</td>
										<td>
											<xsl:value-of select="Test" />
										</td>
									</tr>
								</xsl:if>
								<xsl:if test="count(Type) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Type</b>
										</td>
										<td>
											<xsl:value-of select="Type" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Location</b>
									</td>
									<td>
										<xsl:value-of select="Location" />
									</td>
								</tr>
								<xsl:if test="count(Value) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>Value</b>
										</td>
										<td>
											<xsl:value-of select="Value" />
										</td>
									</tr>
								</xsl:if>
								<tr>
									<td width="100" valign="top">
										<b>Description</b>
									</td>
									<td>
										<xsl:value-of select="Description" />
									</td>
								</tr>
								<xsl:if test="count(HL7Table) &gt; 0">
									<tr>
										<td width="100" valign="top">
											<b>HL7 Table</b>
										</td>
										<td>
											<xsl:element name="a">
												<xsl:attribute name="href">
													<xsl:text>http://gazelle.ihe.net/GazelleHL7v2Validator/viewResource.seam?oid=</xsl:text>
													<xsl:value-of
															select="HL7Table" />
												</xsl:attribute>
												<xsl:attribute name="target">blank</xsl:attribute>
												<xsl:value-of select="HL7Table" />
											</xsl:element>
										</td>
									</tr>
								</xsl:if>
							</table>
							<br />
						</xsl:for-each>
					</xsl:if>
				</div>
			</div>
			<br />
			<xsl:if
					test="count(SummaryResults/ValidationResults/ResultXML/ProfileExceptions) &gt; 0">
				<div class="rich-panel">
					<div class="rich-panel-header">Profile exceptions</div>
					<div class="rich-panel-body">
						<p>Those profile exceptions have been raised because the HL7
							message profile, on some points, differs from the Java class
							representing the HL7 message. Those are not errors in the given
							message but may cause (partially) wrong validation results</p>
						<ul>
							<xsl:for-each
									select="SummaryResults/ValidationResults/ResultXML/ProfileExceptions/Exception">
								<li>
									<xsl:value-of select="text()" />
								</li>
							</xsl:for-each>
						</ul>

					</div>
				</div>
			</xsl:if>
			<xsl:if
					test="count(SummaryResults/ValidationResults/ProfileException) &gt; 0">
				<div class="rich-panel">
					<div class="rich-panel-header">Profile exceptions</div>
					<div class="rich-panel-body">
						<p>Those profile exceptions have been raised because the HL7
							message profile, on some points, differs from the Java class
							representing the HL7 message. Those are not errors in the given
							message but may cause (partially) wrong validation results</p>
						<ul>
							<xsl:for-each
									select="SummaryResults/ValidationResults/ProfileException">
								<li>
									<xsl:value-of select="Description" />
								</li>
							</xsl:for-each>
						</ul>

					</div>
				</div>
			</xsl:if>
		</html>
	</xsl:template>
</xsl:stylesheet>
