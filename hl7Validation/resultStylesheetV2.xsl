<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xsl:template match="/">
        <xsl:variable name="limitDisplayTo">50</xsl:variable>
        <h2>External Validation Report</h2>
        <br/>
        <xsl:if test="count(SummaryResults/ValidationResultsOverview/Disclaimer) = 1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Disclaimer</h3>
                </div>
                <div class="panel-body">
                    <xsl:value-of select="SummaryResults/ValidationResultsOverview/Disclaimer"/>
                </div>
            </div>
            <br/>
        </xsl:if>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">General Informations</h3>
            </div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <div>
                        <dt>Validation Date</dt>
                        <dd>
                            <xsl:value-of select="SummaryResults/ValidationResultsOverview/ValidationDate"/> -
                            <xsl:value-of select="/SummaryResults/ValidationResultsOverview/ValidationTime"
                            />
                        </dd>
                    </div>
                    <div>
                        <dt>Validation Service</dt>
                        <dd>
                            <xsl:value-of
                                    select="SummaryResults/ValidationResultsOverview/ValidationServiceName"/> (
                            <xsl:value-of
                                    select="SummaryResults/ValidationResultsOverview/ValidationServiceVersion"/> )
                        </dd>
                    </div>

                    <xsl:if test="count(SummaryResults/ValidationResultsOverview/Oid) = 1">
                        <div>
                            <dt>Message OID</dt>
                            <dd>
                                <xsl:value-of select="SummaryResults/ValidationResultsOverview/Oid"/>
                            </dd>
                        </div>

                    </xsl:if>
                    <xsl:if test="count(SummaryResults/ValidationResultsOverview/MessageOID) = 1">
                        <div>
                            <dt>Message OID</dt>
                            <dd>
                                <xsl:value-of select="SummaryResults/ValidationResultsOverview/MessageOID"/>
                            </dd>
                        </div>

                    </xsl:if>

                    <xsl:if test="count(SummaryResults/ValidationResultsOverview/ProfileOID) = 1">
                        <div>
                            <dt>Profile OID</dt>
                            <dd>
                                <xsl:value-of select="SummaryResults/ValidationResultsOverview/ProfileOID"/>
                                <xsl:if test="SummaryResults/ValidationResultsOverview/ProfileRevision">
                                    <xsl:text> (Revision: </xsl:text>
                                    <xsl:value-of
                                            select="SummaryResults/ValidationResultsOverview/ProfileRevision"/>
                                    <xsl:text>)</xsl:text>
                                </xsl:if>
                            </dd>
                        </div>

                    </xsl:if>


                    <div>
                        <dt>Validation Test Status</dt>
                        <dd>
                            <xsl:if
                                    test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                <div class="PASSED">
                                    <xsl:value-of
                                            select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
                                </div>
                            </xsl:if>
                            <xsl:if
                                    test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                <div class="FAILED">
                                    <xsl:value-of
                                            select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
                                </div>
                            </xsl:if>
                            <xsl:if
                                    test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                                <div class="ABORTED">
                                    <xsl:value-of
                                            select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
                                </div>
                            </xsl:if>
                            <xsl:if
                                    test="contains(SummaryResults/ValidationResultsOverview/ValidationTestResult, 'INVALID REQUEST')">
                                <div class="ABORTED">
                                    <xsl:value-of
                                            select="SummaryResults/ValidationResultsOverview/ValidationTestResult"/>
                                </div>
                            </xsl:if>
                        </dd>
                    </div>

                </dl>

            </div>
        </div>

        <xsl:if test="SummaryResults/Resources">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Resources</h3>
                </div>

                <div class="panel-body">


                    <p>Find below the list of HL7 tables which have been used to validate the coded values of the
                        message
                    </p>
                    <ul>
                        <xsl:for-each select="SummaryResults/Resources/Resource">
                            <li>
                                <xsl:value-of select="oid"/>
                                <xsl:text> (Revision: </xsl:text>
                                <xsl:value-of select="revision"/>
                                <xsl:text>)</xsl:text>
                            </li>
                        </xsl:for-each>
                    </ul>
                </div>
            </div>
        </xsl:if>
        <xsl:if test="count(SummaryResults/ValidationResultsOverview/ReferencedStandard) = 1">

            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title">Referenced Standard</h3>
                </div>
                <div class="panel-body">

                    <table border="0">
                        <tr>
                            <td>
                                <b>Name</b>
                            </td>
                            <td>
                                <xsl:value-of
                                        select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardName"
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Version</b>
                            </td>
                            <td>
                                <xsl:value-of
                                        select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardVersion"
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Extension</b>
                            </td>
                            <td>
                                <xsl:value-of
                                        select="SummaryResults/ValidationResultsOverview/ReferencedStandard/StandardExtension"
                                />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </xsl:if>

        <xsl:variable name="nbError">
            <xsl:call-template name="maximun">
                <xsl:with-param name="pSequence"
                                select="SummaryResults/ValidationCounters/NrOfValidationErrors | SummaryResults/ValidationResults/errorCounter"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="nbWarning">
            <xsl:call-template name="maximun">
                <xsl:with-param name="pSequence"
                                select="SummaryResults/ValidationCounters/NrOfValidationWarnings | SummaryResults/ValidationResults/warningCounter"/>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="nbReport">
            <xsl:call-template name="maximun">
                <xsl:with-param name="pSequence"
                                select="SummaryResults/ValidationCounters/NrOfValidatedAssertions | SummaryResults/ValidationResults/reportCounter"/>
            </xsl:call-template>
        </xsl:variable>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Validation counters</h3>
            </div>
            <div class="panel-body">
                <ul>
                    <li>

                        <xsl:choose>
                            <xsl:when test="$nbError &gt; 0">
                                <a href="#Errors_p">
                                    <xsl:value-of select="$nbError"/>
                                    error(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>No error</xsl:otherwise>
                        </xsl:choose>
                    </li>
                    <li>


                        <xsl:choose>
                            <xsl:when test="$nbWarning &gt; 0">
                                <a href="#Warnings_p">
                                    <xsl:value-of select="$nbWarning"
                                    /> warning(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>No warning</xsl:otherwise>
                        </xsl:choose>
                    </li>
                    <li>
                        <xsl:choose>
                            <xsl:when test="$nbReport &gt; 0">
                                <a href="#Reports_p">
                                    <xsl:value-of select="$nbReport"
                                    /> report(s)
                                </a>
                            </xsl:when>
                            <xsl:otherwise>No report</xsl:otherwise>
                        </xsl:choose>
                    </li>
                </ul>
            </div>
        </div>
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Validation details</h3>
            </div>
            <div class="panel-body">


                <xsl:if test="$nbError &gt; 0">
                    <xsl:choose>
                        <xsl:when test="count(SummaryResults/ValidationResults/Error) &gt;= $limitDisplayTo">
                            <span style="font-weight:bold;">Errors (only first
                                <xsl:value-of
                                        select="$limitDisplayTo"/> th displayed)
                            </span>

                        </xsl:when>
                        <xsl:otherwise>
                            <span style="font-weight:bold;">Errors</span>
                        </xsl:otherwise>
                    </xsl:choose>

                    <div id="Errors_p">

                        <div class="panel panel-default">
                            <div class="panel-body ">
                                <xsl:for-each
                                        select="SummaryResults/ValidationResults/Error[position() &lt;= $limitDisplayTo]">
                                    <xsl:call-template name="viewnotification">
                                        <xsl:with-param name="kind">dl-horizontal gzl-notification
                                            gzl-notification-red
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </div>
                        </div>
                    </div>

                </xsl:if>
                <xsl:if test="$nbWarning &gt; 0">
                    <xsl:choose>
                        <xsl:when
                                test="count(SummaryResults/ValidationResults/ResultXML/Warning) &gt;= $limitDisplayTo">
                            <p id="warnings">
                                <b>Warnings
                                    <span class="gzl-label gzl-label-orange">(only first
                                        <xsl:value-of
                                                select="$limitDisplayTo"/> th displayed)
                                    </span>
                                </b>
                            </p>
                        </xsl:when>
                        <xsl:otherwise>
                            <p id="warnings">
                                <b>Warnings</b>
                            </p>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:for-each
                            select="SummaryResults/ValidationResults/ResultXML/Warning[position() &lt;= $limitDisplayTo]">
                        <table class="warning">
                            <xsl:if test="count(Test) &gt; 0">
                                <tr>
                                    <td width="100" valign="top">
                                        <b>Test</b>
                                    </td>
                                    <td>
                                        <xsl:value-of select="Test"/>
                                    </td>
                                </tr>
                            </xsl:if>
                            <xsl:if test="count(Type) &gt; 0">
                                <tr>
                                    <td width="100" valign="top">
                                        <b>Type</b>
                                    </td>
                                    <td>
                                        <xsl:value-of select="Type"/>
                                    </td>
                                </tr>
                            </xsl:if>
                            <tr>
                                <td width="100" valign="top">
                                    <b>Location</b>
                                </td>
                                <td>
                                    <xsl:value-of select="Location"/>
                                </td>
                            </tr>
                            <xsl:if test="count(Value) &gt; 0">
                                <tr>
                                    <td width="100" valign="top">
                                        <b>Value</b>
                                    </td>
                                    <td>
                                        <xsl:value-of select="Value"/>
                                    </td>
                                </tr>
                            </xsl:if>
                            <tr>
                                <td width="100" valign="top">
                                    <b>Description</b>
                                </td>
                                <td>
                                    <xsl:value-of select="Description"/>
                                </td>
                            </tr>
                            <xsl:if test="count(HL7Table) &gt; 0">
                                <tr>
                                    <td width="100" valign="top">
                                        <b>HL7 Table</b>
                                    </td>
                                    <td>
                                        <xsl:element name="a">
                                            <xsl:attribute name="href">
                                                <xsl:text>http://gazelle.ihe.net/GazelleHL7v2Validator/viewResource.seam?oid=</xsl:text>
                                                <xsl:value-of select="HL7Table"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="target">blank</xsl:attribute>
                                            <xsl:value-of select="HL7Table"/>
                                        </xsl:element>
                                    </td>
                                </tr>
                            </xsl:if>
                        </table>
                        <br/>
                    </xsl:for-each>
                    <div id="Warnings_p">
                        <div class="panel panel-default">
                            <div class="panel-body ">
                                <xsl:for-each
                                        select="SummaryResults/ValidationResults/Warning[position() &lt;= $limitDisplayTo]">
                                    <xsl:call-template name="viewnotification">
                                        <xsl:with-param name="kind">dl-horizontal gzl-notification
                                            gzl-notification-orange
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </div>
                        </div>
                    </div>
                </xsl:if>
                <xsl:if test="SummaryResults/ValidationCounters/NrOfValidationConditions &gt; 0">
                    <p id="conditions">
                        <b>Conditions</b>
                    </p>
                    <xsl:for-each select="SummaryResults/ValidationResults/ResultXML/Condition">
                        <table class="note">
                            <xsl:if test="count(Test) &gt; 0">
                                <tr>
                                    <td width="100" valign="top">
                                        <b>Test</b>
                                    </td>
                                    <td>
                                        <xsl:value-of select="Test"/>
                                    </td>
                                </tr>
                            </xsl:if>
                            <tr>
                                <td width="100" valign="top">
                                    <b>Location</b>
                                </td>
                                <td>
                                    <xsl:value-of select="Location"/>
                                </td>
                            </tr>
                            <tr>
                                <td width="100" valign="top">
                                    <b>Description</b>
                                </td>
                                <td>
                                    <xsl:value-of select="Description"/>
                                </td>
                            </tr>
                        </table>
                        <br/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="SummaryResults/ValidationCounters/NrOfValidationUnknown &gt; 0">
                    <p id="unknown">
                        <b>Unknown exceptions</b>
                    </p>
                    <xsl:for-each select="SummaryResults/ValidationResults/ResultXML/Unknown">
                        <table class="unknown">
                            <xsl:if test="count(Test) &gt; 0">
                                <tr>
                                    <td width="100" valign="top">
                                        <b>Test</b>
                                    </td>
                                    <td>
                                        <xsl:value-of select="Test"/>
                                    </td>
                                </tr>
                            </xsl:if>
                            <tr>
                                <td width="100" valign="top">
                                    <b>Location</b>
                                </td>
                                <td>
                                    <xsl:value-of select="Location"/>
                                </td>
                            </tr>
                            <tr>
                                <td width="100" valign="top">
                                    <b>Description</b>
                                </td>
                                <td>
                                    <xsl:value-of select="Description"/>
                                </td>
                            </tr>
                        </table>
                        <br/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="$nbReport &gt; 0">
                    <xsl:choose>
                        <xsl:when test="count(SummaryResults/ValidationResults/Report) &gt;= $limitDisplayTo">
                            <p id="reports">
                                <b>Report
                                    <span class="gzl-label gzl-label-orange">(only first
                                        <xsl:value-of select="$limitDisplayTo"
                                        /> reports are displayed)
                                    </span>
                                </b>
                            </p>
                        </xsl:when>
                        <xsl:otherwise>
                            <p id="reports">
                                <b>Reports</b>
                            </p>
                        </xsl:otherwise>
                    </xsl:choose>

                    <div id="Reports_p">

                        <div class="panel panel-default">
                            <div class="panel-body ">
                                <xsl:for-each
                                        select="SummaryResults/ValidationResults/Report[position() &lt;= $limitDisplayTo]">
                                    <xsl:call-template name="viewnotification">
                                        <xsl:with-param name="kind">dl-horizontal gzl-notification
                                            gzl-notification-green
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </div>
                        </div>
                    </div>
                </xsl:if>
            </div>
        </div>
        <br/>
        <xsl:if test="count(SummaryResults/ValidationResults/ResultXML/ProfileExceptions) &gt; 0">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Profile exceptions</h3>
                </div>
                <div class="panel-body">
                    <p>Those profile exceptions have been raised because the HL7 message profile, on some points,
                        differs from the Java class representing the HL7 message. Those are not errors in the given
                        message but may cause (partially) wrong validation results
                    </p>
                    <ul>
                        <xsl:for-each
                                select="SummaryResults/ValidationResults/ResultXML/ProfileExceptions/Exception">
                            <li>
                                <xsl:value-of select="text()"/>
                            </li>
                        </xsl:for-each>
                    </ul>

                </div>
            </div>
        </xsl:if>
        <xsl:if test="count(SummaryResults/ValidationResults/ProfileException) &gt; 0">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Profile exceptions</h3>
                </div>
                <div class="panel-body">
                    <p>Those profile exceptions have been raised because the HL7 message profile, on some points,
                        differs from the Java class representing the HL7 message. Those are not errors in the given
                        message but may cause (partially) wrong validation results
                    </p>
                    <ul>
                        <xsl:for-each select="SummaryResults/ValidationResults/ProfileException">
                            <li>
                                <xsl:value-of select="Description"/>
                            </li>
                        </xsl:for-each>
                    </ul>

                </div>
            </div>
        </xsl:if>
    </xsl:template>


    <xsl:template name="viewnotification">
        <xsl:param name="kind"/>

        <dl>
            <xsl:attribute name="class">
                <xsl:value-of select="$kind"/>
            </xsl:attribute>
            <xsl:if test="count(Type) &gt; 0">
                <dt>Type</dt>
                <dd>
                    <xsl:value-of select="Type"/>
                </dd>

            </xsl:if>
            <xsl:if test="count(Location) &gt; 0">
                <dt>Location</dt>
                <dd>
                    <xsl:value-of select="Location"/>
                </dd>

            </xsl:if>
            <xsl:if test="count(Value) &gt; 0">
                <dt>Value</dt>
                <dd>
                    <xsl:value-of select="Value"/>
                </dd>

            </xsl:if>
            <dt>Description</dt>
            <dd>
                <xsl:value-of select="Description"/>

            </dd>
            <xsl:if test="count(HL7Table) &gt; 0">
                <dt>HL7 Table</dt>
                <dd>
                    <xsl:element name="a">
                        <xsl:attribute name="href">
                            <xsl:text>http://gazelle.ihe.net/GazelleHL7v2Validator/viewResource.seam?oid=</xsl:text>
                            <xsl:value-of select="HL7Table"/>
                        </xsl:attribute>
                        <xsl:attribute name="target">blank</xsl:attribute>
                        <xsl:value-of select="HL7Table"/>
                    </xsl:element>
                </dd>

            </xsl:if>
        </dl>

    </xsl:template>

    <xsl:template name="maximun">
        <xsl:param name="pSequence"/>
        <xsl:for-each select="$pSequence">
            <xsl:sort select="." data-type="number" order="descending"/>
            <xsl:if test="position()=1">
                <xsl:value-of select="."/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
