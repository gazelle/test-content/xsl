<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 02, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Abderrazek Boufahja, IHE Development, Kereval</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="viewdown">true</xsl:param>
    <xsl:param name="constraintPath">#</xsl:param>
    <xsl:param name="assertionManagerPath">/AssertionManagerGui/</xsl:param>
    <xsl:param name="ShowGeneralInformation">true</xsl:param>


    <xsl:template match="/">
        <xsl:variable name="limitDisplayTo">
            50
        </xsl:variable>
        <html>
            <head>
                <title>External Validation Report</title>
            </head>
            <body>
                <script type="text/javascript">
                    function hideOrViewValidationDetailsMB() {
                    var detaileddiv = document.getElementById('resultdetailedann');
                    if (detaileddiv != null){
                    var onn = document.getElementById('resultdetailedann_switch_on');
                    if (onn != null){
                    if (onn.style.display == 'block') onn.style.display = 'none';
                    else if (onn.style.display == 'none') onn.style.display = 'block';
                    }
                    var off = document.getElementById('resultdetailedann_switch_off');
                    if (off != null){
                    if (off.style.display == 'block') off.style.display = 'none';
                    else if (off.style.display == 'none') off.style.display = 'block';
                    }
                    var body = document.getElementById('resultdetailedann_body');
                    if (body != null){
                    if (body.style.display == 'block') body.style.display = 'none';
                    else if (body.style.display == 'none') body.style.display = 'block';
                    }
                    }
                    }

                    function hideOrUnhideMB(elem){
                    var elemToHide = document.getElementById(elem.name + '_p');
                    if (elemToHide != null){
                    if (elem.checked){
                    elemToHide.style.display = 'none';
                    }
                    else{
                    elemToHide.style.display = 'block';
                    }
                    }
                    }

                    function detectDownloadResultButton(){
                    var elemDown = document.getElementById('resultForm:downloadMBResultB');
                    if (elemDown != null) return true;
                    return false;
                    }

                    function extractDownloadResultButton(parentNoeud){
                    var elemDown = document.getElementById('resultForm:downloadMBResultB').cloneNode(true);
                    if (elemDown != null) {
                    var downloadspan = document.getElementById('downloadspan');
                    downloadspan.appendChild(elemDown);
                    }
                    }

                </script>

                <h4>External Validation Report</h4>


                <div class="panel panel-info">
                    <div class="panel-heading">General Informations (from XSL)</div>
                    <div class="panel-body">
                        <dl class="dl-horizontal">
                            <div>
                                <dt>Validation Date</dt>
                                <dd><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationDate"/> -
                                        <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTime"
                                    /></dd>
                            </div>
                            <div>
                                <dt>Validation Service</dt>
                                <dd>
                                    <xsl:value-of
                                        select="detailedResult/ValidationResultsOverview/ValidationServiceName"/>
                                    <xsl:if
                                        test="count(detailedResult/ValidationResultsOverview/ValidationServiceVersion) = 1"
                                        > (Version : <xsl:value-of
                                            select="detailedResult/ValidationResultsOverview/ValidationServiceVersion"
                                        />) </xsl:if>
                                </dd>
                            </div>
                            <div>
                                <dt>Validation Test Status</dt>
                                <dd>
                                    <xsl:if
                                        test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                        <div class="PASSED">
                                            <xsl:value-of
                                                select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                        </div>
                                    </xsl:if>
                                    <xsl:if
                                        test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                        <div class="FAILED">
                                            <xsl:value-of
                                                select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                        </div>
                                    </xsl:if>
                                    <xsl:if
                                        test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                                        <div class="ABORTED">
                                            <xsl:value-of
                                                select="detailedResult/ValidationResultsOverview/ValidationTestResult"/>
                                        </div>
                                    </xsl:if>
                                </dd>
                            </div>
                        </dl>

                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading ">Result overview</div>
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <li >
                                <a data-toggle="tab" href="#wellformed">XML (<xsl:attribute name="class"><xsl:value-of
                                            select="detailedResult/DocumentWellFormed/Result"/></xsl:attribute>
                                    <xsl:value-of select="detailedResult/DocumentWellFormed/Result"/>)</a>
                            </li>
                            <xsl:if test="count(detailedResult/DocumentValidXSD/Result) = 1">
                                <li>
                                    <a data-toggle="tab" href="#xsd">XSD (<xsl:attribute name="class"><xsl:value-of
                                                select="detailedResult/DocumentValidXSD/Result"/></xsl:attribute>
                                        <xsl:value-of select="detailedResult/DocumentValidXSD/Result"/>)</a>
                                </li>
                            </xsl:if>
                            <li class="active">

                                <a data-toggle="tab" href="#mbv">Object Checker Validation ( <xsl:attribute name="class"
                                            ><xsl:value-of
                                            select="detailedResult/ValidationResultsOverview/ValidationTestResult"
                                        /></xsl:attribute>
                                    <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"
                                    />)</a>

                            </li>
                        </ul>


                        <div class="tab-content">
                            <div id="wellformed" class="tab-pane fade in active">
                                <h5>XML Validation Report</h5>
                                <i>The document you have validated is supposed to be an XML document. The validator has
                                    checked if it is well-formed, results of this validation are gathered in this
                                    part.</i>

                                <xsl:choose>
                                    <xsl:when test="detailedResult/DocumentWellFormed/Result = 'PASSED'">
                                        <p class="PASSED">The XML document is well-formed</p>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <p class="FAILED">The XML document is not well-formed, for the following
                                            reasons: </p>
                                        <xsl:for-each select="detailedResult/DocumentWellFormed">
                                            <xsl:call-template name="viewXSDFailMessages"/>
                                        </xsl:for-each>
                                    </xsl:otherwise>
                                </xsl:choose>

                            </div>
                            <xsl:if test="count(detailedResult/DocumentValidXSD) = 1">

                                <div id="xsd" class="tab-pane fade">
                                    <h3>XSD Validation detailed Results</h3>

                                    <i>Your XML document has been validating about the appropriate XSD schema, here is
                                        the detail of the validation outcome.</i>
                                    <xsl:if test="count(detailedResult/DocumentValidXSD) = 1">
                                        <xsl:choose>
                                            <xsl:when
                                                test="(detailedResult/DocumentValidXSD/Result='PASSED') or (detailedResult/DocumentValidXSD/nbOfErrors=0 and count(detailedResult/DocumentValidXSD/Result)=0)">
                                                <p class="PASSED">The XML document is valid regarding the schema</p>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <p class="FAILED">The XML document is not valid regarding the schema
                                                    because of the following reasons: </p>
                                                <xsl:for-each select="detailedResult/DocumentValidXSD">
                                                    <xsl:call-template name="viewXSDFailMessages"/>
                                                </xsl:for-each>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:if>
                                </div>
                            </xsl:if>
                            <xsl:if test="count(detailedResult/MDAValidation) = 1">
                                <div id="mbv" class="tab-pane fade">
                                    <h3>Model Based Validation details</h3>
                                    <div class="rich-stglpanel-marker">
                                        <div id="resultdetailedann_switch_on" class="rich-stglpnl-marker"
                                            style="display: block;">
                                            <xsl:text disable-output-escaping="yes">&amp;laquo;</xsl:text>
                                        </div>
                                        <div id="resultdetailedann_switch_off" class="rich-stglpnl-marker"
                                            style="display: none;">
                                            <xsl:text disable-output-escaping="yes">&amp;raquo;</xsl:text>
                                        </div>
                                    </div>
                                    <table class="styleResultBackground">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <b>Result</b>
                                                </td>
                                                <td>
                                                    <xsl:if test="count(detailedResult/MDAValidation/Error) = 0">
                                                        <div class="PASSED">PASSED</div>
                                                    </xsl:if>
                                                    <xsl:if test="count(detailedResult/MDAValidation/Error) &gt; 0">
                                                        <div class="FAILED">FAILED</div>
                                                    </xsl:if>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100px" valign="top">
                                                    <b>Summary</b>
                                                </td>
                                                <td>
                                                    <xsl:value-of
                                                        select="count(detailedResult/MDAValidation/Error) + count(detailedResult/MDAValidation/Warning) + count(detailedResult/MDAValidation/Info) + count(detailedResult/MDAValidation/Note)"
                                                    /> checks <br/>
                                                    <xsl:value-of select="count(detailedResult/MDAValidation/Error)"/>
                                                    errors <br/>
                                                    <xsl:value-of select="count(detailedResult/MDAValidation/Warning)"/>
                                                    warning <br/>
                                                    <xsl:value-of select="count(detailedResult/MDAValidation/Info)"/>
                                                    infos <br/>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <b>HIDE : </b>
                                    <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Errors">Errors</input>
                                    <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Warnings"
                                        >Warnings</input>
                                    <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Infos">Infos</input>
                                    <input type="checkbox" onclick="hideOrUnhideMB(this)" name="Reports">Reports</input>
                                    <xsl:if test="count(detailedResult/MDAValidation/Error) &gt; 0">
                                        <div id="Errors_p">
                                            <span style="font-weight:bold;">Errors </span>
                                            <div class="panel panel-default">
                                                <div class="panel-body ">
                                                    <xsl:for-each select="detailedResult/MDAValidation/Error">
                                                        <xsl:call-template name="viewnotification">
                                                            <xsl:with-param name="kind">dl-horizontal gzl-notification
                                                            gzl-notification-red</xsl:with-param>
                                                        </xsl:call-template>
                                                    </xsl:for-each>
                                                </div>
                                            </div>
                                        </div>
                                    </xsl:if>
                                    <xsl:if test="count(detailedResult/MDAValidation/Warning) &gt; 0">
                                        <div id="Warnings_p">

                                            <span style="font-weight:bold;">Warnings </span>
                                            <div class="panel panel-default">
                                                <div class="panel-body ">
                                                    <xsl:for-each select="detailedResult/MDAValidation/Warning">
                                                        <xsl:call-template name="viewnotification">
                                                            <xsl:with-param name="kind">dl-horizontal gzl-notification
                                                            gzl-notification-orange</xsl:with-param>
                                                        </xsl:call-template>
                                                    </xsl:for-each>
                                                </div>
                                            </div>
                                        </div>
                                    </xsl:if>
                                    <xsl:if test="count(detailedResult/MDAValidation/Info) &gt; 0">
                                        <div id="Infos_p">
                                            <span style="font-weight:bold;">Infos </span>
                                            <div class="panel panel-default">
                                                <div class="panel-body ">
                                                    <xsl:for-each select="detailedResult/MDAValidation/Info">
                                                        <xsl:call-template name="viewnotification">
                                                            <xsl:with-param name="kind">dl-horizontal gzl-notification
                                                            gzl-notification-green</xsl:with-param>
                                                        </xsl:call-template>
                                                    </xsl:for-each>
                                                </div>
                                            </div>
                                        </div>
                                    </xsl:if>
                                    <xsl:if test="count(detailedResult/MDAValidation/Note) &gt; 0">
                                        <div id="Reports_p">
                                            <div class="panel panel-default">
                                                <div class="panel-body ">
                                                    <span style="font-weight:bold;">Reports </span>
                                                    <xsl:for-each select="detailedResult/MDAValidation/Note">
                                                        <xsl:if test="position() &lt;= $limitDisplayTo">
                                                            <xsl:call-template name="viewnotification">
                                                            <xsl:with-param name="kind">dl-horizontal gzl-notification
                                                            gzl-notification-blue</xsl:with-param>
                                                            </xsl:call-template>
                                                        </xsl:if>
                                                    </xsl:for-each>
                                                    <xsl:if test="count(detailedResult/MDAValidation/Note) &gt; $limitDisplayTo">
                                                        <table class="gzl-notification gzl-notification-green"
                                                            width="98%">
                                                            <tr>
                                                            <td valign="top" width="100">
                                                            <b>..........</b>
                                                            </td>
                                                            </tr>
                                                        </table>
                                                        <br/>
                                                        <table class="moore" width="98%">
                                                            <tr>
                                                            <td valign="top" width="100">
                                                            <b>All errors and warnings are shown above. If you want to
                                                            view the complete report including all positive checks,
                                                            please download the 'Model Based Validation Result'.</b>
                                                            <br/>
                                                            <span id="downloadspan">
                                                            <script type="text/javascript">extractDownloadResultButton()</script>
                                                            </span>
                                                            </td>
                                                            </tr>
                                                        </table>
                                                    </xsl:if>
                                                </div>
                                            </div>
                                        </div>
                                    </xsl:if>

                                </div>
                            </xsl:if>
                            <xsl:if test="count(detailedResult/nistValidation) = 1">
                                <xsl:if test="detailedResult/nistValidation != ''">
                                    <br/>
                                    <div class="panel panel-info" id="resultdetailedannNist">
                                        <div class="panel-heading" onclick="hideOrViewNistValidationDetails();"> Nist
                                            Validation details <div class="rich-stglpanel-marker">
                                                <div id="resultdetailedann_switch_on_nist" class="rich-stglpnl-marker"
                                                    style="display: block;"
                                                    ><xsl:text disable-output-escaping="yes">&amp;laquo;</xsl:text></div>
                                                <div id="resultdetailedann_switch_off_nist" class="rich-stglpnl-marker"
                                                    style="display: none;"
                                                    ><xsl:text disable-output-escaping="yes">&amp;raquo;</xsl:text></div>
                                            </div>
                                        </div>
                                        <div class="panel-body" style="display: block;" id="resultdetailedann_body_nist">
                                            <xsl:value-of select="detailedResult/nistValidation"
                                                disable-output-escaping="yes"/>
                                        </div>
                                    </div>
                                </xsl:if>
                            </xsl:if>
                        </div>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="viewnotification">
        <xsl:param name="kind"/>

        <dl>
            <xsl:attribute name="class">
                <xsl:value-of select="$kind"/>
            </xsl:attribute>
            <xsl:if test="count(Test) &gt; 0">
                <dt> Test </dt>
                <dd>
                    <xsl:value-of select="Test"/>
                </dd>

            </xsl:if>
            <xsl:if test="count(Location) &gt; 0">
                <dt> Location</dt>
                <dd>
                    <xsl:value-of select="Location"/>
                    <xsl:if test="$viewdown = 'true'">
                        <img src="/EVSClient/img/icons64/down.gif" style="vertical-align: middle;" width="15px"
                            onclick="gotoo(this)"/>
                    </xsl:if>
                </dd>

            </xsl:if>

            <dt> Description</dt>
            <dd>
                <xsl:value-of select="Description"/>
                <xsl:if test="Identifiant and ($constraintPath != '#')"> [<a>
                        <xsl:attribute name="href"><xsl:value-of select="$constraintPath"/><xsl:value-of
                                select="Identifiant"/>.html</xsl:attribute>
                        <xsl:attribute name="target">_blank</xsl:attribute> Constraint... </a>] </xsl:if>
                <xsl:if test="assertion and ($assertionManagerPath != '#')"> [<a>
                        <xsl:attribute name="href"><xsl:value-of select="$assertionManagerPath"
                                />rest/testAssertion/assertion?idScheme=<xsl:value-of select="assertion/@idScheme"
                                />&amp;assertionId=<xsl:value-of select="assertion/@assertionId"/></xsl:attribute>
                        <xsl:attribute name="target">_blank</xsl:attribute> Assertion... </a>] </xsl:if>
            </dd>

        </dl>
        <br/>
    </xsl:template>

    <xsl:template name="viewXSDFailMessages">
        <ul>
            <xsl:for-each select="XSDMessage">
                <li>
                    <xsl:value-of select="Severity"/>
                    <xsl:text>: </xsl:text>
                    <xsl:value-of select="Message"/>
                    <xsl:if test="lineNumber">
                        <xsl:text> (see line </xsl:text>
                        <xsl:value-of select="lineNumber"/>
                        <xsl:text>, column </xsl:text>
                        <xsl:value-of select="columnNumber"/>
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>



</xsl:stylesheet>
