<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xd"
    version="1.0">
<xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>    
<xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> May 16, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> aberge</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="DvtSummaryResultsFile">
        <html>
            <head>
                <title>DVT validation report</title>
           </head>
            <body>
                <h2>DVT validation report</h2>
                <br/>
                <!-- general information -->
                <div class="rich-panel">
                    <div class="rich-panel-header">General Information</div>
                    <div class="rich-panel-body styleResultBackground">
                        <p><b>Tool name and version: </b><xsl:value-of select="SessionDetails/ModelName"/> (<xsl:value-of select="SessionDetails/SoftwareVersions"/>)</p>
                        <p><b>Validation Date: </b><xsl:value-of select="SessionDetails/TestDate"/></p>
                        <p><b>Validation result: </b><xsl:value-of select="ValidationCounters/ValidationTest"/></p>
                    </div>
                </div>
                <div class="rich-panel">
                    <div class="rich-panel-header">Validation Counters</div>
                    <div class="rich-panel-body styleResultBackground">
                        <p><b>Validation errors: </b><xsl:value-of select="ValidationCounters/NrOfValidationErrors"/></p>
                        <p><b>Validation warnings: </b><xsl:value-of select="ValidationCounters/NrOfValidationWarnings"/></p>
                        <p><b>General errors: </b><xsl:value-of select="ValidationCounters/NrOfGeneralErrors"/></p>
                        <p><b>General warnings: </b><xsl:value-of select="ValidationCounters/NrOfGeneralWarnings"/></p>
                        <p><b>User errors: </b><xsl:value-of select="ValidationCounters/NrOfUserErrors"/></p>
                        <p><b>User warnings: </b><xsl:value-of select="ValidationCounters/NrOfUserWarnings"/></p>
                    </div>
                </div>
                <xsl:if test="count(ValidationResults) &gt; 0">
                <div class="rich-panel">
                    <div class="rich-panel-header">Validation details</div>
                    <div class="rich-panel-body styleResultBackground">
                        <xsl:apply-templates select="ValidationResults"/>
                    </div>
                </div>
                </xsl:if>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="Module">
        <table border="1" width="100%" style="font-size:10;">
            <tr>
                <td colspan="7" align="center"><b>Module: <xsl:value-of select="@Name"/> (<xsl:value-of select="@Usage"/>)</b></td>
            </tr>
            <tr align="center">
                <td><b>Attribute</b></td>
                <td><b>VR</b></td>
                <td><b>Type</b></td>
                <td><b>Length</b></td>
                <td><b>Attribute name</b></td>
                <td colspan="2"><b>Value(s) and/or Comments</b></td>
            </tr>
            <xsl:apply-templates select="Attribute">
                <xsl:with-param name="depth" select="0"></xsl:with-param>
            </xsl:apply-templates>
        </table>
        <br/>
    </xsl:template>
    <xsl:template match="ValidationResults">
        <h3><xsl:value-of select="ValidationDicomMessage/Name"/></h3>
        <xsl:apply-templates select="ValidationDicomMessage/Message"/>
        <br/>
        <br/>
        <xsl:apply-templates select="ValidationDicomMessage/Module"/>
    </xsl:template>
    <xsl:template match="Attribute">
        <xsl:param name="depth"/>
        <xsl:choose>
            <xsl:when test="count(Values/Value) &gt; 0">
                <tr>
                    <td valign="middle">
                        <xsl:if test="$depth = 1">&gt;</xsl:if>
                        <xsl:if test="$depth = 2">&gt;&gt;</xsl:if>
                        (<xsl:value-of select="@Group"/>,<xsl:value-of select="@Element"/>)
                    </td>
                    <td valign="middle"><xsl:value-of select="@DefVR"/></td>
                    <td valign="middle"><xsl:value-of select="@Type"/></td>
                    <td valign="middle"><xsl:value-of select="@Length"/></td>
                    <td valign="middle"><xsl:value-of select="@Name"/></td>
                    <td valign="middle">
                        <ul>
                            <xsl:for-each select="Values/Value">
                                <li><xsl:value-of select="text()"/></li>
                            </xsl:for-each>
                        </ul>
                    </td>
                    <td valign="middle">
                        <xsl:apply-templates select="Message"/>
                    </td>
                </tr>   
            </xsl:when>
            <xsl:when test="count(Values/Sequence) &gt; 0">
                <tr>
                    <td valign="middle">
                        <xsl:if test="$depth = 1">&gt;</xsl:if>
                        <xsl:if test="$depth = 2">&gt;&gt;</xsl:if>
                        (<xsl:value-of select="@Group"/>,<xsl:value-of select="@Element"/>)
                    </td>
                    <td valign="middle"><xsl:value-of select="@DefVR"/></td>
                    <td valign="middle"><xsl:value-of select="@Type"/></td>
                    <td valign="middle"><xsl:value-of select="@Length"/></td>
                    <td valign="middle"><xsl:value-of select="@Name"/></td>
                    <td colspan="2" valign="middle">
                        <xsl:apply-templates select="Message"/>
                    </td>
                </tr>   
                <xsl:apply-templates select="Values/Sequence/Item/Attribute">
                    <xsl:with-param name="depth" select="$depth + 1"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise>
                <tr>
                    <td valign="middle"><xsl:if test="$depth = 1">&gt;</xsl:if>
                        <xsl:if test="$depth = 2">&gt;&gt;</xsl:if>
                        (<xsl:value-of select="@Group"/>,<xsl:value-of select="@Element"/>)
                    </td>
                    <td valign="middle"><xsl:value-of select="@DefVR"/></td>
                    <td valign="middle"><xsl:value-of select="@Type"/></td>
                    <td valign="middle"><xsl:value-of select="@Length"/></td>
                    <td valign="middle"><xsl:value-of select="@Name"/></td>
                    <td colspan="2" valign="middle">
                        <xsl:apply-templates select="Message"/>
                    </td>
                </tr>   
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="Message">
        <xsl:element name="span">
            <xsl:attribute name="class">dvt<xsl:value-of select="Type/text()"/></xsl:attribute>
            <xsl:value-of select="Type"/>: <xsl:value-of select="Meaning"/>
        </xsl:element>     
    </xsl:template>
</xsl:stylesheet>
