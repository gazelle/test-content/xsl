<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ihe="urn:ihe:iti:svs:2008" version="2.0">
    
    <xsl:output encoding="UTF-8" method="html" indent="yes"/>
	<xsl:template match="/">
        <xsl:variable name="limitDisplayTo">50</xsl:variable>
        <html>
            <head>
                <link xmlns="http://www.w3.org/1999/xhtml" class="user" href="http://gazelle.ihe.net/css/default.css" rel="stylesheet" type="text/css"/>

                <meta name="Generator" content="LCSD_XSL.xsl;"/>
                
                <xsl:comment>
                do NOT edit this HTML directly, it was generated
                via an XSLT transformation from the original Level 1
                document.
                </xsl:comment>
               
            
                <title>Display of the LCSD Code Set</title>
            </head>
            
            
            
            
            <body>
                <div id="banner">
                    
                </div>
				<!-- The variable below are used to replace special characters in the Code links. -->
                <xsl:variable name="before" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÂÊÛÎÔÉéèàêâûîô'"/>
                <xsl:variable name="after" select="'abcdefghijklmnopqrstuvwxyzaeuioeeeaeauio'"/>

                <h1>Content of the code set : <xsl:value-of select="ihe:RetrieveValueSetResponse/ihe:ValueSet/@displayName"/>  </h1>
                
                <h2>Global Information :</h2>
                <table>
                    
              
               <tr><td width="500" align="right" style="font-weight:bold">Number of Codes  : </td><td><xsl:value-of select="count(/ihe:RetrieveValueSetResponse/ihe:ValueSet/ihe:ConceptList/ihe:Concept)"/></td></tr>
               <tr><td width="500" align="right" style="font-weight:bold">Master File Identifier : </td><td><xsl:value-of select="ihe:RetrieveValueSetResponse/ihe:ValueSet/@masterFileIdentifier"/> </td></tr>
               <tr><td width="500" align="right" style="font-weight:bold">Display Name : </td><td><xsl:value-of select="ihe:RetrieveValueSetResponse/ihe:ValueSet/@displayName"/> </td></tr>
               <tr><td width="500" align="right" style="font-weight:bold">Master File Application Identifier : </td><td><xsl:value-of select="ihe:RetrieveValueSetResponse/ihe:ValueSet/@masterFileApplicationIdentifier"/> </td></tr>
               <tr><td width="500" align="right" style="font-weight:bold">File Level Event Code : </td><td><xsl:value-of select="ihe:RetrieveValueSetResponse/ihe:ValueSet/@fileLevelEventCode"/> </td></tr>
               <tr><td width="500" align="right" style="font-weight:bold">Version : </td><td><xsl:value-of select="ihe:RetrieveValueSetResponse/ihe:ValueSet/@version"/> </td></tr>
               <tr><td width="500" align="right" style="font-weight:bold">Effective Date Time : </td><td><xsl:value-of select="ihe:RetrieveValueSetResponse/ihe:ValueSet/@effectiveDateTime"/> </td></tr>
               <tr><td width="500" align="right" style="font-weight:bold">Response Level Code : </td><td><xsl:value-of select="ihe:RetrieveValueSetResponse/ihe:ValueSet/@responseLevelCode"/> </td></tr>
                </table>
                <br/>
                
                <p id="tableau_top">
                    <h2>List of Codes :</h2>
                    
                    <table  border="1">
                        <tr>
                            <th>Display Name</th>
                            <th>Code</th>
                            <th>Code System</th>
                        </tr>
                        <xsl:for-each select="ihe:RetrieveValueSetResponse/ihe:ValueSet/ihe:ConceptList/ihe:Concept">
                            
                            <tr>
                                <td><a title="Click to see details about this Code."><xsl:attribute  name="href">#<xsl:value-of select="translate(@code,$before,$after)" /></xsl:attribute>  <xsl:value-of select="@code"/></a></td>
                                <td><xsl:value-of select="@displayName"/></td>
                                <td><xsl:value-of select="@codeSystem"/></td>
                            </tr>
                            
                        </xsl:for-each>
                    </table>
                </p>
                
                <xsl:for-each select="ihe:RetrieveValueSetResponse/ihe:ValueSet/ihe:ConceptList/ihe:Concept">
<hr/>
                    <p><xsl:attribute  name="id"><xsl:value-of select="translate(@code,$before,$after)" /></xsl:attribute>
                        <h2 ><xsl:value-of select="@code"/></h2>
                        
                        <!-- MFE Segment -->
                        <xsl:if test="(ihe:MFE)">
                            <h3 style="text-decoration : underline;">MFE Segment Details :</h3>
                            <table>
                        <xsl:for-each select="ihe:MFE/@*">
                            <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                        </xsl:for-each>
                        <xsl:for-each select="ihe:MFE/*">
                            <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                        </xsl:for-each>
                                </table>
                        </xsl:if>
                        
                        <!-- OM1 Segment -->   
                        <xsl:if test="(ihe:OM1)">
                            <h3 style="text-decoration : underline;">OM1 Segment Details :</h3>
                            <table>
                                <xsl:for-each select="ihe:OM1/@*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                                <xsl:for-each select="ihe:OM1/*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                            </table>
                        </xsl:if>
                        
                        <!-- OM2 Segment -->    
                        <xsl:if test="(ihe:OM2)">
                            <h3 style="text-decoration : underline;">OM2 Segment Details :</h3>
                            <table>
                                <xsl:for-each select="ihe:OM2/@*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                                <xsl:for-each select="ihe:OM2/*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                            </table>
                        </xsl:if>
                        
                        <!-- OM3 Segment -->    
                        <xsl:if test="(ihe:OM3)">
                            <h3 style="text-decoration : underline;">OM3 Segment Details :</h3>
                            <table>
                                <xsl:for-each select="ihe:OM3/@*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                                <xsl:for-each select="ihe:OM3/*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                            </table>
                        </xsl:if>
                        
                        <!-- OM4 Segment -->    
                        <xsl:if test="(ihe:OM4)">
                            <h3 style="text-decoration : underline;">OM4 Segment Details :</h3>
                            <table>
                                <xsl:for-each select="ihe:OM4/@*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                                <xsl:for-each select="ihe:OM4/*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                            </table>
                        </xsl:if>
                        
                        <!-- OM5 Segment --> 
                        <xsl:if test="(ihe:OM5)">
                            <h3 style="text-decoration : underline;">OM5 Segment Details :</h3>
                            <table>
                                <xsl:for-each select="ihe:OM5/@*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                                <xsl:for-each select="ihe:OM5/*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                            </table>
                        </xsl:if>
                        
                        <!-- OM6 Segment -->                         
                        <xsl:if test="(ihe:OM6)">
                            <h3 style="text-decoration : underline;">OM6 Segment Details :</h3>
                            <table>
                                <xsl:for-each select="ihe:OM6/@*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                                <xsl:for-each select="ihe:OM6/*">
                                    <tr><td width="500" align="right" style="font-weight:bold"><xsl:value-of select="name()"/> : </td><td><xsl:value-of select="."/></td></tr>
                                </xsl:for-each>
                            </table>
                        </xsl:if>
                        
                        <a href="#tableau_top" title="Click to go to the top of this document.">Top</a>
                    </p>
                </xsl:for-each>
                
                
            </body>
        </html>
    </xsl:template>
    
</xsl:stylesheet>
