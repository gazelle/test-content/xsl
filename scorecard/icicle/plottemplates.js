var labelType, useGradients, nativeTextSupport, animate;

(function () {
    var ua = navigator.userAgent,
        iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
        typeOfCanvas = typeof HTMLCanvasElement,
        nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
        textSupport = nativeCanvasSupport
            && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
    //I'm setting this based on the fact that ExCanvas provides text support for IE
    //and that as of today iPhone/iPad current text support is lame
    labelType = (!nativeCanvasSupport || (textSupport && !iStuff)) ? 'Native' : 'HTML';
    nativeTextSupport = labelType == 'Native';
    useGradients = nativeCanvasSupport;
    animate = !(iStuff || !nativeCanvasSupport);
})();

var icicle;

function drawtemplate() {
    //left panel controls
    controls();

    // init data
    var json =  getvaluesfordisplay();
    // end
    // init Icicle
    icicle = new $jit.Icicle({
        // id of the visualization container
        injectInto: 'infovis',
        // whether to add transition animations
        animate: animate,
        // nodes offset
        offset: 1,
        // whether to add cushion type nodes
        cushion: false,
        //show only three levels at a time
        constrained: false,
        levelsToShow: 3,
        // enable tips
        Tips: {
            enable: true,
            type: 'Native',
            // add positioning offsets
            offsetX: 20,
            offsetY: 20,
            // implement the onShow method to
            // add content to the tooltip when a node
            // is hovered
            onShow: function (tip, node) {
                // add tooltip info
                tip.innerHTML = "<dl class=\"dl-horizontal\"><dt>Name</dt><dd>" + node.name
                    + "</dd><dt>Status</dt><dd>" + node.getData('status') + "</dd><dt>Conformity</dt><dd>" + node.getData('validity') + "</dd><dt>Richness</dt><dd>" + node.getData('richness') + "</dt></dl>";
            }
        },
        // Add events to nodes
        Events: {
            enable: true,
            onMouseEnter: function (node) {
                //add border and replot node
                var nodecolor = node.getData('color');
                if (nodecolor == '#ebccd1'){ // red
                    node.setData('border', '#ebccd1');
                } else if (nodecolor == '#dff0d8'){ // green
                     node.setData('border', '#d6e9c6');
                } else if (nodecolor == '#fcf8e3') {// orange
                    node.setData('border', '#faebcc');
                } else if (nodecolor == ''){ // grey
                    node.setData('border', '#ddd');
                } else {
                    node.setData('border', '#bce8f1');
                }
                ; // border of the cell on mouse hover
                icicle.fx.plotNode(node, icicle.canvas);
                icicle.labels.plotLabel(icicle.canvas, node, icicle.controller);
            },
            onMouseLeave: function (node) {
                node.removeData('border');
                icicle.fx.plotNode(node, icicle.canvas);
            },
            onClick: function (node) {
                if (node) {
                    //hide tips and selections
                    icicle.tips.hide();
                    if (icicle.events.hovered)
                        this.onMouseLeave(icicle.events.hovered);
                    //perform the enter animation
                    icicle.enter(node);
                }
            },
            onRightClick: function () {
                //hide tips and selections
                icicle.tips.hide();
                if (icicle.events.hovered)
                    this.onMouseLeave(icicle.events.hovered);
                //perform the out animation
                icicle.out();
            }
        },
        // Add canvas label styling
        Label: {
            overridable: true,
            type: 'HTML', //'SVG', 'Native'
        },
        // Add the name of the node in the corresponding label
        // This method is called once, on label creation and only for DOM and not
        // Native labels.
        onCreateLabel: function (domElement, node) {
            domElement.innerHTML = "<div>" + node.name + "</div>";
            var style = domElement.style;
            style.fontSize = '12';
            style.cursor = 'pointer';
            style.color = 'black';
            style.textAlign = 'center';
            style.fontWeight = 'bold';
        },
        // Change some label dom properties.
        // This method is called each time a label is plotted.
        onPlaceLabel: function (domElement, node) {
            domElement.innerHTML = "<div>" + node.name + "</div>";
            var style = domElement.style,
                width = node.getData('width'),
                height = node.getData('height');
       /*     if (width < 20 || height < 20) {
                style.display = 'none';
            } else {*/
                style.display = 'block';
                style.width = width + 'px';
                style.height = height + 'px';
           /* }*/
        }
    });
    // load data
    icicle.loadJSON(json);
    icicle.config.constrained = true;
    icicle.config.levelsToShow = 2;
    // compute positions and plot
    icicle.refresh();
    //end
}

//init controls
function controls() {
    var jit = $jit;
    var gotoparent = jit.id('update');
    jit.util.addEvent(gotoparent, 'click', function () {
        icicle.out();
    });
    var select = jit.id('s-orientation');
    jit.util.addEvent(select, 'change', function () {
        icicle.layout.orientation = select[select.selectedIndex].value;
        icicle.refresh();
    });
    var levelsToShowSelect = jit.id('i-levels-to-show');
    jit.util.addEvent(levelsToShowSelect, 'change', function () {
        var index = levelsToShowSelect.selectedIndex;
        if (index == 0) {
            icicle.config.constrained = false;
        } else {
            icicle.config.constrained = true;
            icicle.config.levelsToShow = index;
        }
        icicle.refresh();
    });
    var colorbase = jit.id('s-color');
    jit.util.addEvent(colorbase, 'change', function(){
        var dataname = colorbase.options[colorbase.selectedIndex].id;
        icicle.graph.eachNode(function(node) {
            var newcolor = node.getData(dataname);
            node.setData('color', newcolor);
        });
        icicle.refresh();
    });
}
//end
