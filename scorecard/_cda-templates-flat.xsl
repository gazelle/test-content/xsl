<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="1.0" encoding="utf-8" indent="yes"/>

    <xsl:include href="_progress_bar_def.xsl"/>

    <xsl:include href="_ewir_def.xsl"/>

    <xsl:template name="flatTemplateScoring">
        <a href="#templatetree" class="gzl-btn-blue" style="margin-bottom: 10px">Access tree view</a>
        <xsl:for-each select="templateStats">
            <xsl:call-template name="firstLevelStat"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="firstLevelStat">
        <div>
            <xsl:attribute name="id">
                <xsl:value-of select="templateIdentifier"/>
            </xsl:attribute>
            <xsl:attribute name="class">
                <xsl:text>panel panel-</xsl:text>
                <xsl:choose>
                    <xsl:when test="isPresent = 'true' and validationResult = 'Report'">
                        <xsl:text>success</xsl:text>
                    </xsl:when>
                    <xsl:when test="isPresent = 'true' and validationResult = 'Error'">
                        <xsl:text>danger</xsl:text>
                    </xsl:when>
                    <xsl:when test="isPresent = 'true' and validationResult = 'Warning'">
                        <xsl:text>warning</xsl:text>
                    </xsl:when>
                    <xsl:when test="isPresent = 'false'">
                        <xsl:text>default</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>default</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <div class="panel-heading">
                <xsl:value-of select="templateName"/>
                <xsl:text> [</xsl:text>
                <xsl:value-of select="templateIdentifier"/>
                <xsl:text>]</xsl:text>
            </div>
            <div class="panel-body">
                <xsl:choose>
                    <xsl:when test="isPresent = 'false'">
                        <p>This template is not used</p>
                    </xsl:when>
                    <xsl:otherwise>

                        <dl class="dl-horizontal large-title">
                            <xsl:apply-templates select="templateChecksSummaryStat"/>
                        </dl>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="subtemplate">
                    <h4>Sub template(s)</h4>
                    <xsl:if test="count(subtemplate) &gt; 0">
                        <div class="row">
                            <div class="col-md-2 offset-md-1">
                                <b>Template Id</b>
                            </div>
                            <div class="col-md-9">
                                <b>Is present</b>
                            </div>
                        </div>
                        <xsl:for-each select="subtemplate">
                            <xsl:call-template name="subtemplatesummary"/>
                        </xsl:for-each>
                    </xsl:if>
                </xsl:if>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="subtemplatesummary">
        <div class="row">
            <div class="col-md-2 offset-md-3">
                <xsl:choose>
                    <xsl:when test="@isPresent = 'true'">
                        <a>
                            <xsl:attribute name="href">
                                <xsl:text>#</xsl:text>
                                <xsl:value-of select="@templateId"/>
                            </xsl:attribute>
                            <b>
                                <xsl:value-of select="@templateId"/>
                            </b>
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@templateId"/>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
            <div class="col-md-7">
                <xsl:value-of select="@isPresent"/>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="templateChecksSummaryStat">
        <dt style="width:260px;">Summary of rules outcome</dt>
        <dd>
            <xsl:call-template name="ewir"/>
        </dd>
        <!--<xsl:call-template name="dl-progress-bar">
            <xsl:with-param name="displayPercent">true</xsl:with-param>
            <xsl:with-param name="infoOnly">false</xsl:with-param>
            <xsl:with-param name="label">Conformity</xsl:with-param>
            <xsl:with-param name="count" select="numberValidExecutedCheck"/>
            <xsl:with-param name="total" select="numberExecutedCheck"/>
            <xsl:with-param name="percentage" select="percentageValidExecutedCheck"/>
            <xsl:with-param name="title-text-1">out of</xsl:with-param>
            <xsl:with-param name="title-text-2">executed checks are valid</xsl:with-param>
        </xsl:call-template>-->
        <!--
        <xsl:call-template name="dl-progress-bar">
        <xsl:with-param name="displayPercent">true</xsl:with-param>
            <xsl:with-param name="infoOnly">false</xsl:with-param>
            <xsl:with-param name="label">Conformity (based on types)</xsl:with-param>
            <xsl:with-param name="count" select="numberValidKindExecutedCheck"/>
            <xsl:with-param name="total" select="numberKindExecutedCheck"/>
            <xsl:with-param name="percentage" select="percentageValidKindExecutedCheck"/>
            <xsl:with-param name="title-text-1">out of</xsl:with-param>
            <xsl:with-param name="title-text-2">executed types of rule are valid</xsl:with-param>
        </xsl:call-template>
        -->
    </xsl:template>

    <!-- <xsl:template match="templateRichnessStats">
         <xsl:call-template name="dl-progress-bar">
         <xsl:with-param name="displayPercent">true</xsl:with-param>
             <xsl:with-param name="infoOnly">false</xsl:with-param>
             <xsl:with-param name="label">Richness (based on used templates)</xsl:with-param>
             <xsl:with-param name="count" select="numberKindSubTemplates"/>
             <xsl:with-param name="total" select="persentagePresentKindSubTemplates"/>
             <xsl:with-param name="percentage" select="persentagePresentKindSubTemplates"/>
             <xsl:with-param name="title-text-1">out of</xsl:with-param>
             <xsl:with-param name="title-text-2">defined templates are used</xsl:with-param>
         </xsl:call-template>
     </xsl:template>-->

</xsl:stylesheet>
