<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="1.0" encoding="utf-8" indent="yes"/>
    <xsl:template name="createjscript">
        <xsl:param name="templateParentId"/>
        <xsl:element name="script">
            <xsl:attribute name="type">text/javascript</xsl:attribute>
            
            function getvaluesfordisplay() {
                var json = 
            <xsl:call-template name="json4Template">
                <xsl:with-param name="templateStats" select="templateStats[templateIdentifier=$templateParentId]" />
                <xsl:with-param name="totalCount" select="1"/>
                <xsl:with-param name="idprefix" select="r"/>
                <xsl:with-param name="iter" select="0" />
            </xsl:call-template><xsl:text>; return json; }</xsl:text>
        </xsl:element>
    </xsl:template>
    <!--
    <xsl:template match="//templatesStatistics">
        <xsl:param name="totalCount" select="1"/>
        <xsl:param name="idprefix" select="r"/>
        <xsl:call-template name="createjscript">
            <xsl:with-param name="templateParentId">1.3.6.1.4.1.12559.11.10.1.3.1.1.3</xsl:with-param> 
        </xsl:call-template>
    </xsl:template>
    -->
<!--
    <xsl:template match="subtemplate">
        <xsl:param name="countSub"/>
        <xsl:param name="parentPrefix"/>
        <xsl:param name="subid" select="@templateId" />
        <xsl:call-template name="json4Template">
            <xsl:with-param name="templateStats" select="../../templateStats[templateIdentifier=$subid]" />
            <xsl:with-param name="totalCount" select="$countSub"/>
            <xsl:with-param name="idprefix" select="$parentPrefix"/>
        </xsl:call-template>
    </xsl:template>
    
    -->

    <xsl:template name="json4Template">
        <xsl:param name="templateStats"/>
        <xsl:param name="totalCount"/>
        <xsl:param name="idprefix"/>
        <xsl:param name="iter"  />
        <xsl:text>{ "id": "temp_</xsl:text>
        <xsl:value-of select="concat($idprefix, '_', position())"/>
        <xsl:text>", "name": "</xsl:text>
        <xsl:value-of select="$templateStats/templateName"/><xsl:text>[</xsl:text><xsl:value-of select="$templateStats/templateIdentifier"/><xsl:text>]</xsl:text>
        <xsl:text>", "data" : { "$color": "</xsl:text>
        <xsl:call-template name="setValidityColor">
            <xsl:with-param name="result" select="$templateStats/validationResult"/>
        </xsl:call-template>
        <xsl:text>","$richcolor": "</xsl:text>
        <xsl:call-template name="setRichnessColor">
            <xsl:with-param name="score" select="$templateStats/templateRichnessStats/templatesRichnessScoring"/>
        </xsl:call-template>
        <xsl:text>", "$validcolor": "</xsl:text>
        <xsl:call-template name="setValidityColor">
            <xsl:with-param name="result" select="$templateStats/validationResult"/>
        </xsl:call-template>
        <xsl:text>", "$status": "</xsl:text>
        <xsl:value-of select="$templateStats/validationResult"/>
        <xsl:text>", "$richness" : "</xsl:text>
        <xsl:value-of select="$templateStats/templateRichnessStats/templatesRichnessScoring"/>
        <xsl:text>%", "$validity" : "</xsl:text>
        <xsl:value-of select="$templateStats/templateChecksSummaryStat/percentageValidExecutedCheck"/>
        <xsl:text>%" }</xsl:text>
        <xsl:if test="(count($templateStats/subtemplate) &gt; 0) and (number($iter) &lt; 5)">
            <xsl:text>, "children" : [</xsl:text>
            <xsl:for-each select="$templateStats/subtemplate">
                <xsl:variable name="templateId" select="@templateId" />
                <xsl:call-template name="json4Template">
                    <xsl:with-param name="templateStats" select="../../templateStats[templateIdentifier=$templateId]" />
                    <xsl:with-param name="totalCount" select="count($templateStats/subtemplate)"/>
                    <xsl:with-param name="idprefix" select="concat($idprefix, '_', position())"/>
                    <xsl:with-param name="iter" select="number($iter) + 1" />
                </xsl:call-template>
            </xsl:for-each>
            <!--
            <xsl:apply-templates select="$templateStats/subtemplate">
                <xsl:with-param name="countSub" select="count($templateStats/subtemplate)"/>
                <xsl:with-param name="parentPrefix" select="concat($idprefix, '_', position())"/>
            </xsl:apply-templates>
           -->
            <xsl:text>]</xsl:text>
        </xsl:if>
        <xsl:text>}</xsl:text>
        <xsl:if test="position() &lt; $totalCount">
            <xsl:text>,</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template name="setValidityColor">
        <xsl:param name="result"/>
        <xsl:choose>
            <xsl:when test="$result = 'Report'">
                <xsl:text>#dff0d8</xsl:text>
            </xsl:when>
            <xsl:when test="$result = 'Error'">
                <xsl:text>#ebccd1</xsl:text>
            </xsl:when>
            <xsl:when test="$result = 'Warning'">
                <xsl:text>#ffd699</xsl:text>
            </xsl:when>
            <xsl:when test="$result = 'Missing'">
                <xsl:text>#bce8f1</xsl:text>
            </xsl:when>
            <xsl:when test="$result = 'unknown'">
                <xsl:text>#f5f5f5</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>#f5f5f5</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template name="setRichnessColor">
        <xsl:param name="score"/>
        <xsl:choose>
            <xsl:when test="$score = 100">
                <xsl:text>#dff0d8</xsl:text>
            </xsl:when>
            <xsl:when test="$score = 0">
                <xsl:text>#ebccd1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>#fcf8e3</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
