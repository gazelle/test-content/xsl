<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:include href="_cda-components-scorecard.xsl"/>

    <xsl:template match="documentStatistics">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="documentSummaryAccordion">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#documentSummaryCollapse"
                           aria-expanded="true" aria-controls="documentSummaryCollapse">
                            Summary
                        </a>
                    </h4>
                </div>
                <div id="documentSummaryCollapse" class="panel-collapse collapse in" role="tabpanel"
                     aria-labelledby="documentSummaryAccordion">
                    <div class="panel-body">
                        <xsl:apply-templates select="componentValidationStats"/>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="documentDetailAccordion">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                           href="#documentDetailCollapse" aria-expanded="false" aria-controls="documentDetailCollapse">
                            Detail
                        </a>
                    </h4>
                </div>
                <div id="documentDetailCollapse" class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="documentDetailAccordion">
                    <div class="panel-body">
                        <xsl:for-each select="componentValidationStats">
                            <xsl:call-template name="detailsOnComponent"/>
                        </xsl:for-each>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>


    <xsl:template name="detailsOnComponent">
        <div>
            <xsl:attribute name="class">
                <xsl:text>panel panel-</xsl:text>
                <xsl:if test="validationResult = 'PASSED'">success</xsl:if>
                <xsl:if test="validationResult = 'FAILED'">danger</xsl:if>
            </xsl:attribute>
            <div class="panel-heading">
                <xsl:value-of select="ValidationName"/>
                <xsl:element name="span">
                    <xsl:attribute name="style">
                        <xsl:text>float: right!important;</xsl:text>
                    </xsl:attribute>
                    <xsl:element name="title">Percentage of valid executed checks</xsl:element>
                    <xsl:value-of select="concat(constraintSummaryStat/percentageValidExecutedCheck,'%')"/>
                </xsl:element>
            </div>
            <div class="panel-body">
                <dl>
                    <xsl:attribute name="class">
                        <xsl:text>dl-horizontal large-title</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates select="constraintSummaryStat"/>
                </dl>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="constraintSummaryStat">
        <dl class="dl-horizontal large-title">
            <dt>Nb checks valid/executed</dt>
            <dd><xsl:value-of select='numberValidExecutedCheck'/>/<xsl:value-of select='numberExecutedCheck'/>
                (<xsl:value-of select='format-number(percentageValidExecutedCheck,"#.0")'/>%)
            </dd>
            <dt>Nb check kinds valid/executed</dt>
            <dd><xsl:value-of select='numberValidKindExecutedCheck'/>/<xsl:value-of select='numberKindExecutedCheck'/> (<xsl:value-of
                    select='format-number(percentageValidKindExecutedCheck,"#.0")'/>%)
            </dd>
            <dt>Nb check kinds executed/available</dt>
            <dd><xsl:value-of select='numberKindExecutedCheck'/>/<xsl:value-of select='numberKindCheck'/> (<xsl:value-of
                    select='format-number(percentageKindExecutedCheck,"#.0")'/>%)
            </dd>
            <dt>Kind of reports</dt>
            <dd>
                <xsl:call-template name="ewir"/>
            </dd>
        </dl>
    </xsl:template>

</xsl:stylesheet>
