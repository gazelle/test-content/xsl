<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template name="dl-progress-bar-with-link">
        <xsl:param name="infoOnly"/>
        <xsl:param name="label"/>
        <xsl:param name="count"/>
        <xsl:param name="total"/>
        <xsl:param name="percentage"/>
        <xsl:param name="title-text-1"/>
        <xsl:param name="title-text-2"/>
        <xsl:param name="displayPercent"/>
        <xsl:param name="link4label" />
        <dt style="width:260px;">
            <xsl:choose>
                <xsl:when test="string-length($link4label) &gt; 1">
                    <xsl:element name="a">
                        <xsl:attribute name="href">
                            <xsl:value-of select="$link4label"/>
                        </xsl:attribute>
                        <xsl:if test="not(starts-with($link4label, '#'))">
                            <xsl:attribute name="target">
                                <xsl:text>_blank</xsl:text>
                            </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="$label"/>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$label"/>
                </xsl:otherwise>
            </xsl:choose>
        </dt>
        <xsl:call-template name="dl-progress-bar-data">
            <xsl:with-param name="infoOnly" select="$infoOnly"/>
            <xsl:with-param name="displayPercent" select="$displayPercent"/>
            <xsl:with-param name="count" select="$count"/>
            <xsl:with-param name="total" select="$total"/>
            <xsl:with-param name="percentage" select="$percentage"/>
            <xsl:with-param name="title-text-1" select="$title-text-1"/>
            <xsl:with-param name="title-text-2" select="$title-text-2"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="dl-progress-bar">
        <xsl:param name="infoOnly"/>
        <xsl:param name="label"/>
        <xsl:param name="count"/>
        <xsl:param name="total"/>
        <xsl:param name="percentage"/>
        <xsl:param name="title-text-1"/>
        <xsl:param name="title-text-2"/>
        <xsl:param name="displayPercent"/>
        <dt style="width:260px;">
            <xsl:value-of select="$label"/>
        </dt>
        <xsl:call-template name="dl-progress-bar-data">
            <xsl:with-param name="infoOnly" select="$infoOnly"/>
            <xsl:with-param name="displayPercent" select="$displayPercent"/>
            <xsl:with-param name="count" select="$count"/>
            <xsl:with-param name="total" select="$total"/>
            <xsl:with-param name="percentage" select="$percentage"/>
            <xsl:with-param name="title-text-1" select="$title-text-1"/>
            <xsl:with-param name="title-text-2" select="$title-text-2"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="dl-progress-bar-data">
        <xsl:param name="infoOnly"/>
        <xsl:param name="count"/>
        <xsl:param name="total"/>
        <xsl:param name="percentage"/>
        <xsl:param name="title-text-1"/>
        <xsl:param name="title-text-2"/>
        <xsl:param name="displayPercent"/>
        <xsl:variable name="countInvalid" select="$total - $count"/>
        <xsl:variable name="percentInvalid">
        	<xsl:choose>
				<xsl:when test="(100 - $percentage) &gt; 3">
					<xsl:value-of select="100 - $percentage" />
				</xsl:when>
				<xsl:when test="$countInvalid = 0">
					<xsl:value-of select="'0'" />
				</xsl:when>
				<xsl:when test="$countInvalid &lt; 10">
					<xsl:value-of select="'1'" />
				</xsl:when>
				<xsl:when test="$countInvalid &lt; 100">
					<xsl:value-of select="'2'" />
				</xsl:when>
				<xsl:when test="$countInvalid &lt; 1000">
					<xsl:value-of select="'3'" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="100 - $percentage" />
				</xsl:otherwise>
			</xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="percentValid">
        	<xsl:value-of select="100 - $percentInvalid" />
        </xsl:variable>
        
        <dd class="progress">
            <xsl:attribute name="title">
                <xsl:value-of select="$count"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$title-text-1"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$total"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$title-text-2"/>
            </xsl:attribute>
            <!-- failures-->
            <div>
                <xsl:if test="$infoOnly = 'false' and $percentage &lt; 100">
                    <!-- failed-->
                    <xsl:attribute name="class">
                        <xsl:text>progress-bar progress-bar-danger</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="style">
                        <xsl:text>width: </xsl:text>
                        <xsl:value-of select='$percentInvalid'/>
                        <xsl:text>%;white-space: nowrap;</xsl:text>
                        <xsl:text>%; background-color: grey;</xsl:text>
                    </xsl:attribute>
                    <xsl:choose>
                        <xsl:when test="$displayPercent = 'true'">
                            <xsl:value-of select='format-number($percentInvalid, "#.0")'/>
                            <xsl:text>%</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$countInvalid"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
            </div>

            <!-- success-->
            <div>
                <xsl:attribute name="class">
                    <xsl:text>progress-bar progress-bar-</xsl:text>
                    <xsl:choose>
                        <xsl:when test="$infoOnly = 'true'">
                            <xsl:text>info</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>success</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
                <xsl:attribute name="style">
                    <xsl:text>width: </xsl:text>
                    <xsl:value-of select='$percentValid'/>
                    <xsl:text>%</xsl:text>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="$displayPercent = 'true'">
                        <xsl:value-of select='format-number($percentValid, "#.0")'/>
                        <xsl:text>%</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$count"/>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
        </dd>
    </xsl:template>
    
    <xsl:template name="progressBar">
        <xsl:param name="value"/>
        <xsl:param name="legend"/>
        <xsl:param name="infoOnly"/>
        <xsl:param name="width"/>
        <dd class="progress">
            <xsl:attribute name="style">
                <xsl:text>width:</xsl:text>
                <xsl:value-of select="$width"/>
            </xsl:attribute>
            <div>
                <xsl:attribute name="title">
                    <xsl:value-of select="format-number($value, '#.0')"/>
                    <xsl:value-of select="$legend"/>
                </xsl:attribute>
                <xsl:attribute name="class">
                    <xsl:text>progress-bar progress-bar-</xsl:text>
                    <xsl:choose>
                        <xsl:when test="$infoOnly = 'true'">
                            <xsl:text>info</xsl:text>
                        </xsl:when>
                        <xsl:when test="$value = 100">
                            <xsl:text>success</xsl:text>
                        </xsl:when>
                        <xsl:when test="$value = 0">
                            <xsl:text>danger</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>warning</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
                <xsl:attribute name="style">
                    <xsl:text>width:</xsl:text>
                    <xsl:value-of
                            select="$value"/>
                    <xsl:text>%</xsl:text>
                </xsl:attribute>
                <span>
                    <xsl:value-of select="format-number($value, '#.0')"/>
                    <xsl:text>%</xsl:text>
                </span>
            </div>
        </dd>
    </xsl:template>


</xsl:stylesheet>
