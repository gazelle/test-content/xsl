<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template name="buildTree">
        <xsl:call-template name="createjscript">
            <xsl:with-param name="templateParentId" select="templateParentId" />
        </xsl:call-template>
        <xsl:element name="div">
            <xsl:attribute name="class">row</xsl:attribute>
            <xsl:attribute name="id">container</xsl:attribute>
            <xsl:element name="div">
                <xsl:attribute name="id">left-container</xsl:attribute>
                <xsl:attribute name="class">col-md-2</xsl:attribute>
                <xsl:element name="p">
                    <b>Left click</b><xsl:text> to set a node as root for the visualization</xsl:text>
                </xsl:element>
                <xsl:element name="p">
                    <b> Right click</b><xsl:text> to set the parent node as root for the visualization.</xsl:text>
                </xsl:element>
                <xsl:element name="div">
                    <xsl:element name="label">
                        <xsl:attribute name="for">s-color</xsl:attribute>
                        <xsl:text>Color tree based on</xsl:text>
                    </xsl:element>
                    <xsl:element name="select">
                        <xsl:attribute name="name">s-color</xsl:attribute>
                        <xsl:attribute name="id">s-color</xsl:attribute>
                        <xsl:attribute name="class">form-control</xsl:attribute>
                        <xsl:element name="option">
                            <xsl:attribute name="id">validcolor</xsl:attribute>
                            <xsl:attribute name="selected">selected</xsl:attribute>
                            <xsl:text>Conformity</xsl:text>
                        </xsl:element>
                        <xsl:element name="option">
                            <xsl:attribute name="id">richcolor</xsl:attribute>
                            <xsl:text>Richness</xsl:text>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="br"></xsl:element>
                    <xsl:element name="label">
                        <xsl:attribute name="for">s-orientation</xsl:attribute>
                        <xsl:text>Orientation</xsl:text>
                    </xsl:element>
                    <xsl:element name="select">
                        <xsl:attribute name="name">s-orientation</xsl:attribute>
                        <xsl:attribute name="id">s-orientation</xsl:attribute>
                        <xsl:attribute name="class">form-control</xsl:attribute>
                        <xsl:element name="option">
                            <xsl:attribute name="value">h</xsl:attribute>
                            <xsl:attribute name="selected">selected</xsl:attribute>
                            <xsl:text>horizontal</xsl:text>
                        </xsl:element>
                        <xsl:element name="option">
                            <xsl:attribute name="value">v</xsl:attribute>
                            <xsl:text>vertical</xsl:text>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="br"></xsl:element>
                    <xsl:element name="div">
                        <xsl:attribute name="id">max-levels</xsl:attribute>
                        <xsl:element name="label">
                            <xsl:attribute name="for">i-levels-to-show</xsl:attribute>
                            <xsl:text>Levels to diplay</xsl:text>
                        </xsl:element>
                        <xsl:element name="select">
                            <xsl:attribute name="name">i-levels-to-show</xsl:attribute>
                            <xsl:attribute name="id">i-levels-to-show</xsl:attribute>
                            <xsl:attribute name="class">form-control</xsl:attribute>
                            <xsl:element name="option">
                                <xsl:text>all</xsl:text>
                            </xsl:element>
                            <xsl:element name="option">
                                <xsl:text>1</xsl:text>
                            </xsl:element>
                            <xsl:element name="option">
                            	<xsl:attribute name="selected">selected</xsl:attribute>
                                <xsl:text>2</xsl:text>
                            </xsl:element>
                            <xsl:element name="option">
                                <xsl:text>3</xsl:text>
                            </xsl:element>
                            <xsl:element name="option">
                                <xsl:text>4</xsl:text>
                            </xsl:element>
                            <xsl:element name="option">
                                <xsl:text>5</xsl:text>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
                <xsl:element name="br"></xsl:element>
                <xsl:element name="a">
                    <xsl:attribute name="id">update</xsl:attribute>
                    <xsl:attribute name="href">#</xsl:attribute>
                    <xsl:attribute name="class">gzl-btn-blue</xsl:attribute>
                    <xsl:text>Go to Parent</xsl:text>
                </xsl:element>
                <div id="id-list"></div>
            </xsl:element>
            <div id="center-container" class="col-md-9">
                <div id="infovis" class="infovis"></div>
            </div>
        </xsl:element>
    </xsl:template>
    <xsl:include href="_cda-template-to-json.xsl"/>
</xsl:stylesheet>
