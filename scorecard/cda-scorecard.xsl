<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template match="ValidationStatistics">
        <h2>Scorecard</h2>


        <xsl:apply-templates select="contextualInformation"/>

        <ul class="nav nav-tabs" role="tablist">
            <xsl:if test="constraintsStatistics/child::*">
                <li role="presentation" class="active">
                    <a href="#constraints" aria-controls="Constraints" role="tab" data-toggle="tab">Constraint
                        scoring
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="templatesStatistics/child::*">
                <li role="presentation">
                    <a href="#templates" aria-controls="Templates" role="tab" data-toggle="tab">Templates scoring</a>
                </li>
            </xsl:if>
            <xsl:if test="documentStatistics/child::*">
                <li role="presentation">
                    <a href="#documents" aria-controls="Documents" role="tab" data-toggle="tab">Documents scoring</a>
                </li>
            </xsl:if>
        </ul>

        <div class="tab-content tab-content-with-border">
            <xsl:if test="constraintsStatistics/child::*">
                <div role="tabpanel" class="tab-pane active" id="constraints">
                    <xsl:apply-templates select="constraintsStatistics"/>
                </div>
            </xsl:if>
            <xsl:if test="templatesStatistics/child::*">
                <div role="tabpanel" class="tab-pane" id="templates">
                    <xsl:apply-templates select="templatesStatistics">
                        <xsl:with-param name="type">flat</xsl:with-param>
                    </xsl:apply-templates>
                </div>
            </xsl:if>
            <xsl:if test="documentStatistics/child::*">
                <div role="tabpanel" class="tab-pane" id="documents">
                    <xsl:apply-templates select="documentStatistics"/>
                </div>
            </xsl:if>
        </div>
        <br/>
<!--         <xsl:if test="templatesStatistics/child::*"> -->
<!--             <div class="panel panel-default"> -->
<!--                 <div class="panel-heading"> -->
<!--                     <h4 class="panel-title">Templates</h4> -->
<!--                 </div> -->
<!--                 <div class="panel-body"> -->
<!--                     <a name="templatetree"/> -->
<!--                     <xsl:apply-templates select="templatesStatistics"> -->
<!--                         <xsl:with-param name="type">tree</xsl:with-param> -->
<!--                     </xsl:apply-templates> -->
<!--                 </div> -->
<!--             </div> -->
<!--         </xsl:if> -->


    </xsl:template>

    <xsl:template match="contextualInformation">
        <div class="panel panel-default">
            <div class="panel-heading">Contextual information</div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt>Validation identifier</dt>
                    <dd>
                        <xsl:value-of select="validationIdentifier"/>
                    </dd>
                    <dt>Statistics identifier</dt>
                    <dd>
                        <xsl:value-of select="statisticsIdentifier"/>
                    </dd>
                    <dt>Validation tool</dt>
                    <dd>
                        <xsl:value-of select="validationToolIdentifier"/>
                        <xsl:text> (version: </xsl:text>
                        <xsl:value-of select="validationVersion"/>
                        <xsl:text>)</xsl:text>
                    </dd>
                    <dt>Statistic tool</dt>
                    <dd>
                        <xsl:value-of select="statisticToolIdentifier"/>
                        <xsl:text> (version: </xsl:text>
                        <xsl:value-of select="statisticVersion"/>
                        <xsl:text>)</xsl:text>
                    </dd>
                    <dt>Effective date</dt>
                    <dd>
                        <xsl:value-of select="effectiveDate"/>
                    </dd>
                </dl>
            </div>
        </div>
    </xsl:template>

    <xsl:include href="_cda-constraints-scorecard.xsl"/>
    <xsl:include href="_cda-templates-scorecard.xsl"/>
    <xsl:include href="_cda-documents-scorecard.xsl"/>

</xsl:stylesheet>
