<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <!--<xsl:include href="_cda-summary-stats-scorecard.xsl"/>-->
    <xsl:param name="constraintPath">/CDAGenerator/docum/constraints/constraintDescriptor.seam?identifier=</xsl:param>
    
    <xsl:template match="constraintsStatistics">
        <div class="help-block">
            <p>This section of the scorecard gives you indications regarding the constraints which have been applied on
                your document during the validation process. Because
                some constraints are used several times, the scorecard also shows you how many distinct constraints were
                executed.
            </p>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="constraintSummaryAccordion">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#constraintSummaryCollapse" aria-expanded="true"
                       aria-controls="constraintSummaryCollapse">
                        Summary
                    </a>
                </h4>
            </div>
            <div id="constraintSummaryCollapse" class="panel-collapse collapse in" role="tabpanel"
                 aria-labelledby="constraintSummaryAccordion">
                <div class="panel-body">
                    <xsl:apply-templates select="summaryStatistics"/>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="constraintDetailAccordion">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#constraintDetailCollapse" aria-expanded="false"
                       aria-controls="constraintDetailCollapse">
                        Detail
                    </a>
                </h4>
            </div>
            <div id="constraintDetailCollapse" class="panel-collapse collapse in" role="tabpanel"
                 aria-labelledby="constraintDetailAccordion">
                <div class="panel-body">
                    <xsl:call-template name="sortedDetailedStatistics">
                        <xsl:with-param name="types" select="summaryStatistics/constraintTypeStats/constraintType"/>
                        <xsl:with-param name="details" select="detailedStatistics/constaintDetailedStats"/>
                    </xsl:call-template>
                    <!--<xsl:apply-templates select="detailedStatistics"/>-->
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="summaryStatistics">
        <xsl:apply-templates select="checksSummaryStat"/>
        <xsl:if test="constraintTypeStats">
            <div class="container-fluid">
                <h4>Types of constraints</h4>
                <xsl:apply-templates select="constraintTypeStats"/>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template match="checksSummaryStat">
        <dl class="dl-horizontal">
            <dt style="width:260px;">Summary of outcomes</dt>
            <dd class="progress">
                <xsl:call-template name="ewir"/>
            </dd>
            <div class="help-block">
                <p>The overall conformity progress bar below summarizes the conformity of your document in percentages, differentiating passed executed
                    constraints from other statuses of execution.
                </p>
            </div>
            <xsl:call-template name="dl-progress-bar">
                <xsl:with-param name="infoOnly">false</xsl:with-param>
                <xsl:with-param name="displayPercent">true</xsl:with-param>
                <xsl:with-param name="label">Overall conformity</xsl:with-param>
                <xsl:with-param name="count" select="numberValidExecutedCheck"/>
                <xsl:with-param name="total" select="numberExecutedCheck"/>
                <xsl:with-param name="percentage" select="percentageValidExecutedCheck"/>
                <xsl:with-param name="title-text-1">out of</xsl:with-param>
                <xsl:with-param name="title-text-2">executed constraints are passed</xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="unique-constraint-conformity">
                <xsl:with-param name="label">Conformity of unique constraints</xsl:with-param>
                <xsl:with-param name="valid" select="numberValidKindExecutedCheck"/>
                <xsl:with-param name="executed" select="numberKindExecutedCheck"/>
                <xsl:with-param name="total" select="numberKindCheck"/>
            </xsl:call-template>
        </dl>
    </xsl:template>

    <xsl:template match="constraintTypeStats">
        <div class="col-md-6">
            <dl class="dl-horizontal">
                <xsl:call-template name="dl-progress-bar-with-link">
                    <xsl:with-param name="label">
                        <xsl:value-of select="constraintType"/>
                    </xsl:with-param>
                    <xsl:with-param name="infoOnly">false</xsl:with-param>
                    <xsl:with-param name="displayPercent">false</xsl:with-param>
                    <xsl:with-param name="count" select="numberValidExecutedChecks"/>
                    <xsl:with-param name="total" select="numberExecutedChecks"/>
                    <xsl:with-param name="percentage" select="percentageValidExecutedChecks"/>
                    <xsl:with-param name="title-text-1">out of</xsl:with-param>
                    <xsl:with-param name="title-text-2">executed constraints are passed</xsl:with-param>
                    <xsl:with-param name="link4label">
                        <xsl:text>#</xsl:text>
                        <xsl:if test="not (percentageValidExecutedChecks = '100.0')">
                            <xsl:text>E</xsl:text>
                            <xsl:value-of select="constraintType"/>
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>
            </dl>
        </div>
    </xsl:template>

    <xsl:template name="sortedDetailedStatistics">
        <xsl:param name="types"/>
        <xsl:param name="details"/>
        <p class="help-block">Only the constraints which raised errors at least once are displayed below.</p>
        <xsl:for-each select="$types">
            <xsl:variable name="currentType" select="current()/text()"/>
            <xsl:variable name="matchingElements"
                          select="$details[constraintType = $currentType and not(percentageValidExecutedChecks = '100.0')]"/>
            <xsl:if test="count($matchingElements) &gt; 0">
                <h4>
                    <xsl:value-of select="$currentType"/>
                </h4>
                <div class="container-fluid">
                    <xsl:attribute name="id">
                        <xsl:text>E</xsl:text>
                        <xsl:value-of select="$currentType"/>
                    </xsl:attribute>
                    <xsl:for-each select="$matchingElements">
                        <xsl:apply-templates select="current()"/>
                    </xsl:for-each>
                </div>
            </xsl:if>
        </xsl:for-each>
        <!-- not all constraints have a constraintType attribute but we need to display them also -->
        <h4>
            <xsl:text>Untyped constraints</xsl:text>
        </h4>
        <div class="container-fluid">
            <xsl:for-each select="$details[not (constraintType) and not(percentageValidExecutedChecks = '100.0')]">
                <xsl:apply-templates select="current()"/>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template match="constaintDetailedStats">
        <div class="col-md-6">
            <dl class="dl-horizontal">
                <xsl:call-template name="dl-progress-bar-with-link">
                    <xsl:with-param name="infoOnly">false</xsl:with-param>
                    <xsl:with-param name="displayPercent">false</xsl:with-param>
                    <xsl:with-param name="label" select="constraintName"/>
                    <xsl:with-param name="link4label">
                        <xsl:value-of select="$constraintPath"/>
                        <xsl:value-of select="constraintIdentifier"/>
                    </xsl:with-param>
                    <xsl:with-param name="count" select="numberValidExecutedChecks"/>
                    <xsl:with-param name="total" select="numberExecutedChecks"/>
                    <xsl:with-param name="percentage" select="percentageValidExecutedChecks"/>
                    <xsl:with-param name="title-text-1">out of</xsl:with-param>
                    <xsl:with-param name="title-text-2">executions are passed</xsl:with-param>
                </xsl:call-template>
            </dl>
        </div>
    </xsl:template>

    <xsl:template name="unique-constraint-conformity">
        <xsl:param name="label"/>
        <xsl:param name="executed"/>
        <xsl:param name="total"/>
        <xsl:param name="valid"/>
        <xsl:variable name="countInvalid" select="$executed - $valid"/>
        <xsl:variable name="countNonExecuted" select="$total - $executed"/>
        <xsl:variable name="percentValid">
        	<xsl:call-template name="choosePercentage">
            	<xsl:with-param name="checks" select="number($valid)" />
            	<xsl:with-param name="total" select="$total" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="percentNonValid">
       		<xsl:call-template name="choosePercentage">
            	<xsl:with-param name="checks" select="number($countInvalid)" />
            	<xsl:with-param name="total" select="$total" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="percentNonExecuted" select="100 - ($percentValid + $percentNonValid)"/>
        <dt style="width:260px;">
            <xsl:value-of select="$label"/>
        </dt>
        <dd class="progress">
            <xsl:attribute name="title">
                <xsl:text>Successfully executed: </xsl:text>
                <xsl:value-of select="$valid"/>
                <xsl:text>, Executed with errors: </xsl:text>
                <xsl:value-of select="$executed - $valid"/>
                <xsl:text>, Not executed </xsl:text>
                <xsl:value-of select="$total - $executed"/>
            </xsl:attribute>
            <!-- failures-->
            <div>
                <xsl:attribute name="class">
                    <xsl:text>progress-bar progress-bar-danger</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="style">
                    <xsl:text>width: </xsl:text>
                    <xsl:value-of select="$percentNonValid"/>
                    <xsl:text>%</xsl:text>
                </xsl:attribute>
                <xsl:value-of select="$countInvalid"/>
            </div>
            <!-- success-->
            <div>
                <xsl:attribute name="class">
                    <xsl:text>progress-bar progress-bar-success</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="style">
                    <xsl:text>width: </xsl:text>
                    <xsl:value-of select="$percentValid"/>
                    <xsl:text>%</xsl:text>
                </xsl:attribute>
                <xsl:value-of select="$valid"/>
            </div>
            <!-- success-->
            <div>
                <xsl:attribute name="class">
                    <xsl:text>progress-bar</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="style">
                    <xsl:text>width: </xsl:text>
                    <xsl:value-of select="$percentNonExecuted"/>
                    <xsl:text>%; background-color: grey;</xsl:text>
                </xsl:attribute>
                <xsl:value-of select="$countNonExecuted"/>
            </div>
        </dd>
    </xsl:template>

</xsl:stylesheet>
