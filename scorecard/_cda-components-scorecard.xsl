<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template match="componentValidationStats">
        <h3><xsl:value-of select="ValidationName"/></h3>
        <dl>
            <xsl:attribute name="class">
                <xsl:text>dl-horizontal gzl-notification gzl-notification-</xsl:text>
                <xsl:if test="validationResult = 'PASSED'">green</xsl:if>
                <xsl:if test="validationResult = 'FAILED'">red</xsl:if>
            </xsl:attribute>
            <dt>Conformity</dt>
            <xsl:call-template name="progressBar">
                <xsl:with-param name="legend">% of valid checks</xsl:with-param>
                <xsl:with-param name="value" select="format-number(constraintSummaryStat/percentageValidExecutedCheck, '#.0')"/>
                <xsl:with-param name="width">20%</xsl:with-param>
                <xsl:with-param name="infoOnly">false</xsl:with-param>
            </xsl:call-template>
            <dt>Richness</dt>
            <xsl:call-template name="progressBar">
                <xsl:with-param name="legend">% of possible checks executed</xsl:with-param>
                <xsl:with-param name="value" select="format-number(constraintSummaryStat/percentageKindExecutedCheck, '#.0')"/>
                <xsl:with-param name="width">20%</xsl:with-param>
                <xsl:with-param name="infoOnly">true</xsl:with-param>
            </xsl:call-template>
        </dl>
    </xsl:template>



</xsl:stylesheet>