<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html" omit-xml-declaration="yes"/>

    <xsl:template match="/">

        <html>
            <head>
                <title>Scorecard</title>
                <link href="icicle/Icicle.css" rel="stylesheet" type="text/css"/>
                <link href="gazelle-theme.css" rel="stylesheet" type="text/css"/>
                <script language="javascript" type="text/javascript" src="icicle/jit-yc.js"></script>
                <script language="javascript" type="text/javascript" src="icicle/plottemplates.js"></script>
                <script src="jquery_1_11_2.min.js"/>
                <script src="bootstrap.min.js"/>
                <script type="text/javascript">
                    jq162 = jQuery.noConflict(true);
                </script>
            </head>
            <body onload="drawtemplate()">
                <div id="gzl-container">
                    <xsl:apply-templates select="ValidationStatistics"/>
                </div>
            </body>
        </html>

    </xsl:template>

    <xsl:include href="cda-scorecard.xsl"/>

</xsl:stylesheet>
