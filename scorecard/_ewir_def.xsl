<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

       <xsl:template name="ewir">
        <xsl:variable name="total" select="numberExecutedCheck"/>
        <xsl:variable name="valid" select="numberValidExecutedCheck"/>
        <xsl:variable name="info" select="numberInfoFound"/>
        <xsl:variable name="warning" select="numberWarningFound"/>
        <xsl:variable name="error" select="numberErrorsFound"/>
        <xsl:variable name="percentageInfo">
            <xsl:call-template name="choosePercentage">
            	<xsl:with-param name="checks" select="$info" />
            	<xsl:with-param name="total" select="$total" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="percentageWarning">
        	<xsl:call-template name="choosePercentage">
            	<xsl:with-param name="checks" select="$warning" />
            	<xsl:with-param name="total" select="$total" />
            </xsl:call-template>
        </xsl:variable>
           
        <xsl:variable name="percentageError">
            <xsl:call-template name="choosePercentage">
            	<xsl:with-param name="checks" select="$error" />
            	<xsl:with-param name="total" select="$total" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="percentageSuccess"
                      select="100 - ($percentageError + $percentageInfo + $percentageWarning)"/>
        <xsl:if test="numberExecutedCheck &gt; 0">
            <div class="progress">
                <xsl:attribute name="title">
                    <xsl:text>Error: </xsl:text>
                    <xsl:value-of select="$error"/>
                    <xsl:text>, Warning: </xsl:text>
                    <xsl:value-of select="$warning"/>
                    <xsl:text>, Info: </xsl:text>
                    <xsl:value-of select="$info"/>
                    <xsl:text>, Successful: </xsl:text>
                    <xsl:value-of select="$valid"/>
                </xsl:attribute>
                <xsl:if test="numberErrorsFound &gt; 0">
                    <div class="progress-bar progress-bar-danger">
                        <xsl:attribute name="style">
                            <xsl:text>width: </xsl:text>
                            <xsl:value-of select="$percentageError"/>
                            <xsl:text>%</xsl:text>
                        </xsl:attribute>
                        <span>
                            <xsl:value-of select="numberErrorsFound"/>
                        </span>
                    </div>
                </xsl:if>
                <xsl:if test="numberWarningFound &gt; 0">
                    <div class="progress-bar progress-bar-warning">
                        <xsl:attribute name="style">
                            <xsl:text>width: </xsl:text>
                            <xsl:value-of select="$percentageWarning"/>
                            <xsl:text>%</xsl:text>
                        </xsl:attribute>
                        <span>
                            <xsl:value-of select="numberWarningFound"/>
                        </span>
                    </div>
                </xsl:if>
                <xsl:if test="numberInfoFound &gt; 0">
                    <div class="progress-bar progress-bar-info">
                        <xsl:attribute name="style">
                            <xsl:text>width: </xsl:text>
                            <xsl:value-of select="$percentageInfo"/><xsl:text>%</xsl:text>
                        </xsl:attribute>
                        <span>
                            <xsl:value-of select="numberInfoFound"/>
                        </span>
                    </div>
                </xsl:if>
                <xsl:if test="$percentageSuccess &gt; 0">
                    <div class="progress-bar progress-bar-success">
                        <xsl:attribute name="style">
                            <xsl:text>width:</xsl:text>
                            <xsl:value-of select="$percentageSuccess"/>
                            <xsl:text>%</xsl:text>
                        </xsl:attribute>
                        <span>
                            <xsl:value-of select="numberValidExecutedCheck"/>
                        </span>
                    </div>
                </xsl:if>
            </div>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="choosePercentage">
		<xsl:param name="checks" />
		<xsl:param name="total" />
		<xsl:choose>
			<xsl:when test="($checks * 100 div $total) &gt; 3">
				<xsl:value-of select="($checks * 100 div $total)" />
			</xsl:when>
			<xsl:when test="$checks = 0">
				<xsl:value-of select="'0'" />
			</xsl:when>
			<xsl:when test="$checks &lt; 10">
				<xsl:value-of select="'1'" />
			</xsl:when>
			<xsl:when test="$checks &lt; 100">
				<xsl:value-of select="'2'" />
			</xsl:when>
			<xsl:when test="$checks &lt; 1000">
				<xsl:value-of select="'3'" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$checks * 100 div $total" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
    

</xsl:stylesheet>
