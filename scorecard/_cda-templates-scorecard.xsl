<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="1.0" encoding="utf-8" indent="yes"/>
    <xsl:template match="templatesStatistics">
        <xsl:param name="type"/>
<!--         <xsl:if test="$type = 'tree'"> -->
<!--             <xsl:call-template name="buildTree"/> -->
<!--         </xsl:if> -->
        <xsl:if test="$type = 'flat'">
            <xsl:call-template name="flatTemplateScoring"/>
        </xsl:if>
    </xsl:template>
    <xsl:include href="_cda-templates-flat.xsl"/>
<!--     <xsl:include href="_cda-templates-tree.xsl"/> -->
</xsl:stylesheet>