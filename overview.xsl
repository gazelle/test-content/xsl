<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/TR/xhtml1/strict">

	<!-- This stylesheet is used to build the main index HTML file.
		It uses typedb.xml as input. It uses triky stuff as xsl:key -->

	<xsl:output method="html" indent="yes" />

	<!-- xsl:key is used to index the transform elements by their module name -->
	<xsl:key name="trans-by-mod" match="transform" use="@module" />

	<xsl:template match="ValidationResultsOverview">
		<html>
			<head>
				<title>Validation Results Overview</title>
				<link rel="stylesheet" type="text/css" media="screen" href="theme.css"/> 
			</head>
			<body>
			<div id="main">
				<h1>Inria HL7 Message Validation Result Overview</h1>
				<table>
					<tr>
						<td>ObjectGuid:</td>
						<td>
							<xsl:value-of
								select="ValidationResultsOverview/ObjectGuid/text()" />
						</td>
					</tr>
					<tr>
						<td>Standard Name:</td>
						<td>
							<xsl:value-of
								select="ReferencedStandard/StandardName/text()" />
						</td>
					</tr>
					<tr>
						<td>Standard Version:</td>
						<td>
							<xsl:value-of
								select="ReferencedStandard/StandardVersion/text()" />
						</td>
					</tr>
					<tr>
						<td>Standard Extension:</td>
						<td>
							<xsl:value-of
								select="ReferencedStandard/StandardExtension/text()" />
						</td>
					</tr>
					<tr></tr><tr></tr><tr></tr>
					<h2>Validation Service Information</h2>
					<tr>
						<td>Validation Service Name:</td>
						<td>
							<xsl:value-of
								select="ValidationServiceName/text()" />
						</td>
					</tr>
					<tr>
						<td>Validation Service Version:</td>
						<td>
							<xsl:value-of
								select="ValidationServiceVersion/text()" />
						</td>
					</tr>
					<tr>
						<td>Validation Service Status:</td>
						<td>
							<xsl:value-of
								select="ValidationServiceStatus/Status/text()" />
						</td>
					</tr>
					<tr>
						<td>Validation Service Additional Info:</td>
						<td>
							<xsl:value-of
								select="ValidationServiceStatus/AdditionalStatusInfo/text()" />
						</td>
					</tr>
						
				<tr></tr>
				<tr></tr>
				<tr></tr>
				<h2>Validation Test Information</h2>
					<tr>
						<td>Validation Date:</td>
						<td>
							<xsl:value-of
								select="ValidationDate/text()" />
						</td>
					</tr>
					<tr>
						<td>Validation Time:</td>
						<td>
							<xsl:value-of
								select="ValidationTime/text()" />
						</td>
					</tr>
					<tr>
						<td>Validation Test ID:</td>
						<td>
							<xsl:value-of
								select="ValidationTestId/text()" />
						</td>
					</tr>
					<tr>
						<td>Validation Test:</td>
						<td>
							<xsl:value-of
								select="ValidationTest/text()" />
						</td>
					</tr>
				</table>
				</div>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
