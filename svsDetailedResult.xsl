<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 02, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Abderrazek Boufahja, IHE Development, Kereval</xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:param name="index"/>
    
    <xsl:template match="/">
        <html>
            <head>
                <title>External Validation Report</title>
                <link href="http://gazelle.ihe.net/xsl/resultStyle.css" rel="stylesheet" type="text/css" media="screen"/>
            </head>
            <body>
                <script type="text/javascript">
                    function hideOrViewValidationDetails() {
                       var detaileddiv = document.getElementById('resultdetailedann');
                       if (detaileddiv != null){
                           var onn = document.getElementById('resultdetailedann_switch_on');
                           if (onn != null){
                               if (onn.style.display == 'block') onn.style.display = 'none';
                               else if (onn.style.display == 'none') onn.style.display = 'block';
                           }
                           var off = document.getElementById('resultdetailedann_switch_off');
                           if (off != null){
                               if (off.style.display == 'block') off.style.display = 'none';
                               else if (off.style.display == 'none') off.style.display = 'block';
                           }
                           var body = document.getElementById('resultdetailedann_body');
                           if (body != null){
                               if (body.style.display == 'block') body.style.display = 'none';
                               else if (body.style.display == 'none') body.style.display = 'block';
                           }
                       }
                    }
 		    function hideOrUnhide(elem){
                        var elemToHide = document.getElementById(elem.name + '_p');
                        if (elemToHide != null){
                                if (elem.checked){
                                    elemToHide.style.display = 'none';
                                }
                                else{
                                    elemToHide.style.display = 'block';
                                }
                           }
                    }

                </script>
                <h2>External Validation Report</h2>
                <br/>
                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header">General Information</div>
                    <div class="rich-panel-body">
                        <table border="0">
                            <tr>
                                <td><b>Validation Date</b></td>
                                <td><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationDate"/> - <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTime"/></td> 
                            </tr>
                            <tr>
                                <td><b>Validation Service</b></td>
                                <td><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceName"/> (<xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceVersion"/>)</td>         
                            </tr>
                            <tr>
                                <td><b>Validation Test Status</b></td>
                                <td>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                        <div class="PASSED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                        <div class="FAILED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                                        <div class="ABORTED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br/>
               
                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header ">Result overview</div>
                    <div class="rich-panel-body ">
                        <span>
                            <div>
                                <table border="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <b>
                                                    <a>
                                                        <xsl:attribute name="href">#wellformed<xsl:value-of select="$index"/></xsl:attribute>
                                                        XML 
                                                    </a>
                                                </b>
                                            </td>
                                            <td>
                                                <div>
                                                    <xsl:attribute name="class"><xsl:value-of select="detailedResult/DocumentWellFormed/Result"/></xsl:attribute>
                                                    <xsl:value-of select="detailedResult/DocumentWellFormed/Result"/>
                                                </div>
                                            </td>
                                        </tr>
                                        <xsl:if test="count(detailedResult/DocumentValidXSD) = 1">
                                        <tr>
                                            <td>
                                                <b>
                                                    <a>
                                                        <xsl:attribute name="href">#xsd<xsl:value-of select="$index"/></xsl:attribute>
                                                        XSD 
                                                    </a>
                                                </b>
                                            </td>
                                            <td>
                                                <div>
                                                    <xsl:attribute name="class"><xsl:value-of select="detailedResult/DocumentValidXSD/Result"/></xsl:attribute>
                                                    <xsl:value-of select="detailedResult/DocumentValidXSD/Result"/>
                                                </div>
                                            </td>
                                        </tr>
                                        </xsl:if>
                                        <xsl:if test="count(detailedResult/MDAValidation) = 1">
                                        <tr>
                                            <td>
                                                <b>
                                                    <a>
                                                        <xsl:attribute name="href">#mbv<xsl:value-of select="$index"/></xsl:attribute>
                                                        Model Based Validation 
                                                    </a>
                                                </b>
                                            </td>
                                            <td>
                                                <div>
                                                    <xsl:attribute name="class"><xsl:value-of select="detailedResult/MDAValidation/Result"/></xsl:attribute>
                                                    <xsl:value-of select="detailedResult/MDAValidation/Result"/>
                                                </div>
                                            </td>
                                        </tr>
                                        </xsl:if>
                                    </tbody>
                                </table>
                            </div>
                        </span>
                    </div>
                </div>
                <br/>
                
                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header ">
                        <a>
                            <xsl:attribute name="name">wellformed<xsl:value-of select="$index"/></xsl:attribute>
                            XML Validation Report
                        </a>
                    </div>
                    <div class="rich-panel-body ">
                        <span>
                            <div>
                                <xsl:choose>
                                    <xsl:when test="detailedResult/DocumentWellFormed/Result = 'PASSED'">
                                        <p class="PASSED">The XML document is well-formed</p>                        
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <p class="FAILED">The XML document is not well-formed</p>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                        </span>
                    </div>
                </div>
                <br/>
                
                <xsl:if test="count(detailedResult/DocumentValidXSD) = 1">
                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header">
                        <a>
                           <xsl:attribute name="name">xsd<xsl:value-of select="$index"/></xsl:attribute>
                            XSD Validation detailed Results
                        </a>
                    </div>
                    <div class="rich-panel-body">
                        <i>The document you have validated is supposed to be an XML document. The validator has checked if it is well-formed and has validated it against one ore several XSD schemas, results of those validations are gathered in this part.</i>
                        
                            <xsl:choose>
                                <xsl:when test="detailedResult/DocumentValidXSD/Result = 'PASSED'">
                                    <p class="PASSED">The XML document is valid regarding the schema</p>
                                </xsl:when>
                                <xsl:otherwise>
                                    <p class="FAILED">The XML document is not valid regarding the schema because of the following reasons: </p>
                                    <xsl:if test="count(detailedResult/DocumentValidXSD/XSDMessage) &gt; 0">
                                        <ul>
                                            <xsl:for-each select="detailedResult/DocumentValidXSD/XSDMessage">
                                                <li><xsl:value-of select="Message"/></li>
                                            </xsl:for-each>
                                        </ul>
                                    </xsl:if>
                                </xsl:otherwise>
                            </xsl:choose>
                    </div>
                </div>
                <br/>
                <br/>
                </xsl:if>
                
                <xsl:if test="count(detailedResult/MDAValidation) = 1">
                <div class="rich-stglpanel" id="resultdetailedann">
                    <div class="rich-stglpanel-header" onclick="hideOrViewValidationDetails();">
                        <a>
                            <xsl:attribute name="name">mbv<xsl:value-of select="$index"/></xsl:attribute>
                            Model Based Validation details
                        </a>
                        <div class="rich-stglpanel-marker">
                            <div id="resultdetailedann_switch_on" class="rich-stglpnl-marker" style="display: block;"><xsl:text disable-output-escaping="yes">&amp;laquo;</xsl:text></div>
                            <div id="resultdetailedann_switch_off" class="rich-stglpnl-marker" style="display: none;"><xsl:text disable-output-escaping="yes">&amp;raquo;</xsl:text></div>
                        </div>
                    </div>
                    <div class="rich-stglpanel-body styleResultBackground" style="display: block;" id="resultdetailedann_body">
                        <table class="styleResultBackground">
                            <tbody>
                                <tr>
                                    <td>
                                        <b>Result</b>
                                    </td>
                                    <td>
					<div>
                                                    <xsl:attribute name="class"><xsl:value-of select="detailedResult/MDAValidation/Result"/></xsl:attribute>
                                                    <xsl:value-of select="detailedResult/MDAValidation/Result"/>
                                                </div>                                    
				</td>
                                </tr>
                                <tr>
                                    <td width="100px" valign="top">
                                        <b>Summary</b>
                                    </td>
                                    <td>
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Error) + count(detailedResult/MDAValidation/Warning) + count(detailedResult/MDAValidation/Info) + count(detailedResult/MDAValidation/Note)"/> checks <br/>
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Error)"/> errors <br/>
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Warning)"/> warning <br/>
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Note)"/> reports <br/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <b>HIDE : </b>
                        <input type="checkbox" onclick="hideOrUnhide(this)" name="Errors">Errors</input>
                        <input type="checkbox" onclick="hideOrUnhide(this)" name="Warnings">Warnings</input>
                        <input type="checkbox" onclick="hideOrUnhide(this)" name="Infos">Reports</input>
                        <xsl:if test="count(detailedResult/MDAValidation/Error) &gt; 0">
                            <div id="Errors_p">
                            <p id="errors"><b>Errors</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Error">
                                <table class="Error" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>Test</b></td>
                                        <td><xsl:value-of select="Test"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Location</b></td>
                                        <td><xsl:value-of select="Location"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Description</b></td>
                                        <td><xsl:value-of select="Description"/></td>
                                    </tr>
                                </table>
                                <br/>
                            </xsl:for-each>
				</div>
                        </xsl:if>
                        <xsl:if test="count(detailedResult/MDAValidation/Warning) &gt; 0">
                            <div id="Warnings_p">
                            <p id="warnings"><b>Warnings</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Warning">
                                <table class="Warning" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>Test</b></td>
                                        <td><xsl:value-of select="Test"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Location</b></td>
                                        <td><xsl:value-of select="Location"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Description</b></td>
                                        <td><xsl:value-of select="Description"/></td>
                                    </tr>
                                </table>
                                <br/>
                            </xsl:for-each>
			   </div>
                        </xsl:if>
                        <xsl:if test="count(detailedResult/MDAValidation/Note) &gt; 0">
		 	   <div id="Infos_p">
                            <p id="notes"><b>Reports</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Note">
                                <table class="Report" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>Test</b></td>
                                        <td><xsl:value-of select="Test"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Location</b></td>
                                        <td><xsl:value-of select="Location"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Description</b></td>
                                        <td><xsl:value-of select="Description"/></td>
                                    </tr>
                                </table>
                                <br/>
                            </xsl:for-each>
			  </div>
                        </xsl:if>
                        
                    </div>
                </div>
                    </xsl:if>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
