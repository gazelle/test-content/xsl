<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xd"
	version="1.0">
	<xd:doc scope="stylesheet">
		<xd:desc>
			<xd:p>
				<xd:b>Created on:</xd:b>
				Jun 29, 2012
			</xd:p>
			<xd:p>
				<xd:b>Author:</xd:b>
				aberge
			</xd:p>
			<xd:p></xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="/">
		<html>
			<head>
				<link href="/css/hl7Tables.css" rel="stylesheet"
					type="text/css" media="screen" />
			</head>
			<body>
				<h1>HL7 Tables</h1>
				<div class="rich-panel">
					<div class="rich-panel-header">Specification</div>
					<div class="rich-panel-body">
						<table>
							<tr width="40%">
								<td style="font-weight:bold;">Conformance type</td>
								<td>
									<xsl:value-of select="Specification/@ConformanceType" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">HL7 OID</td>
								<td>
									<xsl:value-of select="Specification/@HL7OID" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">HL7 Version</td>
								<td>
									<xsl:value-of select="Specification/@HL7Version" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Organisation name</td>
								<td>
									<xsl:value-of select="Specification/@OrgName" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Procedure rule</td>
								<td>
									<xsl:value-of select="Specification/@ProcRule" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Role</td>
								<td>
									<xsl:value-of select="Specification/@Role" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Specification name</td>
								<td>
									<xsl:value-of select="Specification/@SpecName" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Specification version</td>
								<td>
									<xsl:value-of select="Specification/@SpecVersion" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Status</td>
								<td>
									<xsl:value-of select="Specification/@Status" />
								</td>
							</tr>
						</table>
					</div>
				</div>
				<br />
				<div class="rich-panel">
					<div class="rich-panel-header">Conformance</div>
					<div class="rich-panel-body">
						<table>
							<tr width="40%">
								<td style="font-weight:bold;">Acceptation acknowledgment</td>
								<td>
									<xsl:value-of select="Specification/Conformance/@AccAck" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Application acknowledgment</td>
								<td>
									<xsl:value-of select="Specification/Conformance/@AppAck" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Dynamic ID</td>
								<td>
									<xsl:value-of select="Specification/Conformance/@DynamicID" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Message acknowledgment mode</td>
								<td>
									<xsl:value-of select="Specification/Conformance/@MsgAckMode" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Query mode</td>
								<td>
									<xsl:value-of select="Specification/Conformance/@QueryMode" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Query status</td>
								<td>
									<xsl:value-of select="Specification/Conformance/@QueryStatus" />
								</td>
							</tr>
							<tr>
								<td style="font-weight:bold;">Static ID</td>
								<td>
									<xsl:value-of select="Specification/Conformance/@StaticID" />
								</td>
							</tr>
						</table>
					</div>
				</div>
				<br />
				<div class="rich-panel">
					<div class="rich-panel-header">Encodings</div>
					<div class="rich-panel-body">
						<p>Below is the list of encodings</p>
						<ul>
							<xsl:for-each select="Specification/Encodings/Encoding">
								<li>
									<xsl:value-of select="text()" />
								</li>
							</xsl:for-each>
						</ul>
					</div>
				</div>
				<br />
				<xsl:element name="form">
					<xsl:attribute name="name">formular</xsl:attribute>
					<table>
						<tr>
							<td align="right">List of HL7 tables gathered in this file</td>
							<td>
								<xsl:element name="select">
									<xsl:attribute name="name">listbox</xsl:attribute>
									<xsl:attribute name="onChange">location = this.options[this.selectedIndex].value;</xsl:attribute>
									<xsl:for-each select="Specification/hl7tables/hl7table">
										<xsl:sort select="@id" />
										<xsl:element name="option">
											<xsl:attribute name="value">#<xsl:value-of
												select="@id" /></xsl:attribute>
											<xsl:value-of select="@id" />
											<xsl:text> - </xsl:text>
											<xsl:value-of select="@name" />
										</xsl:element>
									</xsl:for-each>
								</xsl:element>
							</td>
						</tr>
					</table>

				</xsl:element>
				<br />
				<xsl:apply-templates select="Specification/hl7tables/hl7table" />
			</body>
		</html>
	</xsl:template>
	<xsl:template match="hl7table">
		<div class="rich-panel">
			<xsl:element name="a">
				<xsl:attribute name="name">
                    <xsl:value-of select="@id" />
                </xsl:attribute>
			</xsl:element>
			<div class="rich-panel-header">
				<xsl:value-of select="@id" />
				<xsl:text> - </xsl:text>
				<xsl:value-of select="@name" />
			</div>
			<div class="rich-panel-body">
				<p>
					<a href="#top">Top</a>
				</p>
				<table border="1">
					<thead>
						<tr style="font-weight:bold;">
							<th>ID</th>
							<th>Name</th>
							<th>System code</th>
							<th>Type</th>
						</tr>
					</thead>
					<tbody>
						<td>
							<xsl:value-of select="@id" />
						</td>
						<td>
							<xsl:value-of select="@name" />
						</td>
						<td>
							<xsl:value-of select="@codeSys" />
						</td>
						<td>
							<xsl:value-of select="@type" />
						</td>
					</tbody>
				</table>
				<br />
				<xsl:choose>
					<xsl:when test="count(tableElement) = 0">
						<xsl:text>No element present in this table</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<p>
							<i>
								<xsl:text>The table below gathers the subset elements of the HL7 defined table valid in the context of this validation. Attributes date, creator, instruction and order are not shown; download the XML file to see them.</xsl:text>
							</i>
						</p>
						<table border="1">
							<thead>
								<tr style="font-weight:bold;">
									<th>Code</th>
									<th>Display name</th>
									<th>Source</th>
									<th>Description</th>
									<th>Usage</th>
								</tr>
							</thead>
							<tbody>
								<xsl:for-each select="tableElement">
									<xsl:if test="@usage = 'Forbidden'">
										<tr onmouseover="this.style.backgroundColor='#94C2E6';"
											onmouseout="this.style.backgroundColor='#FFFFFF'"
											style="text-decoration:line-through;">
											<td>
												<xsl:value-of select="@code" />
											</td>
											<td>
												<xsl:value-of select="@displayName" />
											</td>
											<td>
												<xsl:value-of select="@source" />
											</td>
											<td>
												<xsl:value-of select="@description" />
											</td>
											<td>
												<xsl:value-of select="@usage" />
											</td>
										</tr>
									</xsl:if>
									<xsl:if test="not(@usage = 'Forbidden')">
										<tr onmouseover="this.style.backgroundColor='#94C2E6';"
											onmouseout="this.style.backgroundColor='#FFFFFF'">
											<td>
												<xsl:value-of select="@code" />
											</td>
											<td>
												<xsl:value-of select="@displayName" />
											</td>
											<td>
												<xsl:value-of select="@source" />
											</td>
											<td>
												<xsl:value-of select="@description" />
											</td>
											<td>
												<xsl:value-of select="@usage" />
											</td>
										</tr>
									</xsl:if>
								</xsl:for-each>
							</tbody>
						</table>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
		<br />
	</xsl:template>
</xsl:stylesheet>
