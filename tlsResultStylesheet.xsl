<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html" media-type="text/html"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jul 19, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Anne-Gaëlle BERGE, IHE Europe</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
                <h2>Certificates Validation Report</h2>
                <br/>
                <!-- general information -->
                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header">General Information</div>
                    <div class="rich-panel-body">
                        <table border="0">
                            <tr>
                                <td><b>Validation Date</b></td>
                                <td><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationDate"/> - <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTime"/></td> 
                            </tr>
                            <tr>
                                <td><b>Validation Service</b></td>
                                <td><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceName"/> (<xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceVersion"/>)</td>         
                            </tr>
                            
                            <tr>
                                <td>
                                    <b>Validator</b>
                                </td>
                                <td>
                                    <xsl:value-of
                                        select="detailedResult/ValidationResultsOverview/ValidationMethod" />
                                </td>
                            </tr>
                            <tr>
                                <td><b>Revocation</b></td>
                                <td>                              
                                    <xsl:choose>
                                        <xsl:when test="contains(detailedResult/ValidationResultsOverview/CheckRevocation, 'true')">
                                            <xsl:text>Checked</xsl:text>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:text>Not checked</xsl:text>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </td></tr>                        
                            <tr>
                                <td><b>Validation Test Status</b></td>
                                <td>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                        <div class="PASSED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                        <div class="FAILED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br/>
                <br/>
                <xsl:if test="count(detailedResult/CertificatesValidation) = 1">
                    <div class="rich-panel styleResultBackground" >
                        <div class="rich-panel-header">Certificates Validation</div>
                        <div class="rich-panel-body">
                            <xsl:if test="count(detailedResult/CertificatesValidation/Error) &gt; 0">
                                <p id="errors"><b>Errors</b></p>
                                <xsl:for-each select="detailedResult/CertificatesValidation/Error">
                                    <table class="Error" width="98%">
                                        <tr>
                                            <td valign="top" width="100"><b>Test</b></td>
                                            <td><xsl:value-of select="Test"/></td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><b>Description</b></td>
                                            <td><xsl:value-of select="Description"/></td>
                                        </tr>
                                    </table>
                                    <br/>
                                </xsl:for-each>
                            </xsl:if>
                            <xsl:if test="count(detailedResult/CertificatesValidation/Warning) &gt; 0">
                                <p id="warnings"><b>Warnings</b></p>
                                <xsl:for-each select="detailedResult/CertificatesValidation/Warning">
                                    <table class="Warning" width="98%">
                                        <tr>
                                            <td valign="top" width="100"><b>Test</b></td>
                                            <td><xsl:value-of select="Test"/></td>
                                        </tr>
                                        <tr>
                                            <td valign="top"><b>Description</b></td>
                                            <td><xsl:value-of select="Description"/></td>
                                        </tr>
                                    </table>
                                    <br/>
                                </xsl:for-each>
                            </xsl:if>
                        </div>
                    </div>
                </xsl:if>
                
    </xsl:template>
</xsl:stylesheet>
