<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xdw="urn:ihe:iti:xdw:2011"
    xmlns:hl7="urn:hl7-org:v3"
    xmlns:oasis="http://docs.oasis-open.org/ns/bpel4people/ws-humantask/types/200803">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"
        omit-xml-declaration="yes"/>
            
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 10, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Abderrazek Boufahja, IHE-Europe Development, Kereval</xd:p>
        </xd:desc>
    </xd:doc>
    <!-- CDA document -->

    <xsl:variable name="tableWidth">50%</xsl:variable>

    <xsl:variable name="title">
        <xsl:choose>
            <xsl:when test="/xdw:XDW.WorkflowDocument/xdw:title">
                <xsl:value-of select="/xdw:XDW.WorkflowDocument/xdw:title"/>
            </xsl:when>
            <xsl:otherwise>XDW Document</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:template match="/">
        <xsl:apply-templates select="xdw:XDW.WorkflowDocument"/>
    </xsl:template>

    <xsl:template match="xdw:XDW.WorkflowDocument">
        <html>
            <head>
                <!-- <meta name='Generator' content='&CDA-Stylesheet;'/> -->
                <title>
                    <xsl:value-of select="$title"/>
                </title>
            </head>
            <body>

                <h2 align="center">
                    <xsl:value-of select="$title"/>
                </h2>
                
                <xsl:call-template name="generalInformation"/>
                
                <xsl:call-template name="patient"/>
                
                <xsl:call-template name="listAuthors"/>
                
                <h2>List of Tasks</h2>
                <xsl:if test="count(/xdw:XDW.WorkflowDocument/xdw:TaskList) = 1">
                    <xsl:for-each select="/xdw:XDW.WorkflowDocument/xdw:TaskList/xdw:XDWTask">
                        <div style="margin-left:10px;margin-right:10px;margin-bottom:10px;background-color:#aaddff;width:99%">
                            <div style="margin-left:10;">
                                <xsl:if test="count(xdw:taskData) = 1">

                                    <h3>Task 
                                        <xsl:if test="count(xdw:taskData/oasis:taskDetails) = 1">
                                            <xsl:value-of select="xdw:taskData/oasis:taskDetails/oasis:name"/>
                                        </xsl:if>
                                    </h3>
                                    <xsl:if test="count(xdw:taskData/oasis:taskDetails) = 1">
                                        <b>Task Type : </b>
                                        <xsl:value-of select="xdw:taskData/oasis:taskDetails/oasis:taskType"/>
                                        <br/>
                                        <b>Status: </b>
                                        <xsl:value-of select="xdw:taskData/oasis:taskDetails/oasis:status"/>
                                        <br/>
                                        <b>Created Time : </b>
                                        <xsl:value-of select="xdw:taskData/oasis:taskDetails/oasis:createdTime"/>
                                        <br/>
                                        <b>Last Modified Time : </b>
                                        <xsl:value-of select="xdw:taskData/oasis:taskDetails/oasis:lastModifiedTime"/>
                                        <br/>
                                    </xsl:if>
                                    <b>Description : </b>
                                    <xsl:value-of select="xdw:taskData/oasis:description"/>
                                    <br/>
                                    <xsl:if test="count(xdw:taskData/oasis:input/oasis:part) > 0">
                                    <table>
                                        <tr>
                                            <td style='width:50px;vertical-align:top;'>
                                                <xsl:attribute name="rowspan"><xsl:value-of select="count(xdw:taskData/oasis:input/oasis:part)"/></xsl:attribute>
                                                <b>Input</b>
                                            </td>     
                                            <td>
                                                <xsl:value-of select="xdw:taskData/oasis:input/oasis:part[1]/@name"/> (<xsl:call-template name="extractPartIdentifier"><xsl:with-param name="path" select="xdw:taskData/oasis:input/oasis:part[1]"></xsl:with-param></xsl:call-template>)
                                            </td>
                                        </tr>
                                        <xsl:if test="count(xdw:taskData/oasis:input/oasis:part) > 1">
                                            <xsl:for-each select="xdw:taskData/oasis:input/oasis:part">
                                                <xsl:if test="position()>1">
                                            <tr>
                                                <td>
                                                    <xsl:value-of select="@name"/> (<xsl:call-template name="extractPartIdentifier"><xsl:with-param name="path" select="."></xsl:with-param></xsl:call-template>)
                                                </td>                   
                                            </tr>
                                                </xsl:if>
                                            </xsl:for-each>
                                        </xsl:if>
                                    </table>
                                    </xsl:if>
                                    <xsl:if test="count(xdw:taskData/oasis:output/oasis:part) > 0">
                                        <table>
                                            <tr>
                                                <td style='width:50px;vertical-align:top;'>
                                                    <xsl:attribute name="rowspan"><xsl:value-of select="count(xdw:taskData/oasis:output/oasis:part)"/></xsl:attribute>
                                                    <b>Output</b>
                                                </td>     
                                                <td>
                                                    <xsl:value-of select="xdw:taskData/oasis:output/oasis:part[1]/@name"/> (<xsl:call-template name="extractPartIdentifier"><xsl:with-param name="path" select="xdw:taskData/oasis:output/oasis:part[1]"></xsl:with-param></xsl:call-template>)
                                                </td>
                                            </tr>
                                            <xsl:if test="count(xdw:taskData/oasis:output/oasis:part) > 1">
                                                <xsl:for-each select="xdw:taskData/oasis:output/oasis:part">
                                                    <xsl:if test="position()>1">
                                                        <tr>
                                                            <td>
                                                                <xsl:value-of select="@name"/> (<xsl:call-template name="extractPartIdentifier"><xsl:with-param name="path" select="."></xsl:with-param></xsl:call-template>)
                                                            </td>                   
                                                        </tr>
                                                    </xsl:if>
                                                </xsl:for-each>
                                            </xsl:if>
                                        </table>
                                    </xsl:if>
                                </xsl:if>

                                <table width="99%" cellspacing="1" cellpadding="5" style="margin-left:10px;margin-right:10px;margin-bottom:10px;">
                                    <thead style="font-weight: bold;">
                                        <tr>
                                            <td style="text-align:center;background-color:#99ccee;" colspan="5">TaskEventHistory</td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td style="background-color:#99ccee;">id</td>
                                            <td style="background-color:#99ccee;">eventTime</td>
                                            <td style="background-color:#99ccee;">identifier</td>
                                            <td style="background-color:#99ccee;">eventType</td>
                                            <td style="background-color:#99ccee;">status</td>
                                        </tr>
                                    </thead>
                                    <tbody cellspacing="1">
                                        <xsl:if test="count(xdw:taskEventHistory) = 1">
                                            <xsl:for-each select="xdw:taskEventHistory/xdw:taskEvent">
                                                <tr>
                                                    <td style="background-color:#88bbdd;">
                                                        <xsl:value-of select="xdw:id"/>
                                                    </td>
                                                    <td style="background-color:#88bbdd;">
                                                        <xsl:value-of select="xdw:eventTime"/>
                                                    </td>
                                                    <td style="background-color:#88bbdd;">
                                                        <xsl:value-of select="xdw:identifier"/>
                                                    </td>
                                                    <td style="background-color:#88bbdd;">
                                                        <xsl:value-of select="xdw:eventType"/>
                                                    </td>
                                                    <td style="background-color:#88bbdd;">
                                                        <xsl:value-of select="xdw:status"/>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:if>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </xsl:for-each>
                </xsl:if>
                
                <xsl:call-template name="listDocumentHistory"/>
                
            </body>
        </html>
    </xsl:template>
    
    <xsl:template name="extractPartIdentifier">
        <xsl:param name="path" />
        <xsl:if test="count($path//@uid)>0">
            <xsl:value-of select="$path//@uid"/>
        </xsl:if>
        <xsl:if test="count($path//oasis:identifier)>0">
            <xsl:value-of select="$path//oasis:identifier/text()"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="generalInformation">
        <table width="100%" cellspacing="1">
            <tr>
                <td style="width:200px;background-color:#aaaaee;">Instance ID</td>
                <td style="background-color:#aaccff;">
                    <xsl:value-of select="/xdw:XDW.WorkflowDocument/xdw:workflowInstanceId"/>
                </td>
            </tr>
            
            <tr>
                <td style="width:200px;background-color:#aaaaee;">Sequence Number</td>
                <td style="background-color:#aaccff;">
                    <xsl:value-of select="/xdw:XDW.WorkflowDocument/xdw:workflowDocumentSequenceNumber"/>
                </td>
            </tr>
            
            <tr>
                <td style="width:200px;background-color:#aaaaee;">Status</td>
                <td style="background-color:#aaccff;color : #0000aa; font-weight : bold;">
                    <xsl:value-of select="/xdw:XDW.WorkflowDocument/xdw:workflowStatus"/>
                </td>
            </tr>
            
            <tr>
                <td style="width:200px;background-color:#aaaaee;">WD Reference</td>
                <xsl:for-each select="/xdw:XDW.WorkflowDocument/xdw:workflowDefinitionReference">
                    <td style="background-color:#aaccff;">
                        <xsl:call-template name="getContent">
                            <xsl:with-param name="content" select="/xdw:XDW.WorkflowDocument/xdw:workflowDefinitionReference"/>
                        </xsl:call-template>
                    </td>
                </xsl:for-each>
            </tr>
        </table>
    </xsl:template>
    
    <xsl:template name="patient">
        <h2>Patient</h2>
        <div style="margin-left:10px;margin-right:10px;margin-bottom:10px;background-color:#aaddff;width:99%;">
            
           <table style="width:100%;" cellspacing="1">
               <tr><td width="10%" style="background-color:#99ccee;"><b>Patient: </b></td>
                   <td width="40%" style="background-color:#99ccee;">
                       <xsl:if test="count(/xdw:XDW.WorkflowDocument/xdw:patient) = 1">
                           <xsl:call-template name="getName">
                               <xsl:with-param name="name" select="/xdw:XDW.WorkflowDocument/xdw:patient/xdw:name"/>
                           </xsl:call-template>
                       </xsl:if></td>
                   <td width="25%" align="right" style="background-color:#99ccee;"><b>MRN: </b></td>
                   <td width="25%" style="background-color:#99ccee;"><xsl:if test="count(/xdw:XDW.WorkflowDocument/xdw:patient) = 1">
                       <xsl:if test="count(/xdw:XDW.WorkflowDocument/xdw:patient/xdw:id) = 1">
                           <xsl:value-of select="/xdw:XDW.WorkflowDocument/xdw:patient/xdw:id/@extension"/>
                       </xsl:if>
                   </xsl:if></td>		       
               </tr>
               
               <tr>
                   <td width="15%" style="background-color:#99ccee;"><b>Birthdate: </b></td>
                   <td width="40%" style="background-color:#99ccee;"><xsl:if test="count(/xdw:XDW.WorkflowDocument/xdw:patient) = 1">
                       <xsl:if test="count(/xdw:XDW.WorkflowDocument/xdw:patient/xdw:birthTime) = 1">
                           <xsl:call-template name="formatDate">
                               <xsl:with-param name="date" select="/xdw:XDW.WorkflowDocument/xdw:patient/xdw:birthTime/@value"/>
                           </xsl:call-template>
                       </xsl:if>
                   </xsl:if></td>
                   <td width="25%" align="right" style="background-color:#99ccee;"><b>Sex: </b></td>
                   <td width="25%" style="background-color:#99ccee;"><xsl:if test="count(/xdw:XDW.WorkflowDocument/xdw:patient) = 1">
                       <xsl:if test="count(/xdw:XDW.WorkflowDocument/xdw:patient/xdw:administrativeGenderCode) = 1">
                           <xsl:variable name="sex" select="/xdw:XDW.WorkflowDocument/xdw:patient/xdw:administrativeGenderCode/@code"/>
                           <xsl:choose>
                               <xsl:when test="$sex='M'">Male</xsl:when>
                               <xsl:when test="$sex='F'">Female</xsl:when>
                           </xsl:choose>
                       </xsl:if>
                   </xsl:if></td>		       
               </tr>
           </table>
           
           <xsl:if test="count(/xdw:XDW.WorkflowDocument/xdw:patient) = 0">
               <div style="background-color:#77aaff;"><b>The patient is not defined !</b></div>
           </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template name="listAuthors">
        <h2>List of Authors</h2>
        <div style="margin-left:10px;margin-right:10px;margin-bottom:10px;background-color:#ffffaa;width:99%">
            <table style="width:100%; background-color:#ffffaa;" cellspacing="1">
                <tbody>
                    <xsl:for-each select="/xdw:XDW.WorkflowDocument/xdw:author">
                        <xsl:call-template name="createAuthor"/>
                    </xsl:for-each>
                </tbody>
            </table>
        </div>
    </xsl:template>
    
    <xsl:template name="listDocumentHistory">
        <xsl:if test="count(/xdw:XDW.WorkflowDocument/xdw:workflowStatusHistory) = 1">
            <h2>Worflow Status History</h2>
            <xsl:if test="not (count(/xdw:XDW.WorkflowDocument/xdw:workflowStatusHistory/xdw:documentEvent) = 0)">
                <div style="margin-left:10px;margin-right:10px;margin-bottom:10px;width:99%">
                    <table width="100%" style="background-color:#ffffaa;" cellspacing="1" cellpadding="5">
                        <thead style="font-weight: bold;">
                            <tr>
                                <td style="background-color:#eeee99;">eventTime</td>
                                <td style="background-color:#eeee99;">eventType</td>
                                <td style="background-color:#eeee99;">taskEventIdentifier</td>
                                <td style="background-color:#eeee99;">author</td>
                                <td style="background-color:#eeee99;">previousStatus</td>
                                <td style="background-color:#eeee99;">actualStatus</td>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="/xdw:XDW.WorkflowDocument/xdw:workflowStatusHistory/xdw:documentEvent">
                                <tr>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="xdw:eventTime"/>
                                    </td>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="xdw:eventType"/>
                                    </td>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="xdw:taskEventIdentifier"/>
                                    </td>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="xdw:author"/>
                                    </td>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="xdw:previousStatus"/>
                                    </td>
                                    <td style="background-color:#dddd88;">
                                        <xsl:value-of select="xdw:actualStatus"/>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                        
                    </table>
                </div>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="extractAttachement">   
        <tr>
            <td>
                <xsl:value-of select="@name"/> (<xsl:value-of select="reference/@uid"/>)
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template name="reference">        
        <xsl:value-of select="@uid"/>
    </xsl:template>
    
    <xsl:template name="createAuthor">
        <xsl:if test="count(xdw:assignedAuthor/hl7:assignedPerson) = 1">
           <tr>
               
               <td width="12%" valign="center" style="background-color: #dddd88;"><b>Author </b></td>
               <td width="40%" style="background-color: #dddd88;">
                   <xsl:call-template name="getName">
                       <xsl:with-param name="name" select="xdw:assignedAuthor/hl7:assignedPerson/hl7:name"/>
                   </xsl:call-template>
               </td>
               <td width="25%" align="right" valign="center" style="background-color: #dddd88;">
                   <b>MRN: </b>
               </td>
               <td width="25%" style="background-color: #dddd88;">
                   <xsl:if test="count(xdw:assignedAuthor/hl7:assignedPerson/hl7:typeId) = 1">
                       <xsl:value-of select="xdw:assignedAuthor/hl7:assignedPerson/hl7:typeId/@extension"/>
                    </xsl:if>
               </td>		       
           </tr>
        </xsl:if>
        
        <xsl:if test="count(xdw:assignedAuthor/hl7:assignedAuthoringDevice) = 1">
            <tr>
                
                <td width="12%" valign="center" style="background-color: #dddd88;"><b>Authoring Device </b></td>
                <td width="25%" style="background-color: #dddd88;" valign="center">
                    <xsl:if test="count(xdw:assignedAuthor/hl7:assignedAuthoringDevice/hl7:typeId) = 1">
                        <xsl:value-of select="xdw:assignedAuthor/hl7:assignedAuthoringDevice/hl7:typeId/@extension"/>
                    </xsl:if>
                </td>
                <td width="25%" align="right" style="background-color: #dddd88;vertical-align:center;">
                    <b>Informations: </b>
                </td>
                <td width="38%" style="background-color: #dddd88;" valign="center">
                    <b>Code: </b><xsl:if test="count(xdw:assignedAuthor/hl7:assignedAuthoringDevice/hl7:code) = 1">
                        <xsl:value-of select="xdw:assignedAuthor/hl7:assignedAuthoringDevice/hl7:code/@code"/></xsl:if><br/>
                    <b>manufacturer Model Name: </b><xsl:if test="count(xdw:assignedAuthor/hl7:assignedAuthoringDevice/hl7:manufacturerModelName) = 1">
                        <xsl:value-of select="xdw:assignedAuthor/hl7:assignedAuthoringDevice/hl7:manufacturerModelName/@code"/></xsl:if><br/>
                    <b>software Name: </b><xsl:if test="count(xdw:assignedAuthor/hl7:assignedAuthoringDevice/hl7:softwareName) = 1">
                        <xsl:value-of select="xdw:assignedAuthor/hl7:assignedAuthoringDevice/hl7:softwareName/@code"/></xsl:if>
                </td>		       
            </tr>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="getContent">
        <xsl:param name="content"/>
        <xsl:value-of select="$content"/>
    </xsl:template>

    <!-- Get a Name  -->
    <xsl:template name="getName">
        <xsl:param name="name"/>
        <xsl:choose>
            <xsl:when test="$name/xdw:family">
                <xsl:value-of select="$name/xdw:given"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$name/xdw:family"/>
                <xsl:text> </xsl:text>
                <xsl:if test="$name/xdw:suffix">
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="$name/xdw:suffix"/>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$name"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--  Format Date 
    
      outputs a date in Month Day, Year form
      e.g., 19991207  ==> December 07, 1999
-->
    <xsl:template name="formatDate">
        <xsl:param name="date"/>
        <xsl:variable name="month" select="substring ($date, 5, 2)"/>
        <xsl:choose>
            <xsl:when test="$month='01'">
                <xsl:text>January </xsl:text>
            </xsl:when>
            <xsl:when test="$month='02'">
                <xsl:text>February </xsl:text>
            </xsl:when>
            <xsl:when test="$month='03'">
                <xsl:text>March </xsl:text>
            </xsl:when>
            <xsl:when test="$month='04'">
                <xsl:text>April </xsl:text>
            </xsl:when>
            <xsl:when test="$month='05'">
                <xsl:text>May </xsl:text>
            </xsl:when>
            <xsl:when test="$month='06'">
                <xsl:text>June </xsl:text>
            </xsl:when>
            <xsl:when test="$month='07'">
                <xsl:text>July </xsl:text>
            </xsl:when>
            <xsl:when test="$month='08'">
                <xsl:text>August </xsl:text>
            </xsl:when>
            <xsl:when test="$month='09'">
                <xsl:text>September </xsl:text>
            </xsl:when>
            <xsl:when test="$month='10'">
                <xsl:text>October </xsl:text>
            </xsl:when>
            <xsl:when test="$month='11'">
                <xsl:text>November </xsl:text>
            </xsl:when>
            <xsl:when test="$month='12'">
                <xsl:text>December </xsl:text>
            </xsl:when>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="substring ($date, 7, 1)=&quot;0&quot;">
                <xsl:value-of select="substring ($date, 8, 1)"/>
                <xsl:text>, </xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="substring ($date, 7, 2)"/>
                <xsl:text>, </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="substring ($date, 1, 4)"/>
    </xsl:template>

</xsl:stylesheet>