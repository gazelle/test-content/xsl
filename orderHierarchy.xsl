<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    exclude-result-prefixes="xd"
    version="1.0">
    <xsl:output indent="yes" omit-xml-declaration="yes" method="html"/>
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Apr 12, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> aberge</xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
    	<html>
    		<head>
    			<link href="https://gazelle.ihe.net/css/orderHierarchy.css" rel="stylesheet" type="text/css" media="screen"/>
    		</head>
    		<body>
		<h1><xsl:value-of select="OrderHierarchy/@name"/> (<xsl:value-of select="OrderHierarchy/@version"/>)</h1>
    		    <xsl:element name="form">
    		        <xsl:attribute name="name">formular</xsl:attribute>
    		        <table>
    		            <tr>
    		                <td align="right">List of ordering codes</td>
    		                <td>
    		                    <xsl:element name="select">
    		                        <xsl:attribute name="name">listbox</xsl:attribute>
    		                        <xsl:attribute name="onChange">location = this.options[this.selectedIndex].value;</xsl:attribute>
    		                        <xsl:for-each select="OrderHierarchy/Order">
    		                            <xsl:sort select="@universalServiceID"/>
    		                            <xsl:element name="option">
    		                              <xsl:attribute name="value">#<xsl:value-of select="@universalServiceID"/></xsl:attribute>
    		                                <xsl:value-of select="@universalServiceID"/>
    		                            </xsl:element>
    		                        </xsl:for-each>
    		                    </xsl:element>
    		                </td>
    		            </tr>
    		        </table>
    		    </xsl:element>
    			<xsl:apply-templates select="OrderHierarchy/Order"/>
    		</body>
    	</html>
    </xsl:template>
    <xsl:template match="Order">
        <div class="order">
            <h1><xsl:element name="a"><xsl:attribute name="id"><xsl:value-of select="@universalServiceID"/></xsl:attribute></xsl:element>Order</h1>
            <p><b>Universal Service ID: </b><xsl:value-of select="@universalServiceID"/></p>
            <table border="0">
                <tr>
                    <xsl:apply-templates select="RequestedProcedure"/>
                </tr>
            </table>
            <xsl:element name="a">
                <xsl:attribute name="href">#</xsl:attribute>top
            </xsl:element>
        </div>
    </xsl:template>
    <xsl:template match="RequestedProcedure">
        <td>
            <div class="procedure">
                <h1>Requested Procedure <xsl:value-of select="position()"/></h1>
                <p><b>Code: </b><xsl:value-of select="@code"/> 
                    <xsl:if test="count(@codeMeaning) > 0"><xsl:text> - </xsl:text><xsl:value-of select="@codeMeaning"/></xsl:if>
                    <xsl:if test="count(@codingSchemeDesignator) > 0"><xsl:text> (</xsl:text><xsl:value-of select="@codingSchemeDesignator"/><xsl:text>)</xsl:text></xsl:if></p>
                <xsl:if test="count(@description) > 0">
                <p><b>Description: </b><xsl:value-of select="@description"/></p>
                </xsl:if>
                <table border="0">
                    <tr>
                        <xsl:apply-templates select="ScheduledProcedureStep"/>
                    </tr>
                </table>
            </div>
        </td>
    </xsl:template>
    <xsl:template match="ScheduledProcedureStep">
        <td>
            <div class="sps">
                <h1>Scheduled Procedure Step <xsl:value-of select="position()"/></h1>
                <p><b>Description: </b><xsl:value-of select="@description"/></p>
                <xsl:if test="count(@modality) > 0">
                <p><b>Modality: </b><xsl:value-of select="@modality"/></p>
                </xsl:if>
                <xsl:if test="count(@modality) = 0">
                    <p><b>Modality: </b>Unspecified</p>
                </xsl:if>
                <table border="0">
                    <tr>
                        <xsl:apply-templates select="ProtocolItem"/>
                    </tr>
                </table>
            </div>
        </td>
    </xsl:template>
    <xsl:template match="ProtocolItem">
        <td>
            <div class="protocol">
                <h1>Protocol <xsl:value-of select="position()"/></h1>
                <p><b>Code: </b><xsl:value-of select="@code"/></p>
                <p><b>Code meaning: </b><xsl:value-of select="@codeMeaning"/></p>
                <p><b>Coding Scheme Designator: </b><xsl:value-of select="@codingSchemeDesignator"/></p>
            </div>
        </td>
    </xsl:template>
</xsl:stylesheet>
