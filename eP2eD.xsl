<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:n1="urn:hl7-org:v3" xmlns="urn:hl7-org:v3"
    xmlns:n2="urn:hl7-org:v3/meta/voc" xmlns:voc="urn:hl7-org:v3/voc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:epsos="urn:epsos-org:ep:medication"
    xmlns:darva="xalan://com.projet.util.Utilitaire">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes" />
    <!-- CDA document -->
    <xsl:param name="id" />
    <xsl:param name="extension" />
    <xsl:param name="aa" /> 
    <xsl:param name="effectivetime"/>
    <xsl:param name="language" />
    
    <xsl:param name="pharma.id.root" />
    <xsl:param name="pharma.id.extension" />
    <xsl:param name="pharma.id.assigningAuthorityName" />
    <xsl:param name="pharma.given" />
    <xsl:param name="pharma.family" />
    <xsl:param name="pharma.org.id.root" />
    <xsl:param name="pharma.org.id.extension" />
    <xsl:param name="pharma.org.name" />
    
    <xsl:param name="custodian.id.root" />
    <xsl:param name="custodian.id.extension" />
    <xsl:param name="custodian.name" />
    
    <xsl:template match="/">
        <xsl:apply-templates select="n1:ClinicalDocument"/>
    </xsl:template>
    <xsl:template match="n1:ClinicalDocument">
        <ClinicalDocument xmlns="urn:hl7-org:v3" xmlns:cda="urn:hl7-org:v3"
            xmlns:epsos="urn:epsos-org:ep:medication"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <typeId root="2.16.840.1.113883.1.3" extension="POCD_HD000040"/>
            <templateId root="1.3.6.1.4.1.12559.11.10.1.3.1.1.2"/>
            <templateId root="1.3.6.1.4.1.12559.11.10.1.3.1.2.2"/>
            <id displayable="true">
                <xsl:attribute name="root">
                    <xsl:value-of select="$id"/>
                </xsl:attribute>
                <xsl:attribute name="extension">
                    <xsl:value-of select="$extension"/>
                </xsl:attribute>
                <xsl:attribute name="assigningAuthorityName">
                    <xsl:value-of select="$aa"/>
                </xsl:attribute>
            </id>
            <code code="60593-1" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC"
                displayName="eDispensation"/>
            <title>Generated Dispensation By CDAGenerator</title>
            <effectiveTime>
                <xsl:attribute name="value">
                    <xsl:value-of select="$effectivetime"/>
                </xsl:attribute>
            </effectiveTime>
            <confidentialityCode code="N" codeSystem="2.16.840.1.113883.5.25"
                codeSystemName="Confidentiality" displayName="normal"/>
            <languageCode>
                <xsl:attribute name="code">
                    <xsl:value-of select="$language"/>
                </xsl:attribute>
            </languageCode>
            
            <xsl:copy-of select="/n1:ClinicalDocument/n1:recordTarget"/>
            
            <author typeCode="AUT" contextControlCode="OP">
                <functionCode code="2262" codeSystem="2.16.840.1.113883.2.9.6.2.7" codeSystemName="ISCO" displayName="Pharmacists"/>
                <!--Koostamise aeg-->
                <time>
                    <xsl:attribute name="value">
                        <xsl:value-of select="$effectivetime"/>
                    </xsl:attribute>
                </time>
                <assignedAuthor classCode="ASSIGNED">
                    <xsl:call-template name="addPersonInfo" />
                </assignedAuthor>
            </author>
            
            <custodian typeCode="CST">
                <assignedCustodian classCode="ASSIGNED">
                    <representedCustodianOrganization classCode="ORG" determinerCode="INSTANCE">
                        <id>
                            <xsl:attribute name="root"><xsl:value-of select="$custodian.id.root"/></xsl:attribute>
                            <xsl:attribute name="extension"><xsl:value-of select="$custodian.id.extension"/></xsl:attribute>
                        </id>
                        <name>
                            <xsl:value-of select="$custodian.name"/>
                        </name>
                    </representedCustodianOrganization>
                </assignedCustodian>
            </custodian>
            
            <legalAuthenticator>
                <time>
                    <xsl:attribute name="value"><xsl:value-of select="$effectivetime"/></xsl:attribute>
                </time>
                <signatureCode code="S"/>
                <assignedEntity>
                    <id>
                        <xsl:attribute name="root"><xsl:value-of select="$pharma.id.root"/></xsl:attribute>
                        <xsl:attribute name="extension"><xsl:value-of select="$pharma.id.extension"/></xsl:attribute>
                        <xsl:attribute name="assigningAuthorityName"><xsl:value-of select="$pharma.id.assigningAuthorityName"/></xsl:attribute>
                        <xsl:attribute name="displayable">false</xsl:attribute>
                    </id> 
                    <!-- R1.10.8 required nullFlavor=NI allowed and the "value" and "use" attributes shall be omitted otherwise  " attributes shall be present-->
                    <telecom nullFlavor="NI"/>
                    <assignedPerson>
                        <name>
                            <family>
                                <xsl:value-of select="$pharma.family"/>
                            </family>
                            <given>
                                <xsl:value-of select="$pharma.given"/>
                            </given>
                        </name>
                    </assignedPerson>
                    <xsl:call-template name="representedORGTemplate" />
                </assignedEntity>
            </legalAuthenticator>
            
            <xsl:for-each select="/n1:ClinicalDocument/n1:documentationOf">
                <xsl:copy-of select="self::*" />
            </xsl:for-each>
            
            <xsl:for-each select="/n1:ClinicalDocument/n1:relatedDocument">
                <xsl:copy-of select="self::*" />
            </xsl:for-each>
            
            <component typeCode="COMP" contextConductionInd="true">
                <structuredBody classCode="DOCBODY" moodCode="EVN">
                    
                    <xsl:for-each select="/n1:ClinicalDocument/n1:component/n1:structuredBody/n1:component">
                        <component contextConductionInd="true" typeCode="COMP">
                            <xsl:for-each select="child::node()[name()='section']" xml:base="ss">
                                <section classCode="DOCSECT" moodCode="EVN">
                                    <templateId root="2.16.840.1.113883.10.20.1.8" />
                                    <templateId root="1.3.6.1.4.1.12559.11.10.1.3.1.2.2" />
                                    <xsl:copy-of select="child::node()[name()='id']" />
                                    <code code="60590-7" codeSystem="2.16.840.1.113883.6.1" codeSystemName="LOINC" displayName="Medication dispensed"/>
                                    <title>eDispensation</title>
                                    <xsl:copy-of select="child::node()[name()='text']" />
                                    <xsl:for-each select="child::node()[name()='entry']">
                                        <entry typeCode="COMP" contextConductionInd="true">
                                            <supply classCode="SPLY" moodCode="EVN">
                                                <templateId root="2.16.840.1.113883.10.20.1.34" />
                                                <templateId root="1.3.6.1.4.1.19376.1.5.3.1.4.7.3" />
                                                <templateId root="1.3.6.1.4.1.12559.11.10.1.3.1.3.3" />
                                                <!-- the id should be created as an unique identifier, and not copied from the prescription -->
                                                <xsl:for-each select="child::node()[name()='substanceAdministration']">
                                                    <xsl:for-each select="child::*">
                                                        <xsl:if test="name()='id'
                                                            or name()='text' or name()='statusCode'
                                                            or name()='quantity' or name()='effectiveTime'">
                                                            <xsl:copy-of select="self::*"/>
                                                        </xsl:if>
                                                        
                                                        <xsl:if test="name()='consumable'">
                                                            <quantity value="1" unit="1" />
                                                            <product>
                                                                <xsl:for-each select="child::node()[name()='manufacturedProduct']">
                                                                    <manufacturedProduct classCode="MANU">
                                                                        <xsl:for-each select="child::*">
                                                                            <xsl:if test="not(self::node()[name()='manufacturedMaterial'])">
                                                                                <xsl:copy-of select="self::*"/>
                                                                            </xsl:if>
                                                                            <xsl:if test="self::node()[name()='manufacturedMaterial']">
                                                                                <manufacturedMaterial classCode="MMAT" determinerCode="KIND">
                                                                                   <xsl:for-each select="child::*">
                                                                                       <xsl:if test="self::node()[name()='realmCode' or name()='typeId' or name()='templateId']">
                                                                                           <xsl:copy-of select="self::*"/>
                                                                                       </xsl:if>
                                                                                   </xsl:for-each>
                                                                                   <epsos:id>
                                                                                       <xsl:attribute name="root">
                                                                                           <xsl:variable name="uid" xmlns:uuid="java.util.UUID" select="uuid:randomUUID()"/>
                                                                                           <xsl:value-of select="$uid"/>
                                                                                       </xsl:attribute>
                                                                                       <xsl:attribute name="extension">CDAGEN</xsl:attribute>
                                                                                   </epsos:id>
                                                                                   <xsl:for-each select="child::*">
                                                                                       <xsl:if test="not(self::node()[name()='realmCode' or name()='typeId' or name()='templateId'])">
                                                                                           <xsl:if test="self::node()[name()='epsos:asContent']">
                                                                                               <epsos:asContent classCode="CONT">
                                                                                                   <xsl:for-each select="child::*">
                                                                                                       <xsl:if test="self::node()[name()='epsos:containerPackagedMedicine']">
                                                                                                           <epsos:containerPackagedMedicine classCode="CONT" determinerCode="INSTANCE">
                                                                                                               <xsl:for-each select="child::*">
                                                                                                                   <xsl:if test="self::node()[name()='epsos:formCode']">
                                                                                                                       <xsl:for-each select="parent::node()">
                                                                                                                           <xsl:for-each select="parent::node()">
                                                                                                                               <xsl:for-each select="parent::node()">
                                                                                                                                   <xsl:copy-of select="child::node()[name()='epsos:formCode']"/>
                                                                                                                                </xsl:for-each>
                                                                                                                            </xsl:for-each>
                                                                                                                       </xsl:for-each>  
                                                                                                                   </xsl:if>
                                                                                                                   <xsl:if test="not(self::node()[name()='epsos:formCode'])">
                                                                                                                       <xsl:copy-of select="self::*"/>   
                                                                                                                   </xsl:if>
                                                                                                               </xsl:for-each>
                                                                                                           </epsos:containerPackagedMedicine>
                                                                                                       </xsl:if>
                                                                                                       <xsl:if test="not(self::node()[name()='epsos:containerPackagedMedicine'])">
                                                                                                           <xsl:copy-of select="self::*"/>    
                                                                                                       </xsl:if>
                                                                                                   </xsl:for-each>
                                                                                               </epsos:asContent>
                                                                                           </xsl:if>
                                                                                           <xsl:if test="not(self::node()[name()='epsos:asContent'])">
                                                                                               <xsl:copy-of select="self::*"/>    
                                                                                           </xsl:if>
                                                                                       </xsl:if>
                                                                                   </xsl:for-each>
                                                                                </manufacturedMaterial>
                                                                            </xsl:if>
                                                                        </xsl:for-each>
                                                                    </manufacturedProduct>
                                                                </xsl:for-each>
                                                            </product>
                                                            
                                                            <performer typeCode="PRF">
                                                                <time>
                                                                    <xsl:attribute name="value">
                                                                        <xsl:value-of select="$effectivetime"/>
                                                                    </xsl:attribute>
                                                                </time>
                                                                <assignedEntity>
                                                                    <xsl:call-template name="addPersonInfo" />
                                                                </assignedEntity>
                                                            </performer>
                                                            
                                                        </xsl:if>
                                                    </xsl:for-each>
                                                </xsl:for-each>
                                                <entryRelationship typeCode="REFR">
                                                    <xsl:copy-of select="child::node()[name()='substanceAdministration']" />
                                                </entryRelationship>
                                            </supply>
                                        </entry>
                                    </xsl:for-each>
                                </section>
                            </xsl:for-each>   
                        </component>
                    </xsl:for-each>
                    
                </structuredBody>
            </component>
            
            
        </ClinicalDocument>
        
    </xsl:template>
    
    <xsl:template name="addPersonInfo">
        <id>
            <xsl:attribute name="root">
                <xsl:value-of select="$pharma.id.root"/> 
            </xsl:attribute>
            <xsl:attribute name="extension">
                <xsl:value-of select="$pharma.id.extension"/>
            </xsl:attribute>
            <xsl:attribute name="assigningAuthorityName">
                <xsl:value-of select="$pharma.id.assigningAuthorityName"/>
            </xsl:attribute>
            <xsl:attribute name="displayable">false</xsl:attribute>
        </id>
        <telecom nullFlavor="NI"/>
        <assignedPerson classCode="PSN" determinerCode="INSTANCE">
            <name>
                <given>
                    <xsl:value-of select="$pharma.given"/>
                </given>
                <family>
                    <xsl:value-of select="$pharma.family"/>
                </family>
            </name>
        </assignedPerson>
        <xsl:call-template name="representedORGTemplate" />
    </xsl:template>
    
    <xsl:template name="representedORGTemplate">
        <representedOrganization>
            <id>
                <xsl:attribute name="root">
                    <xsl:value-of select="$pharma.org.id.root"/>
                </xsl:attribute>
                <xsl:attribute name="extension">
                    <xsl:value-of select="$pharma.org.id.extension"/>
                </xsl:attribute>
            </id>
            <name>
                <xsl:value-of select="$pharma.org.name"/>
            </name>
            <telecom nullFlavor="NI"/>
            <addr nullFlavor="NA"/>
        </representedOrganization>
    </xsl:template>
    
    
</xsl:stylesheet>

