<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0" 
    xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"   
    xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
    xmlns:b64="https://github.com/ilyakharlamov/xslt_base64"
    xmlns:v3="urn:hl7-org:v3"
   >
    <xsl:output method="html" version="4.0" omit-xml-declaration="no"
        standalone="yes"
        encoding="UTF-8" indent="yes"/>
    <xsl:include href="base64.xsl"/>
    <xsl:template match="/">
        <html>
            <head></head>
            <body>
                <h1>SAML <xsl:value-of select="saml2:Assertion/@Version"/> Assertion </h1>
                
                
                <xsl:apply-templates select="*"/>       
            </body>
        </html>
    </xsl:template>
    
    <!-- Header -->
    
    
    <xsl:template match="saml2:Assertion">
        <table border="1" width="100%">
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" >
                    <strong>@ID  (required)</strong></td>
                <td style="padding-left:4px"><xsl:value-of select="@ID"/>
                    
                </td>
                
            </tr>
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>@Version (required)</strong></td>
                <td style="padding-left:4px"><xsl:value-of select="@Version"/></td>
            </tr>
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>@IssueInstant (required)</strong></td>
                <td style="padding-left:4px"><xsl:value-of select="@IssueInstant"/></td>
            </tr>            
            
        </table>
        <h2 style="margin-top:10px;">Issuer</h2>
        <p>The Issuer provides information about the issuer of a SAML assertion or protocol message.</p>
        <table border="1" width="100%">
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>Issuer</strong></td>
                <td style="padding-left:4px"><xsl:value-of select="saml2:Issuer"/></td>    
            </tr>       
        </table>
        
        <xsl:apply-templates select="ds:Signature"/>
        
        <xsl:apply-templates select="saml2:Subject"/>
        <xsl:if test="saml2:Conditions">
            <xsl:apply-templates select="saml2:Conditions"/>    
        </xsl:if>
        
        <xsl:apply-templates select="saml2:AuthnStatement"/>
        <xsl:apply-templates select="saml2:AttributeStatement"/>
        
        
        
    </xsl:template>
    
    
    <!-- Signature  -->
    <xsl:template match="ds:Signature">
        <h2 style="margin-top:10px;">Signature</h2>
        
        <table border="1" width="100%">
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>ID</strong></td>
                <td style="padding-left:4px"><xsl:value-of select="ds:KeyInfo/ds:X509Data/ds:X509Certificate"/>               </td>
            </tr>
        </table>
    </xsl:template>
    
    <!-- Subject  -->
    <xsl:template match="saml2:Subject">
        <h2 style="margin-top:10px;">Subject</h2>
        <p>The Subject identifies the authenticated principal</p>
        
        <table border="1" width="100%">
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>NameID</strong> (<xsl:choose>
                    
                    
                    <xsl:when test="@Format">
                        <xsl:choose>     
                            <xsl:when test="saml2:NameID/@Format='urn:oasis:names:tc:SAML:2.0:nameid-format:persistent'">persistent</xsl:when>
                            <xsl:when test="saml2:NameID/@Format='urn:oasis:names:tc:SAML:2.0:nameid-format:transient'">emailAddress</xsl:when>
                            <xsl:when test="saml2:NameID/@Format='urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress'">emailAddress</xsl:when>
                            <xsl:when test="saml2:NameID/@Format='urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified'">unspecified</xsl:when>
                            <xsl:when test="saml2:NameID/@Format='urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName'">X509SubjectName</xsl:when>
                            <xsl:when test="saml2:NameID/@Format='urn:oasis:names:tc:SAML:1.1:nameid-format:WindowsDomainQualifiedName'">WindowsDomainQualifiedName</xsl:when>
                            <xsl:when test="saml2:NameID/@Format='urn:oasis:names:tc:SAML:2.0:nameid-format:kerberos'">kerberos</xsl:when>
                            <xsl:when test="saml2:NameID/@Format='urn:oasis:names:tc:SAML:2.0:nameid-format:entity'">entity</xsl:when>
                            
                            <xsl:otherwise><font color='red'>Unknown format</font></xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise><font color='red'>Format not specified</font></xsl:otherwise>
                </xsl:choose>)
                </td>
                <td style="padding-left:4px"><xsl:value-of select="saml2:NameID"/></td>    
            </tr> 
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>SubjectConfirmation</strong></td>
                <td style="padding-left:4px">
                    <xsl:choose>     
                        <xsl:when test="saml2:SubjectConfirmation/@Method='urn:oasis:names:tc:SAML:1.0:cm:holder-of-key'">holder-of-key</xsl:when>
                        <xsl:when test="saml2:SubjectConfirmation/@Method='urn:oasis:names:tc:SAML:1.0:cm:sender-vouches'">sender-vouches</xsl:when>
                        <xsl:when test="saml2:SubjectConfirmation/@Method='urn:oasis:names:tc:SAML:1.0:cm:artifact'">artifact</xsl:when>
                        <xsl:when test="saml2:SubjectConfirmation/@Method='urn:oasis:names:tc:SAML:1.0:cm:bearer'">bearer</xsl:when>
                        <xsl:when test="saml2:SubjectConfirmation/@Method='urn:oasis:names:tc:SAML:2.0:cm:holder-of-key'">holder-of-key</xsl:when>
                        <xsl:when test="saml2:SubjectConfirmation/@Method='urn:oasis:names:tc:SAML:2.0:cm:sender-vouches'">sender-vouches</xsl:when>
                        <xsl:when test="saml2:SubjectConfirmation/@Method='urn:oasis:names:tc:SAML:2.0:cm:bearer'">bearer</xsl:when>
                        <xsl:otherwise><font color='red'>Unknown subject confirmation</font></xsl:otherwise>
                    </xsl:choose> </td>
            </tr>   
            <xsl:if test="(saml2:SubjectConfirmation/@Method='urn:oasis:names:tc:SAML:1.0:cm:bearer' ) or (
                saml2:SubjectConfirmation/@Method='urn:oasis:names:tc:SAML:2.0:cm:bearer')">                
                <tr>
                    <td style="width:200px;text-align:right;padding-right:4px" ><strong>SubjectConfirmationData</strong></td>
                    <td style="padding-left:4px">
                        @InResponseTo : <xsl:value-of
                            select="saml2:SubjectConfirmation/SubjectConfirmationData/@InResponseTo"/><br/>
                        @Recipient : <xsl:value-of
                            select="saml2:SubjectConfirmation/SubjectConfirmationData/@Recipient"/><br/>
                        @NotOnOrAfter : <xsl:value-of
                            select="saml2:SubjectConfirmation/SubjectConfirmationData/@NotOnOrAfter"/><br/>
                        
                    </td>
                </tr>   
            </xsl:if>
            
        </table> 
    </xsl:template>
    
    <!-- Conditions  -->
    <xsl:template match="saml2:Conditions">
        <h2 style="margin-top:10px;">Conditions</h2>
        <p> Conditions element, which gives the conditions under which the assertion is to be considered valid</p>
        
        <table border="1" width="100%">
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>@NotBefore</strong></td>
                <td style="padding-left:4px"><xsl:value-of select="@NotBefore"/></td>
            </tr>
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>@NotOnOrAfter</strong></td>
                <td style="padding-left:4px"><xsl:value-of select="@NotOnOrAfter"/></td>
            </tr>        
            <xsl:if test="saml2:AudienceRestriction">
                <tr>
                    <td style="width:200px;text-align:right;padding-right:4px" ><strong>AudienceRestriction</strong></td>
                    <td style="padding-left:4px"><xsl:value-of select="saml2:AudienceRestriction/saml2:Audience"/></td>
                </tr>     
            </xsl:if>
        </table>
    </xsl:template>
    
    <!-- AuthnStatement  -->
    <xsl:template match="saml2:AuthnStatement">
        <h2 style="margin-top:10px;">AuthnStatement</h2>
        <p>The AuthnStatement element describes a statement by the SAML authority asserting that the assertion subject was authenticated by a particular means at a particular time.</p>
        <table border="1" width="100%">
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>@AuthnInstant (required)</strong></td>
                <td style="padding-left:4px"><xsl:value-of select="@AuthnInstant"/></td>
            </tr>
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>@SessionIndex (optional)</strong></td>
                <td style="padding-left:4px"><xsl:value-of select="@SessionIndex"/></td>
            </tr>
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>@SessionNotOnOrAfter (optional)</strong></td>
                <td style="padding-left:4px"><xsl:value-of select="@SessionNotOnOrAfter"/></td>
            </tr>
            <tr>
                <td style="width:200px;text-align:right;padding-right:4px" ><strong>AuthnContext</strong></td>
                <td style="padding-left:4px">
                    
                    
                    <xsl:choose>     
                        <xsl:when test="saml2:AuthnContext/saml2:AuthnContextClassRef='urn:oasis:names:tc:SAML:2.0:ac:classes:Password'">User Name and Password</xsl:when>
                        <xsl:when test="saml2:AuthnContext/saml2:AuthnContextClassRef='urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport'">Password Protected Transport</xsl:when>
                        <xsl:when test="saml2:AuthnContext/saml2:AuthnContextClassRef='urn:oasis:names:tc:SAML:2.0:ac:classes:TLSClient'">Transport Layer Security (TLS) Client</xsl:when>
                        <xsl:when
                            test="saml2:AuthnContext/saml2:AuthnContextClassRef='urn:oasis:names:tc:SAML:2.0:ac:classes:X509'">X.509 Certificate</xsl:when>
                        <xsl:when test="saml2:AuthnContext/saml2:AuthnContextClassRef='urn:federation:authentication:windows'">Integrated Windows Authentication</xsl:when>
                        <xsl:when test="saml2:AuthnContext/saml2:AuthnContextClassRef='urn:oasis:names:tc:SAML:2.0:ac:classes:Kerberos'">Kerberos</xsl:when>
                        <xsl:otherwise><font color='red'>Unknown authorization context <pre>&apos;<xsl:value-of
                            select="saml2:AuthnContext/saml2:AuthnContextClassRef"/>&apos;</pre></font></xsl:otherwise>
                    </xsl:choose>
                </td>
            </tr>            
        </table>
    </xsl:template>
    
    <!-- AttributeStatement  -->
    <xsl:template match="saml2:AttributeStatement">
        <h2 style="margin-top:10px;">AttibuteStatement</h2>
        <p>The AttibuteStatement element asserts a multi-valued attribute associated with the authenticated principal
        </p>
        <table border="1" width="100%">
            <xsl:apply-templates select="saml2:Attribute"/>
        </table>
    </xsl:template>
    
    
    <!-- AttributeStatement  -->
    <xsl:template match="saml2:Attribute">
        <tr>
            <td style="width:200px;text-align:right;padding-right:4px" ><strong>
                <xsl:choose>     
                    <xsl:when test="@Name='urn:oasis:names:tc:xacml:1.0:subject:subject-id' or
                        @Name='urn:oasis:names:tc:xspa:2.0:subject:subject-id'">Subject ID</xsl:when>
                    <xsl:when test="@Name='urn:oasis:names:tc:xspa:1.0:subject:organization'">Subject Organization</xsl:when>
                    <xsl:when test="@Name='urn:oasis:names:tc:xspa:1.0:subject:organization-id'">Subject Organization
                        ID</xsl:when>
                    <xsl:when test="@Name='urn:oasis:names:tc:xspa:2.0:subject:npi'">National Provider Identifier (NPI) </xsl:when>
                    <xsl:when test="@Name='urn:ihe:iti:xca:2010:homeCommunityId'">Home Community ID</xsl:when>
                    <xsl:when test="@Name='urn:oasis:names:tc:xacml:2.0:subject:role'">Subject Role</xsl:when>
                    <xsl:when test="@Name='urn:oasis:names:tc:xacml:1.0:resource:resource-id'">Resource ID</xsl:when>
                    <xsl:when test="@Name='urn:oasis:names:tc:xspa:1.0:subject:purposeofuse'">Purpose Of Use</xsl:when>
                    <xsl:when test="@Name='urn:ihe:iti:bppc:2007:docid'">Patient Privacy Policy Acknowledgement Document
                    (Auth-Consent Option)</xsl:when>
                    <xsl:when test="@Name='urn:ihe:iti:xua:2012:acp'">Patient Privacy Policy Identifier
                        (Auth-Consent Option)</xsl:when>
                    <xsl:otherwise><font color='red'><xsl:value-of select="@Name"/></font></xsl:otherwise>
                </xsl:choose>
                
                
            </strong></td>
            <td style="padding-left:4px">
                <xsl:choose>
                    <xsl:when test="@Name='urn:oasis:names:tc:xacml:2.0:subject:role'">
                        <xsl:value-of select="saml2:AttributeValue/v3:Role"/></xsl:when>
                    <xsl:when test="@Name='urn:oasis:names:tc:xspa:1.0:subject:purposeofuse'">
                        <xsl:value-of select="saml2:AttributeValue/v3:PurposeOfUse/@code"/> <xsl:text> - </xsl:text> <xsl:value-of
                            select="saml2:AttributeValue/v3:PurposeOfUse/@displayName"/></xsl:when>  
                    <xsl:when test="@Name='urn:ihe:iti:bppc:2007:docid'">
                        <xsl:value-of select="saml2:AttributeValue"/></xsl:when>
                    <xsl:when test="@Name='urn:ihe:iti:xua:2012:acp'">
                        <xsl:value-of select="saml2:AttributeValue"/></xsl:when>
                    
                    <xsl:otherwise><xsl:value-of select="saml2:AttributeValue"/></xsl:otherwise>
                </xsl:choose></td>
        </tr>
        
    </xsl:template>
</xsl:stylesheet>