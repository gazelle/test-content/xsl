<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:map="urn:internal"
	exclude-result-prefixes="map">
	<xsl:output method="html" />
	<xsl:template match="Hl7v2ValidationReport/MessageValidation">
		<html>
			<head>
				<style> .row4 a, .row3 a { color: #003399; text-decoration:
					underline; } .row4 a:hover, .row3 a:hover { color: #000000;
					text-decoration: underline; } .headerReport { width: 250px; } .row1
					{ vertical-align: top; background-color: #EFEFEF; width: 100px; }
					.row2 { background-color: #DEE3E7; width: 100px; } .row3 {
					background-color: #D1D7DC; vertical-align: top; } .row4 {
					background-color: #EFEFEF; vertical-align: top; } .row5 {
					background-color: #FFEC9D; vertical-align: top; } .forumline {
					background-color:#FFFFFF; border: 2px #006699 solid; width: 700px;
					} .maintitle { font-weight: bold; font-size: 22px; font-family:
					Georgia, Verdana; text-decoration: none; line-height : 120%; color :
			#000000; } </style> </head> <body>
		
        <!-- Header -->
		<div id="mvrTitle" style="display:block;">
			<table width="100%" cellpadding="2" cellspacing="1" 
				border="0" class="forumline">
				<tr>
					<td>
						<font class="maintitle">Message Validation Report</font>
					</td>
					<td align="right">
						Date:
						<xsl:call-template name="dateTransformer">
            				<xsl:with-param name="myDate" select="MetaData/@Datetime"/>
        				</xsl:call-template>
					</td>
				</tr>
			</table>
		</div>

        <!-- Profile -->
		<div id="mvrProfileBox" style="display:block;">
			<br />
			<table width="100%" cellpadding="2" cellspacing="1" 
				border="0" class="forumline">
				<xsl:apply-templates select="MetaData/Profile" />
			</table>
		</div>
        
        <!-- Message -->
		<div id="mvrMessageBox" style="display:block;">
			<br />
			<table width="100%" cellpadding="2" cellspacing="1" 
				border="0" class="forumline">
				<xsl:apply-templates select="MetaData/Message" />
			</table>
		</div>
		
		<!-- validation context -->
		<xsl:if test="Context">
			<div id="mvrContextBox" style="display:block;">
				<br />
				<table width="100%" cellpadding="2" cellspacing="1" 
					border="0" class="forumline">
					<tr>
						<th class="headerReport">Failure Type</th>
						<th class="headerReport">Failure Level</th>
					</tr>
					<xsl:apply-templates select="Context/Failure" />
				</table>
			</div>
		</xsl:if>
        
        <!-- Result index-->
		<div id="mvrSummary" style="display:block;">
			<br />
			<table width="100%" cellpadding="2" cellspacing="1" 
				border="0" class="forumline">
				<tr>
					<th colspan="3">Summary</th>
				</tr>
				<xsl:apply-templates select="Result" />
				<xsl:apply-templates select="Result" mode="other" />
			</table>
		</div>
		
		<div id="mvrProfileErrors" style="display:block;">
			<br />
			<table width="100%" cellpadding="2" cellspacing="1" 
				border="0" class="forumline">
				<tr>
					<th colspan="3">Profile Errors</th>
				</tr>
				<xsl:if test="count(Result/Error) &gt; 0">
					<tr>
						<td colspan="4" class="row5">Error Details</td>
					</tr>
					<xsl:apply-templates select="Result/Error" />
				</xsl:if>
			</table>
		</div>
		
		<div id="mvrProfileWarnings" style="display:block;">
			<br />
			<table width="100%" cellpadding="2" cellspacing="1" 
				border="0" class="forumline">
				<tr>
					<th colspan="3">Profile Warnings</th>
				</tr>
				<xsl:if test="count(Result/Warning) &gt; 0">
					<tr>
						<td colspan="4" class="row5">Warning Details</td>
					</tr>
					<xsl:apply-templates select="Result/Warning" />
				</xsl:if>
			</table>
		</div>
		
		<div id="mvrVCD" style="display:block;">
			<br />
			<table width="100%" cellpadding="2" cellspacing="1" 
				border="0" class="forumline">
				<tr>
					<th colspan="3">Validation Context Details</th>
				</tr>
				<xsl:if test="count(Result/User) &gt; 0">
					<tr>
						<td colspan="4" class="row5">Alerts</td>
					</tr>
					<xsl:apply-templates select="Result/User" />
				</xsl:if>
			</table>
		</div>
        </body>
	</html> 
	</xsl:template>
	<xsl:template match="MetaData/Profile">
		<tr>
			<td class="row1" rowspan="5" valign="top">Profile</td>
			<td class="row2">Name</td>
			<td class="row3">
				<xsl:value-of select="@Name" />
			</td>
		</tr>
		<tr>
			<td class="row2">Id</td>
			<td class="row3">
				<xsl:apply-templates select="@Id" />
			</td>
		</tr>
		<tr>
			<td class="row2">Type</td>
			<td class="row3">
				<xsl:apply-templates select="@Type" />
			</td>
		</tr>
		<tr>
			<td class="row2">HL7 Version</td>
			<td class="row3">
				<xsl:apply-templates select="@HL7Version" />
			</td>
		</tr>
		<tr>
			<td class="row2">File</td>
			<td class="row3">
				<xsl:apply-templates select="@Filename" />
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="MetaData/Message">
		<tr>
			<td class="row1" rowspan="4" valign="top">
				Message
				<br />
			</td>
			<td class="row2">Type</td>
			<td class="row3">
				<xsl:value-of select="@Type" />
			</td>
		</tr>
		<tr>
			<td class="row2">Id</td>
			<td class="row3">
				<xsl:apply-templates select="@Id" />
			</td>
		</tr>
		<tr>
			<td class="row2">File</td>
			<td class="row3">
				<xsl:apply-templates select="@Filename" />
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Context/Failure">
		<tr>
			<td class="row2">
				<xsl:value-of select="@Type" />
			</td>
			<td class="row1">
				<xsl:value-of select="@Level" />
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Result">
		<tr>
			<td rowspan="3" class="row1">Message Validation Results</td>
			<td class="row2">Errors</td>
			<td class="row3">
				<xsl:value-of select="@ErrorCount" />
			</td>
		</tr>
		<tr>
			<td class="row2">Warnings</td>
			<td class="row3">
				<xsl:apply-templates select="@WarningCount" />
			</td>
		</tr>
		<tr>
			<td class="row2">Ignored</td>
			<td class="row3">
				<xsl:value-of select="@IgnoreCount" />
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Result" mode="other">
		<tr>
			<td rowspan="3" class="row1">
				Other Warnings
				<br />
				(User Input)
			</td>
			<td class="row2">Alerts</td>
			<td class="row3">
				<xsl:value-of select="count(User[@FailureType = 'TABLE_NOT_FOUND'])" />
			</td>
		</tr>
		<tr>
			<td class="row2">Location Not Found</td>
			<td class="row3">
				<xsl:value-of
					select="count(User[@FailureType = 'MESSAGE_VALIDATION_CONTEXT'])" />
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Result/*">
		<xsl:variable name="row">row<xsl:if test="not(position() mod 2)">4</xsl:if><xsl:if test="position() mod 2">3</xsl:if></xsl:variable>
		<xsl:variable name="locationElement">
			<xsl:value-of
				select="count(Location/SegGroup) + count(Location/Segment) + count(Location/Field) + count(Location/Component) + count(Location/SubComponent)" />
		</xsl:variable>
		<xsl:variable name="elementContent">
			<xsl:value-of select="count(ElementContent)" />
		</xsl:variable>
		<xsl:variable name="positionRowspan">
			<xsl:choose>
				<xsl:when test="$locationElement = 0">
					<xsl:value-of select="2 + $elementContent " />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="3 + $locationElement + $elementContent" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="locationRowspan">
			<xsl:value-of select="1 + $locationElement" />
		</xsl:variable>
		<tr>
			<td class="{$row}" rowspan="{$positionRowspan}" style="width:10px;">
				<xsl:value-of select="position()" />
			</td>
			<td class="{$row}">Type:</td>
			<td class="{$row}" colspan="2">
				<xsl:call-template name="BeautifyMessageFailureType">
					<xsl:with-param name="type" select="@FailureType" />
				</xsl:call-template>
			</td>
		</tr>
		<tr>
			<td class="{$row}">Description:</td>
			<td class="{$row}" colspan="2">
				<xsl:value-of select="Description" />
				<xsl:if test="@FailureSeverity = 'FATAL'">
					<span style="color:red;font-weight:bold"> This is a severe failure and validation has been
						terminated.</span>
				</xsl:if>
			</td>
		</tr>
		<xsl:if test="$locationElement > 0">
			<tr class="{$row}">
				<td style="width:100px;" rowspan="{$locationRowspan}">
					<a class="jslink" onclick="showError({position()-1})" title="Show the current error">
						Location:</a>
				</td>
				<xsl:if test="Location/EPath">
					<td style="font-weight: bold">Canonical Form:</td>
					<td>
						<xsl:value-of select="Location/EPath" />
					</td>
				</xsl:if>
				<xsl:if test="Location/XPath">
					<td style="font-weight: bold">XPath Expression:</td>
					<td>
						<xsl:value-of select="Location/XPath" />
					</td>
				</xsl:if>
			</tr>
			<xsl:for-each select="Location/SegGroup">
				<tr class="{$row}">
					<td style="font-weight: bold">Segment Group:</td>
					<td>
						<xsl:value-of select="@Name" />
					</td>
				</tr>
			</xsl:for-each>
			<xsl:if test="Location/Segment">
				<tr class="{$row}">
					<td style="font-weight: bold">Segment:</td>
					<td>
						<xsl:value-of select="Location/Segment/@Name" />
					</td>
				</tr>
			</xsl:if>
			<xsl:if test="Location/Field">
				<tr class="{$row}">
					<td style="font-weight: bold">Field:</td>
					<td>
						<xsl:value-of select="Location/Field/@Name" />
					</td>
				</tr>
			</xsl:if>
			<xsl:if test="Location/Component">
				<tr class="{$row}">
					<td style="font-weight: bold">Component:</td>
					<td>
						<xsl:value-of select="Location/Component/@Name" />
					</td>
				</tr>
			</xsl:if>
			<xsl:if test="Location/SubComponent">
				<tr class="{$row}">
					<td style="font-weight: bold">SubComponent:</td>
					<td>
						<xsl:value-of select="Location/SubComponent/@Name" />
					</td>
				</tr>
			</xsl:if>
		</xsl:if>
		<xsl:if test="ElementContent">
			<tr>
				<td class="{$row}">ElementContent:</td>
				<td class="{$row}" colspan="2">
					<xsl:value-of select="ElementContent" />
				</td>
			</tr>
		</xsl:if>
	</xsl:template>
	<xsl:template name="BeautifyMessageFailureType">
		<xsl:param name="type" />
		<xsl:choose>
			<xsl:when test="$type ='MESSAGE_STRUCTURE_ID'">
				Message Structure Id
			</xsl:when>
			<xsl:when test="$type ='USAGE'">
				Usage
			</xsl:when>
			<xsl:when test="$type ='LENGTH'">
				Length
			</xsl:when>
			<xsl:when test="$type ='MESSAGE_VALIDATION_CONTEXT'">
				Message Validation Context
			</xsl:when>
			<xsl:when test="$type ='DATA'">
				Data
			</xsl:when>
			<xsl:when test="$type ='TABLE_NOT_FOUND'">
				Table Not Found
			</xsl:when>
			<xsl:when test="$type ='CARDINALITY'">
				Cardinality
			</xsl:when>
			<xsl:when test="$type ='MESSAGE_STRUCTURE'">
				Message Structure
			</xsl:when>
			<xsl:when test="$type ='AMBIGUOUS_PROFILE'">
				Ambiguous Profile
			</xsl:when>
			<xsl:when test="$type ='DATATYPE'">
				Datatype
			</xsl:when>
			<xsl:when test="$type ='VERSION'">
				Version
			</xsl:when>
			<xsl:otherwise>
				Unknow Message Failure Type
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="dateTransformer">
        <xsl:param name="myDate"/>
        <xsl:variable name="year" select="substring-before($myDate, '-')"/>
        <xsl:variable name="month" select="substring-before(substring-after($myDate, '-'), '-')"/>
        <xsl:variable name="day" select="substring-before(substring-after(substring-after($myDate, '-'), '-'), 'T')"/>
        <xsl:variable name="time" select="substring(substring-after($myDate, 'T'),1, 5)"/>
        <xsl:value-of select="$day"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="document('')//map:month[@id = $month]"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$year"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="$time"/>
    </xsl:template>
    <map:dates>
        <map:month id="1">Jan</map:month>
        <map:month id="2">Feb</map:month>
        <map:month id="3">Mar</map:month>
        <map:month id="4">Apr</map:month>
        <map:month id="5">May</map:month>
        <map:month id="6">Jun</map:month>
        <map:month id="7">Jul</map:month>
        <map:month id="8">Aug</map:month>
        <map:month id="9">Sep</map:month>
        <map:month id="01">Jan</map:month>
        <map:month id="02">Feb</map:month>
        <map:month id="03">Mar</map:month>
        <map:month id="04">Apr</map:month>
        <map:month id="05">May</map:month>
        <map:month id="06">Jun</map:month>
        <map:month id="07">Jul</map:month>
        <map:month id="08">Aug</map:month>
        <map:month id="09">Sep</map:month>
        <map:month id="10">Oct</map:month>
        <map:month id="11">Nov</map:month>
        <map:month id="12">Dec</map:month>
    </map:dates>
</xsl:stylesheet>
