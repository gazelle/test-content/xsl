<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ns2="http://docs.oasis-open.org/ns/tag/taml-201002/" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="html"
		media-type="text/xml" omit-xml-declaration="yes" />
	<xsl:template match="collection">
		<html lang="en">
			<head>
				<link href="http://gazelle.ihe.net/css/twitter_bootstrap/css/bootstrap.min.css" rel="stylesheet"
					type="text/css" media="screen" />
				<link href="http://gazelle.ihe.net/css/assertion_manager.css"
					rel="stylesheet" type="text/css" media="screen" />
			</head>
			<body class="container-fluid">

				<xsl:call-template name="navigation" />

				<div class="content">
					<xsl:apply-templates select="/collection/ns2:oasisTestAssertion" />
				</div>
				
				<script>
          			var d = document.getElementById("assertions");
          			d.className += " active";
       			 </script>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="ns2:oasisTestAssertion">
		<section class="row-fluid">
			<article class="span10 offset1">
				<h1>Assertion</h1>
				<!-- <xsl:value-of select="tag[name = 'Section']/content"/> -->
				<xsl:variable name="idScheme" select="target/idScheme"></xsl:variable>
				<xsl:variable name="assertionId" select="assertionId"></xsl:variable>
				<xsl:variable name="url">
					http://gazelle.ihe.net/AssertionManager/rest/testAssertion/assertion/documentSection/open?assertionId=<xsl:value-of select="$assertionId" />&amp;idScheme=<xsl:value-of select="$idScheme" />
				</xsl:variable>
				 <xsl:variable name="assertion_link">http://gazelle.ihe.net/AssertionManager/rest/testAssertion/assertion?assertionId=<xsl:value-of select="$assertionId"/>&amp;idScheme=<xsl:value-of select="$idScheme"/></xsl:variable>
				<div class="span1">
					<a href= "{$assertion_link}" class="label label-info" target="blank"><xsl:value-of select="assertionId"/></a>
				</div>
				<div class="span9">
					<div>
						<div>
							<p>
								<strong>Predicate: </strong>
								<xsl:value-of select="predicate/content" />
							</p>
						</div>
						<div>
							<p>
								<strong>Prescription level: </strong>
								<xsl:value-of select="prescription/level" />
							</p>
						</div>
						<div>
							<p>
								<strong>IdScheme: </strong>
								<xsl:value-of select="target/idScheme" />
							</p>
						</div>
					</div>
					<div>
						<h3>Normative Sources</h3>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Provenance</th>
									<th>Revision</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<xsl:for-each select="normativeSource/refSourceItems">
									<tr>
										<td>
											<xsl:value-of select="name" />
										</td>
										<td>
											<xsl:value-of select="resourceProvenanceId" />
										</td>
										<td>
											<xsl:value-of select="revisionId" />
										</td>
										<td>
											<a href="{uri}" target="blank" class="btn">Open</a>
										</td>
									</tr>
								</xsl:for-each>
							</tbody>
						</table>
					</div>
					<div>
						<h3>Tags</h3>
						<ul>
							<xsl:apply-templates select="tag" />
						</ul>
					</div>
				</div>
				<div class="span1">
					<a href="{$url}" class="btn btn-success" target="blank">Open specification</a>
				</div>
			</article>
		</section>
		<hr />

	</xsl:template>

	<xsl:template match="tag">
		<li>
			<strong>
				<xsl:value-of select="./name" />
				:
			</strong>
			<xsl:value-of select="./content" />
		</li>
	</xsl:template>

	<xsl:include href="header.xsl" />
</xsl:stylesheet>
