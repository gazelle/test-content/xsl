<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	        xmlns:ns2="http://docs.oasis-open.org/ns/tag/taml-201002/" version="1.0">
  <xsl:template name="navigation">
    <div class="fixed">
      <div class="navbar">
	<div class="navbar-inner">

	  <a class="brand" href="http://gazelle.ihe.net" style="padding-top: 0;">
	    <img src="http://gazelle.ihe.net/img/gazelle-2013-small.jpg"
		 height="30" width="30" />
	  </a>
          
	  <small class="brand">Assertion manager</small>

	  <ul class="nav">
	    <li class="divider-vertical"></li>
	    <li id="idschemes">
	      <a
		  href="http://gazelle.ihe.net/AssertionManager/rest/testAssertion/idSchemes">IdSchemes</a>
	    </li>

	    <li id="assertions"><a>Assertions</a></li>

	    <li>
	      <a href="http://gazelle.ihe.net/AssertionManager/apidocs/">Documentation</a>
	    </li>
	  </ul>
	</div>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
