<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ns2="http://docs.oasis-open.org/ns/tag/taml-201002/" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" method="html"
		media-type="text/xml" omit-xml-declaration="yes" />
	<xsl:template match="collection">
		<html lang="en">
			<head>
				<link href="http://gazelle.ihe.net/css/twitter_bootstrap/css/bootstrap.min.css" rel="stylesheet"
					type="text/css" media="screen" />
				<link href="http://gazelle.ihe.net/css/assertion_manager.css"
					rel="stylesheet" type="text/css" media="screen" />
			</head>
			<body class="container-fluid">

				<xsl:call-template name="navigation" />

				<div class="content">
					<section class="row-fluid">
						<article class="span5 offset2">
							<h1>IdSchemes</h1>
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>IdScheme</th>
										<th>Linked assertions</th>
									</tr>
								</thead>
								<tbody>
									<xsl:apply-templates select="/collection/IdSchemes" />
								</tbody>
							</table>
						</article>
					</section>

				</div>
        		<script>
         			var d = document.getElementById("idschemes");
          			d.className += " active";
        		</script>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="/collection/IdSchemes">
		<xsl:variable name="idScheme" select="value"></xsl:variable>
		<xsl:variable name="url">
			http://gazelle.ihe.net/AssertionManager/rest/testAssertion/assertion?idScheme=<xsl:value-of select="$idScheme" />
		</xsl:variable>
		<tr>
			<td>
				<xsl:value-of select="value" />
			</td>
			<td>
				<a href="{$url}" class="btn" target="blank">View</a>
			</td>
		</tr>
	</xsl:template>

	<xsl:include href="header.xsl" />

</xsl:stylesheet>
