<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************
Stylesheet to generate unique ids within a schematron
This stylesheet adds the id attribute to all elements in the schematron

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/sch_add_ids.xsl 22462 2011-06-22 16:56:00Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:iso="http://purl.oclc.org/dsdl/schematron"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	>

	<xsl:param name="master_prefix" />
	<xsl:output method="xml" omit-xml-declaration="yes" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="iso:pattern/@id">
		<!-- these elements already contained an id. We add the position number for additional help -->
		<xsl:call-template name="create-id">
			<xsl:with-param name="type" select="'iso:pattern'"/>
			<xsl:with-param name="name" select="concat(.,'-')"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="iso:rule/@id">
		<!-- these elements already contained an id. We add the position number for additional help -->
		<xsl:call-template name="create-id">
			<xsl:with-param name="type" select="'iso:rule'"/>
			<xsl:with-param name="name" select="concat(.,'-')"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="iso:assert/@id">
		<!-- these elements already contained an id. We add the position number for additional help -->
		<xsl:call-template name="create-id">
			<xsl:with-param name="type" select="'iso:assert'"/>
			<xsl:with-param name="name" select="concat(.,'-')"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="iso:report/@id">
		<!-- these elements already contained an id. We add the position number for additional help -->
		<xsl:call-template name="create-id">
			<xsl:with-param name="type" select="'iso:report'"/>
			<xsl:with-param name="name" select="concat(.,'-')"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:choose>
				<xsl:when test="self::iso:pattern">
					<!-- these elements did not contain an id. We generate one and add the position number for additional help -->
					<xsl:call-template name="create-id">
						<xsl:with-param name="type" select="'iso:pattern'"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="self::iso:rule">
					<!-- these elements did not contain an id. We generate one and add the position number for additional help -->
					<xsl:call-template name="create-id">
						<xsl:with-param name="type" select="'iso:rule'"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="self::iso:assert">
					<!-- these elements did not contain an id. We generate one and add the position number for additional help -->
					<xsl:call-template name="create-id">
						<xsl:with-param name="type" select="'iso:assert'"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="self::iso:report">
					<!-- these elements did not contain an id. We generate one and add the position number for additional help -->
					<xsl:call-template name="create-id">
						<xsl:with-param name="type" select="'iso:report'"/>
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>


	<xsl:template name="create-id">
		<xsl:param name="type"/>
		<xsl:param name="name"/>
		<xsl:variable name="id_prefix">
			<xsl:value-of select="concat($master_prefix, $name)"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$type='iso:pattern'">
				<xsl:variable name="num">
					<xsl:number level="any" count="iso:pattern" />
				</xsl:variable>
				<xsl:variable name="prefix">pa</xsl:variable>
				<xsl:attribute name="id">
					<xsl:value-of select="concat($id_prefix, $prefix, format-number($num, '00000'), '-', parent::*/@role)"/>
				</xsl:attribute>
			</xsl:when>
			<xsl:when test="$type='iso:rule'">
				<xsl:variable name="num">
					<xsl:number level="any" count="iso:rule" />
				</xsl:variable>
				<xsl:variable name="prefix">ru</xsl:variable>
				<xsl:attribute name="id">
					<xsl:value-of select="concat($id_prefix, $prefix, format-number($num, '00000'))"/>
				</xsl:attribute>
			</xsl:when>
			<xsl:when test="$type='iso:assert'">
				<xsl:variable name="num">
					<xsl:number level="any" count="iso:assert" />
				</xsl:variable>
				<xsl:variable name="prefix">as</xsl:variable>
				<xsl:attribute name="id">
					<xsl:value-of select="concat($id_prefix, $prefix, format-number($num, '00000'))"/>
				</xsl:attribute>
			</xsl:when>
			<xsl:when test="$type='iso:report'">
				<xsl:variable name="num">
					<xsl:number level="any" count="iso:report" />
				</xsl:variable>
				<xsl:variable name="prefix">re</xsl:variable>
				<xsl:attribute name="id">
					<xsl:value-of select="concat($id_prefix, $prefix, format-number($num, '00000'))"/>
				</xsl:attribute>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
<!-- EOF -->
