<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************
Stylesheet to add the processing instruction for the report.xsl to a servlet

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/srvlt_finish.xsl 22576 2011-06-24 18:38:00Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0"
	xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	>

	<xsl:output method="xml" omit-xml-declaration="no" version="1.0" encoding="UTF-8" indent="yes"/>

	<!-- parameters -->
	<xsl:param name="cda_filename" />
	<xsl:param name="schematron_filename" />

	<!-- main output -->
	<xsl:template match="@*|text()|comment()|processing-instruction()">
		<xsl:copy-of select="."/>
	</xsl:template>

	<!-- omit templates section -->

	<!-- match templates section -->
	<xsl:template match="*">
		<xsl:choose>
			<xsl:when test="self::svrl:schematron-output">
				<xsl:processing-instruction name="transform-stylesheet">href="report.xsl"</xsl:processing-instruction>
				<xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="dynamic_xslt.xsl"</xsl:processing-instruction>
				<xsl:copy>
					<xsl:apply-templates select="@*"/>
					<xsl:element name="xs:annotation">
						<xsl:attribute name="id">cda_filename</xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="$cda_filename"/></xsl:attribute>
					</xsl:element>
					<xsl:element name="xs:annotation">
						<xsl:attribute name="id">schematron_filename</xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="$schematron_filename"/></xsl:attribute>
					</xsl:element>
					<xsl:element name="xs:annotation">
						<xsl:attribute name="id">validation_timestamp</xsl:attribute>
						<xsl:attribute name="value"><xsl:value-of select="current-dateTime()"/></xsl:attribute>
					</xsl:element>
					<xsl:apply-templates select="node()"/>
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
