<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************

Stylesheet for reporting Schematron validation results

parameters:
	baseUrl:		url of our own page.
	lang:			filters the desired language.
	defaultLang:	definition of the alternative language that should be used, when the desired language is not available. 
	showError:		filters all failed asserts with the role error.
	showWarning:	filters all failed asserts with the role warning.
	showNote:		filters all failed asserts with the role information.
	showDebug
	showReport:		filters all successful reports

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/doc.xsl 23010 2011-07-16 12:45:26Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
	xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:iso="http://purl.oclc.org/dsdl/schematron"
	xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:cda="urn:hl7-org:v3">

	<xsl:output method="html" omit-xml-declaration="no" encoding="UTF-8" indent="yes"/>
	
	<!-- parameters -->
	<xsl:param name="baseUrl"/>
	<xsl:param name="lang">en</xsl:param>
	<xsl:param name="defaultLang">en</xsl:param>

	<xsl:variable name="title"
		select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='SchDocumentation']/@displayName"/>

	<!-- main output -->
	<xsl:template match="/">
		<html>
			<head>
				<title>
					<xsl:value-of select="$title"/>
					<xsl:text>'</xsl:text>
					<xsl:call-template name="getFilename">
						<xsl:with-param name="uri">
							<xsl:value-of select="$baseUrl"/>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:text>'</xsl:text>
				</title>
				<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
				<link rel="stylesheet" type="text/css" href="http://gazelle.ihe.net/xsl/pharm/doc.css"/>
			</head>
			<body>
				<xsl:call-template name="doHeader"/>
				<xsl:apply-templates select="//iso:pattern"/>
			</body>
		</html>
	</xsl:template>

	<!-- omit templates section -->
	<xsl:template match="text()"/>
	<xsl:template match="xhtml:p[@class='debug']"/>
	<xsl:template match="xhtml:p[@class='specification']"/>
	<xsl:template match="iso:report[@id]" mode="entitydocumented"/>

	<!--  Display an entity -->
	<xsl:template match="iso:pattern">
		<div>
			<table class="entity">
				<thead>
					<tr>
						<th class="label1" width="10%">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Pattern']/@displayName"/>
							<xsl:text> ID:</xsl:text>
						</th>
						<th class="value2">
							<xsl:value-of select="@id"/>
						</th>
						<th class="label2" width="10%">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Role']/@displayName"/>
							<xsl:text>:</xsl:text>
						</th>
						<th class="value2" width="10%">
							<xsl:choose>
								<xsl:when test="string(@role)">
									<xsl:variable name="role">
										<xsl:value-of select="@role"/>
									</xsl:variable>
									<xsl:value-of
										select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value=$role]/@displayName"
									/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of
										select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='error']/@displayName"
									/>
								</xsl:otherwise>
							</xsl:choose>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="label1">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='File']/@displayName"/>
							<xsl:text>:</xsl:text>
						</td>
						<td>
							<xsl:value-of select="xhtml:ul/xhtml:li[@class='filename']"/>
						</td>
						<td class="label2">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Version']/@displayName"/>
							<xsl:text>:</xsl:text>
						</td>
						<td class="value2">
							<xsl:value-of select="xhtml:ul/xhtml:li[@class='version']"/>
						</td>
					</tr>
					<tr>
						<td class="label1">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='LastUpdate']/@displayName"/>
							<xsl:text>:</xsl:text>
						</td>
						<td colspan="3">
							<xsl:value-of select="xhtml:ul/xhtml:li[@class='lastupdate']"/>
						</td>
					</tr>
				</tbody>
			</table>
			<br/>
		</div>
		<xsl:apply-templates select="iso:rule" mode="entitydocumented"/>

	</xsl:template>

	<!-- Display a rule -->
	<xsl:template match="iso:rule" mode="entitydocumented">
		<div>
			<table class="rule">
				<thead>
					<tr>
						<th class="label1" width="10%">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Rule']/@displayName"/>
							<xsl:text> ID:</xsl:text>
						</th>
						<th class="value2">
							<xsl:value-of select="@id"/>
						</th>
						<th class="label2" width="10%">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Pattern']/@displayName"/>
							<xsl:text>:</xsl:text>
						</th>
						<th class="value1" width="30%">
							<xsl:value-of select="parent::iso:pattern/@id"/>
						</th>
					</tr>
				</thead>
				<tbody>
					<xsl:call-template name="documentEntry"/>
				</tbody>
			</table>
			<br/>
		</div>
		<xsl:apply-templates select="iso:assert|iso:report" mode="entitydocumented"/>
	</xsl:template>

	<!--  Display an Assert/Test as a table -->
	<xsl:template match="iso:assert[@id]" mode="entitydocumented">
		<div>
			<table class="body" width="100%">
				<thead>
					<tr>
						<th class="label1" width="10%">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Assert']/@displayName"/>
							<xsl:text> ID:</xsl:text>
						</th>
						<th class="value1">
							<xsl:value-of select="@id"/>
						</th>
						<th class="label2" width="10%">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Role']/@displayName"/>
							<xsl:text>:</xsl:text>
						</th>
						<th class="value2" width="10%">
							<xsl:choose>
								<xsl:when test="string(@role)">
									<xsl:variable name="role">
										<xsl:value-of select="@role"/>
									</xsl:variable>
									<xsl:value-of
										select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value=$role]/@displayName"
									/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of
										select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='error']/@displayName"
									/>
								</xsl:otherwise>
							</xsl:choose>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="label1">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Assert']/@displayName"/>
							<xsl:text>:</xsl:text>
						</td>
						<td colspan="3">
							<xsl:call-template name="replaceVariables">
								<xsl:with-param name="test"><xsl:value-of select="@test"></xsl:value-of></xsl:with-param>
							</xsl:call-template>
						</td>
					</tr>
					<xsl:call-template name="documentEntry"/>
				</tbody>
			</table>
			<br/>
		</div>
	</xsl:template>

	<xsl:template match="xhtml:h1 | xhtml:h2" mode="rulestranslated">
		<xsl:value-of select="./text()"/>
	</xsl:template>

	<!-- named templates section -->
	<xsl:template name="doHeader">
		<h1>
			<xsl:choose>
				<xsl:when test="//xhtml:h1[@lang=$lang]">
					<xsl:apply-templates select="//xhtml:h1[@lang=$lang]/text()" mode="documenting"
					/>
				</xsl:when>
				<xsl:otherwise>(<xsl:value-of select="$defaultLang"/>): <xsl:apply-templates
						select="//xhtml:h1[@lang=$defaultLang or not(@lang)]/text()"
						mode="documenting"/>
				</xsl:otherwise>
			</xsl:choose>
		</h1>
		<form name="dynamicSelection">
			<table width="100%">
				<tr>
					<td width="*">
						<h2>
							<xsl:choose>
								<xsl:when test="//xhtml:h2[@lang=$lang]">
									<xsl:apply-templates select="//xhtml:h2[@lang=$lang]/text()"
										mode="documenting"/>
								</xsl:when>
								<xsl:otherwise>(<xsl:value-of select="$defaultLang"/>):
										<xsl:apply-templates
										select="//xhtml:h2[@lang=$defaultLang or not(@lang)]/text()"
										mode="documenting"/>
								</xsl:otherwise>
							</xsl:choose>
						</h2>
					</td>
					<td class="label2" valign="bottom" width="20%">
						<xsl:value-of
							select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='SchDisplayLanguage']/@displayName"/>
						<xsl:text> </xsl:text>
						<xsl:call-template name="createLanguageSelector"/>
					</td>
				</tr>
			</table>
		</form>
	</xsl:template>

	<xsl:template name="createLanguageSelector">
		<xsl:variable name="foundLanguages">
			<xsl:for-each select="//xhtml:*">
				<xsl:if test="@lang"><xsl:value-of select="@lang"/>;</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:element name="select">
			<xsl:attribute name="name">listboxLanguages</xsl:attribute>
			<xsl:attribute name="onchange">
				<xsl:call-template name="createNewURL"/>
			</xsl:attribute>
			<xsl:call-template name="addLanguageIfPresent">
				<xsl:with-param name="currentLanguage">en</xsl:with-param>
				<xsl:with-param name="foundLanguages">
					<xsl:value-of select="$foundLanguages"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="addLanguageIfPresent">
				<xsl:with-param name="currentLanguage">de</xsl:with-param>
				<xsl:with-param name="foundLanguages">
					<xsl:value-of select="$foundLanguages"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="addLanguageIfPresent">
				<xsl:with-param name="currentLanguage">fr</xsl:with-param>
				<xsl:with-param name="foundLanguages">
					<xsl:value-of select="$foundLanguages"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="addLanguageIfPresent">
				<xsl:with-param name="currentLanguage">it</xsl:with-param>
				<xsl:with-param name="foundLanguages">
					<xsl:value-of select="$foundLanguages"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:element>
	</xsl:template>

	<xsl:template name="addLanguageIfPresent">
		<xsl:param name="foundLanguages"/>
		<xsl:param name="currentLanguage"/>
		<xsl:if test="contains($foundLanguages, concat($currentLanguage,';'))">
			<xsl:element name="option">
				<xsl:attribute name="value">
					<xsl:value-of select="$currentLanguage"/>
				</xsl:attribute>
				<xsl:if test="$currentLanguage=$lang">
					<xsl:attribute name="selected"/>
				</xsl:if>
				<xsl:value-of select="$currentLanguage"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template name="documentEntry">
		<xsl:if test="xhtml:h3">
			<xsl:choose>
				<xsl:when test="xhtml:h3[@lang=$lang]">
					<xsl:call-template name="documentEntryRow">
						<xsl:with-param name="title">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Title']/@displayName"
							/>
						</xsl:with-param>
						<xsl:with-param name="content">
							<xsl:value-of
								select="xhtml:h3[not(@class='debug' or @class='specification') and @lang=$lang]/text()"
							/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="documentEntryRow">
						<xsl:with-param name="title">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Title']/@displayName"
							/>
						</xsl:with-param>
						<xsl:with-param name="content"> (<xsl:value-of select="$defaultLang"/>):
								<xsl:value-of
								select="xhtml:h3[not(@class='debug' or @class='specification')  and (@lang=$defaultLang or not(@lang))]/text()"
							/></xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="xhtml:p[@lang=$lang]">
				<xsl:if test="xhtml:p[@class='specification']">
					<xsl:call-template name="documentEntryRow">
						<xsl:with-param name="title">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Specification']/@displayName"
							/>
						</xsl:with-param>
						<xsl:with-param name="content">
							<xsl:value-of select="xhtml:p[@class='specification' and @lang=$lang]"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
				<xsl:call-template name="documentEntryRow">
					<xsl:with-param name="title">
						<xsl:value-of
							select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Description']/@displayName"
						/>
					</xsl:with-param>
					<xsl:with-param name="content">
						<xsl:value-of
							select="xhtml:p[not(@class='debug' or @class='specification')  and @lang=$lang]"
						/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="xhtml:p[@class='specification']">
					<xsl:call-template name="documentEntryRow">
						<xsl:with-param name="title">
							<xsl:value-of
								select="document('doc-xsl-voc.xml')/localization/text[@language=$defaultLang and @value='Specification']/@displayName"
							/>
						</xsl:with-param>
						<xsl:with-param name="content"> (<xsl:value-of select="$defaultLang"/>):
							<xsl:value-of select="xhtml:p[@class='specification' and @lang=$defaultLang]"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
				<xsl:call-template name="documentEntryRow">
					<xsl:with-param name="title">
						<xsl:value-of
							select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Description']/@displayName"
						/>
					</xsl:with-param>
					<xsl:with-param name="content"> (<xsl:value-of select="$defaultLang"/>):
							<xsl:value-of
							select="xhtml:p[not(@class='debug' or @class='specification')  and (@lang=$defaultLang or not(@lang))]"
						/></xsl:with-param>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="documentEntryRow">
		<xsl:param name="title"/>
		<xsl:param name="content"/>
		<tr>
			<td>
				<xsl:value-of select="$title"/>
				<xsl:text>:</xsl:text>
			</td>
			<td colspan="3">
				<xsl:value-of select="$content"/>
			</td>
		</tr>
	</xsl:template>

	<xsl:template name="getFilename">
		<xsl:param name="uri"/>
		<xsl:variable name="rest">
			<xsl:value-of select="substring-after($uri, '/')"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="contains($rest, '/')">
				<xsl:call-template name="getFilename">
					<xsl:with-param name="uri">
						<xsl:value-of select="$rest"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$rest"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="replaceVariables">
		<xsl:param name="test" />
		<xsl:choose>
			<xsl:when test="contains($test,'$test')">
				<xsl:variable name="varToReplace" select="'test'" />
				<xsl:call-template name="replaceVariables">
					<xsl:with-param name="test">
						<xsl:value-of select="concat(substring-before($test,concat('$',$varToReplace)), xhtml:p[@class='debug' and @var=$varToReplace and @lang=$lang]/text(), substring-after($test,concat('$',$varToReplace)))" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$test"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="createNewURL">location = "<xsl:value-of select="$baseUrl"
			/>?lang="+document.getElementsByName('listboxLanguages')[0].options[document.getElementsByName('listboxLanguages')[0].selectedIndex].value+"&amp;defaultLang=<xsl:value-of
			select="$defaultLang"/>";</xsl:template>

</xsl:stylesheet>
<!-- EOF -->
