<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************

Stylesheet for reporting Schematron validation results

parameters:
	baseUrl:		url of our own page.
	lang:			filters the desired language.
	defaultLang:	definition of the alternative language that should be used, when the desired language is not available. 
	showDocu:		filters all successful reports
	showError:		filters all failed asserts with the role error.
	showWarning:	filters all failed asserts with the role warning.
	showNote:		filters all failed asserts with the role information.
	showReport:		filters all successful reports
	showDebug:		filters all debugging information

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/report.xsl 23007 2011-07-14 16:47:24Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
	xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:iso="http://purl.oclc.org/dsdl/schematron"
	xmlns:xi="http://www.w3.org/2001/XInclude">

	<xsl:output method="html" omit-xml-declaration="no" encoding="UTF-8" indent="yes"/>

	<!-- parameters -->
	<xsl:param name="baseUrl"/>
	<xsl:param name="lang">en</xsl:param>
	<xsl:param name="defaultLang">en</xsl:param>
	<xsl:param name="showDocu">true</xsl:param>
	<xsl:param name="showError">true</xsl:param>
	<xsl:param name="showWarning">true</xsl:param>
	<xsl:param name="showNote">true</xsl:param>
	<xsl:param name="showReport">true</xsl:param>
	<xsl:param name="showDebug">false</xsl:param>

	<xsl:variable name="cda_filename" select="//xs:annotation[@id='cda_filename']/@value" />
	<xsl:variable name="schematron_filename" select="//xs:annotation[@id='schematron_filename']/@value" />
	<xsl:variable name="validation_timestamp" select="//xs:annotation[@id='validation_timestamp']/@value" />
	<xsl:variable name="title" select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='SchValidationReportTitle']/@displayName"/>

	<!-- main output -->
	<xsl:template match="svrl:schematron-output">
		<html>
			<head>
				<title>
					<xsl:value-of select="$title"/>
					<xsl:text>'</xsl:text>
					<xsl:value-of select="$cda_filename"/>
					<xsl:text>'</xsl:text>
				</title>
				<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
				<link rel="stylesheet" type="text/css" href="doc.css"/>
			</head>
			<body>
				<xsl:call-template name="doHeader"/>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>

	<!-- omit templates section -->
	<xsl:template match="text()"/>
	<xsl:template match="xhtml:p[@class='debug']" />
	<xsl:template match="xhtml:p[@class='specification']" />

	<!-- match templates section -->
	<xsl:template match="xhtml:*">
		<xsl:if test="$showDocu='true'">
			<xsl:element name="{local-name()}">
				<xsl:for-each select="@*">
					<xsl:if test="name()!='lang'">
						<xsl:attribute name="{name()}">
							<xsl:value-of select="."/>
						</xsl:attribute>
					</xsl:if>
				</xsl:for-each>
				<xsl:value-of select="text()"/>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template match="svrl:fired-rule">
		<xsl:if test="$showDocu='true'">
			<div>
				<table class="entity">
					<thead>
						<tr>
							<th class="label1" width="10%">
								<xsl:value-of
									select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Rule']/@displayName"/>
								<xsl:text> ID:</xsl:text>
							</th>
							<th class="value2">
								<xsl:value-of select="@id"/>
							</th>
						</tr>
					</thead>
				</table>
				<br/>
			</div>
			<xsl:apply-templates select="iso:rule" mode="entitydocumented"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="svrl:successful-report | svrl:failed-assert">
		<xsl:if
			test="(self::svrl:failed-assert and (($showError='true' and @role='error') or ($showWarning='true' and @role='warning') or ($showNote='true' and @role='information') or ($showDebug='true' and @role='debug')) or (self::svrl:successful-report and $showReport='true'))">
			<div>
				<table class="body">
					<thead>
						<tr>
							<th class="label1" width="10%">
								<xsl:choose>
									<xsl:when test="self::svrl:failed-assert">
										<xsl:value-of
											select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Assert']/@displayName"
										/>
									</xsl:when>
									<xsl:when test="self::svrl:successful-report">
										<xsl:value-of
											select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Report']/@displayName"
										/>
									</xsl:when>
								</xsl:choose>
								<xsl:text> ID:</xsl:text>
							</th>
							<th class="value2">
								<xsl:value-of select="@id"/>
							</th>
							<th width="20">
								<img>
									<xsl:variable name="role">
										<xsl:choose>
											<xsl:when test="self::svrl:failed-assert">
												<xsl:value-of select="@role"/>
											</xsl:when>
											<xsl:when test="self::svrl:successful-report"
												>report</xsl:when>
										</xsl:choose>
									</xsl:variable>
									<xsl:attribute name="src"><xsl:value-of select="$role"
										/>.gif</xsl:attribute>
								</img>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="label1" width="10%">
								<xsl:value-of
									select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Test']/@displayName"/>
								<xsl:text>:</xsl:text>
							</td>
							<td class="value1" colspan="2">
								<xsl:call-template name="replaceVariables">
									<xsl:with-param name="test"><xsl:value-of select="@test"></xsl:value-of></xsl:with-param>
								</xsl:call-template>
							</td>
						</tr>
						<tr>
							<td class="label1" width="10%">
								<xsl:value-of
									select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Location']/@displayName"/>
								<xsl:text>:</xsl:text>
							</td>
							<td class="value1" colspan="2">
								<xsl:value-of
									select="@location"/>
							</td>
						</tr>
						<tr>
							<td class="label1">
								<xsl:value-of
									select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Description']/@displayName"/>
								<xsl:text>:</xsl:text>
							</td>
							<td class="value1" colspan="2">
								<xsl:choose>
									<xsl:when test="./svrl:text/xhtml:*[@lang=$lang]">
										<xsl:apply-templates
											select="./svrl:text/xhtml:*[not(@class='debug' or @class='specification') and @lang=$lang]/text()"
											mode="documenting"/>
									</xsl:when>
									<xsl:otherwise>(<xsl:value-of select="$defaultLang"/>):
											<xsl:apply-templates
											select="./svrl:text/xhtml:*[not(@class='debug' or @class='specification') and @lang=$defaultLang or not(@lang)]/text()"
											mode="documenting"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
						<xsl:if test="svrl:text/xhtml:p[@class='specification' and @lang=$lang]">
							<tr>
								<td class="label1" width="10%">
									<xsl:value-of
										select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='Specification']/@displayName"/>
									<xsl:text>:</xsl:text>
								</td>
								<td class="value1" colspan="2">
									<xsl:value-of select="svrl:text/xhtml:p[@class='specification' and @lang=$lang]/text()"/>
								</td>
							</tr>
						</xsl:if>
						
						<xsl:if test="$showDebug='true' and svrl:text/xhtml:p[@class='debug' and @lang=$lang and not(@var='test')]">
							<xsl:for-each select="svrl:text/xhtml:p[@class='debug' and @lang=$lang and not(@var='test')]">
								<tr>
									<td class="label1" width="10%">
										<xsl:value-of select="./@var"/>
										<xsl:text>:</xsl:text>
									</td>
									<td class="value1" colspan="2">
										<xsl:value-of select="./text()"/>
									</td>
								</tr>
							</xsl:for-each>
						</xsl:if>
					</tbody>
				</table>
				<br/>
			</div>
		</xsl:if>
	</xsl:template>

	<!-- named templates section -->
	<xsl:template name="doHeader">
		<h1>
					<xsl:value-of select="$title"/>
					<xsl:text>'</xsl:text>
					<xsl:value-of select="$cda_filename"/>
					<xsl:text>'</xsl:text>
			
		</h1>
		<h2><xsl:value-of select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='SchValidationReportSubtitle']/@displayName"/></h2>
		<form name="dynamicSelection">
			<table>
				<tr>
					<td align="right" valign="bottom"><xsl:value-of select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='SchDisplayValidationResults']/@displayName"/></td>
					<xsl:call-template name="createSeverityLevelSelector" />
				</tr>
				<tr>
					<td align="right" valign="bottom"><xsl:value-of select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='SchDisplayLanguage']/@displayName"/></td>
					<td colspan="5" >
						<xsl:call-template name="createLanguageSelector" />
					</td>
				</tr>
				<tr>
					<td align="right" valign="bottom"><xsl:value-of select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='SchValidatedCDA']/@displayName"/></td>
					<td colspan="5" >
						<xsl:value-of select="$cda_filename"/>
					</td>
				</tr>
				<tr>
					<td align="right" valign="bottom"><xsl:value-of select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='SchUsedForValidation']/@displayName"/></td>
					<td colspan="5" >
						<xsl:value-of select="$schematron_filename"/>
					</td>
				</tr>
				<tr>
					<td align="right" valign="bottom"><xsl:value-of select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value='SchValidationTimestamp']/@displayName"/></td>
					<td colspan="5" >
						<xsl:value-of select="$validation_timestamp"/>
					</td>
				</tr>
				<tr />
				<tr />
			</table>
		</form>
	</xsl:template>

	<xsl:template name="createLanguageSelector">
		<xsl:variable name="foundLanguages">
			<xsl:for-each select="//xhtml:*">
				<xsl:if test="@lang"><xsl:value-of select="@lang"/>;</xsl:if>
			</xsl:for-each>
		</xsl:variable>
		<xsl:element name="select">
			<xsl:attribute name="name">listboxLanguages</xsl:attribute>
			<xsl:attribute name="onchange">
				<xsl:call-template name="createNewURL"/>
			</xsl:attribute>
			<xsl:call-template name="addLanguageIfPresent">
				<xsl:with-param name="currentLanguage">en</xsl:with-param>
				<xsl:with-param name="foundLanguages">
					<xsl:value-of select="$foundLanguages"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="addLanguageIfPresent">
				<xsl:with-param name="currentLanguage">de</xsl:with-param>
				<xsl:with-param name="foundLanguages">
					<xsl:value-of select="$foundLanguages"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="addLanguageIfPresent">
				<xsl:with-param name="currentLanguage">fr</xsl:with-param>
				<xsl:with-param name="foundLanguages">
					<xsl:value-of select="$foundLanguages"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="addLanguageIfPresent">
				<xsl:with-param name="currentLanguage">it</xsl:with-param>
				<xsl:with-param name="foundLanguages">
					<xsl:value-of select="$foundLanguages"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:element>
	</xsl:template>

	<xsl:template name="addLanguageIfPresent">
		<xsl:param name="foundLanguages"/>
		<xsl:param name="currentLanguage"/>
		<xsl:if test="contains($foundLanguages, concat($currentLanguage,';'))">
			<xsl:element name="option">
				<xsl:attribute name="value">
					<xsl:value-of select="$currentLanguage"/>
				</xsl:attribute>
				<xsl:if test="$currentLanguage=$lang">
					<xsl:attribute name="selected"/>
				</xsl:if>
				<xsl:value-of select="$currentLanguage"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="createSeverityLevelSelector">
		<xsl:call-template name="addSeverityCheckbox">
			<xsl:with-param name="name">docu</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="addSeverityCheckbox">
			<xsl:with-param name="name">error</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="addSeverityCheckbox">
			<xsl:with-param name="name">warning</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="addSeverityCheckbox">
			<xsl:with-param name="name">note</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="addSeverityCheckbox">
			<xsl:with-param name="name">Report</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="addSeverityCheckbox">
			<xsl:with-param name="name">debug</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="addSeverityCheckbox">
		<xsl:param name="name"/>
		<td valign="bottom">
			<xsl:element name="input">
				<xsl:attribute name="name">checkbox_<xsl:value-of select="$name"/></xsl:attribute>
				<xsl:attribute name="type">checkbox</xsl:attribute>
				<xsl:if test="$name='docu'">
					<xsl:if test="$showDocu='true'">
						<xsl:attribute name="checked">true</xsl:attribute>
					</xsl:if>
				</xsl:if>
				<xsl:if test="$name='error'">
					<xsl:if test="$showError='true'">
						<xsl:attribute name="checked">true</xsl:attribute>
					</xsl:if>
				</xsl:if>
				<xsl:if test="$name='warning'">
					<xsl:if test="$showWarning='true'">
						<xsl:attribute name="checked">true</xsl:attribute>
					</xsl:if>
				</xsl:if>
				<xsl:if test="$name='note'">
					<xsl:if test="$showNote='true'">
						<xsl:attribute name="checked">true</xsl:attribute>
					</xsl:if>
				</xsl:if>
				<xsl:if test="$name='Report'">
					<xsl:if test="$showReport='true'">
						<xsl:attribute name="checked">true</xsl:attribute>
					</xsl:if>
				</xsl:if>
				<xsl:if test="$name='debug'">
					<xsl:if test="$showDebug='true'">
						<xsl:attribute name="checked">true</xsl:attribute>
					</xsl:if>
				</xsl:if>
				<xsl:attribute name="onclick">
					<xsl:call-template name="createNewURL"/>
				</xsl:attribute>
			</xsl:element>
			<xsl:text> </xsl:text>
			<xsl:value-of
				select="document('doc-xsl-voc.xml')/localization/text[@language=$lang and @value=$name]/@displayName"/>
			<xsl:text>; </xsl:text>
		</td>
	</xsl:template>

	<xsl:template name="replaceVariables">
		<xsl:param name="test" />
		<xsl:choose>
			<xsl:when test="contains($test,'$test')">
				<xsl:variable name="varToReplace" select="'test'" />
				<xsl:call-template name="replaceVariables">
					<xsl:with-param name="test">
						<xsl:value-of select="concat(substring-before($test,concat('$',$varToReplace)), svrl:text/xhtml:p[@class='debug' and @var=$varToReplace and @lang=$lang]/text(), substring-after($test,concat('$',$varToReplace)))" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$test"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="createNewURL">location = "<xsl:value-of select="$baseUrl"
			/>?lang="+document.getElementsByName('listboxLanguages')[0].options[document.getElementsByName('listboxLanguages')[0].selectedIndex].value+"&amp;defaultLang=<xsl:value-of
			select="$defaultLang"
		/>&amp;showDocu="+document.getElementsByName('checkbox_docu')[0].checked+"&amp;showError="+document.getElementsByName('checkbox_error')[0].checked+"&amp;showWarning="+document.getElementsByName('checkbox_warning')[0].checked+"&amp;showNote="+document.getElementsByName('checkbox_note')[0].checked+"&amp;showReport="+document.getElementsByName('checkbox_Report')[0].checked+"&amp;showDebug="+document.getElementsByName('checkbox_debug')[0].checked+"";</xsl:template>

</xsl:stylesheet>
<!-- EOF -->
