<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************
Stylesheet containing tests only

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/test.xsl 22462 2011-06-22 16:56:00Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:iso="http://purl.oclc.org/dsdl/schematron"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	>

	<xsl:output method="xml" omit-xml-declaration="yes" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:attribute name="number">
				<!-- these elements did not contain an id. We generate one and add the position number for additional help -->
				<!--<xsl:value-of select="position()"/>-->
				<xsl:number level="any" count="iso:pattern | iso:rule | iso:assert | iso:report" />
			</xsl:attribute>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>


</xsl:stylesheet>
<!-- EOF -->
