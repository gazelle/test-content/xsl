<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************
Stylesheet to generate the included phases
This stylesheet adds an active phase for each included template

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/sch_finish.xsl 23005 2011-07-13 22:36:50Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0" xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
	xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:iso="http://purl.oclc.org/dsdl/schematron"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias"	
	>

	<xsl:output method="xml" omit-xml-declaration="no" version="1.0" encoding="UTF-8" indent="yes"/>

	<!-- parameters -->
	<xsl:param name="cda_filename" />
	<xsl:param name="schematron_filename" />

	<!-- main output -->
	<xsl:template match="/">
		<xsl:processing-instruction name="transform-stylesheet">href="doc.xsl"</xsl:processing-instruction>
		<xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="dynamic_xslt.xsl"</xsl:processing-instruction>
		<xsl:apply-templates/>
	</xsl:template>

	<!-- omit templates section -->

	<!-- match templates section -->
	<xsl:template match="@*|text()|comment()|processing-instruction()">
		<xsl:copy-of select="."/>
	</xsl:template>

	<xsl:template match="*">
		<xsl:choose>
			<xsl:when test="self::iso:phase">
				<xsl:call-template name="do-phase">
					<xsl:with-param name="phase-id">
						<xsl:value-of select="@id"/>
					</xsl:with-param>
					<xsl:with-param name="namespace">
						<xsl:value-of select="namespace-uri()"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="xsl:*">
		<xsl:variable name="nodename"><xsl:value-of select="node-name(.)"/></xsl:variable>		
		<xsl:choose>
			<xsl:when test="compare($nodename,'xsl:text')=0">
				<xsl:value-of select="text()"/>				
			</xsl:when>
			<xsl:when test="compare($nodename,'xsl:value-of')=0">
				<xsl:variable name="expr" select="@select" />
				<xsl:choose>
					<xsl:when test="compare(substring($expr,1,1),'$')=0 and not(contains($expr,' '))">
						<xsl:variable name="value" select="parent::*/preceding::iso:let[@name=substring-after($expr,'$')][1]/@value" />
						<xsl:value-of select='replace($value, "&apos;", "")'/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>[</xsl:text>
						<xsl:value-of select="@select"/>
						<xsl:text>=</xsl:text>
						<xsl:copy-of select="." />
						<xsl:text>]</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				Warning: This stylesheet does not currently supports embedded <xsl:value-of select="node-name(.)"/> commands!
				<xsl:message>Warning: This stylesheet does not currently supports embedded <xsl:value-of select="node-name(.)"/> commands!</xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- named templates section -->
	<xsl:template name="do-phase">
		<xsl:param name="phase-id"/>
		<xsl:param name="namespace"/>
		<xsl:message> in do-phase <xsl:value-of select="$phase-id"/>
		</xsl:message>
		<xsl:element name="iso:phase">
			<xsl:attribute name="id">
				<xsl:value-of select="$phase-id"/>
			</xsl:attribute>
			<xsl:for-each select="parent::node()/child::node()">
				<xsl:if test="local-name(current())='pattern'">
					<xsl:choose>
						<xsl:when test="$phase-id='all'">
							<xsl:element name="iso:active">
								<xsl:attribute name="pattern">
									<xsl:value-of select="@id"/>
								</xsl:attribute>
							</xsl:element>
						</xsl:when>
						<xsl:when test="$phase-id='errors'">
							<xsl:if test="@role='error'">
								<xsl:element name="iso:active">
									<xsl:attribute name="pattern">
										<xsl:value-of select="@id"/>
									</xsl:attribute>
								</xsl:element>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$phase-id='warnings'">
							<xsl:if test="@role='warning'">
								<xsl:element name="iso:active">
									<xsl:attribute name="pattern">
										<xsl:value-of select="@id"/>
									</xsl:attribute>
								</xsl:element>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$phase-id='notes'">
							<xsl:if test="@role='information'">
								<xsl:element name="iso:active">
									<xsl:attribute name="pattern">
										<xsl:value-of select="@id"/>
									</xsl:attribute>
								</xsl:element>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$phase-id='debug'">
							<xsl:if test="@role='debug'">
								<xsl:element name="iso:active">
									<xsl:attribute name="pattern">
										<xsl:value-of select="@id"/>
									</xsl:attribute>
								</xsl:element>
							</xsl:if>
						</xsl:when>
						<xsl:when test="$phase-id='no-codes'">
							<xsl:if test="not(contains(@id,'-codes'))">
								<xsl:element name="iso:active">
									<xsl:attribute name="pattern">
										<xsl:value-of select="@id"/>
									</xsl:attribute>
								</xsl:element>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise ></xsl:otherwise>
					</xsl:choose>
				</xsl:if>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
<!-- EOF -->
