<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************
Stylesheet documenting patterns automatically

param: entity_filename

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/ent_add_docu.xsl 23006 2011-07-14 12:43:13Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:iso="http://purl.oclc.org/dsdl/schematron"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	>
	<xsl:param name="entity_filename" />
	<xsl:output method="xml" omit-xml-declaration="no" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:variable name="entity_name" select="substring($entity_filename, 1, string-length($entity_filename)-4)" />

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@*|text()|comment()|processing-instruction()">
		<xsl:copy-of select="."/>
	</xsl:template>

	<xsl:template match="iso:pattern">
		<xsl:variable name="version" select="xs:annotation[@id='version']/@value" />
		<xsl:copy>
			<xsl:attribute name="id"><xsl:value-of select="$entity_name"/></xsl:attribute>
			<xsl:apply-templates select="@*"/>
			<xhtml:ul>
				<xhtml:li class="filename"><xsl:value-of select="$entity_filename"/></xhtml:li>
				<xhtml:li class="lastupdate"><xsl:value-of select="current-dateTime()" /></xhtml:li>
			</xhtml:ul>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- Add additional paragraphs containing the source specification, the resolved test variable and other debug information  -->
	<xsl:template match="xhtml:p">
		<xsl:variable name="lang" select="@lang" />

		<!-- Add the source specification -->
		<xsl:variable name="spec" select='replace(parent::*/preceding::iso:let[@name="spec"][1]/@value, "&apos;", "")' />
		<xsl:if test="$spec">
			<xsl:element name="xhtml:p">
				<xsl:attribute name="class">specification</xsl:attribute>
				<xsl:attribute name="lang"><xsl:value-of select="$lang"/></xsl:attribute>
				<xsl:value-of select="$spec"/>
			</xsl:element>
		</xsl:if>
		
		<!-- For debugging purposes only (might be commented out for production) -->
		<xsl:element name="xhtml:p">
			<xsl:attribute name="class">debug</xsl:attribute>
			<xsl:attribute name="var">entity_filename</xsl:attribute>
			<xsl:attribute name="lang"><xsl:value-of select="$lang"/></xsl:attribute>
			<xsl:value-of select="$entity_filename"/>
		</xsl:element>
		<xsl:variable name="test" select="parent::*/preceding::iso:let[@name='test'][1]/@value" />
		<xsl:element name="xhtml:p">
			<xsl:attribute name="class">debug</xsl:attribute>
			<xsl:attribute name="var">test</xsl:attribute>
			<xsl:attribute name="lang"><xsl:value-of select="$lang"/></xsl:attribute>
			<xsl:call-template name="replaceVariables">
				<xsl:with-param name="test"><xsl:value-of select="$test"/></xsl:with-param>
			</xsl:call-template>
		</xsl:element>
		<xsl:copy>
			<xsl:attribute name="id"><xsl:value-of select="$entity_name"/></xsl:attribute>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template name="replaceVariables">
		<xsl:param name="test" />
		<xsl:choose>
			<xsl:when test="contains($test,'$')">
				<xsl:variable name="varToReplace" select="tokenize(substring-after($test,'$'),'\W+')[1]" />
				<xsl:call-template name="replaceVariables">
					<xsl:with-param name="test">
						<xsl:value-of select="concat(substring-before($test,concat('$',$varToReplace)), parent::*/preceding-sibling::iso:let[@name=$varToReplace][1]/@value, substring-after($test,concat('$',$varToReplace)))" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$test"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
<!-- EOF -->
