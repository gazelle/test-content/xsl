<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************
Stylesheet to remove the document root node

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/sch_remove_embedded_schema_element.xsl 22576 2011-06-24 18:38:00Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	>

	<xsl:output method="xml" omit-xml-declaration="yes" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="*">
		<!-- debug only:
		<xsl:message>
		self:<xsl:value-of select="local-name(self::*)"/> / parent:<xsl:value-of select="local-name(parent::*)"/> / root:<xsl:value-of select="not(parent::*)"/>
		</xsl:message>
		-->
		<xsl:choose>
			<xsl:when test="self::xs:schema">
				<xsl:choose>
					<xsl:when test="parent::*">
						<xsl:apply-templates select="child::*|text()|comment()|processing-instruction()"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy>
							<xsl:apply-templates select="@*|node()"/>
						</xsl:copy>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="@*|text()|comment()|processing-instruction()">
		<xsl:copy-of select="."/>
	</xsl:template>

</xsl:stylesheet>
<!-- EOF -->
