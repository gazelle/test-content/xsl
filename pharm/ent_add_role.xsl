<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************
Stylesheet adding pattern roles

param: pattern_role

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/ent_add_role.xsl 22462 2011-06-22 16:56:00Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:iso="http://purl.oclc.org/dsdl/schematron"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	>
	<xsl:param name="pattern_role" />
	<xsl:output method="xml" omit-xml-declaration="no" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@*|text()|comment()|processing-instruction()">
		<xsl:copy-of select="."/>
	</xsl:template>

	<xsl:template match="iso:pattern">
		<xsl:copy>
			<xsl:attribute name="role"><xsl:value-of select="$pattern_role"/></xsl:attribute>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="iso:assert">
		<xsl:copy>
			<xsl:attribute name="role"><xsl:value-of select="$pattern_role"/></xsl:attribute>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
<!-- EOF -->
