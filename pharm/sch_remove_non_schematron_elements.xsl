<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************
Stylesheet to remove all xhtml elements from the schematron

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/sch_remove_non_schematron_elements.xsl 22462 2011-06-22 16:56:00Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:iso="http://purl.oclc.org/dsdl/schematron"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	>

	<xsl:output method="xml" omit-xml-declaration="yes" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@*|text()|comment()|processing-instruction()">
		<xsl:copy-of select="."/>
	</xsl:template>

	<!-- remove all xhtml elements -->	
	<xsl:template match="xhtml:*" />

	<!-- remove all annotations  -->	
	<xsl:template match="xs:annotation" />

</xsl:stylesheet>
<!-- EOF -->
