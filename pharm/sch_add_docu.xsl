<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************
Stylesheet to generate the included phases
This stylesheet adds an active phase for each included template

Subversion header information:
$Header: svn+ssh://poiseau@scm.gforge.inria.fr/svn/gazelle/branches/epSOS/schematron/ihe_pharm_all/tools/stylesheets/sch_add_docu.xsl 22576 2011-06-24 18:38:00Z tschalle $

********************************************************
-->
<xsl:stylesheet version="2.0"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:iso="http://purl.oclc.org/dsdl/schematron"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	>

	<xsl:output method="xml" omit-xml-declaration="yes" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template name="add_reference">
		<xsl:copy-of select="xs:title"/>
		<xhtml:h2 class="reference" lang="en">Referenced entities</xhtml:h2>
		<xhtml:ul id="reference">
			<xsl:for-each select="child::node()">
				<xsl:if test="local-name(current())='include'">
					<xhtml:li>
						<xsl:value-of select="@href"/>
					</xhtml:li>
				</xsl:if>
			</xsl:for-each>
		</xhtml:ul>
	</xsl:template>

	<xsl:template match="*">
		<xsl:choose>
			<xsl:when test="self::schema">
				<xsl:copy>
					<xsl:apply-templates select="@*"/>
					<xsl:call-template name="add_reference" />
					<xsl:apply-templates select="node()"/>
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*|node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="@*|text()|comment()|processing-instruction()">
		<xsl:copy-of select="."/>
	</xsl:template>


</xsl:stylesheet>
<!-- EOF -->
