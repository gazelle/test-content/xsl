<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:fhir="http://hl7.org/fhir">
    <xsl:output encoding="UTF-8" indent="yes" method="html" omit-xml-declaration="yes"/>

    <xsl:template match="/">

        <html>
            <head>
                <title>Structure definition (SNAPSHOT)</title>
                <link href="gazelle-theme.css" rel="stylesheet" type="text/css"/>
                <script src="jquery_1_11_2.min.js"/>
                <script src="bootstrap.min.js"/>
                <script type="text/javascript">
                    jq162 = jQuery.noConflict(true);
                </script>
                <style>
                    .report-number {
                    float: right;
                    font-weight: bold;
                    font-size: smaller;
                    }
                </style>
            </head>
            <body>
                <div id="gzl-container">
                    <xsl:apply-templates select="fhir:StructureDefinition"/>
                </div>
            </body>
        </html>

    </xsl:template>

    <xsl:template match="fhir:StructureDefinition">
        <h2>Snapshot</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Contextual information</div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt>Id</dt>
                    <dd>
                        <xsl:value-of select="fhir:id/@value"/>
                    </dd>
                    <dt>Name</dt>
                    <dd>
                        <xsl:value-of select="fhir:name/@value"/>
                    </dd>
                    <dt>Title</dt>
                    <dd>
                        <xsl:value-of select="fhir:title/@value"/>
                    </dd>
                    <dt>Description</dt>
                    <dd>
                        <xsl:value-of select="fhir:description/@value"/>
                    </dd>
                    <dt>FHIR version</dt>
                    <dd>
                        <xsl:value-of select="fhir:fhirVersion/@value"/>
                    </dd>
                    <dt>Type</dt>
                    <dd>
                        <xsl:value-of select="fhir:type/@value"/>
                    </dd>
                    <dt>Status</dt>
                    <dd>
                        <xsl:value-of select="fhir:status/@value"/>
                    </dd>
                    <dt>URL</dt>
                    <dd>
                        <xsl:value-of select="fhir:url/@value"/>
                    </dd>
                    <dt>Base definition</dt>
                    <dd>
                        <xsl:value-of select="fhir:baseDefinition/@value"/>
                    </dd>
                    <dt>Derivation</dt>
                    <dd>
                        <xsl:value-of select="fhir:derivation/@value"/>
                    </dd>
                    <dt>Experimental ?</dt>
                    <dd>
                        <xsl:value-of select="fhir:experimental/@value"/>
                    </dd>
                    <dt>Abstract ?</dt>
                    <dd>
                        <xsl:value-of select="fhir:abstract/@value"/>
                    </dd>
                    <dt>Kind</dt>
                    <dd>
                        <xsl:value-of select="fhir:kind/@value"/>
                    </dd>
                    <dt>Last update date</dt>
                    <dd>
                        <xsl:value-of select="fhir:date/@value"/>
                    </dd>
                    <dt>Publisher</dt>
                    <dd>
                        <xsl:value-of select="fhir:publisher/@value"/>
                    </dd>
                    <dt>Copyright</dt>
                    <dd>
                        <xsl:value-of select="fhir:copyright/@value"/>
                    </dd>
                    <dt>Contact(s)</dt>
                    <dd>
                        <ul>
                            <xsl:for-each select="fhir:contact">
                                <li>
                                    <xsl:apply-templates select="current()"/>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Mappings</div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <th>Identity</th>
                        <th>URI</th>
                        <th>Name</th>
                    </thead>
                    <tbody>
                        <xsl:for-each select="fhir:mapping">
                            <xsl:apply-templates select="current()"/>
                        </xsl:for-each>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Details</div>
            <div class="panel-body">
                <ul class="nav nav-tabs" role="tablist">
                    <xsl:if test="fhir:snapshot">
                        <li role="presentation" class="active">
                            <a href="#t_snapshot" aria-controls="Snapshot" role="tab" data-toggle="tab">Snapshot</a>
                        </li>
                    </xsl:if>
                    <xsl:if test="fhir:differential">
                        <li role="presentation">
                            <a href="#t_differential" aria-controls="Differential" role="tab" data-toggle="tab">Differential</a>
                        </li>
                    </xsl:if>
                </ul>

                <div class="tab-content tab-content-with-border">
                    <xsl:if test="fhir:snapshot">
                        <div role="tabpanel" class="tab-pane active" id="t_snapshot">
                            <xsl:apply-templates select="fhir:snapshot"/>
                        </div>
                    </xsl:if>
                    <xsl:if test="fhir:differential">
                        <div role="tabpanel" class="tab-pane" id="t_differential">
                            <xsl:apply-templates select="fhir:differential"/>
                        </div>
                    </xsl:if>
                </div>

            </div>
        </div>
    </xsl:template>

    <xsl:template match="fhir:differential">
        <div class="row">
            <div class="col-lg-4">
                <xsl:call-template name="differentialSummary">
                    <xsl:with-param name="elements" select="fhir:element"/>
                </xsl:call-template>
            </div>
            <div class="col-lg-8">
                <xsl:call-template name="snapshotDetails">
                    <xsl:with-param name="elements" select="fhir:element"/>
                </xsl:call-template>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="differentialSummary">
        <xsl:param name="elements"/>
        <div class="row">
            <div class="col-md-8">
                <strong>
                    <xsl:text>Path</xsl:text>
                </strong>
            </div>
            <div class="col-md-4">
                <strong>
                    <xsl:text>Cardinality</xsl:text>
                </strong>
            </div>
            <xsl:for-each select="$elements">
                <div class="col-md-8">
                    <xsl:element name="a">
                        <xsl:attribute name="href">
                            <xsl:text>#</xsl:text>
                            <xsl:call-template name="build_id">
                                <xsl:with-param name="elementNode" select="current()"/>
                            </xsl:call-template>
                        </xsl:attribute>
                        <xsl:call-template name="display_formatted_path">
                            <xsl:with-param name="currentNode" select="current()"/>
                        </xsl:call-template>
                    </xsl:element>
                    <xsl:if test="fhir:mustSupport/@value = 'true'">
                        <span class="color:red;" title="mustSupport">*</span>
                    </xsl:if>
                    <xsl:if test="fhir:fixedCode or fhir:fixedUri">
                        <xsl:text> </xsl:text>
                        <span class="badge" title="A fixed value is defined for this element"
                              style="background-color: #449d44; border-color: #255625;">
                            F
                        </span>
                    </xsl:if>
                    <xsl:if test="fhir:binding">
                        <xsl:text> </xsl:text>
                        <span class="badge" title="A binding is defined for this element" style="background-color: #337ab7; border-color: #2e6da4;">
                            B
                        </span>
                    </xsl:if>
                    <xsl:if test="fhir:constraint">
                        <xsl:text> </xsl:text>
                        <span class="badge" title="Number of constraints" style="background-color: #c9302c; border-color: #761c19;">
                            <xsl:value-of select="count(fhir:constraint)"/>
                        </span>
                    </xsl:if>
                </div>
                <div class="col-md-4">
                    <xsl:call-template name="display_cardinality">
                        <xsl:with-param name="currentNode" select="current()"/>
                        <xsl:with-param name="addBrackets">true</xsl:with-param>
                    </xsl:call-template>
                </div>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template name="differentialDetails">
        <xsl:param name="elements"/>

    </xsl:template>

    <xsl:template match="fhir:snapshot">
        <div class="row">
            <div class="col-lg-5">
                <xsl:call-template name="snapshotSummary">
                    <xsl:with-param name="elements" select="fhir:element"/>
                </xsl:call-template>
            </div>
            <div class="col-lg-7">
                <xsl:call-template name="snapshotDetails">
                    <xsl:with-param name="elements" select="fhir:element"/>
                </xsl:call-template>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="fhir:contact">
        <xsl:element name="a">
            <xsl:attribute name="href">
                <xsl:if test="fhir:telecom/fhir:system/@value = 'email'">
                    <xsl:text>mailto:</xsl:text>
                </xsl:if>
                <xsl:value-of select="fhir:telecom/fhir:value/@value"/>
            </xsl:attribute>
            <xsl:value-of select="fhir:name/@value"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="fhir:mapping">
        <tr>
            <td>
                <xsl:value-of select="fhir:identity/@value"/>
            </td>
            <td>
                <xsl:value-of select="fhir:uri/@value"/>
            </td>
            <td>
                <xsl:value-of select="fhir:name/@value"/>
            </td>
        </tr>
    </xsl:template>

    <xsl:template name="snapshotSummary">
        <xsl:param name="elements"/>
        <div class="row">
            <div class="col-md-5">
                <strong>
                    <xsl:text>Path</xsl:text>
                </strong>
            </div>
            <div class="col-md-2">
                <strong>
                    <xsl:text>Cardinality</xsl:text>
                </strong>
            </div>
            <div class="col-md-5">
                <strong>
                    <xsl:text>Type</xsl:text>
                </strong>
            </div>
            <xsl:for-each select="$elements">
                <div class="col-md-5">
                    <xsl:element name="a">
                        <xsl:attribute name="href">
                            <xsl:text>#</xsl:text>
                            <xsl:call-template name="build_id">
                                <xsl:with-param name="elementNode" select="current()"/>
                            </xsl:call-template>
                        </xsl:attribute>
                        <xsl:call-template name="display_formatted_path">
                            <xsl:with-param name="currentNode" select="current()"/>
                        </xsl:call-template>
                    </xsl:element>
                </div>
                <div class="col-md-2">
                    <xsl:call-template name="display_cardinality">
                        <xsl:with-param name="currentNode" select="current()"/>
                        <xsl:with-param name="addBrackets">true</xsl:with-param>
                    </xsl:call-template>
                </div>
                <div class="col-md-5">
                    <xsl:choose>
                        <xsl:when test="fhir:type">
                            <xsl:apply-templates select="fhir:type">
                                <xsl:with-param name="detailed">false</xsl:with-param>
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="fhir:base/fhir:path/@value"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="fhir:fixedCode or fhir:fixedUri">
                        <xsl:text> </xsl:text>
                        <span class="badge" title="A fixed value is defined for this element"
                              style="background-color: #449d44; border-color: #255625;">
                            F
                        </span>
                    </xsl:if>
                    <xsl:if test="fhir:binding">
                        <xsl:text> </xsl:text>
                        <span class="badge" title="A binding is defined for this element" style="background-color: #337ab7; border-color: #2e6da4;">
                            B
                        </span>
                    </xsl:if>
                    <xsl:if test="fhir:constraint">
                        <xsl:text> </xsl:text>
                        <span class="badge" title="Number of constraints" style="background-color: #c9302c; border-color: #761c19;">
                            <xsl:value-of select="count(fhir:constraint)"/>
                        </span>
                    </xsl:if>
                </div>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template name="display_formatted_path">
        <xsl:param name="currentNode"/>
        <xsl:choose>
            <xsl:when test="$currentNode/fhir:min/@value = 1">
                <strong><xsl:value-of select="$currentNode/fhir:path/@value"/></strong>
            </xsl:when>
            <xsl:when test="$currentNode/fhir:max/@value = 0">
                <s><xsl:value-of select="$currentNode/fhir:path/@value"/></s>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$currentNode/fhir:path/@value"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="snapshotDetails">
        <xsl:param name="elements"/>
        <xsl:for-each select="$elements">
            <xsl:apply-templates select="current()"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="fhir:element">
        <div class="panel panel-default">
            <div class="panel-heading">
                <xsl:attribute name="id">
                    <xsl:call-template name="build_id">
                        <xsl:with-param name="elementNode" select="current()"/>
                    </xsl:call-template>
                </xsl:attribute>
                <xsl:value-of select="fhir:path/@value"/>
                <xsl:text> </xsl:text>
                <xsl:if test="fhir:fixedCode or fhir:fixedUri">
                    <xsl:text> </xsl:text>
                    <span class="gzl-label gzl-label-green">Fixed value</span>
                </xsl:if>
                <xsl:if test="fhir:binding">
                    <xsl:text> </xsl:text>
                    <span class="gzl-label gzl-label-blue">Binding</span>
                </xsl:if>
                <xsl:if test="fhir:constraint">
                    <xsl:text> </xsl:text>
                    <span class="gzl-label gzl-label-red">Constrained</span>
                </xsl:if>
            </div>
            <div class="panel-body">
                <dl>
                    <dt>Cardinality</dt>
                    <dd>
                        <xsl:call-template name="display_cardinality">
                            <xsl:with-param name="currentNode" select="current()"/>
                            <xsl:with-param name="addBrackets">false</xsl:with-param>
                        </xsl:call-template>
                    </dd>
                    <xsl:if test="fhir:mustSupport">
                        <dt>Must support ?</dt>
                        <dd>
                            <xsl:value-of select="fhir:mustSupport/@value"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:isSummary">
                        <dt>Part of a summary ?</dt>
                        <dd>
                            <xsl:value-of select="fhir:isSummary/@value"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:slicing">
                        <dt>Slicing</dt>
                        <dd><ul>
                            <li><b>Descriminator type: </b><xsl:value-of select="fhir:slicing/fhir:discriminator/fhir:type/@value"/></li>
                            <li><b>Descriminator path: </b><xsl:value-of select="fhir:slicing/fhir:discriminator/fhir:path/@value"/></li>
                            <li><b>Rules: </b><xsl:value-of select="fhir:slicing/fhir:rules/@value"/></li>
                        </ul></dd>
                    </xsl:if>
                    <xsl:if test="fhir:representation">
                        <dt>Representation</dt>
                        <dd>
                            <xsl:value-of select="fhir:representation"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:short">
                        <dt>Short Description</dt>
                        <dd>
                            <xsl:value-of select="fhir:short/@value"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:sliceName">
                        <dt>Slice name</dt>
                        <dd>
                            <xsl:value-of select="fhir:sliceName/@value"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:definition">
                        <dt>Definition</dt>
                        <dd>
                            <xsl:value-of select="fhir:definition/@value"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:base">
                        <dt>Base</dt>
                        <dd>
                            <xsl:value-of select="fhir:base/fhir:path/@value"/>
                            <xsl:text> </xsl:text>
                            <xsl:call-template name="display_cardinality">
                                <xsl:with-param name="addBrackets">true</xsl:with-param>
                                <xsl:with-param name="currentNode" select="fhir:base"/>
                            </xsl:call-template>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:comment">
                        <dt>Comment</dt>
                        <dd>
                            <xsl:value-of select="fhir:comment/@value"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:alias">
                        <dt>Alias</dt>
                        <dd>
                            <xsl:variable name="aliasCount" select="count(fhir:alias)"/>
                            <xsl:for-each select="fhir:alias">
                                <xsl:value-of select="@value"/>
                                <xsl:if test="position() &lt; $aliasCount">
                                    <xsl:text>, </xsl:text>
                                </xsl:if>
                            </xsl:for-each>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:type">
                        <dt>Type</dt>
                        <dd>
                            <xsl:choose>
                                <xsl:when test="count(fhir:type) = 1">
                                    <xsl:apply-templates select="fhir:type">
                                        <xsl:with-param name="detailed">true</xsl:with-param>
                                    </xsl:apply-templates>
                                </xsl:when>
                                <xsl:otherwise>
                                    <ul>
                                        <xsl:for-each select="fhir:type">
                                            <li><xsl:apply-templates select="current()">
                                                <xsl:with-param name="detailed">true</xsl:with-param>
                                            </xsl:apply-templates> </li>
                                        </xsl:for-each>
                                    </ul>
                                </xsl:otherwise>
                            </xsl:choose>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:fixedCode">
                        <dt>Fixed code</dt>
                        <dd>
                            <xsl:value-of select="fhir:fixedCode/@value"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="fhir:fixedUri">
                        <dt>Fixed URI</dt>
                        <dd>
                            <xsl:value-of select="fhir:fixedUri/@value"/>
                        </dd>
                    </xsl:if>
                    <xsl:if test="count(fhir:binding) &gt; 0">
                        <dt>Binding</dt>
                        <dd>
                            <xsl:apply-templates select="fhir:binding"/>
                        </dd>
                    </xsl:if>
                </dl>
                <xsl:if test="count(fhir:mapping) &gt; 0">
                    <dt>Mapping</dt>
                    <dd>
                        <ul>
                            <xsl:for-each select="fhir:mapping">
                                <li>
                                    <b>
                                        <xsl:value-of select="fhir:identity/@value"/>
                                        <xsl:text>:</xsl:text>
                                    </b>
                                    <xsl:value-of select="fhir:map/@value"/>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </dd>
                </xsl:if>
                <xsl:if test="count(fhir:constraint) &gt; 0">
                    <b>Constraints</b>
                    <xsl:for-each select="fhir:constraint">
                        <xsl:apply-templates select="current()"/>
                    </xsl:for-each>
                </xsl:if>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="display_cardinality">
        <xsl:param name="currentNode"/>
        <xsl:param name="addBrackets"/>
        <xsl:if test="$addBrackets = 'true'">
            <xsl:text>[</xsl:text>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="$currentNode/fhir:min">
                <xsl:value-of select="$currentNode/fhir:min/@value"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>..</xsl:text>
        <xsl:choose>
            <xsl:when test="$currentNode/fhir:max">
                <xsl:value-of select="$currentNode/fhir:max/@value"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>*</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="$addBrackets = 'true'">
            <xsl:text>]</xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template name="build_id">
        <xsl:param name="elementNode"/>
        <xsl:choose>
            <xsl:when test="name(..) = 'snapshot'">
                <xsl:text>s_</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>d_</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$elementNode/@id"/>
    </xsl:template>


    <xsl:template match="fhir:binding">
        <xsl:element name="a">
            <xsl:attribute name="href">
                <xsl:value-of select="fhir:valueSetReference/fhir:reference/@value"/>
            </xsl:attribute>
            <xsl:attribute name="target">blank</xsl:attribute>
            <xsl:value-of select="fhir:extension/fhir:valueString/@value"/>
        </xsl:element>
        <xsl:text>(</xsl:text>
        <xsl:value-of select="fhir:strength/@value"/>
        <xsl:text>) : </xsl:text>
        <em>
            <xsl:value-of select="fhir:description/@value"/>
        </em>
    </xsl:template>

    <xsl:template match="fhir:constraint">
        <div>
            <xsl:attribute name="class">
                <xsl:text>gzl-notification</xsl:text>
                <xsl:choose>
                    <xsl:when test="fhir:severity/@value='error'">
                        <xsl:text> gzl-notification-red</xsl:text>
                    </xsl:when>
                    <xsl:when test="fhir:severity/@value='warning'">
                        <xsl:text> gzl-notification-orange</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text> gzl-notification-blue</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <dl class="dl-horizontal">
                <dt>Severity</dt>
                <dd>
                    <xsl:value-of select="fhir:severity/@value"/>
                    <span class="report-number">
                        <xsl:element name="a">
                            <xsl:value-of select="fhir:key/@value"/>
                        </xsl:element>
                    </span>
                </dd>
                <dt>Description</dt>
                <dd>
                    <xsl:value-of select="fhir:human/@value"/>
                </dd>
                <dt>Expression</dt>
                <dd>
                    <code>
                        <xsl:value-of select="fhir:expression/@value"/>
                    </code>
                </dd>
                <dt>XPath</dt>
                <dd>
                    <xsl:value-of select="fhir:xpath/@value"/>
                </dd>
                <dt>Source</dt>
                <dd>
                    <xsl:value-of select="fhir:source/@value"/>
                </dd>
            </dl>
        </div>
    </xsl:template>

    <xsl:template match="fhir:type">
        <xsl:param name="detailed"/>
        <xsl:choose>
            <xsl:when test="count(fhir:targetProfile) &gt; 0">
                <xsl:element name="a">
                    <xsl:attribute name="href">
                        <xsl:value-of select="fhir:targetProfile/@value"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">blank</xsl:attribute>
                    <xsl:value-of select="fhir:code/@value"/>
                </xsl:element>
                <xsl:if test="$detailed = 'true'">
                    <xsl:text> &gt; </xsl:text>
                    <xsl:value-of select="fhir:targetProfile/@value"/>
                </xsl:if>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="fhir:code/@value"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>