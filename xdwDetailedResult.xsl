<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 02, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> Abderrazek Boufahja, IHE Development, Kereval</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <html>
            <head>
                <title>External Validation Report</title>
                <link href="http://gazelle.ihe.net/xsl/resultStyle.css" rel="stylesheet" type="text/css" media="screen"/>
            </head>
            <body>
                <script type="text/javascript">
                    function hideOrViewValidationDetails() {
                       var detaileddiv = document.getElementById('resultdetailedann');
                       if (detaileddiv != null){
                           var onn = document.getElementById('resultdetailedann_switch_on');
                           if (onn != null){
                               if (onn.style.display == 'block') onn.style.display = 'none';
                               else if (onn.style.display == 'none') onn.style.display = 'block';
                           }
                           var off = document.getElementById('resultdetailedann_switch_off');
                           if (off != null){
                               if (off.style.display == 'block') off.style.display = 'none';
                               else if (off.style.display == 'none') off.style.display = 'block';
                           }
                           var body = document.getElementById('resultdetailedann_body');
                           if (body != null){
                               if (body.style.display == 'block') body.style.display = 'none';
                               else if (body.style.display == 'none') body.style.display = 'block';
                           }
                       }
                    }
                    function hideOrViewNistValidationDetails() {
                       var detaileddiv = document.getElementById('resultdetailedannNist');
                       if (detaileddiv != null){
                           var onn = document.getElementById('resultdetailedann_switch_on_nist');
                           if (onn != null){
                               if (onn.style.display == 'block') onn.style.display = 'none';
                               else if (onn.style.display == 'none') onn.style.display = 'block';
                           }
                           var off = document.getElementById('resultdetailedann_switch_off_nist');
                           if (off != null){
                               if (off.style.display == 'block') off.style.display = 'none';
                               else if (off.style.display == 'none') off.style.display = 'block';
                           }
                           var body = document.getElementById('resultdetailedann_body_nist');
                           if (body != null){
                               if (body.style.display == 'block') body.style.display = 'none';
                               else if (body.style.display == 'none') body.style.display = 'block';
                           }
                       }
                    }
 		    function hideOrUnhide(elem){
                        var elemToHide = document.getElementById(elem.name + '_p');
                        if (elemToHide != null){
                                if (elem.checked){
                                    elemToHide.style.display = 'none';
                                }
                                else{
                                    elemToHide.style.display = 'block';
                                }
                           }
                    }

                </script>
                <h2>External Validation Report</h2>
                <br/>
                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header">General Information</div>
                    <div class="rich-panel-body">
                        <table border="0">
                            <tr>
                                <td><b>Validation Date</b></td>
                                <td><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationDate"/> - <xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTime"/></td> 
                            </tr>
                            <tr>
                                <td><b>Validation Service</b></td>
                                <td><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceName"/> (<xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationServiceVersion"/>)</td>         
                            </tr>
                            <tr>
                                <td><b>Validation Test Status</b></td>
                                <td>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'PASSED')">
                                        <div class="PASSED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'FAILED')">
                                        <div class="FAILED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                    <xsl:if test="contains(detailedResult/ValidationResultsOverview/ValidationTestResult, 'ABORTED')">
                                        <div class="ABORTED"><xsl:value-of select="detailedResult/ValidationResultsOverview/ValidationTestResult"/></div>
                                    </xsl:if>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br/>
               
                
                <div class="rich-panel styleResultBackground">
                    <div class="rich-panel-header">XSD Validation detailed Results</div>
                    <div class="rich-panel-body">
                        <i>The document you have validated is supposed to be an XML document. The validator checks that the document is well-formed and valide  against one ore several XSD schemas, results of these XML validations are gathered in this section.</i>
                        <xsl:choose>
                            <xsl:when test="detailedResult/DocumentWellFormed/Result = 'PASSED'">
                                <p class="PASSED">The XML document is well-formed</p>                        
                            </xsl:when>
                            <xsl:otherwise>
                                <p class="FAILED">The XML document is not well-formed, or your document does not contain the XML strucuture looked for by the validator</p>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:if test="count(detailedResult/DocumentValidXDW) = 1">
                            <xsl:choose>
                                <xsl:when test="detailedResult/DocumentValidXDW/Result = 'PASSED'">
                                    <p class="PASSED">The XML document is valid according to the  schema</p>
                                </xsl:when>
                                <xsl:otherwise>
                                    <p class="FAILED">The XML document is not valid according to the schema for the following reasons: </p>
                                    <xsl:if test="count(detailedResult/DocumentValidXDW/*) &gt; 3">
                                        <ul>
                                            <xsl:for-each select="detailedResult/DocumentValidXDW/*">
                                                <xsl:if test="contains(current(), 'error')">
                                                    <li><xsl:value-of select="current()"/></li>
                                                </xsl:if>
                                            </xsl:for-each>
                                        </ul>
                                    </xsl:if>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                    </div>
                </div>
                <br/>
                <br/>
                <xsl:if test="count(detailedResult/MDAValidation) = 1">
                <div class="rich-stglpanel" id="resultdetailedann">
                    <div class="rich-stglpanel-header" onclick="hideOrViewValidationDetails();">
                        Validation details
                        <div class="rich-stglpanel-marker">
                            <div id="resultdetailedann_switch_on" class="rich-stglpnl-marker" style="display: block;"><xsl:text disable-output-escaping="yes">&amp;laquo;</xsl:text></div>
                            <div id="resultdetailedann_switch_off" class="rich-stglpnl-marker" style="display: none;"><xsl:text disable-output-escaping="yes">&amp;raquo;</xsl:text></div>
                        </div>
                    </div>
                    <div class="rich-stglpanel-body styleResultBackground" style="display: block;" id="resultdetailedann_body">
                        <table class="styleResultBackground">
                            <tbody>
                                <tr>
                                    <td>
                                        <b>Result</b>
                                    </td>
                                    <td>
                                        <xsl:if test="count(detailedResult/MDAValidation/Error) = 0">
                                            <div class="PASSED">PASSED</div>
                                        </xsl:if>
                                        <xsl:if test="count(detailedResult/MDAValidation/Error) &gt; 0">
                                            <div class="FAILED">FAILED</div>
                                        </xsl:if>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100px" valign="top">
                                        <b>Summary</b>
                                    </td>
                                    <td>
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Error) + count(detailedResult/MDAValidation/Warning) + count(detailedResult/MDAValidation/Info) + count(detailedResult/MDAValidation/Note)"/> checks <br />
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Error)" /> errors <br />
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Warning)" /> warning <br />
                                        <xsl:value-of select="count(detailedResult/MDAValidation/Note)" /> reports <br />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <b>HIDE : </b>
                        <input type="checkbox" onclick="hideOrUnhide(this)" name="Errors">Errors</input>
                        <input type="checkbox" onclick="hideOrUnhide(this)" name="Warnings">Warnings</input>
                        <input type="checkbox" onclick="hideOrUnhide(this)" name="Infos">Reports</input>
                        <xsl:if test="count(detailedResult/MDAValidation/Error) &gt; 0">
                            <div id="Errors_p">
                            <p id="errors"><b>Errors</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Error">
                                <table class="Error" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>Test</b></td>
                                        <td><xsl:value-of select="Test"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Location</b></td>
                                        <td><xsl:value-of select="Location"/><img src="/EVSClient/img/icons64/down.gif" style="vertical-align: middle;" width="15px" onclick="gotoo(this)" /></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Description</b></td>
                                        <td><xsl:value-of select="Description"/></td>
                                    </tr>
                                </table>
                                <br/>
                            </xsl:for-each>
				</div>
                        </xsl:if>
                        <xsl:if test="count(detailedResult/MDAValidation/Warning) &gt; 0">
                            <div id="Warnings_p">
                            <p id="warnings"><b>Warnings</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Warning">
                                <table class="Warning" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>Test</b></td>
                                        <td><xsl:value-of select="Test"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Location</b></td>
                                        <td><xsl:value-of select="Location"/><img src="/EVSClient/img/icons64/down.gif" style="vertical-align: middle;" width="15px" onclick="gotoo(this)" /></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Description</b></td>
                                        <td><xsl:value-of select="Description"/></td>
                                    </tr>
                                </table>
                                <br/>
                            </xsl:for-each>
			   </div>
                        </xsl:if>
                        <xsl:if test="count(detailedResult/MDAValidation/Note) &gt; 0">
		 	   <div id="Infos_p">
                            <p id="notes"><b>Reports</b></p>
                            <xsl:for-each select="detailedResult/MDAValidation/Note">
                                <table class="Report" width="98%">
                                    <tr>
                                        <td valign="top" width="100"><b>Test</b></td>
                                        <td><xsl:value-of select="Test"/></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Location</b></td>
                                        <td><xsl:value-of select="Location"/><img src="/EVSClient/img/icons64/down.gif" style="vertical-align: middle;" width="15px" onclick="gotoo(this)" /></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><b>Description</b></td>
                                        <td><xsl:value-of select="Description"/></td>
                                    </tr>
                                </table>
                                <br/>
                            </xsl:for-each>
			  </div>
                        </xsl:if>
                        
                    </div>
                </div>
                    </xsl:if>
                <xsl:if test="count(detailedResult/nistValidation) = 1">
                    <xsl:if test="detailedResult/nistValidation != ''">
                    <br />
                    <div class="rich-stglpanel" id="resultdetailedannNist">
                        <div class="rich-stglpanel-header" onclick="hideOrViewNistValidationDetails();">
                            Nist Validation details
                            <div class="rich-stglpanel-marker">
                                <div id="resultdetailedann_switch_on_nist" class="rich-stglpnl-marker" style="display: block;"><xsl:text disable-output-escaping="yes">&amp;laquo;</xsl:text></div>
                                <div id="resultdetailedann_switch_off_nist" class="rich-stglpnl-marker" style="display: none;"><xsl:text disable-output-escaping="yes">&amp;raquo;</xsl:text></div>
                            </div>
                        </div>
                        <div class="rich-stglpanel-body" style="display: block;" id="resultdetailedann_body_nist">
                            <xsl:value-of select="detailedResult/nistValidation" disable-output-escaping="yes"/>
                        </div>
                    </div>
                    </xsl:if>
                </xsl:if>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
